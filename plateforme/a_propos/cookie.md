<div class="headerLeft">jdb+vm pour Problemata</div>
<div class="headerRight">2021.05.03</div>

[Version en ligne](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/a_propos/cookie.md)


<!--
## Règles pour les cookies

### Conditions requises pour ma politique cookies

Pour être conforme au RGPD et au CCPA, votre politique de cookies doit déclarer :

- Quels types de cookies sont configurés,
- Combien de temps ils restent sur le navigateur de l’internaute,
- Quelles données ils tracent / les catégories d’informations personnelles collectées,
- À quelle fin (nécessaires, préférences, statistiques, marketing, etc.),
- Où ces données sont envoyées et avec qui elles sont partagées / quels tiers y ont accès,
- Comment refuser la configuration de cookies et traceurs dans son navigateur, et comment changer ses paramètres par la suite.

### Nos conditions

- aucun cookie n'est utilisé / exploité sur problemata.huma-num.fr *à ce jour*
- en cas de modification sur l'usage des cookies, ce texte serait mis à jour immédiatement
- il n'y a aucune persistence volontaire des données saisies dans un quelconque champs de recherche, boutons, sélection apparaissant sur la plateforme, hormis la mise en cache par le navigateur de l'utilisateur
- aucun événement javascript (souris, toucher, acceléromètre, gyroscope, microphone, camera ...) hormis le chargement de la page (domcontentloaded) qui recenserait la taille de l'écran et sa capacité à être tactile, n'est utilisé
- sur la question des comptes utilisateurs, nous recoltons :
	- le prénom et le nom : pour identifier l'utilisateur s'il est amené à participer activement à l'écriture d'objets éditoriaux dans la base
	- l'établissement d'affiliation si l'utilisateur est affilié à une institution
	- l'adresse mail : pour autoriser des actions telles que les modifications d'informations, de mot de passe ainsi que la validation de la suppression du compte
- aucun utilisateur disposant d'un compte utilisateur propre à la plateforme ne verra les informations personnelles qu'il aurait saisi publiées sur le site en dehors de son consentement


En cliquant sur ce bouton vous consentez à ces conditions

-->

# Données personnelles

À ce jour, aucun cookie n'est collecté et exploité par la plateforme Problemata. Au gré du développement de la plateforme, ce texte sera mis à jour.

Des données à caractère personnel sont stockées dans la base Omeka S. Ces données sont consultables sur les pages Liste Auteurs et Détail Auteur, elles sont également exposées sur l'API Omeka S. Ces données portent sur : le nom, le prénom, la nationalité, la date de naissance et de mort ainsi qu'un texte biographique.

Dans le cadre de l'utilisation de l'outil Omeka S, des données à caractère personnel sont stockées. Ces données portent sur le nom d'utilisateur et l'adresse courriel. Ces données ne sont pas accessibles pour des utilisateurs non connectées à Omeka S.

Les utilisateurs de la plateforme disposent des droits suivants pour l’utilisation qui est faite de leurs données : le droit d’opposition, d’accès, de rectification, d’effacement, le droit à une utilisation restreinte lorsque les données ne sont pas nécessaires ou ne sont plus utiles et le droit à la portabilité des données.

Ces droits peuvent être exercés en vous adressant à Marie Lejault (marielejault@gmail.com).

L'utilisateur peut également contacter la SECED à l’adresse suivante :

Société d’études critiques en design  
23 rue Émile Zola  
92240 Malakoff

Si un utilisateur estime, après avoir contacté les personnes ci-dessus, que ses droits Informatique et Libertés ne sont pas respectés, il a la possibilité d’introduire une réclamation en ligne auprès de la CNIL ou par courrier postal. 