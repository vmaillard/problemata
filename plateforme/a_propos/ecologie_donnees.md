

<!---

# À propos des principes techniques

*Proposition condensée ↓*

L'approche qui motive le travail sur l'écologie des données de la plateforme Problemata est de permettre l'explicitation du travail de recherche afin que les métadonnées portent ainsi les réflexions que soutient le design. L'articulation d'un site web destiné à la lecture d'articles et d'une base de ressources a pour but de proposer un parcours de lecture confortable avec la possibilité d'une exploration experte.

L'entrepôt de données est construit sur les standards du web sémantique et décrit l'ensemble des relations unissant les cinq objets éditoriaux de Problemata : la ligne, l'article, l'agent, le fonds et la ressource. Cet entrepôt est consultable à même le site mais aussi par une REST API. Il repose sur un usage restreint de vocabulaires sémantiques, à savoir : Dublin Core (dcterms), Friend of a friend (foaf), Bibliographic ontology (bibo) et BIO (bio).

Les articles sont écrits suivant la syntaxe Markdown, augmentée de pseudo-codes afin d'appeler des ressources de la base. Le développement de la plateforme donnera lieu dans un second temps à la création d'un outil d'écriture permettant à la fois d'écrire des articles, d'articuler la construction de ceux-ci avec des ressources existantes et de renseigner des métadonnées.

Problemata aborde les enjeux de la Science Ouverte en proposant des métadonnées sous licence libre copyleft et en travaillant à publier des articles sous licence de libre diffusion. Côté développement, l'ensemble des outils utilisés ainsi que le code produit pour la plateforme sont publiés sous licence libre.

La pérennité des données et de la plateforme elle-même est assurée par un hébergement pris en charge par la TGIR Huma-Num et l'utilisation de technologies sous licence libre, toujours actives et correctement documentées.

*Proposition détaillée ↓*
--->

<div class="headerLeft">jdb+vm pour Problemata</div>
<div class="headerRight">2021.05.03</div>

[Version en ligne](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/a_propos/ecologie_donnees.md)



# Interopérabilité

La plateforme Problemata s'inscrit dans une démarche d'ouverture des données et de conformité avec les standards du Web sémantique mis en place par le Consortium W3C. Les données de la plateforme sont ainsi consultables par un visiteur *via* le site web et interprétables par des machines.



## Modèle de données

Le modèle de données de Problemata comprend cinq objets éditoriaux : la Ligne, l'Article, l'Agent, la Collection et la Ressource. Une Ligne est un ensemble d'Articles réunis autour d'une problématique. Un Article est un texte convoquant des Agents et des Ressources. Un Agent est une personne physique ou morale. Une Collection est un choix de Ressources dans des collections partenaires. Une Ressource est un document image, son, vidéo ou pdf.

Notre modèle de données a été construit de manière collégiale par les acteurs de Problemata des différentes équipes (éditoriale, archive, design, informatique). Nous l'avons construit afin qu'il permette l'explicitation du travail de recherche et pour que les métadonnées contiennent les réflexions que soutient le design. Nous donnons ainsi accès à des métadonnées riches et dont les valeurs sont contrôlées (normes ISO, jeux de mots-clés contrôlés).

Les propriétés des objets sont décrites au moyen de quatre vocabulaires sémantiques : Dublin Core (dcterms), Friend of a friend (foaf), Bibliographic ontology (bibo) et BIO (bio). L'utilisation du Dublin Core constitue un socle nécessaire dans la perspective d'extension de Problemata par des protocoles OAI-PMH.

Voici un schéma récapitulant les relations entre les cinq objets éditoriaux :

![Schéma du modèle de données de Problemata](https://framagit.org/vmaillard/problemata/-/raw/master/plateforme/graph/resource_graph_v3.png)


<div style="page-break-after: always"></div>


### Le modèle Ligne

La class d'une Ligne est : "dctype:Collection". Voici l'alignement du modèle de données Ligne avec les vocubulaires sémantiques :

| Propriété           | Définition                                          |
|---------------------|-----------------------------------------------------|
| dcterms:title       | Titre                                               |
| dcterms:description | Texte court de contextualisation.                   |
| dcterms:abstract    | Présentation de la ligne résumé détaillé avec enjeu et problématique de la ligne |
| dcterms:mediator    | Nom du chercheur en charge de la ligne de recherche |
| dcterms:date        | Date de création                                    |



### Le modèle Article

La class d'un Article est : "dctype:Text". Voici l'alignement du modèle de données Article avec les vocubulaires sémantiques :

| Propriété               | Définition                                                   |
|-------------------------|--------------------------------------------------------------|
| dcterms:mediator        | Nom du chercheur en charge de la ligne de recherche          |
| dcterms:creator         | Auteur                                                       |
| dcterms:contributor     | Contributeur                                                 |
| bibo:translator         | Traducteur                                                   |
| dcterms:title           | Titre                                                        |
| dcterms:alternative     | Sous-titre                                                   |
| dcterms:description     | Résumé très court (1 à 2 phrases maximum)                    |
| dcterms:abstract        | Résumé contextuel et critique de l'article                   |
| dcterms:tableOfContents | Table des matières                                           |
| dcterms:subject         | Liste de sujets                                              |
| dcterms:references      | Liste des personnes physiques et morales citées              |
| dcterms:temporal        | Étendue temporelle du contenu de l'article                   |
| dcterms:spatial         | Zone géographique couverte par le contenu de l'article       |
| dcterms:language        | Langue de rédaction de l'article                             |
| dcterms:dateSubmitted   | Date de mise en ligne sur Problemata                         |
| dcterms:created         | Date de création si ≠ de la date de 1ere publication         |
| dcterms:date            | Date d’édition. Si déjà publié, date de la publication dont est issu l'article |
| dcterms:source          | Référence de la source (si déjà publié)                      |
| dcterms:isFormatOf      | Référence de la première publication (en langue originale)   |
| dcterms:hasFormat       | Éditions ré-augmentées importantes de cet article            |
| dcterms:type            | Type de la ressource                                         |
| dcterms:provenance      | Lieu de conservation                                         |
| dcterms:hasVersion      | Articles qui sont un traduction de cet article               |
| dcterms:isVersionOf     | Ajout de l'article d'origine de cette traduction             |
| dcterms:relation        | Ressources en lien (base Problemata)                         |



### Le modèle Agent

La class d'un Agent est : "foaf:Agent". Voici l'alignement du modèle de données Agent avec les vocubulaires sémantiques :

| Propriété        | Définition                          |
|------------------|-------------------------------------|
| foaf:name        | Nom complet                         |
| foaf:familyName  | Nom de famille                      |
| foaf:givenName   | Prénom                              |
| bio:biography    | Biographie                          |
| bio:date         | Dates de naissance et de mort       |
| bio:state        | Nationalité                         |



### Le modèle Collection

La class d'un Fonds est : "bibo:collectedDocument". Voici l'alignement du modèle de données Collection avec les vocubulaires sémantiques :

| Propriété             | Définition          |
|-----------------------|---------------------|
| dcterms:titre         | Titre               |
| dcterms:description   | Description         |
| dcterms:relation      | Ressources en lien  |




### Le modèle Ressource

La class d'une Ressource est : "bibo:Document". Voici l'alignement du modèle de données Ressource avec les vocubulaires sémantiques :

| Propriété             | Définition                                                                  |
|-----------------------|-----------------------------------------------------------------------------|
| identifiant           | Identifiant unique généré par Omeka                                         |
| dcterms:mediator      | Nom du chercheur en charge de la ligne de recherche                         |
| dcterms:type          | Type du document                                                            |
| dcterms:title         | Titre avec des précisions sur le créateur de la ressource                   |
| dcterms:abstract      | Description développée (contenu, contexte historique, rôles, format/medium) |
| dcterms:dateSubmitted | Date de mise en ligne sur Problemata                                        |
| dcterms:date          | Date de la prise de vue                                                     |
| dcterms:created       | Date de création de l’œuvre représentée (= date de prise de vue si pas de diff.) |
| dcterms:provenance    | Lien vers le fond de conservation                                           |
| bibo:locator          | Cote/Nom du fichier média                                                   |
| dcterms:rights        | Crédits (copier coller de ce que donne le fonds) + lien vers l'institution gérant le fonds |
| dcterms:accessRights  | Droits d'usage                                                              |



### API

L'ensemble des métadonnées de Problemata est accessible via notre API à cette url : <http://problemata.huma-num.fr/base/api/items>. 

Cette API est fournie par l'outil Omeka S et son utilisation est documentée à cette url : <https://omeka.org/s/docs/developer/api/>.



### Réutilisation des données

Pour les modalités de réutilisation des données, se référer aux "Conditions générales d’utilisation et mentions légales".



## Articulation technique



### Un site web consultable et un entrepôt de données

Le fonctionnement de la plateforme Problemata repose sur la combinaison de deux outils : Omeka S et Django.

Omeka S est dédié à la description structurée et sémantique de la base de ressources. Les Lignes, les Articles, les Agents, les Fonds et les Ressources y sont décrits ainsi que leurs relations. L'API d'Omeka S permet la consultation totale de la base.

Django est dédié au service des pages dans le navigateur, l'écriture du texte plein des articles, la gestion des comptes utilisateurs, des bibliothèques lecteurs et l'ajout d'objets éditoriaux supplémentaires (événements et versions de la page d’accueil).

Pour résumer, Omeka S est un entrepôt de données et Django le site web consultable. Les liens entre les objets stockés dans Omeka S et Django sont effectués par leurs identifiants uniques.



### Pérennité des données

Le développement de Problemata est pensé en vue d'assurer la pérennité des données. L'hébergement de Problemata est pris en charge par la TGIR Huma-Num sur ses propres serveurs ce qui assure une maîtrise sur les données et un soutien dans la maintenance technique de la plateforme.

Côté développement, Problemata repose sur des logiciels sous licence libre avec une communauté active et dont la documentation est accessible. Cela minimise le risque d'obsolescence et encourage à l'évolution.

Enfin, le choix de disposer d'un entrepôt de données respectant des standards internationaux de description assure l'interopérabilité de la base et l'inscrit donc dans un réseau plus large.



### Liste des outils utilisés

Pour le développement de Problemata, les principaux outils utilisés sont Omeka S et Django :

- Omeka S est publié depuis 2017 sous licence GPLv3 et est développé par le Roy Rosenzweig Center (Université George-Mason, Virginie).
- Django est publié depuis 2005 sous licence BSD et est développé par Django Software Foundation.

La liste des dépendances Python utilisées par Django, des modules ajoutés à Omeka S, des outils et librairies utilisés dans le cadre du projet Problemata sont disponibles sur le dépôt de la plateforme <https://framagit.org/jdb/problemata>.



### Code source

Le code source de la plateforme Problemata est publié sous licence libre (GPLv3) et accessible sur ce dépôt git : <https://framagit.org/jdb/problemata>.

Les briques de codes source indépendantes créés pour les besoins spécifiques de Problemata sont également publiés sous licence libre (GPLv3) à cette url : <https://framagit.org/problemata-codes>.