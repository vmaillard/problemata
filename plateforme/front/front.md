# Approche front de Problemata

Ce document précise les titres, propriétés rdf, hyperliens et leurs emplacements schématiques pour les quatre objets éditoriaux de Problemata.

Pour voir les définitions des propriétés listées sur cette page : [modèle de données](../interoperabilite/data_model.md)

Le texte en orange correspond à Omeka S. Le texte en vert correspond à Django.

Ce document contient des schémas, ceux-ci sont tous regroupés [par ici](../schemas/schema_entier.png).

**Sommaire**

- [Plan du site](#plan-du-site)
- [La Une](#la-1)
- [Articles](#articles)
    - [Liste d'articles](#liste-darticles)
    - [Détail article](#détail-darticles)
- [Lignes](#lignes)
    - [Liste lignes](#liste-de-lignes)
    - [Détail ligne](#détail-de-ligne)
- [Ressources](#ressources)
    - [Liste ressources](#liste-de-ressources)
    - [Détail ressource](#détail-de-ressource)
- [Agents](#agents)
    - [Liste agents](#liste-dagents)
    - [Détail agent](#détail-dagent)


## Plan du site

**Plan abstrait**

![](../schemas/sitemap_abstraite.png)

**Plan avec schémas de page**

![](../schemas/sitemap_schema.png)


## Liste des renvois inter-pages


```
Liste AR
- auteur / dcterms:creator  ▶ détail AU
- ligne / item set          ▶ détail LI
- titre / dcterms:title     ▶ détail AR
```

```
Liste LI
- titre / dcterms:title     ▶ détail LI
```

```
Liste AU
- nom / foaf:name           ▶ détail AU
```

```
Liste RE
- titre / dcterms:title     ▶ détail RE
```

```
Détail AR
- ligne / item set               ▶ détail LI
- auteur / dcterms:creator       ▶ détail AU
- ressources / dcterms:relation  ▶ détail RE
- mots-clés / dcterms:subject    ▶ list AR filtrée
              dcterms:references
              dcterms:temporal
              dcterms:spatial
```

```
Détail Li
  voir Liste AR
```

```
Détail Au
  voir Liste AR
```

```
Détail RE
- article / inverse dcterms:relation ▶ détail AR
- auteur / dcterms:creator           ▶ détail AU
- date de création / dcterms:created ▶ liste RE filtrée
- type / dcterms:type                ▶ liste RE filtrée
```


## LA 1

**Schéma pour La 1 :**

![](../schemas/la_une.png)




## ARTICLES 

### Liste d'articles

**Schéma**

![](../schemas/article_list.png)

**Les 4 filtres de la liste des Articles**

<table>
  <tr>
    <th>Texte</th>
    <th>Rdf</th>
    <th>Commentaire</th>
    <th>tag HTML</th>
  </tr>
  <tr>
    <td>langue</td>
    <td>dcterms:language</td>
    <td>[voc. "Langue"](../interoperabilite/data_model.md#vocabulaire-contrôlé-langues)</td>
    <td>input select</td>
  </tr>
  <tr>
    <td>type</td>
    <td>dcterms:type</td>
    <td>[voc. "Type d'article"](../interoperabilite/data_model.md#vocabulaire-contrôlé-types-darticle)</td>
    <td>input select</td>
  </tr>
  <tr>
    <td>mise en ligne</td>
    <td>dcterms:dateSubmitted</td>
    <td>voc. "Date de mise en ligne"</td>
    <td>input select</td>
  </tr>
  <tr>
    <td>date de création</td>
    <td>dcterms:created</td>
    <td>[voc. "Dates de création"](../interoperabilite/data_model.md#vocabulaire-contrôlé-date-de-création-darticle)</td>
    <td>input select</td>
  </tr>
</table>


**Les 6 tris / colonnes de la liste des Articles + propriétés au click**


<table>
  <tr>
    <th>Texte</th>
    <th>Rdf</th>
    <th>Commentaire</th>
    <th>Tag HTML</th>
    <th>Destination</th>
  </tr>
  <tr>
    <td>auteur</td>
    <td>dcterms:creator</td>
    <td>Division si auteurs multiples</td>
    <td>a</td>
    <td>la page auteur en question<br/></td>
  </tr>
  <tr>
    <td>titre</td>
    <td>- dcterms:title<br>- dcterms:hasVersion<br/>- dcterms:isVersionOf<br/>- dcterms:language</td>
    <td>[cf. pseudo code](#pseudo-code-titre-article-dans-liste-darticles)</td>
    <td>a</td>
    <td>vers la page article</td>
  </tr>
  <tr>
    <td>ligne</td>
    <td>item set</td>
    <td>possible valeurs multiples [cf. pseudo code](#pseudo-code-valeurs-multiples)</td>
    <td>a</td>
    <td>la page ligne en question</td>
  </tr>
  <tr>
    <td>type</td>
    <td>dcterms:type</td>
    <td></td>
    <td>p</td>
    <td>Ø</td>
  </tr>
  <tr>
    <td>mise en ligne</td>
    <td>dcterms:dateSubmitted</td>
    <td></td>
    <td>p</td>
    <td>Ø</td>
  </tr>
  <tr>
    <td>date de création</td>
    <td>dcterms:created</td>
    <td></td>
    <td>p</td>
    <td>Ø</td>
  </tr>
  <tr>
    <td>Ø</td>
    <td>dcterms:alternative</td>
    <td>Apparaît au click sur ✚</td>
    <td>p</td>
    <td>Ø</td>
  </tr>
  <tr>
    <td>Ø</td>
    <td>dcterms:description</td>
    <td>Apparaît au click sur ✚</td>
    <td>p</td>
    <td>Ø</td>
  </tr>
</table>




### Détail d'articles

**Schéma**

![](../schemas/article_detail.png)

**Les entrées dans le détail d'article**

<table>
  <tr>
    <th>Texte</th>
    <th>Rdf</th>
    <th>Commentaire</th>
    <th>Tag HTML</th>
    <th>Destination</th>
  </tr>
  <tr>
    <td>Ø</td>
    <td>dcterms:title</td>
    <td></td>
    <td>h1</td>
    <td>Ø</td>
  </tr>
  <tr>
    <td>Ø</td>
    <td>dcterms:alternate</td>
    <td></td>
    <td>h2</td>
    <td>Ø</td>
  </tr>
  <tr>
    <td>auteur</td>
    <td>dcterms:creator</td>
    <td></td>
    <td>a</td>
    <td>la page agent en question</td>
  </tr>
  <tr>
    <td>ligne</td>
    <td>item set</td>
    <td>Potentiel probème si valeurs multiples</td>
    <td>a</td>
    <td>la page détail de la ligne en question</td>
  </tr>
  <tr>
    <td>type</td>
    <td>dcterms:type</td>
    <td></td>
    <td>li</td>
    <td>Ø</td>
  </tr>
  <tr>
    <td>mise en ligne</td>
    <td>dcterms:dateSubmitted</td>
    <td></td>
    <td>p</td>
    <td>Ø</td>
  </tr>
  <tr>
    <td>date de création</td>
    <td>dcterms:created</td>
    <td></td>
    <td>p</td>
    <td>Ø</td>
  </tr>
  <tr>
    <td>résumé</td>
    <td>dcterms:description</td>
    <td></td>
    <td>p</td>
    <td>Ø</td>
  </tr>
  <tr>
    <td>plan</td>
    <td>Ø</td>
    <td>générer à partir de h3 et h4. Aller plus bas ?</td>
    <td>ul, li, a</td>
    <td>liens internes</td>
  </tr>
  <tr>
    <td>citer l'article</td>
    <td>Ø</td>
    <td>champ dans Django ou bien générer à partir des métadonnées (voir d'un .bib)<br></td>
    <td>p</td>
    <td>Ø</td>
  </tr>
  <tr>
    <td>mots-clés</td>
    <td>- dcterms:subject<br>- dcterms:references<br>- dcterms:temporal<br>- dcterms:coverage</td>
    <td>recouvre :<br>- mots clés (libres et controlés)<br>- noms cités (physiques et morales)<br>- couverture (spatiale et temporelle).<br> Vu que les mots-clés sont seulement des littéraux, il y a sûrement du bruit dans le filtrage.</td>
    <td>a</td>
    <td>vers la liste des articles filtrée</td>
  </tr>
  <tr>
    <td>ressources</td>
    <td>dcterms:relation</td>
    <td>Liste des ressources en lien (les entrées suivantes correspondent donc à ces ressources)</td>
    <td>ul > li</td>
    <td>Ø</td>
  </tr>
  <tr>
    <td>Ø</td>
    <td>dcterms:title</td>
    <td></td>
    <td>a</td>
    <td>vers la page détail de la ressource en question</td>
  </tr>
  <tr>
    <td>Ø</td>
    <td>dcterms:abstract</td>
    <td></td>
    <td>p</td>
    <td>Ø</td>
  </tr>
  <tr>
    <td>fonds</td>
    <td>dcterms:provenance</td>
    <td>la valeur du lien est renseigné dans les métadonnées</td>
    <td>a</td>
    <td>lien externe vers le fonds en question</td>
  </tr>
  <tr>
    <td>crédits</td>
    <td>dcterms:rights</td>
    <td></td>
    <td>p</td>
    <td>Ø</td>
  </tr>
</table>






## LIGNES 

### Liste de lignes

**Schéma**

![](../schemas/ligne_list.png)

### Détail de ligne

**Schéma**

![](../schemas/ligne_detail.png)




## RESSOURCES
### Liste de ressources

**Schéma**

![](../schemas/ressource_list.png)


### Détail de ressource

**Schéma**

![](../schemas/ressource_detail.png)



## AGENTS

### Liste d'agents

**Schéma**

![](../schemas/agent_list.png)


### Détail d'agent

**Schéma**

![](../schemas/agent_detail.png)

