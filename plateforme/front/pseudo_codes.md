# Approche pseudo-codes pour Problemata

Ce document contient des pseudo-codes pour pouvoir valider les méthodes à employer pour le [multilingue](#pseudo-code-multilingue-des-métadonnées), la [génération des titres d'article](#pseudo-code-titre-article-dans-liste-darticles), la gestion des traductions, les calculs de sommes ([Nombre d'articles](#pseudo-code-nombre-darticles-dans-liste-de-lignes) et [Nombre d'auteurs](#pseudo-code-nombre-dauteurs-dans-liste-de-lignes)), les [valeurs multiples](#pseudo-code-valeurs-multiples).

## Pseudo-codes


### Pseudo-code - Affichage d'un article et de ses traductions dans Liste d'articles

(idem pour Titre article dans Détail Ligne + Titre article dans Détail Agent)

*Affichage d'un article et de ses traductions dans "Liste d'articles"*

```
de l'article à afficher, on récupère :
- dcterms:hasVersion
- dcterms:isVersionOf

s'il y a des article dans "dcterms:hasVersion"
    on boucle dans les articles concernés
        - on récupère leur dcterms:language
    on retourne la succession des codes langues
    on affiche l'article avec les codes langues de ses traductions
s'il y a un article dans "dcterms:isVersionOf"
    on retire cet article pour ne pas l'afficher car il existe une version source à cet article dans Problemata
sinon
    on n'affiche pas de traduction pour cet article
```


### Pseudo-code - Nombre d'articles dans Liste de lignes

```
on cherche tous les articles qui sont dans cette ligne
on retire les articles qui sont des traductions d'un article source
on fait le compte de tous les résultats
```


### Pseudo-code - Nombre d'auteurs dans Liste de lignes

```
on cherche tous les articles qui sont dans cette ligne
on stocke tous les auteurs de ces articles
on dédoublonne la liste des auteurs (de type "resource" et "literal")
on fait le compte de tous les résultats
```


### Pseudo-code - Valeurs multiples

```
à venir
```


### Pseudo-code - Multilingue des métadonnées

*exemple simplifié :*
- un item Omeka qui n'aurait qu'une seule propriété rdf
- cette propriété rdf n'aurait qu'une seule valeur

Concrètement :

``` json
"item" {
    "rdf": {
        "@language": "fr",
        "@value": "un_mot_clé"
    }
}

```

```
on déclare une variable vide nommée "value" pr stocker la valeur à afficher

on a un "item" Omeka
de "item", on récupère la propriété "rdf"
de "rdf", on récupère les valeurs de "@language" et "@value"

on stocke "user_lang", la langue de consultation du site par un utilisateur

si ("user_lang" = en)
    
    si ("@language" = en)
        on remplit "value" par la valeur de "@value"
    sinon
        on ne remplit pas "value" car il n'y pas de valeur en anglais pr cette entrée du tableau

si ("user_lang" = fr)
    
    si ("@language" = fr)
        on remplit "value" par la valeur de "@value"
    sinon
        on ne remplir pas "value" car il n'y pas de valeur en français pr cette entrée du tableau

si ("value" est vide)

  alors la propriété "rdf" n'a pas de code de langue
  on remplit "value" par la valeur de "@value" en préfixant par le code langue

on affiche la valeur de "value"
```

- on pourrait prendre en compte le cas où la valeur d'une propriété ne contient pas d'information de langue, et donc l'afficher quelque soit la langue

Exemples :

```
item:
  rdf:
    0:
      lang: "fr"
      val: "mot-clé"
    1:
      lang: "en"
      val: "keyword"
```

- site fr : "mot-clé"
- site en : "keyword"

---

```
item:
  rdf:
    0:
      lang: "fr"
      val: "mot-clé"
```

- site fr : "mot clé"
- site en : "(fr) mot-clé"

---

```
item:
  rdf:
    0:
      lang: "fr"
      val: "mot-clé"
    1:
      lang: "fr"
      val: "deuxième mot-clé"
    2:
      lang: "en"
      val: "keyword"
```

- site fr : "mot clé, deuxième mot-clé"
- site en : "keyword"

---

```
item:
  rdf:
    0:
      lang: ""
      val: "mot-clé"
    1:
      lang: "fr"
      val: "deuxième mot-clé"
    2:
      lang: "en"
      val: "keyword"
```

- site fr : "deuxième mot-clé"
- site en : "keyword"

---

```
item:
  rdf:
    0:
      lang: ""
      val: "mot-clé"
    1:
      lang: ""
      val: "deuxième mot-clé"
    2:
      lang: "en"
      val: "keyword"
```

- site fr : "mot-clé, deuxième mot-clé, (en) keyword"
- site en : "keyword"

### Pseudo-code - Articles utilisant cette Ressource

```
à venir
```