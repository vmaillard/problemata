# Metadata bibliothèque

## Trois class

1. Author (foaf:person)
2. Publisher (foaf:organization)
3. Book (bibo:book)

## Templates

1. Template "Author" (foaf:person)

| Original label  | Alternate label |
|-----------------|-----------------|
| dcterms:title   | Nom complet     |
| foaf:firstName  | Prénom          |
| foaf:familyName | Nom             |
| dcterms:date    |                 |

- dcterms:title : selon le format "Nom, Prénom". Si auteur avec particule : "Nom, Prénom de"

- dcterms:date : première valeur : date de naissance, seconde valeur : date de mort. Format : [ISO_8601](https://en.wikipedia.org/wiki/ISO_8601)

2. Template "Publisher" (foaf:organization)

| Original label      | Data type                                    | Alternate label     |
|---------------------|----------------------------------------------|---------------------|
| dcterms:title       |                                              |                     |
| dcterms:description |                                              |                     |
| dcterms:subject     |                                              |                     |
| dcterms:spatial     | Geonames: The GeoNames geographical database | Lieu de publication |

- dcterms:spatial : utilisation du module [Value Suggest](https://omeka.org/s/docs/user-manual/modules/valuesuggest/)

2. Template "Book" (bibo:book)

| Propriété           | Label                  | Format                 |
|---------------------|------------------------|------------------------|
| dcterms:title       | Titre                  | litteral               |
| dcterms:alternative | Sous-titre             | litteral               |
| dcterms:creator     | Auteur                 | litteral               |
| dcterms:subject     | Sujets                 | LC Subject Headings    |
| dcterms:description | Description            | litteral               |
| dcterms:publisher   | Éditeur                | Littéral ou Ressource  |
| dcterms:contributor | Contributeur           | litteral               |
| dcterms:date        | Date                   | ISO_8601               |
| dcterms:issued      | Date de la publication | ISO_8601               |
| dcterms:created     | Date de la création    | ISO_8601               |
| dcterms:type        | Type                   |                        |
| dcterms:format      | Format                 |                        |
| dcterms:identifier  | Identifiant            |                        |
| dcterms:source      | Source                 |                        |
| dcterms:language    | Langue                 | LC: ISO639-1 Languages |
| dcterms:relation    | Relation               |                        |
| dcterms:spatial     | Couverture spatiale    |                        |
| dcterms:temporal    | Couverture temporelle  |                        |

- dcterms:creator et dcterms:contributor : selon le format "Nom, Prénom (id MARC RELATOR)". Pour obtenir les id MARC Relator : http://id.loc.gov/vocabulary/relators.html

- dcterms:language : utilisation du module [Value Suggest](https://omeka.org/s/docs/user-manual/modules/valuesuggest/)

- dcterms:date : Date de la ressource. Format : [ISO_8601](https://en.wikipedia.org/wiki/ISO_8601)
- dcterms:issued : Date de la publication.
- dcterms:created : Date de création.


## Vocabulaires utilisés :

- Dublin Core
- Friend Of A Friend
- Bibliographic Ontology
- BIBFRAME

## Exemples

- Flatland (bibo:book)
	- ajout de [bf:notation](http://id.loc.gov/ontologies/bibframe.html#c_Notation) pour renseigner le nom de la typographie

- L'Odyssée (bibo:book)
	- "Philippe Jaccottet" est répété pour (trl), (cwt) et (aft) mais on pourrait tout rassembler en une seule entrée

## Aller plus loin

- bf:otherEdition : Resource has other available editions, for example simultaneously published language editions or reprints.


