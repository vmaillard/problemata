# METADATA DC
---
title: Le titre
subtitle: Le sous-titre
creator:
- Le nom de l'auteur
subject:
description:
publisher:
contributor:
date: YYYY-MM-DD
type:
format:
identifier:
source:
language: xx-XX
relation:
coverage:
rights:
---


# METADATA EBOOK
---
identifier:
- scheme: DOI
  text: doi:xx.xxxxxx.xxx/xx
- scheme: Problemata
  text: xxx
title:
- type: main
  text: Le titre principal
- type: subtitle
  text: Le sous titre
creator:
- role: author
  text: Le nom de l'auteur
publisher: Problemata
rights: © - CC
contributor:
- role: translator
  text: Le nom du traducteur
date: YYYY-MM-DD
lang: xx-XX
subject:
description:
type:
format:
relation:
coverage:
rights: from yyyy to yyyy
cover-image:
---

*description*

identifier
    Either a string value or an object with fields text and scheme. Valid values for scheme are ISBN-10, GTIN-13, UPC, ISMN-10, DOI, LCCN, GTIN-14, ISBN-13, Legal deposit number, URN, OCLC, ISMN-13, ISBN-A, JP, OLCC. 
title
    Either a string value, or an object with fields file-as and type, or a list of such objects. Valid values for type are main, subtitle, short, collection, edition, extended. 
creator
    Either a string value, or an object with fields role, file-as, and text, or a list of such objects. Valid values for role are MARC relators, but pandoc will attempt to translate the human-readable versions (like “author” and “editor”) to the appropriate marc relators. 
contributor
    Same format as creator. 
date
    A string value in YYYY-MM-DD format. (Only the year is necessary.) Pandoc will attempt to convert other common date formats. 
lang (or legacy: language)
    A string value in BCP 47 format. Pandoc will default to the local language if nothing is specified. 
subject
    A string value or a list of such values. 
description
    A string value. 
type
    A string value. 
format
    A string value. 
relation
    A string value. 
coverage
    A string value. 
rights
    A string value. 
cover-image
    A string value (path to cover image).