Titre principal         # Titre 1
Sous-titre              ## Titre 2

Titre de partie         ### Titre 3
Titre de sous-partie	#### Titre 4

Citation                > Blockquote

Liste non ordonnée      * List
                        * List
                        * List
Liste ordonnée          1. Un
                        2. Deux
                        3. Trois

Italique				*italique*
Gras					**gras**
Barré					~~barré~~

Exposant				1^er^
Indice					H~2~O

Ligne horizontale		--- (unique contenu d'une ligne)

Image                   ![Légende](fichier.jpg)
Hyperlien               [Un lien](https://www.example.com)

Image avec identifiant  ![Légende](fichier.jpg)(#fig_1)
Lien interne			[La photographie de l'exposition](#fig_1)

Note de bas de page     [^1]
                        [^1]: Le contenu d'une note

Saut de paragraphe		(deux sauts de ligne consécutifs)
Saut de ligne forcé		\ (anti-slash en fin de ligne)
						ou (deux espaces consécutives en fin de ligne)

Tiret cadratin			---
Tiret demi-cadratin		--

Espace insécable		"\ " (anti-slash suivi d'une espaces)

Petites capitales		[Petites capitales]{.smallcaps}