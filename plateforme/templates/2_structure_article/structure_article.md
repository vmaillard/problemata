**Références**  
[Guide Markdown](https://www.markdownguide.org/basic-syntax)  
[Guide Markdown](https://commonmark.org/help/)

---
identifier:
- scheme: DOI
  text: doi:xx.xxxxxx.xxx/xx
- scheme: Problemata
  text: xxx
title:
- type: main
  text: Le titre principal
- type: subtitle
  text: Le sous-titre
creator:
- role: author
  text: Le nom de l'auteur
publisher: Problemata
rights: © - CC
contributor:
- role: translator
  text: Le nom du traducteur
date: YYYY-MM-DD
lang: xx-XX
subject:
description:
type:
format:
relation:
coverage:
rights: from yyyy to yyyy
cover-image:
---

# Titre
## Sous-tire

### Titre de partie

*Du texte en italique.*

**Du texte en bold.**

[*Un lien*](https://www.example.com)

Un premier paragraphe.

Un nouveau paragraphe (une ligne sautée).  
Un saut de ligne (deux espaces à la fin de la ligne précédente).

#### Titre de sous-partie

Un nouveau paragraphe

> Une citation *une note de bas de page*[^1]

*une liste non ordonnée*

* List
* List
* List

*une liste ordonnée*

1. Un
2. Deux
3. Trois 

Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.

*une ligne horizontale*

---


*une image*

![Légende](prob.jpg)




[^1]: Le contenu d'une note

