# Visualisations

<div class="headerLeft">jdb+vm pour Problemata</div>
<div class="headerRight">2022.06.01</div>

[Version en ligne](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/data_model/visualisation.md)


## Modèle actuel

Le modèle de données a été mise à jour en ajoutant une propriété à la class Ligne :

Modèle de données pour la class Ligne :

| Propriété        | Définition                | Valeur | Format |
|------------------|---------------------------|--------|--------|
| [...]            | [...]                     | [...]  | [...]  |
| dcterms:relation | URL vers la visualisation | URL    | URL    |


## Pour la suite

Un modèle de données plus complexe peut être mis en place en créant un type de contenu supplémentaire : la Visualisation.

Voici le modèle de données possible la class Visualisation :

| Propriété             | Définition          | Valeur     | Format |
|-----------------------|---------------------|------------|--------|
| dcterms:titre         | Titre               | Texte      |        |
| dcterms:description   | Description         | Texte      |        |
| dcterms:creator       | Auteur              | Ressource/Texte | oid / Nom, Prénom  |
| dcterms:contributor   | Contributeur        | Ressource/Texte | oid / Nom, Prénom  |

La class correspondante pourrait être dcterms:InteractiveResource. Le dublin core la définit comme ceci : "A resource requiring interaction from the user to be understood, executed, or experienced. Examples include forms on Web pages, applets, multimedia learning objects, chat services, or virtual reality environments."

L'ajout de cette class permettrait de stocker des informations propres aux visualisations. À terme, cela permettra de faire plus aisément une liste des visualisations (mais cela réclamera du travail de design et développement dans le menu, les urls, etc).

Côté machine virtuelle, voici ce qui faudra faire pour qu'il y ait de l'autonomie dans le dépôt des visualisations :

- création d'un utilisateur spécifique sur la machine virtuelle pour déposer des visualisations
- étude pour les permissions dédiées à la création de base de données avec cet utilisateur
- rédaction de la notice pour ajouter une visualisation

## Questions

- Comment définir une visualisation en quelques mots ?
- Est-ce nécessaire d'augmenter le modèle de données d'une class supplémentaire pour les visualisations (dcterms:InteractiveResource) ?
- Si oui, est-ce qu'il faut les propriétés "dcterms:creator" et "dcterms:contributor" ?
- Est-ce la class dcterms:InteractiveResource et sa définition sont pertinentes pour les visualisations ? (on s'écarte de la notion d'article pour se rapprocher de celle de la ressource)


