# Article (dctype:Text)



## Tableau complet

[Voir le modèle de données](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/interoperabilite/data_model.md#article-class-dctypetext)



## Spécifications



### Class et `dcterms:type`

Dans un souci de simplification, la class des articles est `dctype:Text`. La propriété `dcterms:type` contient des précisions sur la nature de l'article.


### `dcterms:subject`

- 4 sujets max dans le [vocabulaire contrôlé](#vocabulaire-mots-cles-darticle)
- 4 sujets max hors vocabulaire contrôlé


### `dcterms:tableOfContents`

```
* Partie 1
** Titre 1
*** Sous-tire 1
* Partie 2
```

---


## Ressources

- `dcterms:date` / ISO 8601 : https://fr.wikipedia.org/wiki/ISO_8601
- `dcterms:language` / Liste des codes ISO 639-1 : https://fr.wikipedia.org/wiki/Liste_des_codes_ISO_639-1
