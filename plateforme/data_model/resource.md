# Ressource (bibo:Document)


## Tableau complet

[Voir le modèle de données](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/interoperabilite/data_model.md#ressource-class-bibodocument)



## Spécifications


### `dcterms:title`

Titre avec des précisions sur le créateur de la ressource.

Nom de l’œuvre représentée. Si pertinent et différent du nom du créateur du document, nom de l’auteur de l’œuvre. 


### `dcterms:abstract`

Description développée. Consacrée au contenu de la ressource.

Le chercheur est incité à renseigner des informations concernant :
- le contexte historique
- les rôles / professions des créateurs
- le format et le medium
- le contenu représenté


### `dcterms:provenance`

Dans le cas de documents qui ne proviennent pas d'un fonds d'institution, il faut :
- des normes de nommage des fichiers
- mettre dans `dcterms:provenance` le nom du chercheur


### `dcterms:type`

Voir le [vocabulaire contrôlé](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/interoperabilite/data_model.md#vocabulaire-contr%C3%B4l%C3%A9-types-de-ressource).


### `dcterms:created`

On demande de remplir la date de création de la ressource (dcterms:created). C'est cette date qui sera utilisée pour générer une description compacte.

Les dates liées à la source de la ressource (date d'impression, de version) sont décrites dans l'abstract. Idem pour les différentes versions existantes de la ressource décrite.


### En cours

- sameAs : mis de côté pour l'instant : BK + MAD : les urls des ressources ne sont pas pérennes
- trouver une propriété rdf adaptée pour la cote
- prévoir une propriété rdf sur les médias : dcterms:title, par exemple, pour stocker le nom du fichier



## Mis de côté

### `dcterms:format` `dcterms:medium`

Pour la description du support matériel et du format de la ressource, on incite à ce que cela soit fait dans la colonne `abstract`.

Si la ressource est décrite par un autre fonds, il le spécifier par `dcterms:provenance`.