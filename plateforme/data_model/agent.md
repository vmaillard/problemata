# Ressource de type "Agent" (foaf:Agent)

## Tableau complet

[Voir le modèle de données](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/interoperabilite/data_model.md#agent-class-foafagent)

---

Pour installer BIO, il faut passer par leur github : https://github.com/iand/vocab-bio :
- Namespace URI: http://purl.org/vocab/bio/0.1/
- label : BIO 
- prefix : bio

Autre propriétés possibles :

| Propriété           | Définition        |
|---------------------|-------------------|
| dcterms:description | Biographie        |
| marcrel:role        | Profession        |



## Mis de côté

| Propriété           | Définition        |
|---------------------|-------------------|
| marcrel:role        | Profession        |