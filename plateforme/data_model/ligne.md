# Ressource de type "Ligne" (dctype:Collection)

## Tableau complet

| Propriété           | Définition                                          | Valeur          | Format                |
|---------------------|-----------------------------------------------------|-----------------|-----------------------|
| identifiant         | Identifiant unique généré par Omeka                 | Integer         | oid                   |
| dcterms:title       | Titre                                               | Texte           |                       |
| dcterms:description | Description                                         | Texte           |                       |
| dcterms:mediator    | Nom du chercheur en charge de la ligne de recherche | Ressource/Texte | oid / Nom, Prénom     |
| dcterms:date        | Date de création                                    | Date            | YYYY-MM-DD (ISO 8601) |
