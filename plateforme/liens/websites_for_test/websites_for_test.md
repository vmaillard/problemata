# Les sites web pour faire des tests

## Omeka Sample

- url : https://vincent-maillard.fr/projets/problemata/omeka_sample/
- admin : https://vincent-maillard.fr/projets/problemata/omeka_sample/admin
- api : https://vincent-maillard.fr/projets/problemata/omeka_sample/api/items
- db : `_probl_sample_omeka`

**Compte :**
- mail : vincent.maillard.contact@gmail.com
- mdp : wifi valence (a___6)

## Omeka Workshop

- url : https://vincent-maillard.fr/projets/problemata/workshop/omeka/
- admin : https://vincent-maillard.fr/projets/problemata/workshop/omeka/admin
- api : https://vincent-maillard.fr/projets/problemata/workshop/omeka/api/items
- db : `_probl_ws_omeka`

**Compte :**
- mail : vincent.maillard.contact@gmail.com
- mdp : wifi valence (a___6)

## La bibliothèque de Vincent

URLS :
- url : https://vincent-maillard.fr/projets/problemata/test/bibliotheque/
- admin : https://vincent-maillard.fr/projets/problemata/test/bibliotheque/admin
- api : https://vincent-maillard.fr/projets/problemata/test/bibliotheque/api/items
- db : \_probl_bibliotheque_omeka

**Compte :**
- mail : vincent.maillard.contact@gmail.com 
- mdp : wifi valence (a___6)

Vocabulaires :
- voir [Métadata bibliothèque](../templates/3_metadata_bibliotheque) pour les vocabulaires employés

## Un ProcessWire avec des choses installés

URLS :
- url : https://vincent-maillard.fr/projets/problemata/test/processwire
- admin : https://vincent-maillard.fr/projets/problemata/test/processwire/processwire
- db : \_probl_test_pw

**Compte :**
- user : admin
- mdp : wifi valence (a___6)

**Module** | **Champ**
------ | ----
[FieldtypeOmekaBrowser_P1](https://framagit.org/problemata-codes/fieldtypeomekabrowser_p1) | omeka_browser_p1
[FieldtypeOmekaBrowser_P2](https://framagit.org/problemata-codes/fieldtypeomekabrowser_p2) | omeka_browser_p2

**Contenu du dossier template :**

La page home utilise ce [dossier ```/templates```](https://framagit.org/problemata-codes/processwire-profile-db-omeka). Avec les variables pour la base de données Omeka modifiées.


## Un mediawiki avec SemanticWiki

local :
db: \_probl_wiki
nom du wiki : problemata
user : admin
mdp : wifi valence (a___6)

