# Questions liées aux vocabulaires / ontologies / omeka


## class Person / class Organization

- est-ce que la base Problemata accueille des ressources auteurs ?
- que met-on comme propriétés disponibles ?
- est-il nécessaire de préciser les lieux de naissance et de mort ?

Propriétés :

| Propriété       | Définition        |
|-----------------|-------------------|
| foaf:name       | Nom complet       |
| foaf:familyName | Nom de famille    |
| foaf:givenName  | Prénom            |
| marcrel:role    | Profession        |


Autre propriétés possibles :

| Propriété       | Définition        |
|-----------------|-------------------|
| bio:birth       | Date de naissance |
| bio:death       | Date de mort      |
| bio:biography   | Biographie        |


(pour installer BIO, il faut passer par leur github : https://github.com/iand/vocab-bio)

## class Article / Livre


| Propriété               | Définition                                                 |
|-------------------------|------------------------------------------------------------|
| marcrel:col             | Chercheur                                                  |
| dcterms:creator         | Auteur                                                     |
| dcterms:contributor     | Contributeur                                               |
| marcrel:trl             | Traducteur                                                 |
| dcterms:title           | Titre                                                      |
| dcterms:alternative     | Sous-titre                                                 |
| dcterms:abstract        | Abstract                                                   |
| dcterms:tableOfContents | Table des matières                                         |
| dcterms:subject         | Sujet                                                      |
| dcterms:temporal        | Couverture temporelle                                      |
| dcterms:spatial         | Couverture spatiale                                        |
| dcterms:language        | Langue                                                     |
| dcterms:submitted       | Date de mise en ligne sur Problemata                       |
| dcterms:created         | Date de création (si ≠ dcterms:date)                       |
| dcterms:date            | Date de la ressource source à la ressource décrite         |
| dcterms:source          | Ressource source à la ressource décrite                    |
| dcterms:issued          | Date de la première publication en langue originale        |
| dcterms:hasVersion      | Autres versions de la ressource (traduction, augmentation) |
| dcterms:relation        | Ressources en lien (images, autres articles)               |
| dcterms:provenance      | Lieu de conservation                                       |


Autres propriétés possibles :

- bibo:transcriptOf
- bibo:translationOf
- dcterms:isFormatOf pourrait être utilisée au lieu de dcterms:source
- dcterms:mediator pourrait être utilisée au lieu de marcrel:col   

Questions : 
- est-ce que l'on met le titre complet et on spécifie ensuite la division ?
- est-ce l'appartenance à une ligne se fait dans Omeka ? avec les items set ?
- quelle propriété pour la provenance d'un fond ?
- comment renseigner le titre original d'une ressource traduite ?


## class Ressource 

Liste des class possibles : [voir ici](../../reunions/19_09_13/questions.md#class-ditems), à retoucher avec ce qui a été fait en ws à l'ENS.


| Propriété               | Définition                                                 |
|-------------------------|------------------------------------------------------------|
| marcrel:col             | Chercheur                                                  |
| dcterms:creator         | Auteur                                                     |
| dcterms:contributor     | Contributeur                                               |
| dcterms:title           | Titre                                                      |
| dcterms:abstract        | Abstract                                                   |
| dcterms:description     | Description                                                |
| dcterms:subject         | Sujet                                                      |
| dcterms:submitted       | Date de mise en ligne sur Problemata                       |
| dcterms:created         | Date de création (si ≠ dcterms:date)                       |
| dcterms:hasVersion      | Autres versions de la ressource                            |
| dcterms:medium          | Support matériel de la ressource                           |
| dcterms:format          | Format, dimensions de la ressource                         |
| dcterms:relation        | Ressources en lien (images, autres articles)               |
| dcterms:provenance      | Lieu de conservation                                       |
| dcterms:rights          | Droits                                                     |

- pas compris dans le tableau, les éléments en-dessous de "priorisation"
- pas compris dans le tableau, au moment des dates : la différence entre "édition de la ressource" et "édition du contenu" => ah si c'est la différence entre l'image et ce qui est montré par l'image

## Omeka et qualification du rôle d'une Personne

- Comment "qualifier" le travail d'un créateur sur une ressource (tour à tour : designer / écrivain / traducteur)
=> Est-ce qu'il vaut mieux rentrer du texte comme ça : "Sottsass, Ettore (architecte)" et "Sottsass, Ettore (designer)" ? ou alors lier la ressource Omeka "Sottsass, Ettore" et trouver un moyen de la "labeliser"

Quelques sujets sur le forum à ce propos : https://forum.omeka.org/t/using-marc-relators-for-refine-dcterms-contributor/9194

## Multilingue

- quel est la langue par défaut de Problemata ?

En février 2018, une des meilleures options est une installation de site en parallèles : url.com/fr et url.com/en.
Sources :
- https://forum.omeka.org/t/internationalized-omeka-s/3154/23
- Institut Français d'Études Anatoliennes : 
	- http://archivis.ifea-istanbul.net/s/main
	- https://github.com/hajoki/istanbul

## Bibliographie matérielle

Le vocabulaire BIBFRAME paraît plutôt bien. La Library of Congress fournit aussi des normes pour les valeurs (sujets, titres, relators).
À voir : Thesaurus For Graphic Materials 

## Liaison à des bases externes

À quelles bases externes se relier ?
- dbpedia (http://dbpedia.org/fct/facet.vsp / http://dbpedia.org/sparql/)
- wikidata (https://www.wikidata.org/)
- viaf (https://viaf.org/)

Voir ce à quoi se référe https://data.bnf.fr notamment

Par quel vocabulaire et quelle propriété ? owl:sameAs ?

## Bibliographie 

Comment automatiser la bibliographie, notamment les passages entre normes ? Est-ce que l'on décrit les éléments de la bibliographie dans la base Omeka (en référant les élements à des bases externes) ?

## Omeka

Italique dans omeka ?
