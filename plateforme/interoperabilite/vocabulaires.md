# Vocabulaires

## Visualisation

Ontospy :
- http://www.michelepasin.org/support/ontospy-examples/
- http://www.michelepasin.org/projects/ontospy/

## Dublin Core

## Friend Of A Friend

## BIBFRAME

- bf:provisionActivity : Place, name, and/or date information relating to the publication, printing, distribution, issue, release, production, etc. of a resource.

- bf:otherEdition : Resource has other available editions, for example simultaneously published language editions or reprints.

## MARC Relators

## GND Ontology
https://d-nb.info/standards/elementset/gnd#translator

## Schema
https://schema.org/VisualArtwork

## Getty Vocabularies
http://vocab.getty.edu/
http://www.getty.edu/research/tools/vocabularies/index.html

à voir aussi, le vocab en construction de Vial (https://vocadesign.pubpub.org/) (pdf dans mailing list de Recherche-Design)

## RDA Registry
http://www.rdaregistry.info/
