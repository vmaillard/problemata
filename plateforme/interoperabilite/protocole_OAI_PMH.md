# Protocole OAI PMH

Open archives initiative - protocol for metadata harvesting

**Références**

- [Guide BNF](https://www.bnf.fr/sites/default/files/2019-02/Guide_oaipmh.pdf)
- [Omeka + module OAI-MPH](https://omeka.org/s/modules/OaiPmhRepository/)

**Installation**

* métadonnées
* serveur web
* API
* url de la base
* application répondant aux 6 requêtes (Identify, ListSets, ListIdentifiers, ListMetadataFormats, ListRecords, GetRecord)
