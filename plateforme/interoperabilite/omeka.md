# Omeka

## Ressources 

- cours omeka : https://medium.com/@tpasquier/cours-omeka-mars-2016-d4a1600b6cc1 

## API 

- documentation : https://omeka.org/s/docs/developer/key_concepts/api/
- pour la compléter : https://forum.omeka.org/t/example-api-usage-using-curl/8083
- pour le code source : https://github.com/omeka/omeka-s/tree/develop/application/src/Api

## CORS

Pour autoriser des requêtes venant d'un site externe ou d'un localhost, ajouter dans le .htaccess d'Omeka :
- Header set Access-Control-Allow-Origin "*"
ou plutôt :
- Header set Access-Control-Allow-Origin "http://example.com"
où http://example.com est le site à autoriser

Ligne à ajouter dans le .htaccess pour exposer le header `link` pour avoir la pagination

```
Header set Access-Control-Expose-Headers "link, Omeka-S-Total-Results"
```

Ligne à ajouter dans le .htaccess pour exposer le header `link` pour avoir le nombre total de résultats

```
Header set Access-Control-Expose-Headers Omeka-S-Total-Results
```

## Objets de l'API

Detail (tous les medias) :

MP3 :

```
    {
      "type": "o:Media",
      "o:media_type": "audio/mpeg",
      "@id": "http://problemata.huma-num.fr/omeka_beta/api/media/584",
      "o:id": 584,
      "o:title": "test",
      "o:thumbnail_urls": [],
      "o:source": "test.mp3",
      "o:filename": "22/test.mp3"
    }
```

PDF

```
    {
      "type": "o:Media",
      "o:media_type": "application/pdf",
      "@id": "http://problemata.huma-num.fr/omeka_beta/api/media/585",
      "o:id": 585,
      "o:title": "test2",
      "o:thumbnail_urls": [],
      "o:source": "harwood2015.pdf",
      "o:filename": "22/harwood2015.pdf"
    }
```

MP4

```
    {
      "type": "o:Media",
      "o:media_type": "video/mp4",
      "@id": "http://problemata.huma-num.fr/omeka_beta/api/media/587",
      "o:id": 587,
      "o:title": "test mp4",
      "o:thumbnail_urls": [],
      "o:source": "test.mp4",
      "o:filename": "22/test.1.mp4"
    }
```

Relation (seulement 1er media) :

IMG

```
    {
      "type": "resource:item",
      "property_id": 13,
      "property_label": "Relation",
      "is_public": true,
      "@id": "http://problemata.huma-num.fr/omeka_beta/api/items/22",
      "value_resource_id": 22,
      "value_resource_name": "items",
      "url": null,
      "display_title": "Vue l'exposition \"Ettore Sottsass Jr : de l'objet fini à la fin de l'objet\" au Musée des Arts Décoratifs (1976-1977). Photographe non identifié.",
      "thumbnail_url": "http://problemata.huma-num.fr/omeka_beta/files/square/22/M5050_X0031_CCI_025_0016.jpg",
      "thumbnail_title": "M5050_X0031_CCI_025_0016",
      "thumbnail_type": "image/jpeg"
```

MP3

```
 {
      "type": "resource:item",
      "property_id": 13,
      "property_label": "Relation",
      "is_public": true,
      "@id": "http://problemata.huma-num.fr/omeka_beta/api/items/22",
      "value_resource_id": 22,
      "value_resource_name": "items",
      "url": null,
      "display_title": "Vue l'exposition \"Ettore Sottsass Jr : de l'objet fini à la fin de l'objet\" au Musée des Arts Décoratifs (1976-1977). Photographe non identifié.",
      "thumbnail_url": "/omeka_beta/application/asset/thumbnails/audio.png?v=3.1.0",
      "thumbnail_title": "test",
      "thumbnail_type": "audio/mpeg"
    },
```

PDF

```
    {
      "type": "resource:item",
      "property_id": 13,
      "property_label": "Relation",
      "is_public": true,
      "@id": "http://problemata.huma-num.fr/omeka_beta/api/items/22",
      "value_resource_id": 22,
      "value_resource_name": "items",
      "url": null,
      "display_title": "Vue l'exposition \"Ettore Sottsass Jr : de l'objet fini à la fin de l'objet\" au Musée des Arts Décoratifs (1976-1977). Photographe non identifié.",
      "thumbnail_url": "/omeka_beta/application/asset/thumbnails/default.png?v=3.1.0",
      "thumbnail_title": "test2",
      "thumbnail_type": "application/pdf"
    },
```

MP4

```
    {
      "type": "resource:item",
      "property_id": 13,
      "property_label": "Relation",
      "is_public": true,
      "@id": "http://problemata.huma-num.fr/omeka_beta/api/items/22",
      "value_resource_id": 22,
      "value_resource_name": "items",
      "url": null,
      "display_title": "Vue l'exposition \"Ettore Sottsass Jr : de l'objet fini à la fin de l'objet\" au Musée des Arts Décoratifs (1976-1977). Photographe non identifié.",
      "thumbnail_url": "/omeka_beta/application/asset/thumbnails/video.png?v=3.1.0",
      "thumbnail_title": "test mp4",
      "thumbnail_type": "video/mp4"
    }
```


thumbnail_display_urls : 

IMG 

```
    "thumbnail_display_urls": {
      "large": "http://problemata.huma-num.fr/omeka_beta/files/large/47/M5050_X0031_CCI_125_0038_P.jpg",
      "medium": "http://problemata.huma-num.fr/omeka_beta/files/medium/47/M5050_X0031_CCI_125_0038_P.jpg",
      "square": "http://problemata.huma-num.fr/omeka_beta/files/square/47/M5050_X0031_CCI_125_0038_P.jpg",
      "small": "http://problemata.huma-num.fr/omeka_beta/files/small/47/M5050_X0031_CCI_125_0038_P.jpg"
    },
```

PDF

```
    "thumbnail_display_urls": {
      "large": "/omeka_beta/application/asset/thumbnails/default.png?v=3.1.0",
      "medium": "/omeka_beta/application/asset/thumbnails/default.png?v=3.1.0",
      "square": "/omeka_beta/application/asset/thumbnails/default.png?v=3.1.0",
      "small": "/omeka_beta/application/asset/thumbnails/default.png?v=3.1.0"
    },
```