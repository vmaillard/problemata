# Processwire

## Modules 
- hanna code : [[omeka: id]] puis php qui formate pour une requête asynchrone js de l'api d'Omeka
- hanna code : [[omeka: id]] puis php qui formate avec une requête à l'api d'Omeka
- hanna code : [[omeka: id]] puis php avec requête à la base de données d'Omeka
- FieldtypeSelect : qui pioche dans l'API d'Omeka
- FieldtypeSelect : qui pioche dans la base de données d'Omeka (FieldtypeSelectExtOption)


## Développement de module

- Documentation : https://processwire.com/docs/modules/development/

- Création d'un nouveau type de champ (field) :
	- étendre la class Fieldtype
	- FieldtypeNomDuModule.module : 
	- étendre la class Inputfield

- Tutorial : https://abdus.co/blog/creating-a-simple-and-configurable-module-for-processwire/

## Modules existants
- FieldtypeSelectExtOption : https://processwire.com/talk/topic/9320-fieldtype-select-external-option/page/3/


## FieldtypeSelectExtOption
Pour faire un input select de tous les titres d'une base omeka :
- Source table : value
- Option value : id
- Option label : value
Filter :
- Column : property_id
- Selector operator : =
- Value : 1


