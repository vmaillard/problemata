# Wikidata

## Requête SPARQL

- manuel : https://www.mediawiki.org/wiki/Wikidata_Query_Service/User_Manual

- interface utilisateur : https://query.wikidata.org/

=> tutoriel : https://www.mediawiki.org/wiki/File:Wikidata_Sparql_Query_Tutorial.webm

- point d'accès :
    - https://query.wikidata.org/bigdata/namespace/wdq/sparql?query={SPARQL}
    - https://query.wikidata.org/sparql?query={SPARQL}

=> tutoriel pour une requête en python : https://ramiro.org/notebook/us-presidents-causes-of-death/