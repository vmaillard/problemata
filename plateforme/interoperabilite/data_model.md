


# Modèle de données pour Problemata

Ce document contient des schémas, des tableaux et des liens destinés à décrire le modèle de données sur lequel repose les ressources de Problemata.

<div class="headerLeft">jdb+vm pour Problemata</div>
<div class="headerRight">2024.01.11</div>

<!--
[Version en ligne](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/interoperabilite/data_model.md)
-->

<!--
pandoc -c ressources/css/print.css plateforme/interoperabilite/data_model.md --pdf-engine=weasyprint -s -o data_model_problemata.pdf
-->

<!--
## Sommaire

- [Schéma général](#schéma-général)
- [Ligne](#ligne-dctypecollection)
	- [Page spécifique pour Ligne](../data_model/ligne.md)
- [Article](#article-class-dctypetext)
	- [Page spécifique pour Article](../data_model/article.md)
- [Collection](#collection-class-bibocollecteddocument)
- [Resource](#ressource-class-bibodocument)
	- [Page spécifique pour Resource](../data_model/resource.md)
- [Agent](#agent-class-foafagent)
	- [Page spécifique pour Agent](../data_model/agent.md)
- [Vocabulaires contrôlés](#vocabulaires-contrôlés)
-->


## Liste des ressources possibles sur Problemata

Problemata comporte six ressources : Ligne, Article, Collection, Institution, Ressource et Agent.

**Ligne**

Une Ligne est un regroupement d’articles constitué par un chercheur. Elle est introduite par un texte, rédigé par cette même personne.

**Article**

Un Article est un texte rédigé par un auteur accompagné de ressources personnelles et/ou externes mis en forme dans Problemata. Toutes les ressources externes utilisées dans l’article sont consultables sur les plateformes des fonds partenaires à l’aide d’hyperliens.

**Collection**

Éditorialisation de ressources.

**Institution**

Éditorialisation de ressources provenant d'une institution partenaire de la plateforme.

**Ressource**

Une Ressource est un matériau de type textuel et/ou visuel. Elle peut être issue d’un fonds personnel (qui sera stocké dans le fonds Problemata), ou bien issue d’un fonds externe partenaire (BK...).

**Agent**

Un Agent est un créateur d'un article. L'ajout dans la base d'une ressource Agent n'est pas nécessaire pour décrire une Ressource.

Tableau présentant les class en fonction du type de ressource :

| Nom         | Class             |
|-------------|-------------------|
| Line        | dctype:Collection |
| Article     | dctype:Text       |
| Collection  | bibo:CollectedDocument |
| Institution | foaf:Organization      |
| Resource    | bibo:Document     |
| Agent       | foaf:Agent        |




<div style="page-break-after: always"></div>



<!--
## Schéma général
-->
<!--
![](https://framagit.org/vmaillard/problemata/-/raw/master/plateforme/graph/resource_graph.png)
-->
<!--
![](https://framagit.org/vmaillard/problemata/-/raw/master/plateforme/graph/resource_graph_v3.png)


<div style="page-break-after: always"></div>
-->


## Ligne (dctype:Collection)

| Propriété           | Définition                                          | Valeur          | Format                |
|---------------------|-----------------------------------------------------|-----------------|-----------------------|
| identifiant         | Identifiant unique généré par Omeka                 | Integer         | oid                   |
| dcterms:title       | Titre                                               | Texte           |                       |
| dcterms:description | Texte court de contextualisation. Calibrage         | Texte           | env. 300 s            |
| dcterms:abstract    | Présentation de la ligne résumé détaillé avec enjeu et problématique de la ligne | Texte | 1000 à 1500 s |
| dcterms:mediator    | Nom du chercheur en charge de la ligne de recherche | Ressource/Texte | oid / Nom, Prénom     |
| dcterms:date        | Date de création                                    | Date            | YYYY-MM-DD (ISO 8601) |
| dcterms:relation    | URL vers la visualisation                           | URL             | URL                   |


<div style="page-break-after: always"></div>



## Article (class dctype:Text)

| Propriété               | Définition                                                   | Valeur          | Format                     |
|-------------------------|--------------------------------------------------------------|-----------------|----------------------------|
| identifiant             | Identifiant unique généré par Omeka                          | Integer         | oid                        |
| dcterms:mediator        | Nom du chercheur en charge de la ligne de recherche          | Ressource/Texte | oid / Nom, Prénom          |
| dcterms:creator         | Auteur                                                       | Ressource/Texte | oid / Nom, Prénom          |
| dcterms:contributor     | Contributeur                                                 | Ressource/Texte | oid / Nom, Prénom          |
| bibo:translator         | Traducteur                                                   | Ressource/Texte | oid / Nom, Prénom          |
| dcterms:title           | Titre                                                        | Texte           |                            |
| dcterms:alternative     | Sous-titre                                                   | Texte           |                            |
| dcterms:description     | Résumé très court (1 à 2 phrases maximum)                    | Texte           |                            |
| dcterms:abstract        | Résumé contextuel et critique de l'article                   | Texte           |                            |
| dcterms:tableOfContents | Table des matières                                           | Texte           | voir "Plus de détails"     |
| dcterms:subject         | Liste de sujets                                              | Texte           | Vocabulaire contrôlé       |
| dcterms:references      | Liste des personnes physiques et morales citées              | Ressource/Texte | oid / Nom, Prénom          |
| dcterms:temporal        | Étendue temporelle du contenu de l'article                   | Texte           | YYYY/YYYY (ISO 8601)       |
| dcterms:spatial         | Zone géographique couverte par le contenu de l'article       | Texte           |                            |
| dcterms:language        | Langue de rédaction de l'article                             | Texte           | Voc. contrôlé (ISO 639-1)  |
| dcterms:dateSubmitted   | Date de mise en ligne sur Problemata                         | Date            | YYYY-MM-DD (ISO 8601)      |
| dcterms:created         | Date de création si ≠ de la date de 1ere publication         | Date            | YYYY-MM-DD (ISO 8601)      |
| dcterms:date            | Date d’édition. Si déjà publié, date de la publication dont est issu l'article | Date | YYYY-MM-DD (ISO 8601) |
| dcterms:source          | Référence de la source (si déjà publié)                      | Texte           | auteur, titre, éditeur, ville, date |
| dcterms:isFormatOf      | Référence de la première publication (en langue originale)   | Texte           | auteur, titre, éditeur, ville, date |
| dcterms:hasFormat       | Éditions ré-augmentées importantes de cet article            | Texte           | auteur, titre, éditeur, ville, date |
| dcterms:type            | Type de la ressource                                         | Texte           | Vocabulaire contrôlé       |
| dcterms:provenance      | Lieu de conservation                                         | Texte           |                            |
| dcterms:hasVersion      | Articles qui sont un traduction de cet article               | Ressource       | oid                        |
| dcterms:isVersionOf     | Ajout de l'article d'origine de cette traduction             | Ressource       | oid                        |
| dcterms:relation        | Ressources en lien (base Problemata)                         | Ressource       | oid                        |
| dcterms:identifier      | URL publique (pour OAI-PMH)                                  | Texte           | http://problemata.org/articles/oid |

<!--

### Spécifications pour Article


#### Article - Class et `dcterms:type`

Dans un souci de simplification, la class des articles est `dctype:Text`. La propriété `dcterms:type` contient des précisions sur la nature de l'article.


#### Article - `dcterms:subject`

- 4 sujets max dans le [vocabulaire contrôlé](#vocabulaire-mots-cles-darticle)
- 4 sujets max hors vocabulaire contrôlé


#### Article - `dcterms:tableOfContents`

```
* Partie 1
** Titre 1
*** Sous-tire 1
* Partie 2
```

-->


<div style="page-break-after: always"></div>



## Collection (class bibo:CollectedDocument)

| Propriété             | Définition          | Valeur     | Format |
|-----------------------|---------------------|------------|--------|
| dcterms:titre         | Titre               | Texte      |        |
| dcterms:description   | Description         | Texte      |        |
| dcterms:relation      | Ressources en lien  | Ressource  |        |



## Institution (class foaf:Organization)

| Propriété             | Définition          | Valeur     | Format |
|-----------------------|---------------------|------------|--------|
| dcterms:titre         | Titre               | Texte      |        |
| dcterms:description   | Description         | Texte      |        |
| dcterms:relation      | Ressources en lien  | Ressource  |        |



<div style="page-break-after: always"></div>



## Ressource (class bibo:Document)

| Propriété             | Définition                                                                  | Valeur          | Format                |
|-----------------------|-----------------------------------------------------------------------------|-----------------|-----------------------|
| identifiant           | Identifiant unique généré par Omeka                                         | Integer         | oid                   |
| dcterms:mediator      | Nom du chercheur en charge de la ligne de recherche                         | Ressource/Texte | oid / Nom, Prénom     |
| dcterms:type          | Type du document                                                            | Texte           | Vocabulaire contrôlé  |
| dcterms:title         | Titre avec des précisions sur le créateur de la ressource                   | Texte           |                       |
| dcterms:abstract      | Description développée (contenu, contexte historique, rôles, format/medium) | Texte           |                       |
| dcterms:dateSubmitted | Date de mise en ligne sur Problemata                                        | Date            | YYYY-MM-DD (ISO 8601) |
| dcterms:date          | Date de la prise de vue                                                     | Date            | YYYY-MM-DD (ISO 8601) |
| dcterms:created       | Date de création de l’œuvre représentée (= date de prise de vue si pas de diff.) | Date       | YYYY-MM-DD (ISO 8601) |
| dcterms:provenance    | Lien vers le fond de conservation                                           | URL             | Nom (voc. contrôlé ?) + URL |
| bibo:identifier       | Cote                                                                        | Texte           |                       |
| bibo:locator          | Nom du fichier média                                                        | Texte           |                       |
| dcterms:rights        | Crédits (copier coller de ce que donne le fonds) + lien vers l'institution gérant le fonds | URL |                  |
| dcterms:accessRights  | Droits d'usage                                                              | Texte           | Modèle stable         |
| dcterms:identifier    | URL publique (pour OAI-PMH)                                                 | Texte         | http://problemata.org/resources/oid |


<!--

2021.05.03 :

- suppression de `dcterms:publisher` - "Lien vers l'institution gérant le fonds"
- déplacer les urls de `dcterms:publisher` vers les urls de `dcterms:rights`.


### Spécifications pour Ressource


#### Ressource - `dcterms:title`

Titre avec des précisions sur le créateur de la ressource.

Nom de l’œuvre représentée. Si pertinent et différent du nom du créateur du document, nom de l’auteur de l’œuvre. 


#### Ressource - `dcterms:abstract`

Description développée. Consacrée au contenu de la ressource.

Le chercheur est incité à renseigner des informations concernant :
- le contexte historique
- les rôles / professions des créateurs
- le format et le medium
- le contenu représenté


#### Ressource - `dcterms:provenance`

Dans le cas de documents qui ne proviennent pas d'un fonds d'institution, il faut :
- des normes de nommage des fichiers
- mettre dans `dcterms:provenance` le nom du chercheur


#### Ressource - `dcterms:type`

Voir le [vocabulaire contrôlé](#vocabulaire-types-de-ressource).


#### Ressource - `dcterms:created`

On demande de remplir la date de création de la ressource (dcterms:created). C'est cette date qui sera utilisée pour générer une description compacte.

Les dates liées à la source de la ressource (date d'impression, de version) sont décrites dans l'abstract. Idem pour les différentes versions existantes de la ressource décrite.


#### Ressource - En cours

- sameAs : mis de côté pour l'instant : BK + MAD : les urls des ressources ne sont pas pérennes
- trouver une propriété rdf adaptée pour la cote
- prévoir une propriété rdf sur les médias : dcterms:title, par exemple, pour stocker le nom du fichier



#### Ressource - Mis de côté

**`dcterms:format` `dcterms:medium`**

Pour la description du support matériel et du format de la ressource, on incite à ce que cela soit fait dans la colonne `abstract`.

Si la ressource est décrite par un autre fonds, il le spécifier par `dcterms:provenance`.

-->

<div style="page-break-after: always"></div>


## Agent (class foaf:Agent)

| Propriété        | Définition                          | Valeur  | Format                           |
|------------------|-------------------------------------|---------|----------------------------------|
| identifiant      | Identifiant unique généré par Omeka | Integer | oid                              |
| foaf:name        | Nom complet                         | Texte   | Nom, Prénom                      |
| foaf:familyName  | Nom de famille                      | Texte   |                                  |
| foaf:givenName   | Prénom                              | Texte   |                                  |
| bio:biography    | Biographie                          | Texte   |                                  |
| bio:date         | Dates de naissance et de mort       | Dates   | YYYY-MM-DD/YYYY-MM-DD (ISO 8601) |
| bio:state        | Nationalité                         | Texte   | Vocabulaire contrôlé             |
| dcterms:identifier | URL publique (pour OAI-PMH)       | Texte   | http://problemata.org/authors/oid |



<div style="page-break-after: always"></div>



## Vocabulaires contrôlés

Les vocabulaires contrôlés sont rattachés à des propriétés des ressources.

Liste des vocabulaires contrôlés :

- langues
- types d'article
- mots-clés d'article
- types de ressource
- nationalités d'agent


### Vocabulaire contrôlé : Langues

- fr
- en
- de
- it
- es
- pt



### Vocabulaire contrôlé : Types d'article

| fr                    | en                  |
|-----------------------|---------------------|
| Entretien             | Interview           |
| Communication         | Lecture             |
| Extrait de livre      | Book section        |
| Fiction / Non fiction | Fiction/Non fiction |
| Mémoire / Thèse       | Dissertation/Thesis |
| Essai                 | Essay               |
| Essai visuel          | Visual essay        |
| Addendum              | Addendum            |
| Memorendum            | Memorendum          |
| Autres                | Others              |



### Vocabulaire contrôlé : Mots-clés d'article

| fr                        | en                        |
|---------------------------|---------------------------|
| Architecture              | Architecture              |
| Art                       | Art                       |
| Activisme                 | Activism                  |
| Agence                    | Agency                    |
| Artisanat                 | Crafts                    |
| Association               | Association               |
| Automatisation            | Automation                |
| Capitalisme               | Capitalism                |
| Catalogue                 | Catalogue                 |
| Collaboration             | Collaboration             |
| Collectif                 | Collective                |
| Collection                | Collection                |
| Communauté                | Community                 |
| Communication             | Communication             |
| Confort                   | Comfort                   |
| Couleur                   | Colour                    |
| Corps                     | Body                      |
| Critique                  | Criticism                 |
| Data                      | Data                      |
| Décor                     | Decoration                |
| Design                    | Design                    |
| Designer                  | Designer                  |
| Design computationnel     | Computational design      |
| Design de mode            | Fashion design            |
| Design graphique          | Graphic design            |
| Design textile            | Textile design            |
| Design d’espace           | Space design              |
| Design d’interaction      | Interaction design        |
| Design d’intérieur        | Interior design           |
| Design d’objet            | Product design            |
| Design de service         | Service design            |
| Design industriel         | Industrial design         |
| Dessin                    | drawing                   |
| Dispositif                | device                    |
| Domestique                | domestic                  |
| École                     | School                    |
| Écologie                  | ecology                   |
| Économie                  | Economy                   |
| Enseignement              | education                 |
| Entreprise                | Company                   |
| Environnement             | environment               |
| Épistémologie             | epistemology              |
| Ergonomie                 | ergonomics                |
| Esthétique                | æsthetics                 |
| Éthique                   | Ethics                    |
| Étude de cas              | Case Study                |
| Expérience                | experience                |
| Expérimentation           | experimentation           |
| Exposition                | exhibition                |
| Fabrication               | production                |
| Fiction                   | fiction                   |
| Fonction                  | function                  |
| Forme                     | form                      |
| Gamme                     | range                     |
| Genre                     | gender                    |
| Habitat                   | housing                   |
| Histoire                  | History                   |
| Identité                  | Identity                  |
| Inclusivité               | inclusivity               |
| Industrie                 | industry                  |
| Ingéniérie                | engineering               |
| Innovation                | innovation                |
| Intelligence artificielle | Artificial intelligence   |
| Juridique                 | Legal                     |
| Machine                   | Machine                   |
| Marketing                 | Marketing                 |
| Matériaux                 | Materials                 |
| Média                     | Media                     |
| Médiation                 | Mediation                 |
| Méthode                   | Method                    |
| Milieu                    | Environment               |
| Mobilier                  | Furniture                 |
| Modèle                    | Model / template / Design |
| Musée                     | Museum                    |
| Numérique                 | Digital                   |
| Outils                    | tools                     |
| Paysagisme                | Landscaping               |
| Perception                | Perception                |
| Philosophie               | Philosophy                |
| Politique                 | Politics / policy         |
| Pratique                  | Practice                  |
| Processus                 | Process                   |
| Projet                    | Project                   |
| Public                    | Public                    |
| Recherche                 | Research                  |
| Santé                     | Health                    |
| Scénographie              | Scenography               |
| Science                   | Science                   |
| Signalétique              | Signage                   |
| Social                    | Social                    |
| Soin                      | Care                      |
| Standard                  | Standard                  |
| Stratégie                 | Strategy                  |
| Style                     | Style                     |
| Système                   | System                    |
| Technique                 | Technique                 |
| Technologie               | Technology                |
| Théorie                   | Theory                    |
| Typographie               | Typography                |
| Urbanisme                 | Urbanism                  |
| Usage                     | Use                       |
| Vivant                    | Living                    |



### Vocabulaire contrôlé : Date de création d'article

- 2020
- 2010
- 2000
- 1990
- 1980
- 1970
- 1960
- 1950
- 1940
- 1930
- 1920
- 1910
- 1900
- 1890
- 1870
- 1860
- 1850
- 1840
- 1830
- 1820
- 1810
- 1800
- XVIIIe
- XVIIe
- XVIe
- XVe 
- XIVe
- ∞



### Vocabulaire contrôlé : Types de ressource

| fr                       | en                        |
|--------------------------|---------------------------|
| Photographie             | Photography               |
| Dessin / Illustration    | Drawing/illustration      |
| Représentation graphique | Visual representation     |
| Presse / Édition         | Press, media & publishing |
| Document écrit           | Written document          |
| Documentaire             | Documentary               |
| Œuvre                    | Artwork                   |
| Autre                    | Others                    |



### Vocabulaire contrôlé : Nationalités d'agent

ISO_3166_country_codes


<!--

### Vocabulaire contrôlé : Liste des collections

Calculé par requête, comme la liste des lignes.

-->