# Vocabulaire extrait de MARK - Relator

Annotator [ann]
	A person who makes manuscript annotations on an item
Architect [arc]
    A person, family, or organization responsible for creating an architectural design, including a pictorial representation intended to show how a building, etc., will look when completed. It also oversees the construction of structures
 Artist [art]
    A person, family, or organization responsible for creating a work by conceiving, and implementing, an original graphic design, drawing, painting, etc. For book illustrators, prefer Illustrator [ill]
    UF Graphic technician
Artistic director [ard]
    A person responsible for controlling the development of the artistic style of an entire production, including the choice of works to be presented and selection of senior production staff
Author [aut]
    A person, family, or organization responsible for creating a work that is primarily textual in content, regardless of media type (e.g., printed text, spoken word, electronic text, tactile text) or genre (e.g., poems, novels, screenplays, blogs). Use also for persons, etc., creating a new work by paraphrasing, rewriting, or adapting works by another creator such that the modification has substantially changed the nature and content of the original or changed the medium of expression
    UF Joint author
Author in quotations or text abstracts [aqt]
    A person or organization whose work is largely quoted or extracted in works to which he or she did not contribute directly. Such quotations are found particularly in exhibition catalogs, collections of photographs, etc. 
Author of afterword, colophon, etc. [aft]
    A person or organization responsible for an afterword, postface, colophon, etc. but who is not the chief author of a work
Author of introduction, etc. [aui]
    A person or organization responsible for an introduction, preface, foreword, or other critical introductory matter, but who is not the chief author 
Author of screenplay, etc.
    USE Screenwriter
Autographer [ato]
    A person whose manuscript signature appears on an item
Binder [bnd]
    A person who binds an item 
Binding designer [bdd]
    A person or organization responsible for the binding design of a book, including the type of binding, the type of materials used, and any decorative aspects of the binding
    UF Designer of binding
Book designer [bkd]
    A person or organization involved in manufacturing a manifestation by being responsible for the entire graphic design of a book, including arrangement of type and illustration, choice of materials, and process used
    UF Designer of book
    UF Designer of e-book
Book producer [bkp]
    A person or organization responsible for the production of books and other print media
    UF Producer of book
Bookjacket designer [bjd]
    A person or organization responsible for the design of flexible covers designed for or published with a book, including the type of materials used, and any decorative aspects of the bookjacket
    UF Designer of bookjacket
Bookplate designer [bpd]
    A person or organization responsible for the design of a book owner's identification label that is most commonly pasted to the inside front cover of a book
 Calligrapher [cll]
    A person or organization who writes in an artistic hand, usually as a copyist and or engrosser 
Cartographer [ctg]
    A person, family, or organization responsible for creating a map, atlas, globe, or other cartographic work
 Client [cli]
    A person or organization for whom another person or organization is acting 
Collaborator
    USE Contributor
Collection registrar [cor]
    A curator who lists or inventories the items in an aggregate work such as a collection of items or works 
Collector [col]
    A curator who brings together items from various sources that are then arranged, described, and cataloged as a collection. A collector is neither the creator of the material nor a person to whom manuscripts in the collection may have been addressed
Commentator [cmm]
    A performer contributing to a work by providing interpretation, analysis, or a discussion of the subject matter on a recording, film, or other audiovisual medium 
Commentator for written text [cwt]
    A person or organization responsible for the commentary or explanatory notes about a text. For the writer of manuscript annotations in a printed book, use Annotator 
Compiler [com]
    A person, family, or organization responsible for creating a new work (e.g., a bibliography, a directory) through the act of compilation, e.g., selecting, arranging, aggregating, and editing data, information, etc
Compositor
    A person or organization responsible for the creation of metal slug, or molds made of other materials, used to produce the text and images in printed matter
Conceptor [ccp]
    A person or organization responsible for the original idea on which a work is based, this includes the scientific author of an audio-visual item and the conceptor of an advertisement
Conservator [con]
    A person or organization responsible for documenting, preserving, or treating printed or manuscript material, works of art, artifacts, or other media
    UF Preservationist
Consultant [csl]
    A person or organization relevant to a resource, who is called upon for professional advice or services in a specialized field of knowledge or training 
Consultant to a project [csp]
    A person or organization relevant to a resource, who is engaged specifically to provide an intellectual overview of a strategic or operational task and by analysis, specification, or instruction, to create or propose a cost-effective course of action or solution
Contributor [ctb]
    A person, family or organization responsible for making contributions to the resource. This includes those whose work has been contributed to a larger work, such as an anthology, serial publication, or other compilation of individual works. If a more specific role is available, prefer that, e.g. editor, compiler, illustrator
    UF Collaborator
Copyright claimant [cpc]
    A person or organization listed as a copyright owner at the time of registration. Copyright can be granted or later transferred to another person or organization, at which time the claimant becomes the copyright holder 
Copyright holder [cph]
    A person or organization to whom copy and legal rights have been granted or transferred for the intellectual content of a work. The copyright holder, although not necessarily the creator of the work, usually has the exclusive right to benefit financially from the sale and use of the work to which the associated copyright protection applies 
Corrector [crr]
    A person or organization who is a corrector of manuscripts, such as the scriptorium official who corrected the work of a scribe. For printed matter, use Proofreader 
Correspondent [crp]
    A person or organization who was either the writer or recipient of a letter or other communication
Cover designer [cov]
    A person or organization responsible for the graphic design of a book cover, album cover, slipcase, box, container, etc. For a person or organization responsible for the graphic design of an entire book, use Book designer; for book jackets, use Bookjacket designer
    UF Designer of cover
Creator [cre]
    A person or organization responsible for the intellectual or artistic content of a resource 
Curator [cur]
    A person, family, or organization conceiving, aggregating, and/or organizing an exhibition, collection, or other item
    UF Curator of an exhibition
Curator of an exhibition
    USE Curator
Current owner
    USE Owner
Data contributor [dtc]
    A person or organization that submits data for inclusion in a database or other collection of data 
Data manager [dtm]
    A person or organization responsible for managing databases or other data sources
Dedicator [dto]
    A person who writes a dedication, which may be a formal statement or in epistolary or verse form
Delineator [dln]
    A person or organization executing technical drawings from others' designs 
Depicted [dpc]
    An entity depicted or portrayed in a work, particularly in a work of art
Depositor [dpt]
    A current owner of an item who deposited the item into the custody of another person, family, or organization, while still retaining ownership 
Designer [dsr]
    A person, family, or organization responsible for creating a design for an object 
Designer of binding
    USE Binding designer
Designer of book
    USE Book designer
Designer of bookjacket
    USE Bookjacket designer
Designer of cover
    USE Cover designer
Designer of e-book
    USE Book designer
Designer of type
    USE Type designer
Distributor [dst]
    A person or organization that has exclusive or shared marketing rights for a resource
Donor [dnr]
    A former owner of an item who donated that item to another owner 
Draftsman [drm]
    A person, family, or organization contributing to a resource by an architect, inventor, etc., by making detailed plans or drawings for buildings, ships, aircraft, machines, objects, etc
    UF Technical draftsman
Dubious author [dub]
    A person or organization to which authorship has been dubiously or incorrectly ascribed 
Editor [edt]
    A person, family, or organization contributing to a resource by revising or elucidating the content, e.g., adding an introduction, notes, or other critical matter. An editor may also prepare a resource for production, publication, or distribution. For major revisions, adaptations, etc., that substantially change the nature and content of the original work, resulting in a new work, see author 
Editor of compilation [edc]
    A person, family, or organization contributing to a collective or aggregate work by selecting and putting together works, or parts of works, by one or more creators. For compilations of data, information, etc., that result in new works, see compiler
Electrician [elg]
    A person responsible for setting up a lighting rig and focusing the lights for a production, and running the lighting at a performance
    UF Chief electrician
    UF House electrician
    UF Master electrician
Engineer [eng]
    A person or organization that is responsible for technical planning and design, particularly with construction 
Engraver [egr]
    A person or organization who cuts letters, figures, etc. on a surface, such as a wooden or metal plate used for printing 
Etcher [etr]
    A person or organization who produces text or images for printing by subjecting metal, glass, or some other surface to acid or the corrosive action of some other substance 
Event place [evp]
    A place where an event such as a conference or a concert took place 
Expert [exp]
    A person or organization in charge of the description and appraisal of the value of goods, particularly rare items, works of art, etc.
    UF Appraiser
Facsimilist [fac]
    A person or organization that executed the facsimile
    UF Copier
Field director [fld]
    A person or organization that manages or supervises the work done to collect raw data or do research in an actual setting or environment (typically applies to the natural and social sciences) 
Former owner [fmo]
    A person, family, or organization formerly having legal possession of an item 
Funder [fnd]
    A person or organization that furnished financial support for the production of the work 
Host institution [his]
    An organization hosting the event, exhibit, conference, etc., which gave rise to a resource, but having little or no responsibility for the content of the resource 
Illustrator [ill]
    A person, family, or organization contributing to a resource by supplementing the primary content with drawings, diagrams, photographs, etc. If the work is primarily the artistic content created by this entity, use artist or photographer
Inscriber [ins]
    A person who has written a statement of dedication or gift 
Instructor
    USE Teacher
Interviewee [ive]
    A person, family or organization responsible for creating or contributing to a resource by responding to an interviewer, usually a reporter, pollster, or some other information gathering agent 
Interviewer [ivr]
    A person, family, or organization responsible for creating or contributing to a resource by acting as an interviewer, reporter, pollster, or some other information gathering agent 
Inventor [inv]
    A person, family, or organization responsible for creating a new device or process
    UF Patent inventor
Issuing body [isb]
    A person, family or organization issuing a work, such as an official organ of the body 
Joint author
    USE Author
Laboratory [lbr]
    An organization that provides scientific analyses of material samples 
Laboratory director [ldr]
    A person or organization that manages or supervises work done in a controlled setting or environment
    UF Lab director
Landscape architect [lsa]
    An architect responsible for creating landscape works. This work involves coordinating the arrangement of existing and proposed land features and structures 
Lead [led]
    A person or organization that takes primary responsibility for a particular activity or endeavor. May be combined with another relator term or code to show the greater importance this person or organization has regarding that particular role. If more than one relator is assigned to a heading, use the Lead relator only if it applies to all the relators 
Lender [len]
    A person or organization permitting the temporary use of a book, manuscript, etc., such as for photocopying or microfilming
Licensee [lse]
    A person or organization who is an original recipient of the right to print or publish 
Licensor [lso]
    A person or organization who is a signer of the license, imprimatur, etc
    UF Imprimatur
Lighting designer [lgd]
    A person or organization who designs the lighting scheme for a theatrical presentation, entertainment, motion picture, etc. 
Lithographer [ltg]
    A person or organization who prepares the stone or plate for lithographic printing, including a graphic artist creating a design directly on the surface from which printing will be done.
Manufacture place [mfp]
    The place of manufacture (e.g., printing, duplicating, casting, etc.) of a resource in a published form 
Manufacturer [mfr]
    A person or organization responsible for printing, duplicating, casting, etc. a resource 
Marbler [mrb]
    The entity responsible for marbling paper, cloth, leather, etc. used in construction of a resource 
Markup editor [mrk]
    A person or organization performing the coding of SGML, HTML, or XML markup of metadata, text, etc.
    UF Encoder
Medium [med]
    A person held to be a channel of communication between the earthly world and a world 
Metadata contact [mdc]
    A person or organization primarily responsible for compiling and maintaining the original description of a metadata set (e.g., geospatial metadata set) 
Metal-engraver [mte]
    An engraver responsible for decorations, illustrations, letters, etc. cut on a metal surface for printing or decoration 
Minute taker [mtk]
    A person, family, or organization responsible for recording the minutes of a meeting 
Moderator [mod]
    A performer contributing to a resource by leading a program (often broadcast) where topics are discussed, usually with participation of experts in fields related to the discussion 
Monitor [mon]
    A person or organization that supervises compliance with the contract and is responsible for the report and controls its distribution. Sometimes referred to as the grantee, or controlling agency 
Narrator [nrt]
    A performer contributing to a resource by reading or speaking in order to give an account of an act, occurrence, course of events, etc 
Observer
    USE Witness
Opponent [opn]
    A person or organization responsible for opposing a thesis or dissertation 
Organizer [orm]
    A person, family, or organization organizing the exhibit, event, conference, etc., which gave rise to a resource
    UF Organizer of meeting
Originator [org]
    A person or organization performing the work, i.e., the name of a person or organization associated with the intellectual content of the work. This category does not include the publisher or personal affiliation, or sponsor except where it is also the corporate author 
Other [oth]
    A role that has no equivalent in the MARC list. 
Owner [own]
    A person, family, or organization that currently owns an item or collection, i.e.has legal possession of a resource
    UF Current owner
Papermaker [ppm]
    A person or organization responsible for the production of paper, usually from wood, cloth, or other fibrous material 
Patent applicant [pta]
    A person or organization that applied for a patent 
Patent holder [pth]
    A person or organization that was granted the patent referred to by the item
    UF Patentee
Patent inventor
    USE Inventor
Patentee
    USE Patent holder
Patron [pat]
    A person or organization responsible for commissioning a work. Usually a patron uses his or her means or influence to support the work of artists, writers, etc. This includes those who commission and pay for individual works
Permitting agency [pma]
    An organization (usually a government agency) that issues permits under which work is accomplished 
Photographer [pht]
    A person, family, or organization responsible for creating a photographic work
Printer [prt]
    A person, family, or organization involved in manufacturing a manifestation of printed text, notated music, etc., from type or plates, such as a book, newspaper, magazine, broadside, score, etc
Printmaker [prm]
    A person or organization who makes a relief, intaglio, or planographic printing surface 
Process contact [prc]
    A person or organization primarily responsible for performing or initiating a process, such as is done with the collection of metadata sets 
Producer [pro]
    A person, family, or organization responsible for most of the business aspects of a production for screen, audio recording, television, webcast, etc. The producer is generally responsible for fund raising, managing the production, hiring key personnel, arranging for distributors, etc. 
Producer of book
    USE Book producer
Production company [prn]
    An organization that is responsible for financial, technical, and organizational management of a production for stage, screen, audio recording, television, webcast, etc. 
Production designer [prs]
    A person or organization responsible for designing the overall visual appearance of a moving image production 
Production manager [pmn]
    A person responsible for all technical and business matters in a production 
Production personnel [prd]
    A person or organization associated with the production (props, lighting, special effects, etc.) of a musical or dramatic presentation or entertainment 
Production place [prp]
    The place of production (e.g., inscription, fabrication, construction, etc.) of a resource in an unpublished form 
Programmer [prg]
    A person, family, or organization responsible for creating a computer program 
Project director [pdr]
    A person or organization with primary responsibility for all essential aspects of a project, has overall responsibility for managing projects, or provides overall direction to a project manager 
Promoter
    USE Thesis advisor
Proofreader [pfr]
    A person who corrects printed matter. For manuscripts, use Corrector [crr] 
Provider [prv]
    A person or organization who produces, publishes, manufactures, or distributes a resource if specific codes are not desired (e.g. [mfr], [pbl]) 
Publication place [pup]
    The place where a resource is published 
Publisher [pbl]
    A person or organization responsible for publishing, releasing, or issuing a resource 
Publishing director [pbd]
    A person or organization who presides over the elaboration of a collective work to ensure its coherence or continuity. This includes editors-in-chief, literary editors, editors of series, etc.
Recording engineer [rce]
    A person contributing to a resource by supervising the technical aspects of a sound or video recording session 
Recordist [rcd]
    A person or organization who uses a recording device to capture sounds and/or video during a recording session, including field recordings of natural sounds, folkloric events, music, etc. 
Redaktor [red]
    A person or organization who writes or develops the framework for an item without being intellectually responsible for its content 
Renderer [ren]
    A person or organization who prepares drawings of architectural designs (i.e., renderings) in accurate, representational perspective to show what the project will look like when completed 
Reporter [rpt]
    A person or organization who writes or presents reports of news or current events on air or in print 
Repository [rps]
    An organization that hosts data or material culture objects and provides services to promote long term, consistent and shared use of those data or objects 
Research team head [rth]
    A person who directed or managed a research project 
Research team member [rtm]
    A person who participated in a research project but whose role did not involve direction or management of it 
Researcher [res]
    A person or organization responsible for performing research
    UF Performer of research
Restorationist [rsr]
    A person, family, or organization responsible for the set of technical, editorial, and intellectual procedures aimed at compensating for the degradation of an item by bringing it back to a state as close as possible to its original condition 
Reviewer [rev]
    A person or organization responsible for the review of a book, motion picture, performance, etc. 
Rubricator [rbr]
    A person or organization responsible for parts of a work, often headings or opening parts of a manuscript, that appear in a distinctive color, usually red 
Scientific advisor [sad]
    A person or organization who brings scientific, pedagogical, or historical competence to the conception and realization on a work, particularly in the case of audio-visual items
Scribe [scr]
    A person who is an amanuensis and for a writer of manuscripts proper. For a person who makes pen-facsimiles, use Facsimilist [fac]
Secretary [sec]
    A person or organization who is a recorder, redactor, or other person responsible for expressing the views of a organization 
Seller [sll]
    A former owner of an item who sold that item to another owner 
Set designer [std]
    A person who translates the rough sketches of the art director into actual architectural structures for a theatrical presentation, entertainment, motion picture, etc. Set designers draw the detailed guides and specifications for building the set 
Setting [stg]
    An entity in which the activity or plot of a work takes place, e.g. a geographic place, a time period, a building, an event 
Signer [sgn]
    A person whose signature appears without a presentation or other statement indicative of provenance. When there is a presentation statement, use Inscriber [ins].
Speaker [spk]
    A performer contributing to a resource by speaking words, such as a lecture, speech, etc.  
Sponsor [spn]
    A person, family, or organization sponsoring some aspect of a resource, e.g., funding research, sponsoring an event
    UF Sponsoring body
Sponsoring body
    USE Sponsor
Supporting host [sht]
    A person or organization that supports (by allocating facilities, staff, or other resources) a project, program, meeting, event, data objects, material culture objects, or other entities capable of support
    UF Host, supporting
Teacher [tch]
    A performer contributing to a resource by giving instruction or providing a demonstration
    UF Instructor
Technical director [tcd]
    A person who is ultimately in charge of scenery, props, lights and sound for a production 
Technical draftsman
    USE Draftsman
 Thesis advisor [ths]
    A person under whose supervision a degree candidate develops and presents a thesis, mémoire, or text of a dissertation
    UF Promoter
Transcriber [trc]
    A person, family, or organization contributing to a resource by changing it from one system of notation to another. For a work transcribed for a different instrument or performing group, see Arranger [arr]. For makers of pen-facsimiles, use Facsimilist [fac] 
Translator [translates]
    A person or organization who renders a text from one language into another, or from an older form of a language into the modern form 
Type designer [tyd]
    A person or organization who designs the type face used in a particular item
    UF Designer of type
Typesetter
    USE Compositor
Typographer [tyg]
    A person or organization primarily responsible for choice and arrangement of type used in an item. If the typographer is also responsible for other aspects of the graphic design of a book (e.g., Book designer [bkd]), codes for both functions may be needed 
University place [uvp]
    A place where a university that is associated with a resource is located, for example, a university where an academic dissertation or thesis was presented 
Videographer [vdg]
    A person in charge of a video production, e.g. the video recording of a stage production as opposed to a commercial motion picture. The videographer may be the camera operator or may supervise one or more camera operators. Do not confuse with cinematographer
Witness [wit]
    Use for a person who verifies the truthfulness of an event or action.
    UF Deponent
    UF Eyewitness
    UF Observer
    UF Onlooker
    UF Testifier
Wood engraver [wde]
    A person or organization who makes prints by cutting the image in relief on the end-grain of a wood block
Writer of accompanying material [wam]
    A person or organization who writes significant material which accompanies a sound recording or other audiovisual material 
Writer of added commentary [wac]
    A person, family, or organization contributing to an expression of a work by providing an interpretation or critical explanation of the original work 
Writer of added lyrics [wal]
    A writer of words added to an expression of a musical work. For lyric writing in collaboration with a composer to form an original work, see lyricist 
Writer of added text [wat]
    A person, family, or organization contributing to a non-textual resource by providing text for the non-textual work (e.g., writing captions for photographs, descriptions of maps). 
Writer of introduction [win]
    A person, family, or organization contributing to a resource by providing an introduction to the original work 
Writer of preface [wpr]
    A person, family, or organization contributing to a resource by providing a preface to the original work 
Writer of supplementary textual content [wst]
    A person, family, or organization contributing to a resource by providing supplementary textual content (e.g., an introduction, a preface) to the original work 