# Tableau Agents

<table>
  <tr>
    <th>Nom</th>
    <th>Définition<br></th>
    <th>Permission</th>
    <th>Version</th>
    <th>Page perso publique</th>
    <th>Page(s) pointant vers cet agent</th>
  </tr>
  <tr>
    <td><strong>Développeur</strong></td>
    <td>Membre de l'équipe de développement</td>
    <td>Toutes permissions</td>
    <td>v1</td>
    <td>Non</td>
    <td>Aucune</td>
  </tr>
  <tr>
    <td><strong>Administrateur</strong></td>
    <td>Membre de l'équipe éditorial</td>
    <td>
    	<ul>
    		<li>Ajout et création de ressources (Line, Article, Resource, Agent)</li>
    		<li>Ajout d'événements</li>
    		<li>Gestion de la page d'accueil</li>
    	</ul>
    </td>
    <td>v1</td>
    <td>Non</td>
    <td>Pages d'information du site "Equipe éditoriale"</td>
  </tr>
  <tr>
    <td><strong>Médiateur</strong></td>
    <td>Responsable d'une équipe Ligne, membre d'une équipe Ligne</td>
    <td>
    	<ul>
    		<li>Ajout et édition de ressources dans leurs lignes (Article, Resource, Agent)</li>
    		<li>Ajout d'événements</li>
    	</ul>
    </td>
    <td>v1</td>
    <td>Oui</td>
    <td>Page de la Ligne de son appartenance</td>
  </tr>
  <tr>
    <td><strong>Auteur</strong></td>
    <td>Pas de rôle spécifique dans Django mais ressource existante dans Omeka</td>
    <td>Aucune</td>
    <td>v1</td>
    <td>Oui</td>
    <td>Liste d'Agents, Pages Article</td>
  </tr>
  <tr>
    <td><strong>Lecteur</strong></td>
    <td>Personne pouvant se créer un compte pour une bibliothèque personnelle</td>
    <td>Collection personnelle de ressources</td>
    <td>v2</td>
    <td>Non</td>
    <td>Aucune</td>
  </tr>
</table>


## Notes 

- page personnelle: voir la recherche de quelqu'un: posture du chercheur qui cherche intéressante à voir