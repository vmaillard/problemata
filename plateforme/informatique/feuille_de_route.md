# Feuille de route

## À faire

- [ ] header : à mettre en place

- [ ] module Omeka : voir pour ne pas renvoyer de valeur null s'il n'y a pas de valeurs
- [ ] module ProblemataRestApi : exposer la ressource d'origine d'une traduction sur "pb:isVersionOf"
- [ ] mettre à jour Omeka + les modules
- [ ] retoucher un peu les contenus importés automatiquement

- [ ] génération page article
- [ ] faire des filtres pandoc

- [ ] page detail ligne + detail agent => intégration list articles

- [ ] filtre fonds
- [ ] ressource : avoir les côtes

- [ ] notices d'autorités pour les agents (isni) 
- [ ] liste agent : filtre ligne
- [ ] List Agent : nationalité des agents entre crochets

## À améliorer

- [ ] template list : améliorer la gestion des filtres js sur les langues avec les ajouts des codes langues
- [ ] template list : gérer le non-affichage des versions traduites d'un article (invisibles ceux qui ont des valeurs pour "pb:isVersionOf")

- [ ] responsive

## Bugs

- [] problème de rendu d'oid de ressources sur les pages detail article
- [x] click sur "Vilem Flusser" dans la liste Agents renvoie NoReverseMatch