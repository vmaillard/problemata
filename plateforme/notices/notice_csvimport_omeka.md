# Notice CSV

<div class="headerLeft">jdb+vm pour Problemata</div>
<div class="headerRight">2021.01.11</div>

[Version en ligne](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/notices/notice_csvimport_omeka.md)

## Écriture du CSV


### Gabarits et exemples

- [Gabarits CSV vides](https://framagit.org/vmaillard/problemata/tree/master/plateforme/data/gabarits_csv)
- [CSV remplis avec des échantillons](https://framagit.org/vmaillard/problemata/-/tree/master/plateforme/data/sample_data)


### Séparateurs

Voici les séparateurs à utiliser dans le CSV :

- séparateur de colonne : `,`
- séparateur de valeurs multiples : `|`


### Ajout de valeurs multiples 

Pour qu'une même cellule contiennent des valeurs multiples, il faut la séparer par un caractère. Nous proposons celui-ci : `|`

Exemple :

```
| dcterms:creator       |
|-----------------------|
| Créateur 1|Créateur 2 |
```

Exemple avec des oid :

```
| dcterms:creator       |
|-----------------------|
| 10|24                 |
```


### Ajout d'un contenu multilingue

Pour ajouter du contenu multilingue, il faut dupliquer les colonnes, en ajoutant au titre de la propriété le code de la langue utilisée.


Exemple :

```
| dcterms:title @fr | dcterms:title @en |
|-------------------|-------------------|
| Texte fr          | Texte en          |
```

Durant l'importation, cette différence permet de labeliser par des codes langues les contenus renseignés.


### Ajout de liens vers des oid

Pour relier une ressource à une autre ressource, il faut renseigner l'oid correspondant à cette dernière ressource.

Par exemple : un article est lié à un agent par `dcterms:creator`. Dans le CSV, il faut ajouter l'oid de l'agent dans la colonne `dcterms:creator`.

Si l'agent n'existe pas dans la base, il faut alors renseigner un nom qui permette de l'aligner plus tard une fois que l'agent sera créé.

Si le CSV décrit des relations entre des oids existants déjà et des ressources qui seront créées plus tard, il faut dupliquer les colonnes. 

<div style="page-break-after: always"></div>

Exemple :
```
| dcterms:creator     | dcterms:creator @oid |
|---------------------|----------------------|
| Un auteur sans oid  | 10                   |
```

Durant l'importation, cette différence permet de créer les liens pérennes vers les oid et de conserver la valeur literal des ressources n'existant pas encore.




### Ajout de l'appartenance à une ligne / un item set

Pour décrire l'appartenance d'un article à une ligne, il faut une colonne "Ligne" qui contient l'oid de la ligne correspondante (possible de mettre le titre de la ligne mais il peut y avoir des erreurs).

Si la ligne n'existe pas, il faut la créer avant l'import. Ou bien reproduire le protocole pour la partie [Ajout de liens vers des oid](#ajout-de-liens-vers-des-oid)


### Ajout d'une class

- dans le csv, faire une colonne class
- remplir par des valeurs correspondants exactement aux class souhaitées (conformes au modèle de données)


### Ajout d'un template

- dans le csv, faire une colonne template
- remplir par des valeurs correspondants exactement aux templates souhaités (au choix dans : Line, Article, Resource, Agent)

<div style="page-break-after: always"></div>


## Importation dans Omeka

### Ordre d'importation

- importer les agents
- importer les ressources (lien vers les oid des agents)
- importer les lignes (lien vers les oid des agents)
- importer les articles (lien vers les les oid des agents, des ressources et des lignes)

### Réglages

- cliquer sur le module "CSV Import"
- upload du .csv
- cliquer sur "Next"
- alignement template : "add mapping" la colonne en question, dans "Generic data", sélectionner "Resource by template"
- alignement class : "add mapping" la colonne en question, dans "Generic data", sélectionner "Resource class (by term)"
- sur les boutons disponibles sur les noms de colonnes, cliquer sur "Add mapping" si la propriété n'est pas reconnue automatiquement et choisir ensuite la propriété correspondante
- sur les boutons disponibles sur les lignes, cliquer sur "Configure" pour cocher la checkbox "Use multivalue separator" pour que l'import prenne en compte l'utilisation de valeurs multiples (à faire pour chaque colonne où il y a besoin)
- sur les boutons disponibles sur les lignes, cliquer sur "Configure" pour choisir le "Data type" (Text, URI, oid) permettant d'aligner le contenu sur une donnée
- sur les boutons disponibles sur les lignes, cliquer sur "Configure" pour ajouter le code langue à utiliser pour la colonne en question (à faire pour chaque colonne où il y a besoin)
- cliquer sur "Add mapping" pour la colonne "Ligne", dans "Item-specific data", choisir la propriété de la ligne à aller chercher ("Internal Id" ou bien "Titre").
- import media : add mapping => media source => url (la cellule contien l'url) (attention : la source url est stockée)
- import uri : "http://example.com This Is The Label" configure => data type => uri

### php-cli

La routine d'import a besoin de chemin vers le php-cli.

Sur MacOs dans `config/local.config.php` :

```
'cli' => [
    'phpcli_path' => null,
],
```

par : 

```
'cli' => [

    'phpcli_path' => '/Applications/MAMP/bin/php/php7.3.7/bin/php',
],
```
