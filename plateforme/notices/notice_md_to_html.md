Titres
=======

# titre level 1
## titre level 2
### titre level 3
#### titre level 4
##### titre level 5
###### titre level 6



Styles de textes
================

*italique* _italique_
**bold** ou __bold__
***bold et italique*** ou ___bold et italique___
|souligné|
||barré||
\* texte échappé



Objets liés au texte
====================

[texte du lien](permalink "titre du lien (affiché au survol)")
![légende de l'image](permalink "titre de l'image (affiché au survol)")
<mail@domain.ext>



Blocs textes
============

paragraphe : ajouter une ligne avant et après le bloc de texte

paragraph 2

> citation paragraphe 1
> 
> citation paragraphe 2
>
>> citation paragraphe 3 dans citation paragraphe 2
>
> #titre level 1 dans citation paragraphe 4



Listes
======

liste ordonnée
1. item 1
2. item 2
3. item 3

liste non ordonnée
- item 1
- item 2
- item 3

liste composée
-	item 1
	> citation dans item 1



Spécifications Problemata
=========================

archive appelée
auteur appelé
article appelé
ligne appelé