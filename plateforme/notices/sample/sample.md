Ce document présente un échantillon des règles d'écriture markdown pour un article.

[Version en ligne](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/notices/sample/sample.md)




<!-- En-tête de l'article -->

# Le titre de l'article

[Le sous-titre de l'article]{.sous_titre}




<!-- Niveaux hiérarchiques des titres -->

## Titre 2 - Chapitre

### Titre 3 - Sous-chapitre




<!-- Bibliographie -->

Pour ajouter une section bibliographique, il faut impérativement ajouter, au titre de cette section, l'identifiant unique "{#bibliographie}", comme ceci :

## Bibliographie {#bibliographie}

Et ce même si le texte est rédigé en anglais




<!-- Gestion des paragraphes et saut de ligne -->

Voci un paragraphe.

Une ligne laissée vide au-dessus permet de faire un deuxième paragraphe.

S'il y a besoin de sauter une ligne sans faire de nouveaux paragraphes, il faut ajouter deux espaces à la fin des lignes. C'est le cas sur le paragraphe suivant :

Une ligne  
Un saut de ligne grâce à deux espaces à la fin de la ligne au-dessus.  
Idem pour cette ligne.

Pour laisser une ligne vide, insérer ceci :

::: {.blank}
:::




<!-- Compteurs de paragraphe -->

Pour qu'un ou plusieurs paragraphes ne soient pas comptés, il faut les entourer de ceci :

::: {.unnumbered}

Ce paragraphe ne sera pas compté

:::




<!-- Notes -->

Pour faire un appel de note, il faut ajouter ceci : [^1] où "1" est un nombre unique.

Ensuite, il faut ajouter le contenu de la note comme ceci :

[^1]: Le contenu de la note.

On retrouve "1" comme le nombre unique relié à l'appel de note.




<!-- Figures -->

Pour faire un appel de figure, il faut ajouter ceci : [Fig. 1](#fig_1) où "fig_1" est un identifiant unique.

Ensuite, dans le champ "Table de figures", il faut ajouter ceci :

::: {#fig_1}

[[identifiant_omeka]]

Figure 1 - Titre + Description courte

:::

On retrouve "fig_1" comme identifiant unique relié à l'appel de figure. "identifiant_omeka" doit être remplacé par l'identifiant unique de la Ressource Omeka à atteindre. 




<!-- Citations -->

Pour ajouter une citation :

> Une citation


> Une citation...
>
> avec des sauts de lignes




<!-- Listes -->

Une liste à puces :

- entrée 1
- entrée 2

Une liste à numéros :

1. entrée 1
2. entrée 2




<!-- Cas spéciaux -->

Une apostrophe droite deviendra une apostrophe virgule : D'elle.

Pour ajouter du texte en italique : *italique*.

Pour ajouter du texte en indice : H~2~O.

Pour ajouter du texte en exposant : 2^10^ = 1024.

Pour ajouter du texte en [petites capitales]{.smallcaps}.

Pour ajouter un hyperlien : [un lien](https://pandoc.org/MANUAL.html).

Pour ajouter un tiret cadratin : ---

Pour ajouter un tiret demi-cadratin : --

Pour ajouter des points de suspension : ...




<!-- Notule -->

Pour ajouter une notule :

---

## Titre 2 - Titre du commentaire

### Titre 3 - Titre du chapitre

Le contenu de la notule.