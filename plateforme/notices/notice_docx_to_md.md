


# Notice docx

<div class="headerLeft">jdb+vm pour Problemata</div>
<div class="headerRight">2022.03.10</div>

[Version en ligne](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/notices/notice_docx_to_md.md)

Ce document présente des règles à respecter pour l'écriture des articles sur Word afin que l'importation d'un fichier docx soit facilitée.



## Règles générales

Le document doit être correct d'un point de vue sémantique, il faut donc éviter les réglages manuels non standards.

Par exemple, pour les hiérarchies de titres et les enrichissements typographiques, seuls ces outils doivent être utilisés :

- styles de formatage les titres, les citations
- bouton italique
- boutons liste à puce ou à numérotation
- bouton hyperlien

À l'inverse, des options comme changer le caractère typographique, le corps typographique, les boutons gras ou souligné ne doivent pas être utilisées.

Enfin, le document ne doit pas contenir d'images. Les fichiers images doivent être tenues dans un dossier séparé pour pouvoir être importé dans un autre temps dans Omeka S.


## Exemple

Un échantillon .docx est téléchargeable ici : [https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/notices/sample/sample.docx](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/notices/sample/sample.docx)



## Liste des styles

Voici la liste la plus exhaustive possible des styles de paragraphe et de caractère pour Problemata alignés sur leurs équivalents Word :

| Styles de texte    | Équivalents Word                                                      |
|--------------------|-----------------------------------------------------------------------|
| Titre article      | Style de formatage Titre 1                                            |
| Sous-titre         | Style de formatage par défaut ou Corps de texte                       |
| Chapitre           | Style de formatage Titre 2                                            |
| Sous-chapitre      | Style de formatage Titre 3                                            |
| Paragraphe         | Style de formatage par défaut ou Corps de texte                       |
| Italique           | Bouton italique                                                       |
| Liste à puces      | Bouton liste à puce                                                   |
| Liste à numéros    | Bouton liste à numérotation                                           |
| Appel de notes     | Bouton "Insérer une note de bas de page"                              |
| Appel de figures   | Faire figurer simplement "Fig. 1"                                     |
| Citation           | Style de formatage Citation (à confirmer)                             |
| Ligne horizontale  | "Paragraphe" > "Bordures" > "Ligne horizontale"                       |
| Notule             | Ligne horizontale + Chapitre + Sous-chapitre + Paragraphes            |
| Exposant           | Bouton exposant                                                       |
| Indice             | Bouton indice                                                         |
| Hyperlien          | Bouton hyperlien                                                      |
| Petites capitales  | Bouton "Petites capitales" (à confirmer)                              |



## Légendes des figures

Les légendes figures doivent être listées à la fin du document. Par la suite, elles seront intégrées dans un autre champ.

L'ajout peut être fait de cette manière :

```
Figure 1 - Titre + Description courte

Figure 2 - Titre + Description courte
```



## Ce qui ne peut pas être fait dans Word

### La class sous-titre

La class pour le sous-titre de l'article sera ajoutée dans l'outil d'écriture.

### L'id bibliographie

L'id pour la section bibliographie sera ajouté dans l'outil d'écriture.

### Les appels de figure

Les appels de figure seront traités dans l'outil d'écriture. À l'échelle du Word, il suffit de faire figurer "Fig. 1".

<!---
<div style="page-break-after: always"></div>
--->
