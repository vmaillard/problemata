# Notice Omeka S

<div class="headerLeft">jdb+vm pour Problemata</div>
<div class="headerRight">2021.01.11</div>

[Version en ligne](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/notices/notice_omeka.md)

<!--
pandoc -c ressources/css/print.css plateforme/notices/notice_omeka.md --pdf-engine=weasyprint -s -o notice_omeka.pdf
-->

Cette partie explicite des méthodes pour ajouter des contenus dans une installation Omeka S conformément [au modèle de données de Problemata](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/interoperabilite/data_model.md).

Il est nécessaire que les entrées de propriétés pouvant être traduites soient labelisées par un code langue. Le site Problemata fonctionne sur deux langues : le français et l'anglais. Il faut donc labeliser les contenus multilingues soit par "fr", soit par "en". La démarche est précisé sur la partie [Ajout de contenu multilingue](#ajout-de-contenu-multilingue).

Pour des points généraux, il est conseillé de se référer [au manuel utilisateur d'Omeka S](https://omeka.org/s/docs/user-manual/).


<!--
## Connexion et identifiants

- admin : [http://problemata.huma-num.fr/omeka_beta/admin/](http://problemata.huma-num.fr/omeka_beta/admin/)

**Compte :**

- mail : vincent.maillard.contact@gmail.com
- mdp : admin12345

**Création d'autres comptes :**

Il est important de créer d'autres comptes avec son nom et son adresse mail. Cela permet de sauvegarder la "parenté" d'un ajout de contenu.

La démarche à suivre est décrite dans [le manuel Omeka S](https://omeka.org/s/docs/user-manual/admin/users/#create-a-new-user).

-->


## Templates

Il est nécessaire que chaque item et item sets ait un template. Le tableau du site web repose sur cette fonctionnalité d'Omeka.

| Nom du template | Nom de la ressource | Class             |
|-----------------|---------------------|-------------------|
| Line            | Ligne               | dctype:Collection |
| Article         | Article             | dctype:Text       |
| Resource        | Ressource           | bibo:Document     |
| Agent           | Agent               | foaf:Agent        |

<div style="page-break-after: always"></div>

## Ajout d'un item


### Ordre d'ajout conseillé

- ajouter les agents
- ajouter les lignes
- ajouter les ressources
- ajouter les articles



### Ajout d'un Agent

- cliquer sur "Items"
- cliquer sur "Add new item"
- sélectionner le "Resource Template" "Agent"

Des propriétés correspondants à Agent sont maintenant disponibles. De plus, la class a été automatiquement remplie. Les propriétés "Title" et "Description" ne sont pas à remplir pour un Agent, Omeka les fait apparaître par défaut, elles disparaîtront à la sauvegarde.

- remplir les propriétés conformément au modèle [Agent](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/interoperabilite/data_model.md#agent-class-foafagent)
- cliquer sur "Add"



### Ajout d'une Ligne

- cliquer sur "Item sets"
- cliquer sur "Add new item set"
- sélectionner le "Resource Template" "Line"

Des propriétés correspondants à Ligne sont maintenant disponibles. De plus, la class a été automatiquement remplie.

- remplir les propriétés conformément au modèle [Ligne](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/interoperabilite/data_model.md#ligne-dctypecollection)
- cliquer sur "Add"



### Ajout d'une Ressource

- cliquer sur "Items"
- cliquer sur "Add new item"
- sélectionner le "Resource Template" "Resource"

Des propriétés correspondants à Ligne sont maintenant disponibles. De plus, la class a été automatiquement remplie.

- remplir les propriétés conformément au modèle [Ressource](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/interoperabilite/data_model.md#ressource-class-bibodocument)
- cliquer sur "Add"

**Ajout d'un media dans une Ressource**

- éditer un item Ressource
- cliquer sur l'onglet "Media"
- cliquer sur "Upload" (colonne à droite)
- dans "Title" mettre la cote/nom du fichier
- cliquer sur "Browse/Parcourir" pour choisir le fichier à uploader
- cliquer sur "Save" pour démarrer l'upload du fichier

Pour une ressource contenant un pdf, le mieux est de mettre une image en premier media pour qu'elle s'affiche dans les listes et le pdf en deuxième media pour qu'il s'affiche sur la page Détail de cette ressource.



### Ajout d'un Article

- cliquer sur "Items"
- cliquer sur "Add new item"
- sélectionner le "Resource Template" "Article"

Des propriétés correspondants à Article sont maintenant disponibles. De plus, la class a été automatiquement remplie.

- remplir les propriétés conformément au modèle [Article](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/interoperabilite/data_model.md#article-class-dctypetext)
- cliquer sur "Add"

Il est nécessaire que la propriété dcterms:creator soit remplie par des ressources de type Agent. Le tableau du site listant l'ensemble des auteurs reposent sur cette fonctionnalité d'Omeka.

**Pour ajouter un article dans une ligne :**

- se rendre sur la page d'édition d'un article
- cliquer sur l'onglet "Item sets"
- dans le panneau à droite, sélectionner une ou plusieurs lignes

**Pour lier deux articles unis par une traduction :**

Les propriétés "A pour traduction" `dcterms:hasVersion` et "Est une traduction de" `dcterms:isVersionOf` servent à décrire la liaison d'une traduction entre deux articles. Il est nécessaire de les déclarer sur les deux ressources à lier. Le sens permet de déterminer qu'elle est la ressource d'origine.

La relation peut se schématiser ainsi : 

- Ressource en langue d'origine → A pour traduction → Ressource(s) traduite(s)
- Ressource(s) traduite(s) → Est une traduction de → Ressource en langue d'origine

Avec les équivalents class et propriétés :

- `dctype:Text` → `dcterms:hasVersion` → `dctype:Text`
- `dctype:Text` → `dcterms:isVersionOf` → `dctype:Text`

**Pour ajouter un pdf à un article de type "Essai visuel" :**

- cliquer sur l'onglet "Media"
- cliquer sur "Upload" (colonne à droite)
- cliquer sur "Browse/Parcourir" pour choisir le fichier à uploader
- cliquer sur "Save" pour démarrer l'upload du fichier

Le mieux est de mettre l'image de couverture en premier media (pour qu'elle s'affiche dans les Listes Articles et dans le premier bloc de la Détail Article) et le pdf en deuxième media (pour qu'il s'affiche en lien sur la page Détail Article).



### Ajout de contenu multilingue

- se rendre sur la page d'édition d'un item
- dans une propriété à contenu multilingue, cliquer sur le globe
- renseigner un code langue au niveau du globe (voir le [vocabulaire contrôlé](https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/interoperabilite/data_model.md#vocabulaire-contr%C3%B4l%C3%A9-langues)) et en-dessous, ajouter le contenu
- répéter l'opération pour chaque nouvelle entrée de la propriété

![](https://omeka.org/s/docs/user-manual/content/contentfiles/item_lang.png)


### Ajout de contenu multiple

Omeka S permet l'ajout de contenus multiples pour toutes les propriétés. Selon les possibilités de valeurs autorisées, il suffit de cliquer sur "Add value" ou "Text" ou "Omeka resource" pour ajouter une nouvelle valeur à une propriété.

<div style="page-break-after: always"></div>




## Écriture dans un CSV

- [Notice pour l'ajout de contenus dans un CSV](https://framagit.org/vmaillard/problemata/blob/master/plateforme/notices/notice_csvimport_omeka.md)
- [Gabarits CSV vides](https://framagit.org/vmaillard/problemata/tree/master/plateforme/data/gabarits_csv)
- [CSV remplis avec des échantillons](https://framagit.org/vmaillard/problemata/-/tree/master/plateforme/data/sample_data)
