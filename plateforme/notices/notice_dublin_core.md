# Notice Dublin Core

## Schéma simplifié

```
Title
	Titre. Sous-titre
Creator
	Nom, Prénom (date-date)
Subject
Description
Publisher
	éditeur (ville)
Contributor
	Nom, Prénom (date-date)
Date
	AAAA/AAAA ou AAAA-MM-JJ/AAAA-MM-JJ ou 14..
Type
Format
	application/pdf ou image/jpeg
Identifier
Source
Language
	fre
Relation
Coverage
	AAAA/AAAA ou AAAA-MM-JJ/AAAA-MM-JJ ou 14.. ou Roubaix (Nord) -- Rue Hoche
Rights
	domaine public ou public domain
```

## Schéma plus précis 


Propriété | Sous-propriété | Description | Format 
--- | --- | --- | ---
Title / Titre | | Un nom donné à la ressource | | 
Title | Alternative / Titre alternatif | Un autre nom de la ressource | 
Creator / Créateur | | Une entité responsable au premier chef de l'élaboration de la ressource | 
Subject / Sujet | | Le sujet de la ressource (mots-clés) |
Description | | Une explication de la ressource | 
" | Table Of Contents / Table des matières | Une liste de sous-unités de la ressource |
" | Abstract / Résumé | Un résumé de la ressource |
Publisher / Éditeur | | Une entité responsable de la mise à disposition de la ressource |
Contributor / Collaborateur | | Une entité responsable d'une contribution à la ressource |
Date | | Un point ou une période dans le temps associés à un événement dans le cycle de vie de la ressource |
" | Date Created / Date de création | La date de création de la ressource |
" | Date Valid / Date de validité | La date (souvent un intervalle) de validité d'une ressource 	|
" | Date Available / Date de disponibilité | La date (souvent une période) à laquelle la ressource a été disponible ou le deviendra |
" | Date Issued / Date de parution | La date de parution formelle (par exemple, la publication) de la ressource |
" | Date Modified / Date de modification | La date à laquelle la ressource a été modifiée |
" | Date Submitted / Date de soumission | Comme exemples de ressources pour lesquelles une date de soumission serait pertinente, une thèse (soumise à un département d'université) ou un article (soumis à un journal) | 
Type | | La nature ou le genre de la ressource |
Format | | Le format de fichier, le support physique ou les dimensions de la ressource |
" | Extent / Étendue | La taille ou la durée de la ressource |
" | Medium / Support | La matière ou le support physique de la ressource |
Identifier / Identificateur | | Une référence univoque vers la ressource dans un contexte donné |
Source | | Une ressource liée de laquelle dérive la ressource décrite |
Language / Langue | | La langue de la ressource |
Relation | | Une ressource liée |
" | Is Version Of / Est une version de | Une ressource liée dont la ressource décrite est une version, une édition ou une adaptation |
" | Has Version / A pour version | Une ressource liée qui est une version, une édition ou une adaptation de la ressource décrite |
" | Is Replaced By / Est remplacé par | Une ressource liée qui supplante, déplace ou remplace la ressource décrite |
" | Replaces / Remplace | Une ressource liée qui est supplantée, déplacée ou remplacée par la ressource décrite |
" | Is Required By / Est exigé par | Une ressource liée qui exige de la ressource décrite qu'elle soutienne sa fonction, sa distribution (delivery) ou sa cohérence |
" | Requires / Nécessite | Une ressource liée qui est exigée par la ressource décrite pour soutenir sa fonction, sa distribution ou sa cohérence |
" | Is Part Of / Est une partie de | |
" | Has Part / A pour partie | Une ressource liée qui est incluse physiquement ou logiquement dans la ressource décrite | 
" | Is Referenced By / Est référencé par | Une ressource liée qui référence, cite ou encore pointe vers la ressource décrite |
" | References / Référence | Une ressource liée qui est référencée, citée ou encore vers laquelle pointe la ressource décrite |
" | Is Format Of / Est un format de | Une ressource liée qui est en substance la même que la ressource décrite mais dans un autre format |
" | Has Format / A pour format | Une ressource liée qui est en substance la même que la ressource décrite préexistente mais dans un autre format |
" | Is Version Of | |
Coverage / Couverture | | Le thème spatial ou temporel de la ressource, l'applicabilité de la ressource dans l'espace ou la juridiction dont dépend la ressource |
" | Spatial / Couverture spatiale | Les caractéristiques spatiales de la ressource |
" | Temporal / Couverture temporelle | Les caractéristiques temporelles de la ressource |
Rights / Droits | | Une information à propos des droits détenus dans et sur la ressource |
" | License / Licence | Un document légal donnant permission officielle de faire quelque chose avec la ressource |
" | Access Rights / Droits d'accès | Une information à propos de qui peut accéder à la ressource ou une indication de son état de protection |

Source : https://www.dublincore.org/specifications/dublin-core/dcmes-qualifiers/


## Exemples 
Dublin Core utilisé par certains sites :

ex : https://firstmonday.org/ojs/index.php/fm/article/view/6984/6090


