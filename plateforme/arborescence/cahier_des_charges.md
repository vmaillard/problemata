# Retours pour le dossier FNSO

Clôture de l'appel : 31.03.2020


## 200313_criteres

**A.5 "workshops UX" + C.1. "designers UX"**

On est assez gêné par les mentions UX en général. Ce qui est écrit ici est subjectif : ce terme nous évoque plutôt l'idée de ne pas inciter à réfléchir à ce qui est donné à voir car l'expérience est stéréotypée. On a l'impression qu'on peut dire beaucoup plus que UX en parlant de design graphique comme traitement formel d'informations, de forme qui connote, pose question, fait recherche. Et on disait en général car on trouve l'héritage idéologique UX problématique

A voir si c'est seulement notre avis + au regard de ce qu'en dit officeabc. Notre point de vue serait de supprimer UX pour parler de "workshops avec des intervenants designers".


**Spécifiquement pour le point A.5.**

peut-être préciser que "les réunions, moments de travail, ateliers permettent à toute l'équipe de se réunir fréquemment autour du comité de pilotage. Les avancées éditoriales se font alors en dialogue avec le design graphique et le développement. La feuille de route est donc actualisée au gré des discussions."


---

**C.3.**
"aucun services proposés" => "aucun service proposé"


---

**D.2.**

"Le lien vers le dépôt sera accessible."

À relecture, il serait bien d'ajouter que ce dépôt sera hébergé sur framagit => "Le lien vers le dépôt, hébergé sur framagit, sera accessible."



## Cahier des charges

**B.2 Pages et onglets de navigation**

- page contact et à propos : 

On pense qu'il faudrait les dissocier. C'est qu'en se projetant ce sera certainement plus commode pour un visiteur d'avoir accès à une simple page de contact (voir de formulaire) pour à un autre moment trouver une page de présentation plus dense. Cette page "A propos" pourrait d'ailleurs plutôt s'appeler "La plateforme" (avec des sections comme : "Présentation", "Les équipes", "Comment contribuer", "Approche science ouverte et open source", "Mentions léagales" (licences, ours, hébergement), etc).

- page index :

Un prototype pourra être mis en place dès la v0. C'est une sorte de squelette du site qu'il faut penser. On le mettra à jour au regard des discussions qui arriveront sur d'autres pages.

- rajouter des mentions sur la possibilité de se connecter en tant que lecteur. Donc une page lecteur / bibliothèque.


**B.3. Recherche et navigation**

"La version Beta du site ne propose pas de recherche en full text."

=> "La Beta propose des champs recherche basés sur ce que permet Omeka S : la recherche dans l'entièreté des métadonnées (noms d'auteur, mot-clés, descriptions, abstracts, etc). La recherche plein texte dans le corps des articles arrivera dans un autre temps, vers la v3."

(page index en v1)


**B.4. Bases de données**

Proposition :

Renommer le B.4. en "Modèle de données" et donc réserver les questions techniques de bases de données à G.

Avec pour description :

"La plateforme repose sur un entrepôt de données et métadonnées Omeka S qui utilise 4 vocabulaires sémantiques : dublin core, friend of a friend, BIO et bibliographic ontology. À cet endroit la plateforme est consultable par une REST API et se plie aux principes FAIR.

Le texte des articles, la bibliothèque utilisateur, la gestion de la page home sont stockés dans une autre base gérée par Django. C'est Django qui génère les pages consultables du site, en ayant accès à l'entrepôt Omeka S.

Le modèle de données de Problemata a été conçu au gré des discussions pour répondre aux attentes éditoriales et aux besoins du site. Voici un schéma simplifié de ce modèle :"

![](../schemas/data_model_simple.jpg)

([lien data_model_simple.jpg](../schemas/data_model_simple.jpg))


**B.5. Iconographie**

- Page d’accueil

"Un principe de focus éditorialisé avec images. La possibilité d’archiver les focus doit être étudiée."

Les issues de la home seront en place pour la v1, administrée et stockée dans django. Pour la question des images cela dépend de la nature des items (omeka) qui sont référencés par l'issue (si ressource alors image, si article qui n'a pas de ressources alors pas d'images, si ligne quelle image de quelle article et ainsi de suite). Un premier jet serait d'afficher uniquement les métadonnées des items, et ensuite d'aviser sur ce que l'on veut donner à voir et commment. (notre point est ici semblable à la question de l'index : un prototype le plus tôt possible et des ajouts sur les versions suivantes)


---

**C.5.**

"Le Print on Demand" => "CSS media print" ou "CSS to print"


---

**D. - le lecteur**

Le compte lecteur est lié à l'arrivée de la gestion de bibliothèque personnelle. Ce sera donc implementé de manière fonctionnelle en v2.


---

**G. Environnements et leurs articulations**

La plateforme Problemata est basé sur la combinaison de deux outils : Omeka S et Django. Omeka S est dédié à la description structurée et sémantique de la base deressources. Django est dédié à la génération de pages (application lire), l'écriture du texte plein des articles (application écrire), la gestion des comptes utilisateurs et des bibliothèques lecteurs (application bibliothèque). En résumé, Omeka S est un entrepôt de données et Django le site web consultable.

L'articulation des deux outils est schématisé ici :

![](../schemas/plateforme_fonctionnement.jpg)

([lien plateforme_fonctionnement.jpg](../schemas/plateforme_fonctionnement.jpg))

L'hébergement est mis en place par la TGIR Huma-Num. C'est une machine virtuelle administrée par les deux développeurs. Voici quelques détails techniques de la VM : Ubuntu 18.04, serveur LAMP (AllowOverride "All", mod_rewrite activé,  MySQL 5.6.4, PHP 7.1+), ImageMagick, Python 3.8.2, Omeka S 2.1.0, Django 3.0.4.

Le budget prévisionnel prend en compte non seulement le développement des fonctionnalités décrites mais aussi les opérations de maitenance et de dépannage qui se feront dans le cadre de facturations ultérieures.


---

**I. Objectifs de mise en ligne [Calendrier provisoire]**

Petites corrections pour le calendrier :

- AUTOMNE 2020: Version Beta: Site web consultable (Application Lire) / Page index (primaire)
- PRINTEMPS 2021: V1: Page index / Réseaux Sociaux / Newsletter / CSS media print (primaire) / REST API basée sur celle d'Omeka S
- AUTOMNE 2021: V2: Bibliothèque personnelle (Application Bibliothèque) / CSS media print / Outil d’écriture (primaire)
- PRINTEMPS 2022: V3: Moteur de recherche Full Text / Outil d’écriture (Application Ecrire)