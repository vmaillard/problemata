# Documentation VM

Documentation de la machine virtuelle pour Problemata.

## CHANGELOG

Pour voir le log des commandes importantes effectuées, [voir le CHANGELOG](changelog.md).

## Versions

- Ubuntu 18.04
- Python 3.6.9
- PHP 7.2.24
- MySQL 5.7.42

## Modules et packages installés ou activés

- [lamp](https://doc.ubuntu-fr.org/lamp) - Pile logicielle pour mettre en place un serveur web (serveur http, gestion de bases de données, langage serveur)
- [mod_rewrite](https://doc.ubuntu-fr.org/apache2#mod_rewrite) - Module Apache pour réécrire des url (utilisé par des CMS notamment)
- [mod_headers](https://doc.ubuntu-fr.org/apache2#mod_headers) - Module Apache pour personnaliser les en-têtes HTTP
- [mod_wsgi](https://pypi.org/project/mod-wsgi/) - Module Apache pour servir du Python via Apache. Il doit être compilé avec Python dans la même version que le Python de l'environnement virtuel
- [Git](https://git-scm.com/) - Outil pour versionner du code
- [zip et unzip](https://doc.ubuntu-fr.org/zip) - Outil pour décompresser des archives .zip
- [Pandoc](https://pandoc.org/)
- [WeasyPrint](https://weasyprint.org/)
- [ImageMagick](https://imagemagick.org/index.php)
- phpmyadmin
- virtualenv

## Dossiers

- `/usr/local/www` : dossier où sont installés les outils
- `/usr/local/www/django` : dossier où est installé Django
- `/usr/local/www/omeka` : dossier où est installé Omeka S
- `/usr/local/www/massive_writer` : dossier où est installé MassiveWriter/Flask
- `/usr/local/www/matomo` : dossier où est installé Matomo
- `/usr/local/www/ws` : dossier pour les workshops
- `/usr/local/www/visualisations` : dossier pour les visualisations
- `/etc/apache2/sites-available/` : stockage des fichiers conf correspondant aux hôtes virtuels

## Hôte virtuel Apache

Deux hôtes virtuels sont activés :

- `/etc/apache2/sites-available/problemata.huma-num.fr.conf`
- `/etc/apache2/sites-available/problemata.org.conf`

Résumé (URL pointe vers Path) :

| URL                                          | Path                          |
|----------------------------------------------|-------------------------------|
| http://problemata.huma-num.fr                | /usr/local/www/django         |
| http://problemata.huma-num.fr/omeka_beta     | /usr/local/www/omeka          |
| http://problemata.huma-num.fr/massive_writer | /usr/local/www/massive_writer |
| http://problemata.huma-num.fr/ws             | /usr/local/www/ws             |
| http://problemata.huma-num.fr/visualisations | /usr/local/www/visualisations |
| http://problemata.huma-num.fr/phpmyadmin     | /usr/share/phpmyadmin         |

"problemata.huma-num.fr" peut être remplacé par "problemata.org"

Contenu de `/etc/apache2/sites-available/problemata.huma-num.fr.conf` :

```
<VirtualHost *:80>
  
    ServerName problemata.huma-num.fr

    DocumentRoot /usr/local/www/django/

    Alias /media/ /usr/local/www/django/media/
    <Directory /usr/local/www/django/media>
        Require all granted
    </Directory>
    Alias /static/ /usr/local/www/django/static/
    <Directory /usr/local/www/django/static>
        Require all granted
    </Directory>

    WSGIDaemonProcess pbta.humanum.org python-home=/usr/local/www/django/env python-path=/usr/local/www/django/problemata
    WSGIProcessGroup pbta.humanum.org

    WSGIScriptAlias /massive_writer /usr/local/www/massive_writer/massive_writer.wsgi
    <Directory /usr/local/www/massive_writer>
        Order deny,allow
        Allow from all
        Require all granted
    </Directory>

    WSGIScriptAlias / /usr/local/www/django/problemata/problemata/wsgi.py
    <Directory /usr/local/www/django/problemata/problemata>
        <Files wsgi.py>
        Require all granted
        </Files>
    </Directory>

</VirtualHost>

Alias /omeka_beta /usr/local/www/omeka
<Directory /usr/local/www/omeka>
    AllowOverride all
    Require all granted
</Directory>
Alias /omeka /usr/local/www/omeka
<Directory /usr/local/www/omeka>
    AllowOverride all
    Require all granted
</Directory>

Alias /visualisations /usr/local/www/visualisations
<Directory /usr/local/www/visualisations>
    AllowOverride all
    Require all granted
</Directory>

Alias /ws /usr/local/www/ws
<Directory /usr/local/www/ws>
    AllowOverride all
    Require all granted
</Directory>

Alias /matomo /usr/local/www/matomo
<Directory /usr/local/www/matomo>
    AllowOverride all
    Require all granted
</Directory>

Alias /phpmyadmin /usr/share/phpmyadmin
```

Contenu de `/etc/apache2/sites-available/problemata.org.conf` :

```
<VirtualHost *:80>
  
    ServerName problemata.org
    ServerAlias www.problemata.org

    DocumentRoot /usr/local/www/django

    Alias /media/ /usr/local/www/django/media/
    <Directory /usr/local/www/django/media>
        Require all granted
    </Directory>

    Alias /static/ /usr/local/www/django/static/
    <Directory /usr/local/www/django/static>
        Require all granted
    </Directory>

    WSGIDaemonProcess pbta.org python-home=/usr/local/www/django/env python-path=/usr/local/www/django/problemata
    WSGIProcessGroup pbta.org

    WSGIScriptAlias /massive_writer /usr/local/www/massive_writer/massive_writer.wsgi
    <Directory /usr/local/www/massive_writer>
        Order deny,allow
        Allow from all
        Require all granted
    </Directory>

    WSGIScriptAlias / /usr/local/www/django/problemata/problemata/wsgi.py
    <Directory /usr/local/www/django/problemata/problemata>
        <Files wsgi.py>
        Require all granted
        </Files>
    </Directory>

</VirtualHost>
```

## Permissions

Les outils mis en place sur la VM ont besoin par endroits de droits de lecture, écriture et exécution. Voici les dossiers et fichiers en question :

- TODO

## Outils

### Installation développement

- [Django](https://framagit.org/jdb/problemata_v1#installation-d%C3%A9veloppement
- [Flask](https://framagit.org/problemata-codes/massive-writer#installation-d%C3%A9veloppement)

### Mise à jour du code des projets sur la VM

- [Django](https://framagit.org/jdb/problemata_v1#mise-jour-du-code-source-sur-la-vm)
- [Flask](https://framagit.org/problemata-codes/massive-writer#mise-jour-du-code-source-sur-la-vm)

### Installation sur la VM

- [Flask](https://framagit.org/problemata-codes/massive-writer#installation-sur-la-vm)

Déploiement problemata_v1/Django sur serveur Apache

*mise en place de l'installation de Django*

- à la racine d'un dossier apache : `git clone [url]`
- `cd problemata/`
- création d'un environnement virtuel nommé "env" avec python 3.6.9 : `python3 -m venv env/` 
- activation de l'environnement : `source env/bin/activate`
- installation des dépendances de django : `pip install -r requirements.txt`
- vérification avec `pip show django` par exemple
- ajuster les permissions pour que Apache puisse écrire dans la base de données (SQLite par exemple) : `664 [rw-rw-r--]` pour db.sqlite3 et `sudo chown :www-data` sur le fichier + le dossier le contenant
- dans les settings de Django, ajouter l'hôte : `ALLOWED_HOSTS=["problemata.huma-num.fr"]`
- collecter les fichiers static : `python problemata/manage.py collectstatic`

*mise en place de  l'installation Omeka S*

- dans "omeka_s/", copier tous les fichiers d'une installation Omeka S sans écraser "/modules" et "/files"
- dans les bases de données Apache, importer `omeka_s/_probl_sample_omeka.sql` dans une base qui peut être nommée pareillement
- mettre les informations correspondantes dans "omeka_s/config/database.ini"

*configuration du serveur Apache*

- avec l'exemple ci-dessous, configurer un hôte virtuel Apache pour pointer vers Django et Omeka S en remplaçant :
    - "django_dir" par le chemin absolu où est Django
    - "omeka_dir" par le chemin absolu où est Omeka S

```
<VirtualHost *:80>

    # ServerName www.problemata.huma-num.fr
    # ServerAlias problemata.huma-num.fr

    DocumentRoot django_dir/

    Alias /media/ django_dir/media/
    <Directory django_dir/media>
        Require all granted
    </Directory>
    Alias /static/ django_dir/static/
    <Directory django_dir/static>
        Require all granted
    </Directory>

    # MODE DAEMON de mod_wsgi
    # python-home = chemin absolu vers l'environnement virtuel
    # python-path = chemin absolu vers le projet django
    WSGIDaemonProcess pbta.humanum.org python-home=django_dir/env python-path=django_dir/problemata
    WSGIProcessGroup pbta.humanum.org

    # alias où déployer django + chemin absolu vers le wsgi.py
    WSGIScriptAlias / django_dir/problemata/problemata/wsgi.py
    # chemin absolu du dossier où Apache a accès au wsgi.py
    <Directory django_dir/problemata/problemata>
        <Files wsgi.py>
        Require all granted
        </Files>
    </Directory>

</VirtualHost>

# alias où déployer omeka + chemin absolu vers le dossier omeka
Alias /omeka_beta omeka_dir
<Directory omeka_dir>
    AllowOverride all
    Require all granted
</Directory>
```

- relancer le serveur Apache


## Ressources


### Bouts de commande

- connection en ssh :

`
ssh <nom_utilisateur>@<serveur> 
`

- copie récursives de fichiers et dossiers (visibles et cachés) sans copier certains dossiers :

`
cp -r ./. !(files|modules) {dossier_destination}
`

- changement de mot de passe :

`
passwd
`

- désactivation d'un site dans la config apache :

`
sudo a2dissite problemata.huma-num.fr
`

- activation d'un site dans la config apache :

`
sudo a2ensite problemata.huma-num.fr
`

- édition de la config Apache par défaut :

`
sudo vim /etc/apache2/sites-enabled/000-default.conf
`

- édition de la config Apache pour problemata.huma-num.fr :

`
sudo vim /etc/apache2/sites-available/problemata.huma-num.fr.conf
`

- redémarrer apache :
    
`
sudo systemctl restart apache2
`

- recharger apache :
    
`
sudo systemctl reload apache2
`

- afficher les erreurs apache :

`
sudo vim /var/log/apache2/error.log
`

---

- installer un packet

`
sudo apt install libapache2-mod-wsgi-py3
`

- supprimer un packet :

`
sudo apt-get purge --auto-remove packagename
`

---

- clone seulement une branche d'un dépôt git :

`
git clone -b {nom_branche} --single-branch {remote}
`


---

- changer le groupe d'un fichier pour que Apache puisse écrire :

`
sudo chown vm:www-data {dir/}
`

- ajouter la permission écriture pour que Apache puisse écrire dans un dossier :

`
chmod -R g+w {dir/}
`


---

- django : déployer tous les fichiers static

`
python manage.py collectstatic
`



### Liens

- [Lamp](https://doc.ubuntu-fr.org/lamp)
- [virtual host Apache](https://doc.ubuntu-fr.org/apache2#creation_d_hotes_virtuels)
- [Python3.8](https://linuxize.com/post/how-to-install-python-3-8-on-ubuntu-18-04/)
- [MYSQL](https://doc.ubuntu-fr.org/mysql)
- [Création d'un super utilisateur sql](https://doc.ubuntu-fr.org/phpmyadmin#acces_root)
- [phpmyadmin](https://doc.ubuntu-fr.org/phpmyadmin)
- [fail2ban](https://doc.ubuntu-fr.org/fail2ban)

---

- [How do I run Django and PHP together on one Apache server?](https://stackoverflow.com/questions/1020390/how-do-i-run-django-and-php-together-on-one-apache-server)
- [How to use Django with Apache and mod_wsgi](https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/modwsgi/)
- [mod_wsgi](https://modwsgi.readthedocs.io/en/develop/)
- [bien faire attention à installer mod_wsgi pour Python 3](https://mindchasers.com/dev/apache-install)
- [How To Serve Django Applications with Apache and mod_wsgi on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-serve-django-applications-with-apache-and-mod_wsgi-on-ubuntu-14-04)

---

Django :
- static files et déploiement :
    - https://docs.djangoproject.com/en/3.0/howto/static-files/#deployment
    - https://docs.djangoproject.com/en/3.0/howto/static-files/deployment/
    - https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/modwsgi/#serving-files
    - https://docs.djangoproject.com/en/3.1/howto/static-files/