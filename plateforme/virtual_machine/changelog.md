## CHANGELOG

**2020.04.05**

- sur `/home`, création du dossier `problemata.huma-num.fr` :

    ```sudo mkdir problemata.huma-num.fr/```

- ajout des permissions à vm, à voir si jdb peut écrire dedans (sinon on fait un groupe) :

    ```sudo chown vm:vm problemata.huma-num.fr/```

- installation de [Lamp](https://doc.ubuntu-fr.org/lamp) :

    ```sudo apt install apache2 php libapache2-mod-php mysql-server php-mysql```

- installation de modules pour Lamp :

    ```sudo apt install php-curl php-gd php-intl php-json php-mbstring php-xml php-zip```


--- 

- création d'un [virtual host](https://doc.ubuntu-fr.org/apache2#creation_d_hotes_virtuels) apache pour `problemata.huma-num.fr` qui pointe sur `/home/problemata.huma-num.fr`
    
    ```sudo vim /etc/apache2/sites-available/problemata.huma-num.fr.conf```

avec ce contenu :

```
<VirtualHost *:80>
    ServerName problemata.huma-num.fr
    ServerAlias www.problemata.huma-num.fr
    DocumentRoot "/home/problemata.huma-num.fr"
    <Directory "/home/problemata.huma-num.fr">
        Options +FollowSymLinks
        AllowOverride all
        Require all granted
    </Directory>
    ErrorLog /var/log/apache2/error.problemata.huma-num.fr.log
    CustomLog /var/log/apache2/access.problemata.huma-num.fr.log combined
</VirtualHost>

Alias /test /var/www
<Directory /var/www>
    Order allow,deny
    allow from all
</Directory>
```

L'alias sert à faire un test pour pointer vers un autre dossier. Résumé de cette configuration temporaire :

| problemata.huma-num.fr/      | problemata.huma-num.fr/test/ |
|------------------------------|------------------------------|
| /home/problemata.huma-num.fr | /var/www                     |


- activation du virtual host :
    
    ```sudo a2ensite problemata.huma-num.fr```

- rechargement de la config apache :

    ```sudo systemctl reload apache2```


--- 

- installation de git

    ```sudo apt install git```


--- 

- installation de [python3.8](https://linuxize.com/post/how-to-install-python-3-8-on-ubuntu-18-04/) (accessible par `python3`) :

    ```
    sudo add-apt-repository ppa:deadsnakes/ppa
    sudo apt install python3.8
    ```

- vérification que la commande python3 pointe bien vers python 3.8 :
    
    ```
    sudo update-alternatives --config python3 
    ```

- installation de `pip3` :
    
    ```
    sudo apt install python3-pip
    ```

- installation de `python3.8-venv` pour créer des environnements virtuels python3.8 :

    ```sudo apt install python3.8-venv```


**2020.04.09**

*installation de mod_wsgi*

- en suivant ce tutoriel : https://tecadmin.net/install-apache-mod-wsgi-on-ubuntu-18-04-bionic/ et de ce qui est conseillé ici : https://modwsgi.readthedocs.io/en/develop/user-guides/quick-configuration-guide.html :
    
    ```
    sudo apt-get install libapache2-mod-wsgi
    sudo systemctl restart apache2
    ```

*configuration de mod_wsgi*

- à partir de /home :

```
sudo mkdir www/
sudo chown vm:vm www/
cd www
mkdir documents
mkdir wsgi-scripts
cd wsgi-scripts/
vim myapp.wsgi
```

et copie de l'exemple "hello world"

```
def application(environ, start_response):
    status = '200 OK'
    output = b'Hello World!'

    response_headers = [('Content-type', 'text/plain'),
                        ('Content-Length', str(len(output)))]
    start_response(status, response_headers)

    return [output]
```

- configuration de apache :

    ```sudo vim /etc/apache2/sites-available/problemata.huma-num.fr.conf```

- copie de cette configuration :

```
<VirtualHost *:80>

    ServerName www.problemata.huma-num.fr
    ServerAlias problemata.huma-num.fr

    DocumentRoot /home/www/documents

    <Directory /home/www/documents>
    <IfVersion < 2.4>
        Order allow,deny
        Allow from all
    </IfVersion>
    <IfVersion >= 2.4>
        Require all granted
    </IfVersion>
    </Directory>

    WSGIScriptAlias / /home/www/wsgi-scripts/myapp.wsgi

    <Directory /home/www/wsgi-scripts>
    <IfVersion < 2.4>
        Order allow,deny
        Allow from all
    </IfVersion>
    <IfVersion >= 2.4>
        Require all granted
    </IfVersion>
    </Directory>

</VirtualHost>

Alias /test /var/www
<Directory /var/www>
    Order allow,deny
    allow from all
</Directory>
```

Résumé de cette configuration temporaire :

| problemata.huma-num.fr/            | problemata.huma-num.fr/test/ |
|------------------------------------|------------------------------|
| /home/www/documents par myapp.wsgi | /var/www                     |


Après pas mal de tests suite à l'installation mod_wsgi (libapache2-mod-wsgi), il faut en fait installer la version compatible avec python3 : libapache2-mod-wsgi-py3 :

```
sudo apt-get purge --auto-remove libapache2-mod-wsgi
sudo apt install libapache2-mod-wsgi-py3
```

De plus, il faut que mod_wsgi soit compilé avec la même version de Python. Si l'environnement virtuel de Problemata est fait avec python3.8.2, il faut que mod_wsgi soit compilé avec python3.8.2. Mais sur ubuntu 18.04, python3.8.2 n'est pas accessible direct, il faut passer par des dépôts externes et du coup on "perd" la compatibilité avec mod_wsgi pour python3.8.2.

La solution est de faire **l'environnement virtuel avec python3.6.9** qui est la dernière version de Python qui s'installe pour Ubuntu 18.04 avec `sudo apt-get install python3`. Dans ce cas, il y a compatibilité avec libapache2-mod-wsgi-py3.

- on change donc pour que la commande python3 pointe bien vers python 3.6.9 :
    
    ```
    sudo update-alternatives --config python3 
    ```

*installation de l'environnement virtuel pour Django*

- sur /home/problemata.huma-num.fr/ on fait : `git clone framagit.org/jdb/problemata/`
- `cd problemata/`
- création d'un environnement virtuel nommé "env" avec python 3.6.9 : `python3 -m venv env/'` 
- activation de l'environnement : `source env/bin/activate`
- installation des dépendances de django : `pip install -r requirements.txt`

*Configuration pour pointer vers l'installation de django*

On configure notre hôte virtuel pour pointer vers django :
    ```
    sudo vim /etc/apache2/sites-available/problemata.huma-num.fr.conf
    ```

dans lequel on copie :

```
<VirtualHost *:80>

        ServerName www.problemata.huma-num.fr
        ServerAlias problemata.huma-num.fr

        DocumentRoot /home/problemata.huma-num.fr/problemata/v1

        Alias /static /home/problemata.huma-num.fr/problemata/v1/static
        <Directory /home/problemata.huma-num.fr/problemata/v1/static>
        <IfVersion < 2.4>
                Order allow,deny
                Allow from all
        </IfVersion>
        <IfVersion >= 2.4>
                Require all granted
        </IfVersion>
        </Directory>

        <Directory /home/problemata.huma-num.fr/problemata/v1/v1>
        <Files wsgi.py>
        <IfVersion < 2.4>
                Order allow,deny
                Allow from all
        </IfVersion>
        <IfVersion >= 2.4>
                Require all granted
        </IfVersion>
        </Files>
        </Directory>

        WSGIDaemonProcess v1 python-path=/home/problemata.huma-num.fr/problemata/v1 python-home=/home/problemata.huma-num.fr/problemata/env
        WSGIProcessGroup v1
        WSGIScriptAlias / /home/problemata.huma-num.fr/problemata/v1/v1/wsgi.py

</VirtualHost>

Alias /base /home/problemata.huma-num.fr/problemata/omeka_s_2.1.0
<Directory /home/problemata.huma-num.fr/problemata/omeka_s_2.1.0>
        Require all granted
</Directory>
```

Résumé de cette configuration :

| problemata.huma-num.fr/                                                                              | problemata.huma-num.fr/base/                          |
|------------------------------------------------------------------------------------------------------|-------------------------------------------------------|
| home/problemata.huma-num.fr/problemata/v1/ par /home/problemata.huma-num.fr/problemata/v1/v1/wsgi.py | /home/problemata.huma-num.fr/problemata/omeka_s_2.1.0 |

- autorisation de l'hôte "problemata.huma-num.fr" dans django `v1/v1/settings.py` :

    ```
    ALLOWED_HOSTS=["problemata.huma-num.fr"]
    ```

--- 

**2020.04.10**

*Configuration de MYSQL et phpMyAdmin*

- l'objectif est de créer un "super utilisateur" pour gérer l'intégralité des bases de données par phpMyAdmin. On ne touche pas à l'utilisateur root. [Voir cette méthode](https://doc.ubuntu-fr.org/phpmyadmin#acces_root)

- on crée un utilisateur "problemata" avec comme mot de passe le wifi de Valence (bas de casse)

```
GRANT ALL ON *.* TO 'problemata'@'localhost' IDENTIFIED BY '{mot_de_passe}' WITH GRANT OPTION;
FLUSH PRIVILEGES;
QUIT;
```

- installation de phpMyAdmin (serveur Apache, création de la base, mot de passe wifi de Valence (bas de casse)) :
    ```
    sudo apt install phpmyadmin
    ```

- ajout d'un alias vers phpmyadmin dans la config de notre hôte virtuel :
    ```
    sudo vim /etc/apache2/sites-available/problemata.huma-num.fr.conf
    ```
puis ajout à la fin :
    ```
    Alias /phpmyadmin /usr/share/phpmyadmin
    ```

- à terme il faudra protéger l'accès à phpMyAdmin par [fail2ban](https://doc.ubuntu-fr.org/fail2ban)

--- 

*installation d'un omeka s*

- installation de unzip :
    ```
    sudo apt install unzip
    ```

- corrections de la configuration Apache pour les requirements Omeka S :

    ```
    sudo vim /etc/apache2/sites-available/problemata.huma-num.fr.conf
    ```
puis ajout de :
    ```
    AllowOverride all
    ```

à :

```
Alias /base /home/problemata.huma-num.fr/problemata/omeka_s
<Directory /home/problemata.huma-num.fr/problemata/omeka_s>
        AllowOverride all
        Require all granted
</Directory>
```

et activation du module mod_rewrite :
    ```
    sudo a2enmod rewrite
    sudo systemctl reload apache2
    ```

--- 


cp -r !(files|modules) {dossier_destination}

- rendre wsgi.py exécutable
chmod a+x wsgi.py

- supprimer /wwww


--- 

**2020.04.23**

- problemata.huma-num.fr : django
- problemata.huma-num.fr/base : omeka
- problemata.huma-num.fr/form : form

```
<VirtualHost *:80>

        ServerName www.problemata.huma-num.fr
        ServerAlias problemata.huma-num.fr

        DocumentRoot /home/problemata.huma-num.fr/problemata/v1

        Alias /static /home/problemata.huma-num.fr/problemata/v1/static
        <Directory /home/problemata.huma-num.fr/problemata/v1/static>
        <IfVersion < 2.4>
                Order allow,deny
                Allow from all
        </IfVersion>
        <IfVersion >= 2.4>
                Require all granted
        </IfVersion>
        </Directory>

        <Directory /home/problemata.huma-num.fr/problemata/v1/v1>
        <Files wsgi.py>
        <IfVersion < 2.4>
                Order allow,deny
                Allow from all
        </IfVersion>
        <IfVersion >= 2.4>
                Require all granted
        </IfVersion>
        </Files>
        </Directory>

        WSGIDaemonProcess v1 python-path=/home/problemata.huma-num.fr/problemata/v1 python-home=/home/problemata.huma-num.fr/problemata/env
        WSGIProcessGroup v1
        WSGIScriptAlias / /home/problemata.huma-num.fr/problemata/v1/v1/wsgi.py

</VirtualHost>

Alias /base /home/problemata.huma-num.fr/problemata/omeka_s
<Directory /home/problemata.huma-num.fr/problemata/omeka_s>
        AllowOverride all
        Require all granted
</Directory>

Alias /form /home/problemata.huma-num.fr/form
<Directory /home/problemata.huma-num.fr/form>
        AllowOverride all
        Require all granted
</Directory>
```


--- 

**2020.04.26**

- problemata.huma-num.fr : form
- problemata.huma-num.fr/base : omeka
- problemata.huma-num.fr/django : django

```
<VirtualHost *:80>
    ServerName problemata.huma-num.fr
    DocumentRoot "/home/problemata.huma-num.fr/form"
    <Directory "/home/problemata.huma-num.fr/form">
        Options +FollowSymLinks
        AllowOverride all
        Require all granted
    </Directory>
</VirtualHost>

Alias /static_django /home/problemata.huma-num.fr/problemata/v1/static
<Directory /home/problemata.huma-num.fr/problemata/v1/static>
    Require all granted
</Directory>

WSGIScriptAlias /django /home/problemata.huma-num.fr/problemata/v1/v1/wsgi.py
WSGIPythonHome /home/problemata.huma-num.fr/problemata/env
WSGIPythonPath /home/problemata.huma-num.fr/problemata/v1
<Directory /home/problemata.huma-num.fr/problemata/v1/v1>
<Files wsgi.py>
    Require all granted
</Files>
</Directory>

Alias /base /home/problemata.huma-num.fr/problemata/omeka_s
<Directory /home/problemata.huma-num.fr/problemata/omeka_s>
        AllowOverride all
        Require all granted
</Directory>
```

- changement taille upload max :
    `
    sudo vim /etc/php/7.2/apache2/php.ini 
    `

ajout : 

``` 
upload_max_filesize = 5M
```

--- 

**2020.05.01**

- installation de zip :
    `
    sudo apt-get install zip
    `

---

**2020.05.14**

- dans `/etc/apache2/sites-available/problemata.huma-num.fr.conf` ajout d'un alias pour phpmyadmin :
    `
    Alias /phpmyadmin /usr/share/phpmyadmin
    `

- problemata.huma-num.fr : form
- problemata.huma-num.fr/base : omeka
- problemata.huma-num.fr/django : django
- problemata.huma-num.fr/static_django : v1/static
- problemata.huma-num.fr/phpmyadmin : phpmyadmin

```
<VirtualHost *:80>
    ServerName problemata.huma-num.fr
    DocumentRoot "/home/problemata.huma-num.fr/form"
    <Directory "/home/problemata.huma-num.fr/form">
        Options +FollowSymLinks
        AllowOverride all
        Require all granted
    </Directory>
</VirtualHost>

Alias /static_django /home/problemata.huma-num.fr/problemata/v1/static
<Directory /home/problemata.huma-num.fr/problemata/v1/static>
    Require all granted
</Directory>

WSGIScriptAlias /django /home/problemata.huma-num.fr/problemata/v1/v1/wsgi.py
WSGIPythonHome /home/problemata.huma-num.fr/problemata/env
WSGIPythonPath /home/problemata.huma-num.fr/problemata/v1
<Directory /home/problemata.huma-num.fr/problemata/v1/v1>
<Files wsgi.py>
    Require all granted
</Files>
</Directory>

Alias /base /home/problemata.huma-num.fr/problemata/omeka_s
<Directory /home/problemata.huma-num.fr/problemata/omeka_s>
        AllowOverride all
        Require all granted
</Directory>

Alias /phpmyadmin /usr/share/phpmyadmin
```

---

**2020.08.25**

- problemata.huma-num.fr : django
- problemata.huma-num.fr/static : le dossier "v1/static"
- problemata.huma-num.fr/base : omeka
- problemata.huma-num.fr/form : form
- problemata.huma-num.fr/phpmyadmin : phpmyadmin

```
<VirtualHost *:80>

        ServerName www.problemata.huma-num.fr
        ServerAlias problemata.huma-num.fr

        DocumentRoot /home/problemata.huma-num.fr/problemata/v1

        Alias /static /home/problemata.huma-num.fr/problemata/v1/static
        <Directory /home/problemata.huma-num.fr/problemata/v1/static>
        <IfVersion < 2.4>
                Order allow,deny
                Allow from all
        </IfVersion>
        <IfVersion >= 2.4>
                Require all granted
        </IfVersion>
        </Directory>

        <Directory /home/problemata.huma-num.fr/problemata/v1/v1>
        <Files wsgi.py>
        <IfVersion < 2.4>
                Order allow,deny
                Allow from all
        </IfVersion>
        <IfVersion >= 2.4>
                Require all granted
        </IfVersion>
        </Files>
        </Directory>

        WSGIDaemonProcess v1 python-path=/home/problemata.huma-num.fr/problemata/v1 python-home=/home/problemata.huma-num.fr/problemata/env
        WSGIProcessGroup v1
        WSGIScriptAlias / /home/problemata.huma-num.fr/problemata/v1/v1/wsgi.py

</VirtualHost>

Alias /base /home/problemata.huma-num.fr/problemata/omeka_s
<Directory /home/problemata.huma-num.fr/problemata/omeka_s>
        AllowOverride all
        Require all granted
</Directory>

Alias /form /home/problemata.huma-num.fr/form
<Directory /home/problemata.huma-num.fr/form>
        AllowOverride all
        Require all granted
</Directory>

Alias /phpmyadmin /usr/share/phpmyadmin
```

---

**2020.10.01**

- modification du .htaccess (`/home/vm/problemata.huma-num.fr/problemata/omeka_s/htaccess`) de Omeka S pour que Omeka S autorise du fetch JS :
```
<IfModule mod_headers.c>
    Header set Access-Control-Allow-Origin "*"
</IfModule>
```

- activation du module Apache `mod_headers` et pour que Omeka S autorise du fetch JS :
```
sudo a2enmod headers
sudo service apache2 restart
```

---

**2021.01.10**

- création dossiers `/home/vm/problemata.huma-num.fr/problemata_beta/` et `/home/vm/problemata.huma-num.fr/problemata_beta/omeka_s_beta`
- installation Omeka S 3 dans `omeka_s_beta`
- ajout de cet alias 
```
Alias /omeka_beta /home/vm/problemata.huma-num.fr/problemata_beta/omeka_s_beta
<Directory /home/vm/problemata.huma-num.fr/problemata_beta/omeka_s_beta>
        AllowOverride all
        Require all granted
</Directory>
```

---

**2021.01.17**

- création dossiers `/home/vm/problemata.huma-num.fr/ws/` et `/home/vm/problemata.huma-num.fr/ws/massiv_attack` pour les workshops et le workshop Massiv Attack
- déploiement du git https://framagit.org/problemata-codes/workshop-massive-attack/ dessus
- ajout de cet alias 
```
Alias /ws /home/vm/problemata.huma-num.fr/ws
<Directory /home/vm/problemata.huma-num.fr/ws>
        AllowOverride all
        Require all granted
</Directory>
```

---

**2021.01.21**

- ajout "Options +Indexes" sur /ws pour pouvoir lister le contenu d'un dosier :

```
Alias /ws /home/vm/problemata.huma-num.fr/ws
<Directory /home/vm/problemata.huma-num.fr/ws>
        AllowOverride all
        Require all granted
        Options +Indexes
</Directory>
```

--- 

**2021.02.10**

- installation de Pandoc :

```
sudo apt install pandoc
```

- installation de WeasyPrint :

```
sudo apt-get install build-essential python3-dev python3-pip python3-setuptools python3-wheel python3-cffi libcairo2 libpango-1.0-0 libpangocairo-1.0-0 libgdk-pixbuf2.0-0 libffi-dev shared-mime-info
```

puis (mais WeasyPrint n'est pas installé en global comme ça, donc désinstallation) :

```
/usr/bin/pip3 install WeasyPrint
/usr/bin/pip3 uninstall WeasyPrint
```

du coup installation de WeasyPrint en global ([voir ici](https://stackoverflow.com/a/41352413)):

```
sudo -H /usr/bin/pip3 install WeasyPrint
```

--- 

**2021.03.14**

- installation de ImageMagick :

```
sudo apt install imagemagick
```

--- 

**2021.03.17**

Le paquet Pandoc pour Ubuntu 18.04 bionic date pas mal : pandoc 1.19 (09.2017).

Du coup désinstallation de Pandoc :

```
sudo apt-get remove  pandoc
sudo apt-get remove --auto-remove pandoc
sudo apt-get purge pandoc
```

Et installation à partir d'un deb installer (03.2021) :

```
sudo dpkg -i /home/vm/pandoc-2.12-1-amd64.deb
```

--- 

**2021.04.01**

Modification de `/etc/apache2/sites-available/problemata.huma-num.fr.conf` pour déployer massive_writer (Flask) sur /massive_writer :

```
<VirtualHost *:80>

        ServerName www.problemata.huma-num.fr
        ServerAlias problemata.huma-num.fr

        DocumentRoot /home/problemata.huma-num.fr/problemata/v1

        Alias /static /home/problemata.huma-num.fr/problemata/v1/static
        <Directory /home/problemata.huma-num.fr/problemata/v1/static>
        <IfVersion < 2.4>
                Order allow,deny
                Allow from all
        </IfVersion>
        <IfVersion >= 2.4>
                Require all granted
        </IfVersion>
        </Directory>

        WSGIDaemonProcess myapps python-path=/home/problemata.huma-num.fr/problemata/v1 python-home=/home/problemata.huma-num.fr/problemata/env

        WSGIProcessGroup myapps

        WSGIScriptAlias /massive_writer /home/problemata.huma-num.fr/massive_writer/massive_writer.wsgi
        <Directory /home/problemata.huma-num.fr/massive_writer>
            Order deny,allow
            Allow from all
            Require all granted
        </Directory>

        WSGIScriptAlias / /home/problemata.huma-num.fr/problemata/v1/v1/wsgi.py
        <Directory /home/problemata.huma-num.fr/problemata/v1/v1>
        <Files wsgi.py>
            Require all granted
        </Files>
        </Directory>

</VirtualHost>

Alias /base /home/problemata.huma-num.fr/problemata/omeka_s
<Directory /home/problemata.huma-num.fr/problemata/omeka_s>
        AllowOverride all
        Require all granted
</Directory>

Alias /form /home/problemata.huma-num.fr/form
<Directory /home/problemata.huma-num.fr/form>
        AllowOverride all
        Require all granted
</Directory>

Alias /omeka_beta /home/vm/problemata.huma-num.fr/problemata_beta/omeka_s_beta
<Directory /home/vm/problemata.huma-num.fr/problemata_beta/omeka_s_beta>
        AllowOverride all
        Require all granted
</Directory>

Alias /ws /home/vm/problemata.huma-num.fr/ws
<Directory /home/vm/problemata.huma-num.fr/ws>
        AllowOverride all
        Require all granted
        Options +Indexes
</Directory>

Alias /phpmyadmin /usr/share/phpmyadmin
```


--- 

**2021.04.01**

Installation de virtualenv :

```
/usr/bin/pip3 install virtualenv
```

--- 

**2022-07-04**

Installation de Matomo et mise à jour de `/etc/apache2/sites-available/problemata.huma-num.fr.conf`.

Suppression de l'alternative python3 qui pointe vers python3.8 pour faire pointer python3 de nouveau vers python3.6 :

```
sudo update-alternatives --remove python3 /usr/bin/python3.8
sudo ln -s /usr/bin/python3.6 /usr/bin/python3
```

--- 

**2025-01-14**

Déplacement et renommage de dossiers.

Contenu de `/etc/apache2/sites-available/problemata.huma-num.fr.conf` :

```
<VirtualHost *:80>
  
    ServerName problemata.huma-num.fr

    DocumentRoot /usr/local/www/django/

    Alias /media/ /usr/local/www/django/media/
    <Directory /usr/local/www/django/media>
        Require all granted
    </Directory>
    Alias /static/ /usr/local/www/django/static/
    <Directory /usr/local/www/django/static>
        Require all granted
    </Directory>

    WSGIDaemonProcess pbta.humanum.org python-home=/usr/local/www/django/env python-path=/usr/local/www/django/problemata
    WSGIProcessGroup pbta.humanum.org

    WSGIScriptAlias /massive_writer /usr/local/www/massive_writer/massive_writer.wsgi
    <Directory /usr/local/www/massive_writer>
        Order deny,allow
        Allow from all
        Require all granted
    </Directory>

    WSGIScriptAlias / /usr/local/www/django/problemata/problemata/wsgi.py
    <Directory /usr/local/www/django/problemata/problemata>
        <Files wsgi.py>
        Require all granted
        </Files>
    </Directory>

</VirtualHost>

Alias /omeka_beta /usr/local/www/omeka
<Directory /usr/local/www/omeka>
    AllowOverride all
    Require all granted
</Directory>
Alias /omeka /usr/local/www/omeka
<Directory /usr/local/www/omeka>
    AllowOverride all
    Require all granted
</Directory>

Alias /visualisations /usr/local/www/visualisations
<Directory /usr/local/www/visualisations>
    AllowOverride all
    Require all granted
</Directory>

Alias /ws /usr/local/www/ws
<Directory /usr/local/www/ws>
    AllowOverride all
    Require all granted
</Directory>

Alias /matomo /usr/local/www/matomo
<Directory /usr/local/www/matomo>
    AllowOverride all
    Require all granted
</Directory>

Alias /phpmyadmin /usr/share/phpmyadmin
```

Contenu de `/etc/apache2/sites-available/problemata.org.conf` :

```
<VirtualHost *:80>
  
    ServerName problemata.org
    ServerAlias www.problemata.org

    DocumentRoot /usr/local/www/django

    Alias /media/ /usr/local/www/django/media/
    <Directory /usr/local/www/django/media>
        Require all granted
    </Directory>

    Alias /static/ /usr/local/www/django/static/
    <Directory /usr/local/www/django/static>
        Require all granted
    </Directory>

    WSGIDaemonProcess pbta.org python-home=/usr/local/www/django/env python-path=/usr/local/www/django/problemata
    WSGIProcessGroup pbta.org

    WSGIScriptAlias /massive_writer /usr/local/www/massive_writer/massive_writer.wsgi
    <Directory /usr/local/www/massive_writer>
        Order deny,allow
        Allow from all
        Require all granted
    </Directory>

    WSGIScriptAlias / /usr/local/www/django/problemata/problemata/wsgi.py
    <Directory /usr/local/www/django/problemata/problemata>
        <Files wsgi.py>
        Require all granted
        </Files>
    </Directory>

</VirtualHost>
```