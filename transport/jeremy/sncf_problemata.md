## Déplacements Problemata

OK : 35
### comité scientifique : 16 + 19 = 35
- 22.09.2020 : aller, strasbourg - paris est, 16
- 22.09.2020 : retour, paris est - strasbourg, 19

OK : 146,40
### semaine Allègre : 22 + 61,90 + 37,50 + 25 = 146,40
- 30.08.2020 : retour, paris est - strasbourg, 22
- 29.08.2020 : retour, brioude - paris bercy, 61,90
- 26.08.2020 : aller, paris bercy - brioude, 37,50
- 25.08.2020 : aller, strasbourg - paris est, 25

OK : 41
### présentation partenaires : 19 + 22 = 41
- 10.02.2020 : aller, strasbourg - paris est, 19
- 12.02.2020 : retour, paris est - strasbourg, 22

OK : 232,50
### workshop ENS : 41 + 71,50 + 120 = 232,50
- 25.10.2019 : retour, nevers - strasbourg, 71,50 
- 24.10.2019 : retour, paris bercy - nevers, 41
- 18.10.2019 : aller, bruxelles - marne la vallée chessy, 240/2 (attention, c'est pour aller ET retour à BXL, du coup divisé par 2 (juste retour), prix à vérifier avec Emile = 0K) = 120

OK : 83
### réunion chez Brice du 16 mai 2019 : 15 + 68 = 83
- 23.05.2019 : retour, paris - valence, 68
- 11.05.2019 : aller, nevers - paris, 15

---

### total 537,90 €
### ancien total = 454,90 €


