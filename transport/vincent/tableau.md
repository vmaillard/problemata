# Transports Vincent

| Nature                   | Date       | Montant | Remboursement | Commentaire | Justificatif                                   | Total |
|--------------------------|------------|---------|---------------|-------------|------------------------------------------------|-------|
| Abonnement TGVmax        | 2019.10.12 | 79      |               |             | [tgvmax](src/2019.10_tgvmax.pdf)               | 325.6 |
| Billet Grenoble-Paris    | 2019.10.21 | 0       |               | Annulé      | [billet](src/2019.10.21_annulation.pdf)        |       |
| Billet Grenoble-Paris    | 2019.10.21 | 108     | -27           |             | [billet](src/2019.10.21.pdf)                   |       |
| Billet Paris-Grenoble    | 2019.10.23 | 0       |               |             | [billet](src/2019.10.23.pdf)                   |       |
| Billet Grenoble-Paris    | 2019.11.07 | 0       |               |             | [billet](src/2019.11.07_grenoble_paris.pdf)    |       |
| Billet Paris-Grenoble    | 2019.11.07 | 0       |               |             | [billet](src/2019.11.07_paris_grenoble.pdf)    |       |
| Billet Grenoble-Paris    | 2020.01.30 | 44      |               |             | [billet](src/2020.01.30_paris_grenoble.pdf)    |       |
| Billet Paris-Lyon        | 2020.01.30 | 67      |               |             | [billet](src/2019.01.30_paris_lyon.pdf)        |       |
| Billet Lyon-Brioude      | 2020.08.25 | 34.6    |               |             | [billet](src/2020.08.25_lyon_brioude.pdf)      |       |
| Billet Clermont-Grenoble | 2020.08.29 | 20      |               |             | [billet](src/2020.08.29_clermont_grenoble.pdf) |       |