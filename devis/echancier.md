# Devis

Devis v1 sans options : 12340€ à deux

# juillet

- laschool : 2000
	- vm : 1000 (2019 07)
	- jdb : 1000

- msh : 1500
	- vm : 750 (2019 10 09)
	- jdb : 750


# octobre

- msh : 2500
	- vm : 1250 (2019 11 08)
	- jdb : 1250

# décembre

- msh : 2000
	- vm : 1000 (2020 01 24)
	- jdb : 1000

# mars

- msh : 2000
	- vm : 1000 (2020 04 27)
	- jdb : 1000

# reste

- 2340€

---

Devis v2 : 27 150€

# Septembre 2020

- msh : 2000
	- vm : 1000 (2020 09 23)
	- jdb : 1000