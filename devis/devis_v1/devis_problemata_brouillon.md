# Devis V1 Problemata
---------------------

*Des niveaux d'importance sont attribués à chaque tâche, le plus important étant 0 (zéro).
Le point d'exclamation entre parenthèses indique le caractère non ommissible de la tache.
Le coût de chaque tâche est ferré à gauche sur la même ligne que la tâche qu'il décrit.*



### Social (*officeABC + vm et jdb*)
------------------------------------

- suivi de projet (5% du montant total)
- journées de rendez-vous (sur une base de une par mois ou tous les 2 mois)
- journées de travail commun
- déplacements (à définir au compte goutte selon les RDV ou journées de travail)



### Informatique (*vm & jdb*)
-----------------------------

- chaine éditoriale outillée
	- écriture des ontologies
		- lexique commun
		- gestion des modèles des métadonnées en vue de la SEO et du branchement HumaNum (indexation)
	- mise en place d'une chaine logicielle d'écriture (auteur)
		- étude sur le terrain (format workshop de 3-4 jours [*BK+C.Geel+vm&jdb+B.Domingues*])
		- accompagnement et briefing donateur/éditeur/auteur/lecteur
		- augmentation du langage MarkDown spécifiquement pour la plateforme
		- notices de saisie en MarkDown
		- définition d'un mode de saisie en fonction
	- mise en place d'un back-office
		- choix du cms et de son environnement (langage et philosophie)
		- définition des tables et entrées de la base
		- définition des méthodes
			- multilinguisme
			- recherche
			- filtrage
			- temps et objets (approche calendrier/tableau/liste d'objets spécifiques)
		- définition des modèles d'objets
		- définition de l'architecture du site
		- définition des vues
		- définition des templates
		- migration dans la base
			- ajout des métadonnées en fonction du branchement HumaNum
			- convertisseur MD to HTML sur la base d'une pensée d'écriture des fichiers en dur
	- sortie HTML
		- écriture des fichiers HTML suivant une expertise des standards de balises
		- circulation aisée dans le site
		- pensée responsive des écrans (desktop, laptop, tablet, mobile)
		- pensée des interactions utilisateurs (CSS for affordances, JS for TimeBasedGestures)

- chaine éditoriale branchée (indexation, conversation par interopérabilité) (*+ HumaNum*)
	- discussion et rendez-vous
	- branchement et temporalité entre récolte de données de sources externes et écriture de données en interne selon les diférentes modalités de saisie des contenus
		- config 1 : (BK -> HumaNum -> Problemata)
		- config 2 : (auteur -> HumaNum -> Problemata)
		- config 3 : (BK -> HumaNum -> auteur)
	- lexique humble et juste en fonction du vocabulaire engagé par la plateforme 
	- schéma RDF à construire en fonction, OpenGraph à éprouver



### Design (*officeABC + vm & jdb*)
-----------------------------------

- définition de la charte graphique et identité de la plateforme
	- métaphore visuelle globale
	- zones
		- répartition des informations dans l'écran
			- menus
			- contenu principale
			- contenu annexes
	- texte
		- définition des variables textuelles (corps, graisse, grille et ligne de base) en fonction des besoins
	- couleur
		- définition des variables colorimétriques (rgb calculé, rgb arbitraire)
	- objets
		- dissociation des différentes objets et types de contenus par la pensée d'un système graphique dans les différents mode de rendus dans les différentes pages de la plateforme

- intégration des variables textuelles et visuelles
- normalisation et sérialisation des fichiers de descriptions des éléments graphiques (CSS)



### Modalités de rémunération
-----------------------------

- juillet (acompte 1)
	- social
	- informatique 2/3
	- design 1/3

- octobre (acompte 2)
	- design 1/3

- décembre (solde)
	- informatique 1/3
	- design 2/3

