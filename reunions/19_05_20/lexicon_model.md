RDV 20.05.2019
--------------

# lexique / nomenclature
------------------------

## Modèle d'objet
-----------------

*metadata*

● ligne *nom, date*
	➸ mise en forme
	➸ ressources
		article.html
		fichier (.ext)
		...

● ligne *nom, date*
	➸ article *nom, date*
		◡ mise en forme
		◡ ressources
			file (.ext) *nom, date*


● ligne *Italie radicale*
	➸ article *Architecte et discothèque*
		◡ mise en forme
		◡ ressources
			aa_files_69_rossi (.pdf)
			domus_yéyé (.pdf)
			architecture_goes_disco (transcription eng) (md -> html)
			architecture_goes_disco (traduction fr) (md -> html)
	➸ article *De l'objet fini à la fin de l'objet*
		◡ mise en forme
		◡ ressources
			img_reportage_cci_0 (.jpg)
			img_reportage_cci_1 (.jpg)
			img_reportage_cci_2 (.jpg)
			objet_fini (transcription eng) (md -> html)
			objet_fini (traduction fr) (md -> html)


## Entrées
----------




# métadonnées
-------------

## Modèle dc:*attribut* (Dublin Core)
-------------------------------------
Titre / Title
Créateur / Creator
Sujets / Subject
Description / Description
Éditeur / Publisher
Contributeur / Contributor
Date / Date
Type / Type
Format / Format
Identifiant de la ressource / Identifier
Source / Source
Langue / Language
Relation / Relation
Couverture (couverture spatio-temporelle de la ressource)/ Coverage
Gestion des droits / Rights

## Tableau custom (à affiner)


