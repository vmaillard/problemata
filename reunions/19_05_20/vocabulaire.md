# Vocabulaires de la collection

## 2019 05 13


*collection*
- fonds
- items
- sets
- articles
- notices
- dossiers
- bibliothèque
- exposition

*liens*
- thèmes
- portail
- axes
- fil
- ligne
- rhizome
- branche
- relation
- croisement
- ascendant
- descendant

*classement*
- tag
- mots-clés
- catégorie
- ontologie
- taxonomie
- taxon
- folksonomie
- étiquette
- classification
- cote

*méthodes*
- recherche
- chronologie
- liste
- index
- sélection
- tri
- filtre
- hasard
- lexique
- suggestion
- hyperlien
- synoptique

*acteurs*
- auteurs (designers, architectes, écrivains, etc)
- makers

*mediateurs*
- enquêteur principal
- superviseur
- contributeur
- collectionneur
- administrateur
- utilisateur
- éditeur

*matériau*
- média
- type
- support
- format
- technologie

*archive*
- localisation
- affiliation
- provenance / source
- institution
- partenaire


## Collection d'exemples

**aaaaarg.fail**
- things
- makers
- discussions
- collections

**researchcatalogue.net**
*Meta*
- title
- author
- keyword
- portail
- format
- country
- principal investigator
- supervisor
- contributor
*Tri*
- exposition
- user
- work
- project

**modesofexistence.org**
*Colonnes*
- texte
- vocabulaire
- documents

**archivesetdocumentation.centrepompidou.fr/cdc.html**
Cadre de classement Kandinsky :
- fonds par nature (artiste, photos, vidéos) puis fonds par personnalité

**collection.centrepompidou.fr**
- champs de recherche
- filtre

**centrepompidou.fr/fr/Collections/Les-oeuvres**
*filtres*
- média (image)
- type (photographie)
- ressource (reproduction d'une œuvre)
- source (musée)
- mots-clés

**p-dpa.net**
*menu*
- index des travaux
- articles

*meta*
- authors
- works
- media
- technologies
- platforms
- keywords







