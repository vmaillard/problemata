# Réunion 05.09.2019

## Lieu
Sciences Po - 9 rue de la Chaise

## Présents
- Brice Domingues
- Catherine Geel
- Thomas Huot-Marchand
- Yeelena De Bels (debelsyeelena@gmail.com)
- Vincent Maillard

## Déroulé

- Présentation de Problemata à Yeelena

- Réunion du 13 septembre, Yeelena ne pourra pas être là

- Workshop 21-25 octobre à l'ENS :
participants : des étudiants de master de recherche + 2 étudiants de l'ANRT (certainement)

- Fonts variables :
Thomas en parle comme moyen d'alléger le poids d'une page web (un seul fichier et les axes pour la variabilité) pouvant également incorporer des signe pour la page ou des éléments de navigation.
La variabilité peut aussi être un outil pertinent pour les réglages précis de justification, les réglages optiques, notamment pour le print.
Les variables fonts sont plutôt bien supporter actuellement : https://caniuse.com/#feat=variable-fonts (sur macos, il y a des spécifications de version de l'os) => prévoir des fallbacks
Il existe des variables fonts open source => à voir quant à la qualité, sinon il est aussi possible de forker un projet pour se l'approprier.

- Alphabet : latin
Les quatre langues qui seront utilisées : anglais, italien, allemand et français

- Format d'écriture : 
Comment automatiser la bibliographie, notamment les passages entre normes ? Est-ce que l'on décrit les éléments de la bibliographie dans la base Omeka (en référant les élements à des bases externes) ?


- Biblissima : http://beta.biblissima.fr/
