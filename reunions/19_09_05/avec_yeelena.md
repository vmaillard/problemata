# Avec Yeelena

## Typographie à l'écran

- Réglages de l'interlignage, par la mise en place d'une ligne de base [voir *CSS linear-gradient*] et la gestion des hauteurs de lignes [voir *CSS line-height*], des corps typographiques des différents types de texte [voir *CSS font-size*] et des marges internes et externes des blocs textes [voir *CSS padding, margin*] [pour tout, voir *CSS variables*]

- Gestion graphique des différents blocs sémantiques (citations, listes, bibliographie, notes, etc.)

- Méthodes d'organisation de l'information, du parcours / croisement / recherche des contenus

- Distinction graphique des types d'objets de la base de données (ligne, article, auteur, contributeur, mot-clé, ressources) pour une compréhension facilitée de ce qui se présente à l’œil

- Gestion du contraste texte / fond pour la lecture : mise en place d'un nuancier qui servent à la fois la bonne lecture et l'esthétique de la plateforme [voir *CSS color, background-color*]

- Gestion du layout, des zones aux corps typographiques en passant par les fonctionnalités, selon les attributs spécifiques des différents appareils cibles

- Rendering des lettres en tracés modifiables plutôt que bloc caractère par défaut pour y changer leurs attributs (corps, graisses, inclinaison, forme, nombre de points composant le caractère) [voir *JS opentype.js, paper.js et plumin.js, HTML svg*]

- Mise en place d'une police variable [voir *Opentype variable fonts*, *CSS font-variation-settings*]

*Détails technique*

- Gestion des césures [voir *CSS word-break, white-space*]

- Gestion des spécifités des différents types de balises textes (ex : 10m<sup>2</sup>, ou le contenu de sup est en exposant) [voir *HTML tags, CSS selectors(tag, id, class, dataset, …) , vertical-align, text-transform, …*] 

- Systématisation de l'organisation du texte au fil des pages en combinant zones, filets [voir *CSS borders, HTML hr*]

- Signification de la circulation dans le site : mise en avant graphique et spécifique des hyperliens [voir *CSS a, a:hover, a:visited*]

- Traitement graphique des blocs images [voir *CSS filter, box-shadow*] 

- Systématisation de l'indentation des blocs textes [voir *CSS text-indent, padding(-côtéàajouter)*]

- Mise à l'échelle de la lettre (voir *JS charAt, array, tous les basiques*) de tous ou certains blocs textes [voir *HTML tags*], à un événement particulier (voir *JS node.addEventListener(eventname,function)*] en vue de transformations individuelles (rotation, couleur, décalage, police)
