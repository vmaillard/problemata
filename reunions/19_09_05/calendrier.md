# Calendrier 

| Dates                      | officeabc                                                                        | jdb + vm                                                                                      |
|----------------------------|----------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------|
| **De juillet à octobre**   | Précision besoins utilisateurs /  éditeurs / auteurs                             | Interopérabilité : choix des ontologies                                                       |
|                            | Structuration de l'écriture                                                      | Modalités d’écriture                                                                          |
|                            | Premières pistes de navigation                                                   | Mise en place du back office  (choix de technologies, arborescence  et gabarits, multilingue) |
| **De novembre à décembre** | Détermination des règles typographiques                                          | Mise en place et démonstration du gabarit article                                             |
|                            | Réalisation des gabarits de page                                                 | Prototype navigable                                                                           |
|                            | Adaptabilité des contenus interécran                                             |                                                                                               |
| **De janvier à mars**      | Finalisation des gabarits de page                                                | Développement des gabarits de page                                                            |
|                            | Mise en place des règles typo, textuelles et graphiques pour les futurs éditeurs | Mise en place de l’interopérabilité interne                                                   |
|                            |                                                                                  | Affinage des modalités de consultation et de navigation                                       |