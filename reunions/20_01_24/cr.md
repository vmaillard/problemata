# Point Skype pour les "Agents"

*La plateforme et ses acteurs : gestion des contributions au travers des prismes éditoriaux, sociaux et techniques*

Avec : vm, jdb, cg, ml

## tableau des agents

- [Tableau mise à jour](../../plateforme/agents/agents.md)

**Administrateur**

- pas de page publique

=> pas de page pour chaque membre de l'équipe éditoriale : à insérer dans un *colophon*, un *à propos* plutôt  

**Mediateur**

- rajouter "membre d'une équipe Ligne"
- en v1, un médiateur demande validation avant la publication d'une ressource


**Pages personnelles**

- page personnelle: voir la recherche de quelqu'un: posture du chercheur qui cherche intéressante à voir
- WARNING: parfois, ajouter les informations des utilisateurs: ça devrait montrer que ça vit mais en fait ça fait son contraire
- exemple: le journaliste dans un site de journalisme
- contre-exemple: le réalisateur d'un film
- pages personnelles / pages publiques: la différenciation entre agent et utilisateur au cœur du débat (voir "Dimension recherche")
- gérer cette confusion, qui est souvent un problème dans ce type de projet: pas de solutions, que des choix
- exemple de radicalité: *Tous les agents sont des utilisateurs* : Roland Barthes est *un utilisateur qui ne se connecte jamais* : ça pourrait faire brouillon mais à la fois pas tant selon le degré d'assumer le choix

**Stratégie de gestion des ressources Agents et Agents utilisateurs**

- [1] tout Agent = un utilisateur qui peut ne jamais se connecter
- [2] possibilité de lier un Agent à un utilisateur


## Dimension recherche

- gestion des noms, des rôles et de la structure qui les déploie: dimension de recherche à part entière
	- des espaces d'édition modulables en fonction des rôles: administrateur != médiateur != auteur != lecteur
	- notion de rôle, de gouvernance, d'autorité, de responsabilité
	- des rôles et permissions attribuables en fonction des rôles parents: hiérarchie de rôles et modalité d'attribution
	- exemple: le bon à publier de l'éditeur dans le champ du livre imprimé
	- dimension exploratoire, de recherche du projet à considérer, notamment chez les initiateurs du projet (statut, posture, relativité des temporalités) **validé** 
	- développeurs: faire un inventaire des questions (liste) qui selon nous soulève des questions, et profite à cette dimension moins commande plus recherche  **validé: sert aussi à des documents réponses aux appels à projet (ex: lire voir l'appel de sciences ouvertes)**


## Appel à projet "Fonds national pour la science ouverte"

- date limite : 31 mars
- critères : dipo du code source de la plateforme + communauté autour des outils

=> formuler points et questions qui rentrent dans une pensée de la recherche (gestion des agents, écriture d'articles lié à un entrepôt sémantique)


## Page index / Workflow éditorial

page index: approche log (journal de bord) des objets  **validé** 

- TECH: montrer toutes les fiches omeka S renseignées = ignorer tous les facteurs «published» de django
- TECH: articuler la page autour du système de requête d'omeka S

**Statut d'article**

- C.G. évoque les "pre-papers" d'un article scientifique => une idée serait de pouvoir montrer les "entrailles" du site, le log de ce qu'il s'y passe sans pour autant que les articles ne soient publiés de manière définitive
- exposer les entrailles du site


## Parties / lignes

- nombre limité de lignes par an => pas de parties nécessaire avant qqs années


## Autres

- RAPPEL: l'ENS met en général 4 à 6 mois pour payer: workshop ENS Cachan d'octobre 2019
