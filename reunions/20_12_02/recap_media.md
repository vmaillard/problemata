# Traitement des images dans la plateforme Problemata

jdb, vm, 2020.12.02 (partagé durant la réunion)



## Définition

La base de ressources de Problemata comprend quatre objets éditoriaux : la "ligne", l'"article", l'"agent" et la "ressource". Un "article" fait partie d'une "ligne" et fait appel à des "agents" et à des "ressources".

Pour qu'un média soit décrit dans la base Problemata par un objet "ressource", il faut que le fichier soit stocké sur le serveur de Problemata. Pour des médias déjà stockés dans un fonds, cela occasionne un doublon.

Le traitement des médias se situe dans la relation entre les "articles" et les "ressources" :

- sont seules ajoutées à la base Problemata les médias convoqués dans le corps des articles
- dans l'objet "ressource" des propriétés rdf permettent le renseignement :
    - du nom et de l'url du fonds d'origine
    - de la cote de la ressource dans le fonds d'origine
    - de l'url de la ressource dans le fonds d'origine


Schéma général :

![](schema_general_obj.png)



## Stockage

Les fichiers médias sont stockés par Omeka S dans un dossier "files/".

Un objet "ressource" peut contenir plusieurs fichiers. À chaque média d'un objet "ressource", il est possible d'ajouter un titre par des propriétés rdf (ainsi que d'autres propriétés si besoin). À voir si le modèle de données Problemata est augmenté.

Le nom des fichiers importe peu dans le stockage avec Omeka S. Une nomenclature de fichiers s'adresse surtout aux chercheurs et aux lecteurs.

On pourrait fixer une largeur-hauteur maximales aux fichiers stockés : 800px ?



## Modèle de données

Pour chaque objet "ressource" :
- le fonds est conservé en url {tittle: X; url : X} dans la propriété dcterms:provenance
- la côte de la ressource dans le fonds d'origine est conservée en literal dans la propriété x:x (à définir)
- l'url de la ressource dans le fonds d'origine est conservé par une propriété de type "sameAs"


Modèle ressource :

- ajouts potentiels
    - côte (propre au fonds quand fonds existe et est connu)
    - `sameAs` (url vers le fonds en ligne lorsque numérisée sinon empty)
- langue (`dcterms:language`)
- titre (`dcterms:title` | LANG)
- type (`dcterms:type`)
- date de publication (`dcterms:dateSubmitted`)
- date de création (`dcterms:created`)
- légende (`dcterms:abstract` | LANG)
- médiateur*s* (`dcterms:mediator` | MULTI)
- fonds de provenance (`dcterms:provenance` | LANG) (Que faire si vide ? on a l'impression qu'il manque quelque chose ou que quelque chose a été omis / oublié si affiché)
- crédits & propriétés (`dcterms:rights` | LANG)
- droits d'usage (`dcterms:accessRights` | LANG) : les mêmes pour chaque ressource ?
- médias (o:media | LISTE ORDONNEE)
    - formats autorisés : jpg, png, gif (à verifier : svg, csv, mp3/ogg, mp4/webm, pdf)
    - taille maximale 
        - pdf : 1 facsimilé d'un ouvrage de 50 pages = 4Mo --> 50Mo
        - audio : ex : 1 podcast = 50Mo l'heure --> 100Mo
        - film : 1 film en avi téléchargé illégalement = 700Mo --> 1Go
        - image : 1Mo

Définitions de certaines propriétés :

- `dcterms:title` : Titre avec des précisions sur le créateur de la ressource
- `dcterms:abstract` : Description développée (contenu, contexte historique, rôles, format/medium)



## Affichage

Une "ressource" est consultable / accessible dans les pages articles.

À même la base, il n'est pas possible de faire un regroupement de ressources (collection). Mais il est possible de renseigner plusieurs ressources dans la propriété `dcterms:relation` d'un article. Il est également possible de convoquer plusieurs ressources dans le corps du texte d'un article (dépend des fonctionnalités de l'outil d'écriture et des pseudo-codes disponibles).

Une "ressource" est consultable / accessible en dehors des pages articles, et ce par la page detail d'une ressource.

Le clic droit sur les médias n'est pas désactivé. Il est possible d'afficher un menu contextuel au clic droit pour rappeler les conditions d'utilisation. Ou bien de simplement considérer que les droits d'usage sont présents dans les métadonnées de la ressource + que la page des conditions générales d'utilisation suffisent.




## Partenariats et coût des droits d'image

- Bibliothèque Kandinsky du centre Pompidou : accord spécifique entre les 2 parties
- Musée national des Arts Décoratifs : accord spécifique entre les 2 parties

- De manière générale : [barème de l'ADAGP](https://www.adagp.fr/sites/default/files/bareme_adagp.pdf) :
    - p.44 = SITES SANS RECETTES D’ORGANISMES A BUT NON LUCRATIF = ce qu'est problemata
    - *Contenus culturels et éducatifs*
        - mi 2020 = v0 = De 101 à 200 oeuvres = 179€/mois (forfait) : version BETA (soit 1 ligne CCI) ?
        - mi 2021 = v1 = De 501 à 1 000 = 436€/mois (forfait) : version 1.0 (soit 5 lignes) ?
    - sur la p.45, on passe d'archivage à base de données massives à partir de 100 œuvres, aussi les tarifs sont plus intéressants que lorsque l'on porte le statut de Contenus culturels et éducatifs



## Questions 


*Questions du fonds*

Est-ce que dire "fonds Bibliothèque Kandinsky" est pertinent ?

Surtout si les fonds sont divisés en fonds ou disposent de leur propre hiérarchie dont Problemata n'a pas spécialement besoin puisqu'il se sert d'une unité de base ("ressource") liée à un "article".

Est-ce que le seul champ "Fonds" (dcterms:provenance) suffit-il pour lier une ressource à son fonds ? (relations éditeur, auteur, fonds institutionnels)


*Question de la côte*

Est-ce qu'on a besoin de la stocker si on a les url vers le fonds + la ressource dans le fond ? Est-ce qu'il peut y avoir une confusion avec des ids Problemata et des côtes externes ?


*Question des droits d'usage*

Est-ce que les droits d'usage sont les mêmes partout dans Problemata ? 

Dans le modèle de données, on a `dcterms:accessRights`, mais doit-on les conserver sur chaque ressource ?


*Question sur les urls*

Les urls de nos bases partenaires sont-elles pérennes ? Chez la bibliothèque Kandinsky ?

Exemples à la bibliothèque Kandinsky (PLEADE) :

- url de la ressource numérique : https://archivesetdocumentation.centrepompidou.fr/ead.html?id=FRM5050-X0031_0000074&c=FRM5050-X0031_0000074_FRM5050-X003190810#!{%22content%22:[%22FRM5050-X0031_0000074_FRM5050-X003190814%22,false,%22%22]}
- url via le viewer : https://archivesetdocumentation.centrepompidou.fr/img-viewer/BK/MUS/MUS_197701/viewer.html?name=M5050_X0031_MUS_19770101_001_P.JPG&ns=M5050_X0031_MUS_19770101_001_P.JPG
- url vers l'image source : https://archivesetdocumentation.centrepompidou.fr/img-server/BK/MUS/MUS_197701/M5050_X0031_MUS_19770101_001_P.JPG?r=0


*Question des futurs besoins*

Quels besoins pour le print si un objet print / si une publication existe à un moment (ultérieure) ? Cela est lié à la taille des fichiers sources. S'ils sont fortement réduits (800px), cela aura un impact sur la qualité d'un export .pdf.


## Annexes

Existe-t-il des images indépendantes de l'objet éditorial nommé *ressource* ?

La question du partage ou de la copie, qui conserverait ou respecterait au max les crédits et la provenance des médias

Quid des images qui ne proviennent d'aucun fond ? [*amateur* ?]
    - Exemple : un chercheur (qui a une pratique de designer) prend en photo une des ses réalisations, lors d'un temps de workshop, pour imager une partie de son article traitant de l'usage du verre dans le design de carrosserie automobile.
    - Conséquence = cela fait monter au grade de ressources documentaires les travaux du chercheur ?
