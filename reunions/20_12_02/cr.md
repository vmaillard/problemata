# CR - Traitement des images dans la plateforme Problemata


Avec : Karine Bomel, Sonia Descamps, Laurianne Nehlig, Catherine Geel, Jérémy De Barros, Vincent Maillard

Le 20.12.02 par visio



## Récap général

Voir notamment ce [document récapitulatif](recap_media.md) (partagé durant la réunion).

- deux cas possibles :
    - médias provenant d'institutions partenaires (BK, MAD, MAMC Saint-Étienne)
    - médias provenant d'autres institutions, banques d’images, particuliers, artiste, auteur de la ligne mais aussi scan dans un ouvrage, revue…
- ligne CCI : 20 articles pour ≈ 150 images



## Question du fonds

*Solutions envisagées :*
- `dcterms:provenance` : renseigne `{title: X; url: X}` du fonds de la ressource
- `dcterms:publisher` (ou autre) : renseigne `{title: X; url: X}` de l'institution gérant le fonds
- `sameAs` : mis de côté pour l'instant : BK + MAD : les urls des ressources ne sont pas pérennes

Sur Problemata, les liens vers les fonds d'origines ont pour vocation de stimuler la recherche, quitte dans ce cas à renvoyer à la racine du fonds, pas nécessairement la ressource particulière.

*Point à avoir en tête :*
- `dcterms:provenance` : possible de concaténer fonds/institution, voir multiplier les entrées



## Question de la cote

- il est important de conserver la cote dans les métadonnées
- à la BK et au MAD les fichiers ont une norme de nommage
    - "M5050_X0031_MUS_19770101_001_P.JPG" contient : code Centre Pompidou, code BK, cote de la ressource, n° d’article ou de vue
    - il est donc important de conserver le nom fourni par l'institution lors du stockage + dans les métadonnées

*À faire :*
- trouver une propriété rdf adaptée pour la cote
- prévoir une propriété rdf sur les médias : `dcterms:title`, par exemple, pour stocker le nom du fichier



## Question droits / droits d'usage

- Crédits / `dcterms:rights` : contenu spécifique à chaque ressource (suivant les créateurs impliqués)
- Droits d'usage / `dcterms:accessRights` : contenu idéalement stable à l'échelle de la plateforme (mais répété pour chaque ressource)

*À faire :*
- définir le texte "Droits d'usage" qui figurera dans les métadonnées de toutes les ressources



## Stockage, qualité des images

- la BK est ok pour que des doublons de fichiers soient stockés par la plateforme Problamata
- les fichiers qui peuvent être fournies sont les mêmes que celles dispo sur http://bibliothequekandinsky.centrepompidou.fr/
    - dimensions : autour de 1080-800px. Pour les vues d'expo du CCI beaucoup ont pour dimension : ≈1500×1080px
    - définition : 72dpi, certaines à 300dpi
    - pour des documents textuels qui ne seraient pas lisibles à cette qualité, il est possible d'adresser des demandes pour obtenir une meilleure qualité



## Protocole pour l'ajout des ressources et la fourniture des fichiers

- pour l'instant :
    - des workshops écriture
    - des imports automatisés par les développeurs
- à terme :
    - les chercheurs ont de l'autonomie :
        - ils prennent en charge les contacts avec les ayants-droits
        - ils contactent les fonds et institutions disposant des archives
        - ils renseignent les métadonnées et les objets éditoriaux liés à leur recherche

*Pour la liste CCI :*

Une fois que la liste des ressources souhaitées par Problemata est envoyée, la BK fournit les fichiers accompagnés d'un fichier .csv. Ce qui permettra un import automatisé dans la base Omeka S de Problemata.

Il faut distinguer ce qui relève soit du fonds BK soit des archives du Centre Pompidou.

*À faire :*
- établir l'alignement et les concaténations entre les propriétés de la BK et celles de Problemata



## Question des images qui ne sont pas dans des fonds

Dans ces cas-là il faut :
- des normes de nommage des fichiers
- dans `dcterms:provenance` mettre le nom du chercheur
- ce sont donc bien des ressources en tant que telles



## Prochaine réunion avec Nicolas Lucci-Goutnikov

Il faut lister les images dans la ligne CCI et indiquer :
- Provenance : institution (Centre Pompidou, Arts décoratifs), banque d'image, un particulier etc.
- Service : Bibliothèque Kandinsky, archives du Centre, Bibliothèque et ressources documentaire, collections etc.
- Fonds : CCI, Sottsass, archives UCAD/CCI…
- Nature de ce qui est représenté : photographie, archives écrites etc.
- Légendes, auteur
- Nature du fichier : scan rapide, copie fonds CCI-BK
- Nom du fichier
- Communicabilité de l’image : droits afférents : ADAGP, ayant-droit à rechercher
- Crédit, copyright



## Ressources

- [Rapport *Images/Usages : droit des images, Histoire de l'art et société*](https://www.inha.fr/fr/recherche/le-departement-des-etudes-et-de-la-recherche/domaines-de-recherche/programmes-en-cours/images-usages.html)
- [Benjamin Jean, juriste spécialisé en propriété intellectuelle](https://inno3.fr/equipe/benjamin-jean)
- [Cahiers du CNAM](http://bibliothequekandinsky.centrepompidou.fr/clientBookline/service/reference.asp?INSTANCE=incipio&OUTPUT=PORTAL&DOCID=0468551&DOCBASE=CGPP)
- [Barème de l'ADAGP](https://www.adagp.fr/sites/default/files/bareme_adagp.pdf)