# Questions / avec Jérémy

Question :
- Comment faire pour afficher un article avec plusieurs auteurs ?
- Est-ce qu'un même article peut être affilié à plusieurs lignes ?
- Quel est le scénario à suivre si…
	- j'ai cliqué sur l'auteur de l'article, le tableau est affiché (filtré par le nom de l'auteur correspondant) et je clique sur "Type" ? 
	- j'ai cliqué sur le titre de l'article, le tableau est affiché (trié par ordre alphabétique des titres d'article) et je clique sur "Auteur" ?
	- j'ai cliqué sur le titre de l'article, le tableau est affiché (trié par ordre alphabétique des titres d'article), j'ai scrollé dans la page et je filtre par ordre antéchronologique ? l'article que je lis s'affiche-t-il de nouveau en haut de l'écran ?
	- j'ai cliqué sur l'auteur de l'article, le tableau est affiché et je clique sur "Communication" (le type du second article affiché) ?
	- j'ai cliqué sur la date de mise en ligne et j'ai scrollé dans le tableau, je clique sur le chevron ^ pour trier par ordre anté-chronologique ? la ligne de l'article que je suis en train de lire s'affiche-t-il de nouveau en haut de l'écran ? 
- De manière générale, comment revenir une fois le tableau filtré par le nom d'un Auteur ou d'un Type à un tableau listant l'entièreté des articles ?
- Quelle est la gestion responsive du tableau ?
- Hormis le tableau, est-ce qu'il existe sur le site une page par Ligne ?
- Dans la réunion au MAD, nous avons convenu de ne pas créer de ressources auteurs dans Omeka. Ce qui veut dire qu'il n'y aura pas d'endroit sur Omeka pour ajouter une biographie. Faut-il tout de même prévoir un champ biographie qq part comme l'indique le document ?
- Quel est l'ordre de tri actif si deux articles ont la même date de mise en ligne / ou de création ?
- Est-ce que le tableau existe sur une page à part entière ?

Ce qui pour nous crée de la confusion, c'est l'usage simultané du tri et du filtre par le tableau. 
Par exemple, le site east of borneo présente les choses de manière plus lisible : on filtre par une entrée choisie (tous les types, ou un en particulier) et on trie par un autre moyen (date de mise en ligne, ou ordre alphabetique). Le lecteur construit une phrase de recherche avec des aides visuels pour savoir s'il a trié par date ou s'il a filtré par type (ou bien s'il n'y a aucun filtre).
A l'inverse le tableau du document mélange (les réponses à nos questions nous aiderons à être plus précis ici) les deux modalités suivant l'origine du clic. A partir d'un article, nous sommes d'accord qu'il est intéressant de pouvoir afficher tous les autres articles de cet auteur, la place de cet article dans une ligne. Mais que ce même tableau puisse également afficher l'ensemble des articles du site si le lecteur clique sur le titre sans qu'il y ait d'indice visuel ou de choix clairement voulu, modifie la valeur du tableau.
Pour nous, il faudrait dissocier les modalités de tri des modalités de filtre. Le tableau est un bon outil pour trier (date, alpahabet) mais pour y ajouter du filtre, il serait intéressant que cela ne se fasse pas par un simple clic mais par une sélection de paramètres. Si un lecteur a besoin d'avoir une fourchette [-5;+5] pour afficher une liste de date de création, il ne peut pas le choisir car le tableau le contraint à [-10;+10].
Ces modalités de filtre pourrait également être présenté hors du tableau : à la fin d'un article
Concernant la colonne date de création, il faudrait faire des tests avec les articles dont nous disposons. Pour un certain nombre, ils vont avoir la même date de mise en ligne. Il y aura




