Réunion chercheurs 👀

En visioconférence

28.04.2020 ✿ 9:00 – 15:00

____________________

Outils

Formulaire

    Le formulaire est accessible ici : http://problemata.huma-num.fr/


Discord

    La discussion discord est accessible ici : https://discord.gg/TY8YPGD


Dropbox

    La dropbox où sont mis à disposition les contenus servant de matériau à la journée de travail est accessible ici : https://www.dropbox.com/sh/6oijugqcicck1lx/AAB1rJjHh9aer8cN_PjEWJ1Oa?dl=0


____________________

Retours

L'objet de ce pad est de recueillir les retours, les notes et remarques des chercheurs lors du renseignement de données dans le formulaire.
Pour être en mesure d'échanger sur ces retours, l'idéal serait de se conformer à un protocole comme celui-ci :


#1 : Créer, renseigner, enregistrer un article

Article référent

Archéologie de la ville (1977 ) et l'ouverture du centre national d'art et de culture Georges Pompidou : collaboration spectaculaire et péripéties scénographiques pour une ambition inaugurale
Marie Le Menes

---

    Nom

Gilles

    Objet éditorial

général

    Numéro/Titre de la question du formulaire :

# médiateur, auteur(s), contributeur(s)

    Retour

pb de séparation des virgules

---
Karine Bomel
article
# champs médiateur, auteur(s), contributeur(s)
Séparateurs multi-auteurs : éviter la virgule.

---

Estelle
champs Résumé / table des matières
Lorsqu'on copie-colle un paragraphe, il crée des retours à la ligne mal maitrisés : c'est long et ennuyeux à corriger
Même pour un article en FR, il serait intéressant de pouvoir renseigner un abstract en EN ou autres langues

---

Estelle
mots clés
si plus de 4, comment choisir ? (augmenter à 5 serait bien pour s'aligner avec les standards d'autres publi)

---

Estelle
champ personnes citées
Ce champ m'est peu clair : est-ce les personnes physiques/morales qui font l'objet de l'article ? Ou les citations bibliographiques (dans ce cas, immense à remplir !)

---

    Nom

Claire

    Objet éditorial

Article

    Numéro/Titre de la question du formulaire :

4. auteurs

    Retour

Problème pour les valeurs multiples : oubli de cliquer sur "ajouter" lorsqu'on a une seule valeur.
---

Claire Brunet
article
# champs <field.multi>
ne pas cliquer sur ajouter dans les champs auteurs et couverture temporelle lorsqu'il y a en qu'un => champ vide
et ne pas avoir cliquer sur enregistrer => ne pas voir son article

---

    Nom

Claire

    Objet éditorial

Article

    Numéro/Titre de la question du formulaire :

    Retour

Est-ce qu'il faut être très précis ou concis/synthétique ?
---

Jérôme
article
# champs <textarea>
Ajout de texte dans le Résumé (ou autres entrées)
Comment souligner et mettre en italique certains des mots et expressions directement dans le texte entré? 

---

?
article
#0 : général
très simple
bien, intuitif, parce que l'habitude (notamment wordpress)

---

?
article
les sujets contrôlés apparaissent comme des chiffres 
ambiguïté data de création de l'article / date de soumission de l'entrée

---

Karine Bomel ?
article 
#22 ressources liées
entrée null => pas de ressources renseignées au préalable

---

    Nom

Simone

    Objet éditorial

Article

    Numéro/Titre de la question du formulaire :

12. Personnes physiques ou morales

    Retour

intéressant de référencer des personnes référencées dans le texte
MAIS formatage des nom, prénom inversé par rapport aux fichiers originaux prénom nom (copier-coller pas pratique)


---

    Nom

Estelle

    Objet éditorial

Article

    Numéro/Titre de la question du formulaire :

12. Personnes physiques ou morales

    Retour

Jusqu'où aller : vraiment toutes les personnes citées ou seulement les cas étudiés ?
---


---

    Objet éditorial

Article

    Numéro/Titre de la question du formulaire :

Auteurs
12. Personnes physiques ou morales

    Retour

se dire qu'on écrit : "Prénom, Nom" plutôt que "Nom, prénom" ?
A voir avec ce qui est compatible avec 
---



---

    Nom

Jérôme

    Objet éditorial

Article

    Numéro/Titre de la question du formulaire :

13 et 14. Couverture temporelle / géographique

    Retour

"1977" ou "années 1970" ? Précis ou période ?
A voir si dans le tableau on ajoute cette colonne dans le tableau. => filtre de la couverture temporelle d'un article (en plus de la date de création et date de publication sur Problemata)
=> vocabulaire contrôlé pour les lieux
---

?
article
# couverture géographique
jusqu'où aller : si il y a des autrichiens qui travaillent à NY => NewYork, Autriche ?

---

Karine Bomel
article
# couverture géographique
Si deux villes portent le même nom 'Paris FR', 'Paris USA (Texas)'
faut-il les ajouter / saisir depuis une liste existante ?
suggérer la ville dans les formulaires (suggestion) => unité dans les réponses
préciser le pays par un séparateur + un pays ? => Paris, FR ou FR est suggéré ?

---

Gilles : Je note la diversité de nos compréhensions de Titre / Sous-titre…
# général (page de listing : articles)
En mode "Parcourir les articles", l'auteur et l'année en font-ils pas défaut ?

---

Annick Lantenois
article
# 17, 18, 19, 20
si un article a déjà été publié en ligne, comment renseigner l'url pour y accéder sous forme de lien ?
AUTRE QUESTION amenée par Vincent => dans quel champ renseigner ce type de publication ?

---

Natalia Klanchar
#18 -20
Si dans la couverture géographique nous renseignons le pays, il faudrait peut-être renseigner le pays, et pas la ville sur ces 4 champs

--- Claire
Indiquer que la notice a été rédigée de manière synthétique, sans la volonté d'exhaustivité ?

--- Gilles
Assumer que la base est actuelle, en fonction de ce qui a pu être rempli, "faire comme on peut". Assumer le côté collaboratif pour augmenter la base.

--- Jérôme
Est-ce que c'est le médiateur de la ligne qui se charge d'homogénéiser le renseignement des métadonnées

---
Mica 
Pour des mécanismes de vérifications

--- Vincent
Mettre des questions en prioritaires ?

Question de l'origine de textes ds protocoles d présentation au comité sc; à voir.


#2 : Créer, renseigner, enregistrer une ressource


Article référent

Archéologie de la ville (1977 ) et l'ouverture du centre national d'art et de culture Georges Pompidou : collaboration spectaculaire et péripéties scénographiques pour une ambition inaugurale
Marie Le Menes

Attributions

    fig. 1 Annick

    fig. 2 Catherine Geel

    fig. 3 Claire

    fig. 4 Estelle

    fig. 5 Gilles

    Type de ressource, ne manque-t-il pas "collage", "photomontage"…

    Pour la fig. 5, est indiqué : 

    "dessin, encre sur impression offset, collage” 

    Date de création, il y a 2 dates : c'est possible ?

    Le format (28,2x35 cm) ne justifie-t-il pas d’un formulaire particulier ?

    Le terme de "ressource" n'est-il pas la source de l'ambiguïté ? 

    en identifier 2 ≠ : Iconographie / Œuvre représentée ?

    fig. 6 Jérome

    Problème du titre : qui est l'auteur du titre quand il s'agit d'une vue d'expo. Peut-être préciser qui est l'auteur de ce titre (médiateur, créateur ressource, article). Je pense à un cas particulier quand la même image (représentée) à deux titres différents [exemple: sculpture de Giacometti avec un titre / photo de Man Ray de la sculpture de Giacometti dont le titre est différent de l'oeuvre originale]

    fig. 7 Karine

    fig. 8 Morgane

    fig. 9 Lola

    fig. 10 Marie

    Rendre plus clair la formulation de l'url

    fig. 11 Mica : possibilité de créer des liens/renvois à l'intérieur des ressources vers les articles - source?

    fig.1 Natalia

    fig. 2 Sandra

    fig. 3 Simone


--- Simone
Qu'est ce qui à terme sur l'article quand on parle d'une ressource ? le créateur, la date, la description ?


liens vers la source : directement sur le site source ou vers le gisement spécifique d'images? (Mica)
doit-elle être générique ou alors spécifique ?

Gilles 
Question plus générale, je peux "Voir", Modifier” et surtout "Supprimer" tous les articles, même ceux que je n'ai pas créés… Se pose la question de droits restreints ? 

--- Jérôme
Question sur l'auteur de l'image, l'auteur de ce qui est représenté, l'éditeur de l'objet représenté.
=> différences de crans qui sont confuses dans un article de design => il y aurait alors plusieurs titres possibles

--- Karine
auteur de l'objet représenté
auteur de la ressource

--- Catherine et Marie
Notice perfectible à l'infini

--- Sandra
comment cataloguer en tenant compte du nom de photographe, de l'éditeur du designer ?

--- Gilles 
distinguer dans la ressource : icono et œuvre
Quitte à ne plus parler d'une ressource

--- Sandra
avoir des questions incitatives pour séquencer le renseignement :
    - qui est l'auteur du document ?
    - qui est l'auteur de l'objet représenté ?
    - qui est l'éditeur de l'objet représenté ?
    - qui est le designer du livre ?
    - quel est le support ?
    - quels sont les auteurs impliqués dans la production ?
    
--- Gilles : Si j'ai compris, il s'agirait de "réunir" un ensemble d'images comme un "album final ?" 
Ne peut-on imaginer que la description ne soit pas générique, mais celle de l'article ?

Catherine et Jérome
légende / description ou titre propre à chaque appel de ressource plutôt que d'un ou d'une unique par ressource ?
légende charnue qui organise les choses d'une certaine manière (orientation de celui qui crée ou saisit la ressource)
Peux-t-on versionner les descriptions d'une ressource ?


Karine
ce serait le best de pouvoir récupérer toutes les informations (les orientations d'une légende) (lorsque que quelqu'un s'empare d'un truc, la légende s'enrichit)

Jérome
Une dimension de la base serait “factuelle”, et une autre plus spécifique, liée à l'article "interprétative"

Mica
Une légende technique des ressources, plutôt neutre, mais détaillée, dense.

Jérémy
Renversement de situation sur la légende proposé par Jérémy : le description "personnalisée" ou "orienté" de l'auteur se retrouverait bien dans l'article lui-même et les métadonnées recense bien les articles, charge au lecteur si la ressource l'intéresse d'aller consulter ...


#3 : Saisie d'une ligne complète avec ses articles et ses ressources

début 13h50
fin 

groupes


    GR1 : CCI

    Keren

    Karine

    Lola

    Est-ce qu'on pourrait ajouter les ressources directement depuis l'entrée 22 du module article?

    Natalia


    GR2 : MORRIS

    Jérome

    Sandra

    Catherine


    GR3 : ALLEMAGNE POST TRAUMATIQUE

    Claire

    Simone

    Marie


Possibilité / Pertinence de lier 2 articles ensemble : par exemple pour Adorno (ligne Allemagne post traumatique) : lier le propos liminaire de Catherine au texte d'Adorno, ou le commentaire de Claire au même texte d'Adorno ? 


Retours


Claire
des problèmes avec la fonction d'enregistrement (bouton enregistrer)

Jérome
ambiguïté entre description / abstract / résumé (court)
précision à apporter dans les définitions

personnes morales / physiques citées = faire des choix ou alors ça peut être un peu long
textes qui sont des traduction = quelle est réellement la publication originale ?
format conférence = problème de la source, dans le document Morris

multi (arr:array) : liste déroulante pas confortable
ressource : visualiser les images tout de suite


difficulté dans la saisie des légende - descriptif / titre  d'une ressource
en parallèle de l'écriture d'un article ou alors dedans : pouvoir créer une ressource à la volée

possibilité d'ajouter une information contextuelle d'une ressource

modèle article :
lier des textes entre eux au sein même des textes
La question descriptif & abstract
comme "ressource(s) liée(s)" une entrée "article(s) lié(s)"

Catherine, Karine
Ressources : indiquer le n° d'inventaire au même endroit que le fonds (Point 9)

Catherine et Sandra
le fait de ne pas pouvoir remplir des champs poussent à les remplir, à se creuser pour

Laurence
sans explications du matin, pas senti perdue, assez intuitif, élémentaire
dissociation entre prénom nom

Simone
doublon dans les entrées de l'objet ressource


