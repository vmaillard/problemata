# Journée d'étude sur la saisie des métadonnées
**Compte rendu du 28.04.2020**

*Contexte*

Dans le cadre du développement de la plateforme *Problemata*, nous avons
souhaité réunir les chercheurs pour pratiquer le modèle de données, un
formulaire pour le renseignement de métadonnées en vue de les impliquer
dans le travail en cours et de recueillir leurs avis.

*Intervenants*

- Catherine Geel, éditrice
- Marie Lejault, éditrice
- Vincent Maillard, développeur
- Jérémy De Barros, développeur

*Participants*

- Estelle Berger, Strates école de design
- Karine Bomel, MAD
- Claire Brunet, ENS Paris Saclay
- Lola Carrel, ENS Paris Saclay
- Sandra Cattini, CNAP
- Karen Detton, FRAC Frand Large -- Hauts-de-France
- Simone Fehlinger, Cité du design, Saint-Étienne
- Mica Ghergescu, Bibliothèque Kandinsky
- Natalia Klanchar, Bibliothèque Kandinsky
- Annick Lantenois, ESAD •Valence
- Laurence Mauderli, ESAD Reims
- Laurianne Nehlig, Bibliothèque Kandinsky, MNAM
- Jérôme Pasquet, Paris-Nanterre
- Gilles Rouffineau, ESAD •Valence

## Objectifs

Il s\'agissait ici de mesurer le travail à fournir, le degré
d\'autonomie, de complexité d\'un outil, par le chercheur dans
l\'exercice de saisie des différents objets éditoriaux (articles,
ressources pour commencer). Nous nous sommes donc intéresser aux retours
portant sur la clarté, la pertinence de l'outil et des questions ainsi
qu'aux durées nécessaires pour le renseignement des métadonnées.

Il s\'agissait aussi de familiariser le chercheur avec l\'usage de
vocabulaires contrôlés (ici DublinCore, FOAF et BiBo), *via* une
première approche par la pratique, ces vocabulaires étant alors
présentés sous une forme transcrite, lisible et interprétable (titre,
question, description, format).

L'objet éditorial « agent » a été volontairement mis de côté lors de
cette journée afin de diminuer le nombre d'items à créer, simplifier la
structure des relations et se concentrer sur l'articulation
article-ressource.

## Déroulement

• La journée a débuté par une présentation des éditrices sur les
objectifs de la journée, suivie d\'une présentation / démonstration de
l\'outil par les développeurs.

• La première étape de travail intitulée *\#1 : Créer, renseigner,
enregistrer un article* invitait les participants à renseigner sous
forme de fiche de métadonnées un même article pour mettre en lumière les
ambiguïtés, les imprécisions que pouvait contenir le modèle, et
notamment lors de traduction en titre, question, description et format
des différents champs empruntés à des standards sémantiques (suivant
cette approche, le champ `dcterms:alternative` s\'affichera sous la
dénomination *sous-titre*). Par ailleurs, les participants travaillant
tous sur le même article, il était question d\'avoir un aperçu de
possibles manières d\'aborder un contenu au travers d\'un outil.

• La deuxième étape intitulée *\#2 : Créer, renseigner, enregistrer une
ressource* s'est construite dans le prolongement de la première. Chaque
participant (à trois exceptions près) s'est saisi d'une ressource en
lien avec l'article précédemment décrit. Là aussi le modèle éditorial a
été sujet à discussion, notamment la propriété `dcterms:abstract`  pour
parvenir à faire la différence entre le document décrit de ce qu'il
représente.

• La troisième et dernière étape de travail intitulée *\#3 : Saisie
d\'une ligne complète avec ses articles et ses ressources*, invitait
les participants à former des groupes en vue de saisir des objets plus
conséquents, à la fois en nombre (renseignement des ressources
rattachées aux articles) et en relations (constitution de lignes de
recherches).

## Mise en place

• un outil de saisie a été développé pour l\'occasion, permettant la
constitution d\'une base de données relationnelles en conditions plus ou
moins propres à la future plateforme
([http://problemata.huma-num.fr/index.php](http://problemata.huma-num.fr/index.php)).
Cet outil dispose par ailleurs d\'un script de chronométrage qui permet
l\'archivage des activités de saisie ou de modification d\'objets
([http://problemata.huma-num.fr/chrono.php](http://problemata.huma-num.fr/chrono.php)).
Cet outil recensant les temps de saisie, type d\'objets, médiateurs et
identifiants d\'objets, nous permet d\'avoir des évaluations plus
statistiques d\'un tel outil.

• un pad a été mis en place pour que les participants chercheurs
puissent faire leurs retours en direct, au fil des différentes étapes de
travail
([https://mypads2.framapad.org/p/problemata-form-28-04-2020-wadn691b](https://mypads2.framapad.org/p/problemata-form-28-04-2020-wadn691b)).

• le corpus de base pour cette journée de travail, constitué d\'éléments
qui seront publiés sur la plateforme (potentiellement des lignes, des
articles et des ressources), était accessible via une dropbox
([https://www.dropbox.com/sh/6oijugqcicck1lx/AAB1rJjHh9aer8cN\_PjEWJ1Oa?dl=0](https://www.dropbox.com/sh/6oijugqcicck1lx/AAB1rJjHh9aer8cN_PjEWJ1Oa?dl=0)).

• un canal discord nommé *\#vade-mecum* a été utilisé pour communiquer
toute la journée. Des canaux plus spécialisés ont été créé à la volée
pour la rédaction en équipe des objets *lignes de recherche* (un canal
par ligne de recherche, composée de 4 chercheurs).

---

## Retours généraux

• les chercheurs étaient plutôt contents de saisir les enjeux soulevés
par l\'écriture des métadonnées et leurs usages ultérieurs

• des discussions de fond ont été abordées, sur l\'usage d\'un nom ou
d\'une formulation et de leurs significations chez le chercheur des
différentes entrées à saisir, qui seront détaillés ci-dessous

## Retours de cette journée

→ Outil d\'écriture : ergonomie et expérience de la saisie

• au renseignement du champ `dcterms:relation`, lors de la saisie d\'un
article, être en mesure de pouvoir créer « à la volée » une ressource
plutôt que de contraindre l\'utilisateur à un protocole de saisie
linéaire (saisir les ressources puis saisir l\'article qui appelle les
ressources). Nous imaginons donc qu\'il soit possible de créer des
objets de différents types en parallèle les uns des autres (asynchronie,
caractère dynamique d\'un formulaire)

• au renseignement du champ files d\'une ressource, il serait bien de
pouvoir visualiser l\'image tout de suite, sans avoir procédé à son
téléversement dans un dossier cible sur le serveur

• les entrées pouvant être multiples (listes de valeurs plutôt que
valeur unique) disposaient d\'un bouton *ajouter*, qui souvent n\'a pas
été cliqué, lorsqu\'il y n\'y avait qu\'une seule donnée à renseigner.
En exemple : contributeur(s) : *personne 1*, le bouton *ajouter* n\'est
pas cliqué parce que la saisie de *personne 1* donnent l\'illusion
d\'avoir complété le champ par une saisie textuelle)

• les entrées en liste dépliante, que le choix soit multiple ou unique,
sont pénibles à saisir : un champ de saisie textuel pour réduire /
filtrer les résultats sélectionnables serait plus confortable

• dans le cas d'un champ multi-critères, ne pas utiliser la virgule
comme séparateur, même pour l'affichage (confusion à l'affichage
d'auteurs multiples)

• les pages index (liste d'articles, de lignes, de ressources) devraient
proposer plus de colonnes. Ces pages pourraient reproduire le
fonctionnement du tableau du site de la plateforme.

• pour une relation vers un autre objet éditorial, les pages de vue
doivent affichées le titre de l'iobjet et non son identifiant

• l'outil d'écriture doit proposer un champ de type « url » (deux champs
combinés : le texte à afficher et l'url)

• l'outil d'écriture doit intégrer des persmissions pour limiter les
droits à des ressources qu'une personne n'aurait pas créé, ou bien
limiter les droits à la création et la modification de ressources
seulement si la personne n'est pas responsable d'une ligne

→ Spécifiques aux modèles éditoriaux

### Article

◌ Propositions

▸ article `dcterms:subject` : augmenter de 4 à 5 mot-clés serait bien pour
s\'aligner avec les standards d\'autres publications

▸ article `dcterms:subject` et `dcterms:abstract`  : des précisions sont à
apporter dans les définitions de ces deux entrées pour éviter toute
confusion ou ambiguïté

▸ article `dcterms:references` : le formatage des noms (prénom inversé par
rapport aux fichiers originaux ou figure le prénom puis le nom) ne
permet pas le copier-coller : inverser au sein de l\'outil pour
permettre ce geste

▸ article `dcterms:title` et `dcterms:alternative` : diversité des
compréhensions et du coup du découpage du titre et du sous-titre dans la
transcription en métadonnées des articles du corpus

▸ lier des textes entre eux au sein même des textes : comme il existe
une entrée ressource(s) liée(s), renseigner une entrée article(s) lié(s)

▸ Possibilité et pertinence de lier deux articles ensemble : par
exemple, nous avions ici rencontré le cas pour Adorno (ligne *Allemagne
post-traumatique*) où il était question de manière intentionnelle, avant
la transcription de la ligne dans l\'outil, de lier le propos liminaire
de Catherine Geel au texte d\'Adorno, ou bien encore de lier le
commentaire de Claire Brunet à ce même texte d\'Adorno.

◌ Questions

▸ article › `dcterms:references` : est-ce les personnes physiques/morales
qui font l\'objet de l\'article ? Ou bien les références inscrites dans
la bibliographie ?

▸ article › `dcterms:references` : jusqu\'où aller ? Vraiment toutes les
personnes citées ou seulement les cas étudiés au fil de l\'article ?
Faut être très précis ou au contraire concis/synthétique ?

▸ article › `dcterms:created` et `dcterms:submitted` : ambiguïté entre ces
deux champs pour le chercheur.

▸ article › `dcterms:temporal` : faut-il renseigner *1977* ou *années
1970* ? Quel est le degré de précision à respecter dans le renseignement
de cette entrée ?

▸ article › `dcterms:spatial` : faut-il un vocabulaire contrôlé pour les
lieux ?

▸ article › `dcterms:spatial` : jusqu\'où aller : s\'il existe un cadre ou
des autrichiens travaillent à NY, doit-on saisir *NewYork*, *Autriche* ?

▸ article › `dcterms:spatial` : Si deux villes portent le même nom *Paris
FR*, *Paris USA (Texas)* faut-il les ajouter ou saisir depuis une liste
existante ? Suggérer la ville dans les formulaires (suggestion)
(exemple : saisir Paris, et FR et US sont suggérés) ? Ou alors
constituer une unité dans les réponses en précisant le pays par un
séparateur + code du pays ? (exemple : Paris, FR)

▸ article › `dcterms:spatial` : Si dans la couverture géographique nous
renseignons le pays, il faudrait peut-être renseigner le pays

▸ article › `dcterms:isFormatOf` : s\'il existe des textes qui sont des
traductions, quelle est réellement la publication originale ?

### Ressource

◌ Propositions

▸ ressource `dcterms:type` : ne manque-t-il pas « collage »,
« photomontage », où d\'autres possibles ?

▸ ressource `dcterms:created` : Date de création il y a 2 dates : c\'est
possible ?

▸ ressource `dcterms:provenance` : indiquer le n° d\'inventaire au même
endroit que le fonds (ressource : entrée \[9\])

▸ ressource : Le format (28,2x35 cm) ne justifie-t-il pas d'un
formulaire particulier ?

▸ ressource `dcterms:abstract` :peux-t-on envisager deux dimensions pour
les descriptions : une factuelle (neutre, détaillée, dense) l\'autre
plus interprétative

▸ est-il possible de versionner les descriptions : pouvoir renseigner
plusieurs descriptions pour une même ressource

▸ est-il possible d\'envisager une description contextuelle à l\'appel
de ressource, au sein de l\'article : la ressource disposerai d\'une
description succincte et neutre alors consultée sur sa propre page, et
d\'une description arbitraire, propre à son utilisation au fil de
l\'écriture d\'un article

◌ Questions

▸ Le terme de *ressource* n\'est-il pas la source de l\'ambiguïté entre
le document et l\'objet représenté ?

en identifier 2 ≠ : ▸ Iconographie / Œuvre représentée ?

▸ ressource `dcterms:title` : la dénomination *titre* pose problème : qui
est l\'auteur cité dans le titre quand il s\'agit d\'une vue
d\'exposition par exemple. Peut-être préciser qui est l\'auteur de ce
titre (médiateur, créateur ressource, article) (cas de deux titres
différents possibles)

▸ ressource `dcterms:provenance` : référence sous forme de lien la source
de l\'objet. Faut-il pointer à la racine du site source ou vers l\'url
du gisement spécifique de l\'image ? Cette entrée doit-elle être
générique ou alors spécifique à chaque fonds ?

▸ ressource `dcterms:abstract`  : il faudrait avoir des questions
incitatives pour séquencer le renseignement telles que :

▸ qui est l\'auteur du document ?

▸ qui est l\'auteur de l\'objet représenté ?

▸ qui est l\'éditeur de l\'objet représenté ?

▸ qui est le designer du livre ?

▸ quel est le support ?

▸ quels sont les personnes impliquées dans la production ?
