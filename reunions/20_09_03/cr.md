# CR - Séminaire de recherche en Auvergne



Avec : Claire Brunet, Jérémy De Barros, Brice Domingues, Catherine Geel, Marie Lejault, Vincent Maillard, Laurence Mauderli, Jérôme Pasquet

Le 2020.08.26-29 à Allègre


---

JEUDI 27.08

**DÉFINITION DES PAGES / ORGANISATION DES CONTENUS**

**PRINCIPE DE LA RECHERCHE CONTEXTUELLE ET GÉNÉRALE**

VENDREDI 28 AOÛT

**STRUCTURATION DE LA RECHERCHE P. 4**

**RECHERCHES DE CARACTÈRES ET GABARITS DES ARTICLES P. 6**

**SYNTHÈSE ET FEUILLE DE ROUTE P. 7**


## Jeudi 27 août

- **DÉFINITION DES PAGES / ORGANISATION DES CONTENUS**
- **PRINCIPE DE LA RECHERCHE CONTEXTUELLE ET GÉNÉRALE**


**PAGE D'ENTRÉE DU SITE / FOCUS**

- Pas de page accueil à proprement parler. La page d'accès au site (ou page entrée) est celle du focus.
- La page Focus devient « La 1 » (nom encore à reprendre pour n'avoir qu'un seul mot de minimum 2 lettres.)
- Les publications sur cette page sont de 3 types :
    - Lors de la publication d'une ligne. Publication d'une 1 jusqu'à la prochaine « ligne » mise en ligne : rythme lent. Le porteur de ligne n'est pas mentionné, il s'agit de Problemata
    - Lors de la mise en ligne du fonds d'un partenaire l'institution ou le privé détenteur/trice du fonds est mentionné ;
    - Pour un événement. La publication s'insère pour une période courte, et vient interrompre momentanément La 1 dédiée à la publication d'une ligne ou d'un fond. L'événement est défini ainsi : journée, communication particulière, début d'une exposition, parution d'un ouvrage (avec notule critique et non communiqué de presse) , à condition que l'Histoire du design en soit un des prisme. Cette publication comprend 1 à plusieurs images + texte de 3000 à 6000 signes.
- La création d'événements en tant que focus permet de supprimer la
    page événement.
- Pas d'archives créées pour ces focus, mais simplement un « stockage
    temporaire » régulièrement effacé.


**LES LIGNES**

- Fragments de l'histoire du design \>\>\> qui circonscrivent un
    domaine. (Voir texte rédigé par le groupe chercheur livrable au
    15.09)
- Le minimum d'éléments composant une ligne est de 10 textes + 1 texte d'introduction à ce corpus.
- La notion de « papers » à indiquer très tôt dans la rubrique
    « ligne »
- La ligne doit répondre à une hypothèse, un problème
- FORMATS des textes articles :
    - Notule = 3000-6000 s
    - « Article » = 15 000 -- 40 000 s (*trouver peut être une autre appellation pour qu'il n'y ait pas d'ambiguïté)*
    - Essai = 50 000-60 000 s
    - ~~Entretien \> Designer et Historien du design (nombres de signes max . ?)~~
- Ajout possible de « controverse » ou d'*addendum* au sein d'une
    ligne. Dans ce cas, une phrase type est ajoutée à la fin de
    l'introduction de la ligne lors de la mise en ligne du premier de
    ces ajouts. Phrase à fournir par le groupe chercheurs


**ARTICLES**

Pour rappel, est nommé article : « un texte et les ressources convoquées par ce texte ».

- Un document (pdf / scan d'un texte etc.) est toujours introduit par
    un abstract ou une notule pour être considéré comme un article.
- Dans un premier temps, l'abstract est en FR. ~~Si poss, traduit en
    ENG.~~
- Pour les catégories des mots clés : pas nécessaire de les faire
    apparaître sur la page de l'article.
- Possibilité d'avoir des articles « hors ligne ». Il n'y a pas pour
    autant de ligne intitulée « Hors Ligne ».


**CHAMP DE SAISI**

- La recherche dans le champ de saisie est contextuelle à la page. Pas de recherche globale au site.
- Pour ne pas créer de confusion, dissociation des éléments du
    bouliers (Article / focus / lignes / auteurs / Ressources) du champ de saisi.
- Dans la barre de saisie, la recherche est par défaut mot à mot.
    Possibilité de « sélectionner » une recherche par « termes exacts ».
- Le « boulier » a pour rôle : signalétique et navigation. En d'autres termes, permet d'afficher là où l'on est et les pages du site.

**ORGANISATION DES INFORMATIONS : HEADER / FOOTER**
- **Le Header sera organisé comme suit, sur une même ligne :**

> PROBLEMATA \| Base Line \| Renvoi vers page informations \| FR/EN

Le « boulier signalétique » se situe sous cette ligne.

- Il n'y a pas de *footer*, ce qui permet de soulager la partie basse
    de l'écran pour d'autres information. Les informations diverses ne viennent pas « parasiter » la lecture. La place est laissée aux contenus.
- La page « informations » est structurée en différentes
    rubriques (noms à revoir)
- Les contenus sont les suivants :

> **MON COMPTE**\
> • S'inscrire\
> • Se connecter
>
> **À PROPOS**\
> • Présentation philosophie... (adhésion)\
> • L'équipe\
> • Le Comité Scientifique / Le Conseil Scientifique\
> • Partenaires / financements (Procédure choix des membres)\
> • Disparition du service ?\
> • Contact
>
> **PROPOSER UNE LIGNE**\
> • Le cahier des charges\
> • « Droits et devoirs »
>
> **LÉGAL**\
> • CGU\
> • Déclaration non conflit d'intérêt.\
> • RGPD\
> • Disparition du service ?
>
> **SOCIAL**\
> • Newsletter\
> • Réseaux Sociaux
>
> **INTEROPÉRABILITÉ**\
> • Modèles de données\
> • etc (Jérémy / Vincent)

## Vendredi 28 août

- **STRUCTURATION DE LA RECHERCHE EN LIGNE**
- **RECHERCHES DE CARACTÈRES ET GABARITS DES ARTICLES**


**1. STRUCTURATION DE LA RECHERCHE**

PAGES LIGNE :

- Page globale des lignes : (tableau avec ens. des lignes) :
    - pas de filtres / pas de champ de saisi.
    - Rédaction, prévoir : \[un titre de ligne court + sous-titre long\] ou \[un titre long et sa version courte\]
- Page détail d'une ligne (Prez de la ligne + tableau des
articles)
    - filtres et champ de saisi : oui (pas de distinction, sur la base d'un minimum d'articles)


PAGES ARTICLE :

- Page globale des articles ]{.ul}: (tableau avec l'ens. des articles)
    - Filtres et champ de saisi ok. Validation des intitulés de colonnes. Validation des choix des filtres
    - Lorsqu'un article est écrit par plusieurs auteurs, on duplique
    l'article et on crée une entrée pour chaque auteur.

- Page article développée :
    - Pas de champ de saisi (et pas de filtres)
    - Le type de l'article n'est pas cliquable
    - Les mots clés : les mots clés d'un article sont appliqués de façon systématique, par capillarité, aux ressources contenues dans cet
    article. Il est possible de cliquer sur l'un des mots clés qui
    permet d'ouvrir sur une page « affichage des résultats », accessible
    à date uniquement par ce chemin-là.


PAGE AFFICHAGE DES RÉSULTATS

- Page qui affiche les résultats de la recherche après avoir cliqué sur
un mot clé, au sein d'un article développé. Cette page n'est pas visible
dans le « boulier ». Dans un temps 2, cette page pourra devenir une page
visible de recherche développée.
- La page présente une liste d'articles avec un carrousel de ressources
associées.

PAS DE PAGE INDEX

PAGE RESSOURCES :

- Page ressources globale :
    - Filtres et champ de saisi OK
    - Filtres : types / mise en ligne / date de création / ajout du filtre
    « fonds »
    - Pour l'instant pas de page Fonds à proprement parler, le fonds est
    intégré aux ressources et valorisé temporairement sur page focus.
    Développement d'une page Fonds spécifique à définir et prévoir dans
    un temps 2 (dépend aussi des financements apportés par les
    partenaires détenteurs de fonds).
    - Il faut prévoir pour chaque ressource une version courte de la
    légende pour la page globale (mosaïque).

- Page ressource détail :
    - Liens cliquables : type de ressources / date de création / Relation
    avec l'article associé 
    - Lien non cliquable : date de mise en ligne
    - Mentions présentes dans le détail de la ressource :
        - Type \[Cliquable\]
        - Légende / titre / auteur
        - Description
        - Fonds / institution (éventuellement n° inventaire) \[cliquable\]
        - Crédits
        - Article associé \[Cliquable\]
        - Droits d'usage

    - Stockage des ressources : si pas d'autorisation pour le
            stockage des ressources sur Problemata, un lien doit être
            créé depuis l'institution vers Problemata pour intégrer
            l'image de la ressource dans la page.

    -   Penser à déterminer un lieu de stockage unique (impact
            écologique) pour les fonds numérisés grâce à l'appel Collex
            Persée.

PAGE AUTEURS

- Page générale :
    - Liste des auteurs avec Nom Prénom et nationalité indiqué entre
crochets. (\> Ajouter nationalité dans le modèle de données / tableau métadonnées)
    - Filtres et champ de saisi OK :
    - Filtres présents : nationalité / lignes
    - filtre supprimé : date de naissance et de mort
    -   Colonnes : Tri ok par date de naissance et de mort. Signe utilisé
    ok, voir si poss de donner la même hauteur à l'étoile et à la croix.
    - Suppression de la minibio de l'auteur dans la liste développée.

- Page auteur détaillée :
    -   Validation des contenus et champ de saisi.
    -   Contenus : nbre d'articles / microbio/ associé aux lignes
    \[Cliquable\] / Liste des articles écrits par l'auteur (Filtres OK)
    - Pour les articles co-signés, pas de mention des autres auteurs sur
    cette page.


**2. TYPOGRAPHIE ET GABARITS DES ARTICLES**

Avec Yeelena De Bells

RECHERCHES DE CARACTÈRES :

- Adaptation de la typo Spectral : à poursuivre. S'assurer de la
    licence pour Spectral et de la possibilité de modifier le dessin. /

Précisions apportées le 3.09 par Yeelena : « *Concernant l\'usage et
la modification du Spectral, je me suis renseignée et il est bel et
bien sous Licence OFL, ce qui nous permet de le modifier à tort et à
travers tant que nous n\'oublions pas, lors du partage des fichiers du caractère modifié, de préciser qui est son créateur originel et quel était son nom avant modification. Il ne pourra également pas être vendu. »*

- Choix alternatif à la ROOBERT et à l'INTER : SPACE GROTESK. Voir si
    possibilité de mixer les capitales de l'INTER / les minuscules de la
    SPACE GROTESK / les chiffres de la ROOBERT ?

*Suite réunion, échange Yeelena et Brice le 3.09 :*

*• Proposition de Yeelena (voir PDF : P02. Inter-Roobert-space en
annexe) : partir de l'inter et lui ajouter les particularités
intéressantes prélevées dans la Roobert et la space grotesk. Yeelena
finit d'évaluer le temps nécessaire pour réaliser ces modifications (a
priori ok, à l'exception du changement de chasse)*

• Retour de Brice, le 3.09 : **s'appuyer sur le squelette de l'Inter** (proche de l'Akzidenz Grotesk). Lui **intégrer certaines particularités du Space Grotesk** qui est pour le moment trop expressif sur les chiffres, trop rigide sur les capitales (proche du
caractèreDIN) mais qui possède des **minuscules intéressantes**.

**Les chiffres du Roobert nous intéressent**, ils sont plus
expressifs que ceux de l'Inter mais moins extravagants que ceux du
Space Grotesk**, la balance idéale serait le dessin des chiffres de
l'Akzidenz Grotesk**, une piste à suivre certainement.

Et bien sûr, **les particularités des capitales du Roobert nous
intéressent également**, à vous de **voir maintenant ce que ces
modifications peuvent ajouter à l'histoire de ces choix**, car pour la fonderie Displaay, auteur du caractère, il s'agissait de «fantasmer» un alphabet à partir du logotype Moog.

Trouver un subtil équilibre entre technique (hauteur d'x, affichage
écran...) et l'histoire des modifications qui vont être opérées sur
l'Inter et sur le Spectral. Cf. la présentation du Louize et du
Roobert qui au-délà des aspects formels et techniques d'affichage, ces
derniers racontaient aussi une histoire sur Problemata.

- Pas de 3^e^ typo.


GABARITS :

- Yeelena demande aux chercheurs de lui fournir des textes spécifiques ou de compléter des textes (intertitres dans textes longs / entretiens à 2 voies plutôt que parole retranscrite, etc. ) transmis afin qu'elle puisse avancer sur les hypothèses de gabarits.

\>\>\> \@Yeelena : transmettre les présentations. FAIT LE 3.09. En
Annexes :

- 1. Choix de caractères
- 2. Typologies de textes
- P02. Appareillage typo
- P02. Textes et mise en site
- P02. Inter-Roobert_space

\>\>\> pour le CS du 22.09. Voir avec Thomas Huot Marchant pour une
présentation des enjeux de la recherche typo et l'utilisation /
l'histoire des OFL.



**SYNTHÈSE ET FEUILLE DE ROUTE**

COMITÉ SCIENTIFIQUE DU 22 SEPTEMBRE : \[17h\>20h Auditorium Musée Arts Déco\]

- Rédaction de la philosophie de Problemata \[CB / CG / JP / LM\]
- des principes technique (JDB + VM\]
- et du design \[abc\]
- Rédaction de la présentation ligne de recherche + protocole à l'attention des chercheurs \[CB / CG / JP / LM\]
- Présentation de la ligne CCI = 1^ère^ ligne de Problemata.
- ODJ : 1 heure présentation des lignes de recherche. 1 heure : Thomas Huit Marchand : OFL et ses ambigüités. \[Cath allo THM\]

OCTOBRE : PUBLICATION DES RÉSULTATS FNSO

OCT. NOV 2020 : RÉUNION ÉCOLOGIE DES DONNÉES

- Date à fixer le 22. 09.
- Séance avec Karine / Mica :
- Mettre en relation Sonia de la BK avec développeurs, mais rencontres et réunions diplomatiques à monter au préalable.

COLLEX PERSÉE

- Montage de l'Appel à projet avec Karine + Mica + Jérôme ?

AUTOMNE 2020 : STRUCTURATION DE LA 1

- Marie et Catherine puis présentation à Brice

AUTOMNE 2020 : ÉLABORATION DU CONSEIL SCIENTIFIQUE

Prise de contact (voir liste établie et répartition des contacts)

DÉCEMBRE 2020 : AG DE L'ASSO + COMITÉ DE PILOTAGE

- Élection d'un nouveau bureau

HIVER 2020 : RÉDACTION CGU et textes INFOS

- Marie + Cath + ??

MARS 2021 : RÉUNION MULTILINGUISME / TRADUCTION ET STRUCTURATION DES
ARTICLES

- À l'issue du travail mené par Yeelena

- Pour la question de la traduction, voir avec Antoine Cazé (Disderot
    Paris) et/ou Michel Espagne si partenariats à monter sur pratique et
    théorie de la traduction avec leurs étudiants. Monter un financement
    MSH de la journée ?

2021 : FINANCEMENT CARASSO

- Appel à projet « Composer les savoirs pour mieux comprendre les
    enjeux du monde contemporain.

Point budgétaire : pour information, le coût du séminaire s'élève
à 1750 euros (provision initiale : 2500 €).
