# Questions pour Huma-Num

- comment décrire / display des métadonnées multilingue ?
- quels vocabulaires / normes employer ? pour les lieux, les noms, les langues, les dates
- comment récupérer des données de la BK ?
- comment être interropérable ? métadonnées dans le HEAD ? schéma RDF ? comment créer une API pour le protocole OAI-PMH ?
- comment créer notre propre schéma d'identifiants / cotes ?
- comment écrire les métadonnées (exif) dans les fichiers images / fichiers pdf ?

- à quel moment de la chaîne éditoriale l'interopérabilité entre un fond et un document édité se fait ? et comment ?
