# RDV HumaNum - 14.06.2019 - Stéphane Poullyau @ EHESS/MSH

**présents :** Marie Lejaut, Karine Bomel, Laurence Mauderli, Brice Domingues, Vincent Maillard, Jérémy De Barros


## Sentiments généraux
- Stéphane Poullyau n'a pas l'air de vouloir nous imposer tout une batterie d'outils, car Vincent et Jérémy se sont renseignés sur la question de la nécessité de tous ces outils au sein de la plateforme. Il propose plutôt de nous laisser constituer la chaîne avec Huma-Num en fonction de nos outils, en nous conseillant néanmoins d'être rigoureux dans l'écriture des schémas et lexiques.


## Notes
- Stéphane Poullyau connaît bien Marcello Vitali-Rosati et Nicolas Sauret de la revue *Sens Public*, dont Jérémy refait le design depuis un an et demi déjà. SP peut être une base critique avec son statut d'exemple, pour nous guider par la suite.

<<<<<<< HEAD
## Lexique en acronymes du RDV
- EAD
- RDF
- MD
- HTML
- API
- graphDB
- CMS


=======
## Lexique en acronyme du RDV
- EAD : Encoded Archival Description | format de description de manuscrits et documents d'archives
- RDF : Resource Description Framework | modèle de graphe pour décrire des ressources (métadonnées) destiné à construire des données sémantiques interopérables. On parle de "triplet" RDF : une ressource est décrite par des triplets comprenant : un sujet, un prédicat, un objet
- MD : Markdown | syntaxe de balisage léger
- HTML : Hypertext Markup Language | langage de balisage des pages web
- API : Application Programming Interface | Service web reposant sur un système de requêtes / réponses
- graphDB : Système de gestion de base données pour la description de base de données relationnels (web sémantique) | http://graphdb.ontotext.com/
- CMS : Content management system | Système de gestion de contenu web (back office)
- FOAF : Friend of a friend | Ontologie RDF pour décrire des personnes et leur relation entre elles. Les champs sont plus précis et peuvent être plus facilement exploitable que le Dublin Core
>>>>>>> 34194428f8f493cf824fe01e7ad9c0e206ff304b
---
---

## Question préparées en amont
- comment décrire / display des métadonnées multilingue ?
- quels vocabulaires / normes employés ? pour les lieux, les noms, les langues, les dates
- comment récupérer des données de la BK ?
- comment être interopérable ? métadonnées dans le HEAD ? schéma RDF ? comment créer une API pour le protocole OAI-PMH ?
- comment créer notre propre schéma d'identifiants / cotes ?
- comment écrire les métadonnées (exif) dans les fichiers images / fichiers pdf ?
- à quel moment de la chaîne éditoriale l'interopérabilité entre un fond et un document édité se fait ? et comment ?


## Configuration idéale du branchement
---
- usage d'Omeka S ou d'un autre système de gestion de base de données relationnels
- coupler l'usage d'Omeka S avec un CMS plus performant pour gérer la plateforme et les articles (ProcessWire, Django, Kirby par exemple)
- probablement convertir les données de la BK (qui sont en EAD) pour pouvoir les connecter facilement à l'infrastructure problemata et faire durer ce branchement dans le temps. Le problème avec les données en EAD est que ce format est peu exportable dans un autre contexte.
- le schéma RDF décrit par Omeka S sera facilement moissonnable par Isidore
- l'option d'un serveur *handle* pour gérer l'identification de chaque objet de la base, idéalement pour gérer les questions de comptes utilisateurs dans le futur


## Des métadonnées, mais suivant quel modèle ?
---
- Dublin Core
- Foaf (plus détaillé que DC)
- Prism (vu sur *Sens Public*)
- l'aspect multilingue est géré en natif par les schémas RDF
- pour ajouter des ID aux ressources / personnes : consulter WIKIDATA : https://www.wikidata.org/wiki/Wikidata:Main_Page

## Récupérer et croiser des données internes et externes ?
---
- Orcid, ISNI, Zotero pour les auteurs
- BNF pour des ouvrages


## Afficher des données externes sur la plateforme
- de la BK : le minimum serait de mettre le lien de la ressource empruntée dans sa collection. Des maximums peuvent être toutefois spéculés.


## Travailler avec Huma-Num
---
- Huma-Num ouvre un espace sur leur gitlab pour le projet problemata
- ou alors une machine virtuelle, à condition de spécifier les besoins logiciels (Operating System, applications, programmes, langages, serveurs)
- on peut les contacter via leur email de service (cogrid@huma-num.fr), pas celui de Stéphane Poullyau, en cas de besoin d'assistance, de suivi ou d'initialisation d'instances particulières pour le projet


## Ce que nous devons faire en amont (plus ou moins dans l'ordre)
---
- écrire ensemble un lexique, à hauteur de 100 concepts (ex: auteur, photographe, objet, sculpture, type de reliure...) dans l'idée d'évaluer le *grain* que nous voulons donner au système.
- vérifier / comparer en parallèle si les concepts ajoutés au lexique n'existent pas déjà dans les modèles wikidata
- prospecter les CMS pour déterminer celui avec lequel on veut travailler
- décrire des balises textes à faire figurer en plein texte dans les articles pour y installer la description sémantique en plus des métadonnées


## Articulation entre fond et plateforme: étude de cas de l'écriture d'un article
---
L'idée ici de mobiliser les rôles principaux de la plateforme, le *donateur* de ressources (Karine Bomel @ BK), l'éditeur (Catherine Geel), l'auteur (l'étudiant qui écrit un article), dans un même processus de travail, pour identifier les besoins spécifiques de problemata en terme d'écriture (ex: éditeur de texte en local VS page d'édition sur la plateforme). L'idéal serait que Vincent et Jérémy passe un moment, sous forme de workshop ou d'une journée, à la BK avec le triplet donateur, auteur, éditeur, à la BK, comme l'étudiant le ferait.
