
# Notes sur retours des éditrices (2021.05.17)

**général**

- *Pb avec le texte importé depuis omeka car pas d’enrichissements typo*

On pourrait réfléchir à l'ajout d'un champ de texte enrichi dans django pour les lignes (comme les articles)

**ligne / détail**

- *Comment rendre lisible / visible l’article d’intro de la ligne ? > faire un type spécifique intro ? Comment la faire apparaître en haut du tableau par défaut ?*

Les choix de design restreignent le tri aux colonnes affichées. Ce serait étrange de mettre arbitrairement une ligne en premier alors que l'on est invité à modifier l'ordre et la règle de tri. Il faut bien prendre en compte que les articles ne s'affichent pas uniquement sur la Détail Ligne : sur la Liste Articles, la Détail Ressource : comment devra s'afficher l'introduction d'une Ligne ?

On pourrait faire précéder d'un chiffre.

On pourrait faire un filtre "introduction".

- *Le texte de présentation est la description courte et non l’abstract plus développé (renseigné dans OmekaS).*

À vérifier.

- *Tableau articles dans ligne : l’accès au article n’est pas du tout intuitif : obligation de développer l’article pour pouvoir le lire en cliquant sur FR !*
- *NOTE POUR PLUS TARD : L’apparition et la disparition du développé article : le signe + et le signe - ne sont pas au même endroit, c’est un peu « étrange » et peu intuitif pour refermer.*

On partage aussi ces retours.

**article / détail**

- *Les images peuvent-être téléchargées / enregistrées sous (bloquer cela)*

On peut désactiver le click droit en JS mais on ne peut pas empêcher le téléchargement pour quelqu'un qui s'y connaît un tout petit peu et souhaite récupérer l'image.

- *Le sous-titre devient une ligne en soi est n’apparaît pas comme un sous titre. Ou le titre est coupé (texte de Laurence par ex.)*

À régler.

**ressources / liste**

- *centrer les ressources dans la case (format portrait ?)*

On peut jouer sur le hors champ pour avoir les images qui remplissent la forme carrée tout le temps. Mais ça veut dire rogner sur l'image.

**ressources / détail**

- *Documents écrits composés de plusieurs pages :  étrange d’avoir 1 ressource par page (courrier etc.)  en attendant de trouver une solution, nous renseignons le n° de page dans Omeka S.*

Dans Omeka, on peut mettre plusieurs images pour un même item. À voir quelle solution de design sont à prévoir dans ce cas.

<div style="page-break-after: always"></div>

**agents / liste**

- *Général : on voit apparaître la date de naissance (jour / mois année) > n’y a t il pas un filtre par décennie ?*

Il n'y a pas de filtre par décennie pour les agents (seulement le tri). Si le souci est que l'on voit la date de naissance précise des auteurs, il faut plutôt éditer les items dans Omeka pour renseigner seulement l'année (ou la décennie). Sans ça, on peut éventuellement n'afficher que l'année en HTML mais l'API exposera toujours la date de naissance complète.


---

*Autres retours*

**Liste Articles filtrés par mot-clés**

Pour l'instant seul "dcterms:subject" est affiché sur la page Detail Article pour renvoyer vers la Liste Articles. Nous n'avions pas actés tous ensemble ce qu'il fallait faire. Il existe trois autres propriétés : "dcterms:references", "dcterms:temporal" et "dcterms:spatial" qui pourraient être utilisées. Mais le terme "mot-clé" des maquettes semblent plus correspondre à "dcterms:subject".

Que doit-on faire : 

- sur la Detail Article : afficher toutes les valeurs de ces propriétés comme des liens en filtrant la Liste Articles par leur couple propriété/valeur sous le terme "mot-clé" ?
- sur la Detail Article : afficher toutes les valeurs de ces propriétés comme des liens en filtrant la Liste Articles par toutes les propriétés confondues sous le terme "mot-clé" ?
- sur la Detail Article : afficher toutes les valeurs de ces propriétés comme des liens en filtrant la Liste Articles par leur couple propriété/valeur sous des termes adaptés à la propriété ?

(Remarque : nous ne sommes toujours pas convaincu de l'utilité de cette fonctionnalité, surtout sur la partie filtrant les Ressources)

**Vocabulaires contrôlés**

Les maquettes ne sont pas exactement alignés sur les vocabulaires contrôlés (date, langue, nationalité *à vérifier*). Sur les dates par exemple, il faut qu'elles soient renseignées selon les normes ISO dans Omeka. Si c'est bien fait (c'est le cas actuellement) on peut transformer la date en un autre format à l'affichage. Mais le problème c'est qu'un auteur peut ne pas comprendre pourquoi le format est différent de ce qu'il a renseigné. D'autant plus que s'il fait une erreur, la transformation auto ne fonctionne plus et ça complexifie le travail dans Omeka si la personne ne comprend pas ce qu'il se passe.

**Compteurs et marqueurs**

- Comment compter les articles ?
- Que doit-on afficher pour "dernier ajout" pour les Collections par exemple ?