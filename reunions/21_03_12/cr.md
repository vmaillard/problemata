# 2021.03.12
## ML, VM, JDB

---

## Renseignements et écriture des contenus de la beta

### dates
- indisponibilité pour nous à confirmer :
    - 08.04.2021
    - 14.04.2021
- disponibilités : 
    - 09.04.2021
    -  du 08.04.2021 au 15.04.2021 ML et CG à Allègre

### récapitulatif organisation

#### somme de contenus

- Ligne CCI : 
    - 1 ligne
    - 26 articles
    - 20-26 auteurs
    - 200 ressources

#### choses à faire en amont
0. gérer les échanges et transactions avec la BK
1. récupération des fichiers et tableaux de la BK
2. import des ressources depuis la BK

#### choses à faire en live
0. se répartir le travail clairement et précisemment
1. renseignement (omeka) des agents et de leurs bios
2. renseignement (omeka) manuel des ressources et de leurs médias
3. renseignement (omeka) des lignes
4. renseignement (omeka) des articles
5. écriture (markdown) des articles
- transversal = import CSV

#### rôles
- markdown : SG
- metadata : étudiants, CG & ML
- outillage, importation, markdown + metadata : VM & JDB
- aide et relecture des vocabulaires : VM & JDB & ML
- aide et relecture des contenus: CG & ML & SG

#### notes pendant et après les questions de Marie
- semaine du 22.03.2021 : saisie des objets dans la base omeka
    - ML, CG, VM
    - les formulaire Omeka seront prêts (titres, définitions, vocabulaires contrôlés)
- le 09.04.2021
    - écriture des contenus de page à propos en markdown
    ATTENTION A FAIRE EN AMONT : 
    - écriture des articles (26 articles, SG) donc le 09.04.2021 : les articuler avec la base omeka (ressources via table des figures)
    - pour le 09.04 : fichiers md : mettre la table des figures comme ça on pourra les retrouver, les mettre en relation avec le texte (ancres)

---

### Questions de Marie

- les ressources CCI proviennent de la BK : qu'est-il prévu pour ces images en terme de modalités d'échanges ?
    1. on a ces ressources là : liste / tableau
    2. ils nous envoient les fichiers et un tableau csv rempli des métadonnées pour chaque chose
        - modèle des colonnes et lignes du tableau ?
    3. organiser les images dans un dossier accessible d'une manière ou d'une autre
    4. on peut saisir maintenant les ressources dans omeka (import auto et relecture manuelle cas par cas, les 2)

---

- les agents en 1er dans le processus global de saisie ? = OUI
    - ils peuvent être appelé par une ressource en effet

---

 - y a-t-il une manière visuelle d'afficher les ressources ?
    - vm : onglet media et pas entrée value
    - titre du fichier : nom_du_fichier_fourni_par_le_fonds
    - dans la liste d'items il y a une miniature si on a uploadé un media


- si 2 ressources ont le même titre, comment les différencier ?
    - peut-on afficher l'id (identifiant pérenne) dans les colonnes dans la liste d'items générale ?
    - ressource :
        - cote va dans bibo:locator (!= de dcterms:provenance qui est investi différemment)
        - url : exemple de la BK qui nous dit non car leurs url vont bouger
        - des resources sans fonds = OUI
        - des ressources n'étant pas appelées dans un article = OUI
        - des ressources étant appelées par un fond = OUI

---

- sur les objets qui existent déjà
    - s'assurer que cet objet n'existe pas

---

- reset-on la base de données à un moment ?
    - pour les unique id (int)
    - pour partir sur une base propre
    - partir des id en cours parlent du temps que ça a pris d'en arriver là (comme set JC as 0 in christian calendar)
    - réponse : VM a mis a disposition un omeka pour ML, où Claire Brunet est JC (l'item #1)

---

- connexion en janvier (ML avec account VM) / connexion en février (ML avec account ML)
    - Marie n'arrive pas à créer des utilisateurs
    - il faut qu'un administrateur créé les comptes des autres utilisateurs
    - un user générique unique pour tout faire en back office / administration ?

---

- ordre de saisie des objets (notamment le 09.04.2021)
    1. agent
    2. ressource
    3. ligne
    4. article

---

- dcterms:description de l'objet ligne et peut être en général
    - quel nombre de signes ? quel calibrage ? ou alors tronquée par nous ? (rapport aux formats attendus par les réseaux sociaux)
    - que fait-on avec elle si elle n'est pas visible ?
    - dans class=article : description=résumé très court, et abstract=résumé normal

---

- que se passe-t-il pour les dates dans les objets ?
    - rappel format ISO : YYYY-MM-DD
    - exemple de la date de création : 1984 où on ne connaît pas le jour ni le mois
    - = quand on ne connaît pas le jour ni le mois c'est l'année qui est référencée

---

- question des langues et de la norme en générale
    - Langue = alignement norme ISO639 (ex: arménien = hy) (https://www.loc.gov/standards/iso639-2/php/code_list.php [url= last updated 2017-12-21])
    - LANG = input select
    - MOTS CLES = input select
    - TYPES = input select
    - des inputs select en général pour éviter de ne pas avoir à ouvrir plein de fenêtres
    - VM pose la même question pour dcterms:spatialcoverage (text ou geonames?)
        - geonames n'est qu'en anglais (Angleterre ne renverra à rien)
        - travailler avec leur API pour récupérer les noms dans != langues
        - ML : bien mais que ce ne soit pas une usine à gaz de notre côté
        - CCI : on renseigne les geonames en EN !

---


- object class = article
    - dcterms:tableofcontents = fonctionner avec des #, ##, ###, ####, #####, ######
    - dcterms:subjects = mots-clés sujets : 4 libres et 4 contrôlés (! saisie text ou pioche dans vocabFR, vocabEN)
    - les MOTS-CLES libres enrichissent-ils les contrôlés ? = NON !
    - il faudrait le faire à la main et le traduire à la main
    - contrôlés = éditeur, libres = auteurs
    - dcterms:temporalcoverage = mots-clés temporels : exemple formattage d'une année à une année (1984) ou (1984-1984) ?
    - dcterms:(n'importe quelle date)(x3) : à vraiment re-titrer sinon l'utilisateur se perdra !


---

### Questions pour Marie

- mail :
    - création des mails : contact@seced.fr et/ou contact@problemata.fr
    - pas possible par Huma-Num, il faut passer par un service privé type Gandi (2 boîtes mail de 3Go), OVH
    - seced.fr : 15€/an
    - problemata.fr : déjà pris par eux : https://www.lmedia.fr/
    - problemata.net : 21€/an (très beau, pas cher)

---

- urls Problemata :
    - problemata.huma-num.fr/
    - ou problemata.net/ si nom de domaine enregistré

---

- typographie :
    - on met le Spectral d'origine pour l'instant ?

---

- mises à jour du modèle de données :
    - modèle Ressource
    - dcterms:provenance pour "Fonds"
    - bibo:locator pour "Cote / Nom du fichier"
    - modèle Fonds
    - vocabulaires contrôlés
    - les capitales et espace entre les slashs

---

- rédactionnel :
    - "À propos / Interopérabilité" : on envoie prochainement un texte basé sur ça : https://framagit.org/vmaillard/problemata/-/blob/master/plateforme/a_propos/ecologie_donnees.md 
    - cookies : on rédige qq chose prochainement pour cet endroit (yes, en même temps o npeut garder en réserve que tant que toutes les fonctionalités ne sont pas définies on ne sait pas combien il y en aura = rgpd au texte qui évolue selon les versions)
    - passage du "À propos" en markdown sera nécessaire 

---

- multilingue :
    - Sur la Liste Articles, au click sur un titre d'article à quelle version de l'article est-on dirigé selon la langue en cours de l'utilisateur ? Y a-t-il une corrélation entre bi-linguisme du site et multi-linguisme des articles ?
