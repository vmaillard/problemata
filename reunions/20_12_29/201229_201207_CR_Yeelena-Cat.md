**PROBLEMATA**

**Relevé de décisions**

07.12.20 et 28.12.20

RDVZoom

En présence de :

**ANRT Coordination des chercheurs**

Yeelena de Bels Catherine Geel ( Ens Paris-Saclay / Nancy)

**[Planning YEELENA]{.ul}**

**29.12-19.01 - Etude de gabarits d'articles**

> **Gabarits** 3 longueurs de textes + spécificités (placement et
> interactivité des notes, titres/sous-titres, sommaire, différents
> types d'images, légendes, citations, entretiens Q/R, pdf/video/son/,
> essais visuels) 
>
> **Rendu** Figma
>
> **Screen size** laptop + mobile (si possible)

**19.01-16.02 - Modification de l'Inter**

> **Capitales** à adapter
>
> **B-d-C** élargissement de la chasse, modification des terminaisons et
> des angles
>
> **Chiffres**

**16.02-16.03 - Finalisation V1 S+I + Spectral Italique **

> Préparation clean des fichiers d'exports (Spectral + Inter)
>
> Modification du Spectral Italique 

**Semaine 50 /51** Spectral : travail du gris typo, des empâtements

> Fonction des appels de note & de figure automatisé ( ou automatique ?)

**Semaine 52/52/01** Travail de structuration des *templates* pour
lecture écran

> Selon les 3 modalités de longueur définies\* par les chercheurs et les
> 7 templates.\*\*
>
> Catherine & Marie sont à la disposition de Yeelena pour échanges et
> infos lors de cette période.

**Semaine 2 /3 / 4** W sur les premières modifs de l'Inter

> Réaction de office abc sur le travail de recherche sur les
> structurations de textes des templates.

**Semaine 5 / 6 / 7** Soit second round de travail sur l'Inter.

Soit reprise du travail de structuration des textes.

**-Yeelena envoie à Marie et Catherine la liste des textes qui ne sont
pas en sa possession avant le 4 janvier \> Catherine et Marie les lui
fournissent.**

**RAPPEL / Les 3 formats de textes et 6 templates**

\* trois formats d'écritures sont définis pour la publication en ligne
(doc. présentation ligne)

\*\* 6 typologies de templates. (Voir étude : DOC
01-CHOIX-DE-CARACTERES-200715.)

\*[Entre 3 000 et 6 000 signes]{.ul} : notules, introduction de textes
ou de documents historiques, propos liminaires.

\*\*TEMPLATE 1. les notules qui semblent être des textes très courts et
qui gagneraient à avoir une forme texte \"image\"; + suite d'images.(
amendement du 29.12)

\*[Entre 15 000 à 40 000 signes]{.ul} : textes communément appelés
articles. Ils peuvent comprendre le texte d'introduction de la ligne, un
ou des entretiens, un chapitre d'ouvrage etc.

\*\*TEMPLATE 2 : les articles

\*\*TEMPLATE 3 : les introductions

\*\*TEMPLATE 4 : les conférences

\*\*TEMPLATE 5 : les entretiens à deux (ou plus de) voix qui nécessitent
de montrer les questions et les réponses. (amendement du 29.12)

\*[Entre 50 000 et 60 000 signes]{.ul} : essai. Typologie plus rare et
plus exceptionnelle.

\*\* TEMPLATE 6 : les essais (qui nécessitent un sommaire additionnel)
