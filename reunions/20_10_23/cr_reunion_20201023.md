# Compte rendu de réunion du 23.10.2020
## Workshop outil d'écriture, janvier 2021
### CG, VM, JDB

---

### Intervenants
- Catherine Geel
- Jérémy De Barros
- Vincent Maillard

---

### Logistique

- mi / fin janvier 2021
- 3 jours de workshop (lun 25 - mer 27)
- 8h30 de payable pour vm et jdb soit 4h15 chacun
- tarif horaire : CG va demander mais a priori ce ne sera pas un montant formidable
- qu'est-ce que l'on peut proposer avec ce budget
- étudiants      : 16 - 19
	- ENS        : 15
	- ENSADLAB   : 1 doctorante
	- ESAD Reims : 3 étudiants max

---

### Notes

- questions préalables
	- intitulé et description du workshop ?
	- liste de fonctionnalités pour les outils ?
	- objectifs à atteindre ? pour eux pour nous ?
	- travail sur quels textes ?
	- sur quel(s) outil(s) ?

- en 2 temps
	- 1 temps théorique : poser la question "quels sont les enjeux de ces outils là ?" 
	- 1 temps pratique  : pour se saisir de la question et y apporter de la nuance ou des réponses ou des questions
- volonté commune de ne pas imposer les contenus de PB aux étudiants
- jdb : un temps qui soit agréable et plutôt "jouable" voir "amusant"
- articulation de plusieurs verbes :
	- faire la recherche
	- construire la recherche
	- décrire la recherche
	- exposer la recherche
	- publier la recherche
- les étudiants rassemblent des éléments issus de leurs références, de leurs intêrets, de leurs pratiques, de leurs recherches
- l'idée et de regrouper des étudiants et d'arriver à ce qu'ils construisent les parties d'histoires manquantes (fictives) entre leurs sujets d'études, de créer du croisement même absurde et que l'on y croit
- idée de fiction à construire ensemble comme pretexte d'utilisation des outils par un groupe qui croiseraient les recherches de chaque membre
- faire des groupes qui disposent d'une palette d'outils propres ? (exemples: gr1:pad, gr2:googlesheet, gr3:calc, gr4: discord only, gr5: nos outils)
- multiplier les approches possibles :
	- approche texte (métadonnées, hiérarchie, relation)
	- approche image
	- approche cartographique
	- approche visualisation
- idéalement à la BK mais sinon en visio sur Discord
- CG commence à rédiger les textes de présentation du workshop, on relira ensemble ensuite

---

### Déroulé 
- Nous il faut qu'on arrive à minimiser nos interventions à l'intérieur de ces 3 jours, vu le budget
- ça va être chaud parce qu'on va être contents de faire ça

#### j1
- [CG,dev] présentation du workshop, introduction théorique
- [CG,dev] présentation des corpus matériaux par les étudiants
- [CG] composition des groupes d'étudiants
- [CG] début de discussions intra-groupe

#### j2
- [dev] présentation des set d'outils
- [CG] point individuel pour chaque groupe et accompagnement
- [CG] fabrication de sens par la recherche mise en commun

#### j3
- [CG,dev] intégration des contenus : normalisation et alignement
- [dev] publication / déploiement des contenus quelque part (VM > vm http://problemata.huma-num.fr/nomduworkshop/) pour se délecter des résultats
- [CG, dev] point global / présentation / lecture commune des histoires racontées, et les moyens pur parvenir à la composer



### Outils (à organiser)

- Note(s) (Vincent : écriture dans un pad ou un .md, filtre de l'export)
- loopdir (Vincent : notes hiérarchiques à récursivité infinie, relations associatives possibles, graph et lecture par linéarité hiérarchique ou relations associatives)
- Tesselle (annotation d'images) : https://medialab.github.io/tesselle/#/
- Dicto (annotation vidéo / audio) : https://medialab.sciencespo.fr/outils/dicto/
- Ovide (écriture et édition : images, vidéos, visualisations, références bibliographiques et autres entrées de glossaire avancées, et de produire une multitude d'éditions imprimées et web à partir d'un même corpus d'écrits.) : https://peritext.github.io/ovide/
- Zettlr : https://www.zettlr.com/
- Obsidian : https://obsidian.md/
- stylo
- graphviz : https://graphviz.gitlab.io/
- graphcommons : https://graphcommons.com/
- tropy : https://tropy.org/



### Références

- méthode Zettelkasten : https://zettelkasten.de/posts/overview/
- building a second brain : https://www.buildingasecondbrain.com/