## questions vm

- Liste Agents
	- quelle est la logique à suivre pour l'association avec une Ligne ? pour un Agent, on cherche tous ses Articles où il est dcterms:creator et on récupère leur Ligne ? que fait-on s'il y a plusieurs lignes ? ou est-ce qu'on cherche les lignes pour lesquelles cet Agent est dcterms:mediator ?
	- qu'est-ce qui s'affiche au click sur le + ?

- Détail Ligne et Liste Articles
	- le gabarit est différent entre ces deux pages au niveau des sauts de ligne (avec aussi l'ajout colonne "Ligne")

- Liste Articles
	- quel gestion responsive (problème de la colonne Ligne) ? quel comportement au click sur le + ?

- Liste Ressources/Fonds
	- est-ce qu'il y a une gestion hiérarchique des fonds ? ("Archives d’exposition Frac Hauts de France" contient "Ma vie dans les tubes")
	- quelle est la logique à suivre pour "dernier ajout" ? dans les ressources listées, ce sont les dernières ajoutées ?
	- pour le placement d'une Ressource dans une Ligne, est-ce que la logique est bien : on récupère tous les Articles d'une Ligne, et on liste toutes les Ressources de ces Articles ?
	- que se passe-t-il au click sur "voir tout" ?

- Liste Ressources/Fonds + Détail
	- quelle gestion responsive ?
- design de La 1 et des pages statiques (À propos) 

---

## Questions

- liste d'agents
	- dans la colonne date de naissance / décès : le cadratin n'est pas conforme à la norme ISO
	- apparaît la colonne ligne : quelle est la logique à suivre ?
		- CREATEUR (article) [article.ligne]
			- si l'agent est créateur d'un article qui est recensé dans une ligne, recense-t-on les lignes où les articles apparaissent ?
		- MEDIATEUR (ligne)
			- si l'agent est médiateur de la ligne elle peut être rendue mais à la fois celui-ci peut avoir été médiateur de plusieurs lignes
				- du coup quid s'il y en a plusieurs ?
					- les lister avc leurs titres
					- lister le total de lignes comme les articles
					- lister les articles comme lister les lignes
	- le + est contenu dans la colonne ligne ?
		- que se passe t'il à son clic ?
	- je ne suis vraiment pas sur de la relation direct d'un auteur à une ligne particulière en tant qu'auteur

- détail ligne VS liste d'articles
	- les gabarits présentent des différences
		- sauts de ligne
		- apparition de la colonne ligne

- liste d'articles
	- la colonne ligne = pose un problème pour le format mobile 
	- le + est dans la colonne titre ?
		- que se passe-t-il à son clic ?

- mix liste de ressources / fonds
	- design responsif de ces pages ?
	- y a t-il une hiérarchie dans les fonds 
		- exemple "Archives d'exposition Frac Hauts de France" contient "Ma vie dans les tubes"
	- quelle est la logique à suivre pour "derniers ajouts" ?
		- les ressources dont la date de publication est inférieure à ... ?
	- sur la page racine des ressources
		- sur l'entrée dans les ressources par les fonds (fonds, alphabétique) [hiérarchie???]
			- 1 : on liste les fonds
			- 2 : on liste toutes les ressources dont la provenance est ce fond
		- sur l'entrée dans les ressources par ligne (lignes, alphabétique)
			- 3 : on liste toutes les lignes
			- 4 : on liste tous les articles de chaque ligne
			- 5 : on liste toutes les ressources de tous les articles des lignes
		- sur l'entrée explorer l'ensemble (date de dernier ajout)
			- 6 : on liste toutes les ressources 
	- explorer par lignes = à quoi correspond la date de dernier ajout ?
		- je ne comprends pas du tout cette entrée alors qu'on a un objet fonds et un objet ligne
			- dans le sens où les lignes permettent d'articuler ces ressources avec un contexte, un appareil critique construit et rédigé
			- pour moi le rapport entre lignes et ressources est decontextualisant, et de ce fait perturbant
	- explorer par fonds
		- quelle est la logique pour avoir une image d'illustration de ce fonds ?
	- explorer l'ensemble
		- horschamp sur la droite
			- à quoi correspond la première ligne
			- à quoi correspond la deuxième ligne
			- les ressources ne devraient peut être pas dépasser la pleine largeur de la page mais se "wrapper" au début quand la somme dépasse le nb total de ressources par lignes visuelles
		- bouton "voir tout"
			- que signifie t'il ?
			- sont-elles chargées n ressources dans cette section ?
- détail ressource
	- section lignes ? idem en terme de relations d'objet VS approche éditoriale
	- il ne peut pas y avoir 2 fonds de provenance, à moins que les fonds contiennent des rapports hiérarchiques
- ajax ressource
	- à quel endroit ce mécanisme est-il nécessaire ?

- liste de lignes
	- le médiateur n'apparait pas dans les stats de l'entête de la page, ni dans les blocs lignes individuels
- détail ligne
	- liste d'auteurs dans lignes VS liste de médiateurs
		- le médiateur n'apparaît pas ou est confondu avec les auteurs

- barre d'outils
	- pouvoir fermer la liste d'outils (filtre, recherche, tri) ?
	- ordre des catégories = question ouverte
		- trier / filtrer / rechercher ?
			- on trie dans un sens ce qui est affiché
			- on filtre ce qui est trié
			- on élimine arbitrairement par le texte ce qui est filtré

- vocabulaire
	- "posté le" ? / "mise en ligne" ?
		- léger par rapport aux sérieux des vocabulaires contrôlés, et nos objets ne sont pas des posts comme le seraient des posts habituels d'autrs plateforme voire réseaux sociaux
	- "650 éléments"
		- dans les listes ensuite il est indiqué "ressources"
	- "articles relatifs"
	- "auteurs associés"
	- "plan" VS "table des matières"
	- "à propos de la ligne"

- ergonomie / fonctionnalités
	- carousel avec bouton gauche droite VS scroll
		- les boutons sont-ils nécessaires ?

- design
	- entete de sections de ressources (bas de casse + même graisse + même corps de texte) = je dirais pas assez fort
	- liste d'auteurs avec flèche et séparateurs verticaux = c'est lourd
	- le + du header
		- le + suggère le geste d'ajout en terme général, pas le déploiement
	- bouton de langue
		- blanc sur rouge / noir sur rouge = lequel des 2 signifie actif ? = confusion dans le choix
			- suggestion = opacifier l'inactif pour se fondre dans le rouge
	- bouton du header mini
		- même fonction différent signes
			- + VS X
		- même signe différentes fonctions
			- flêche vers la droite, vers la gauche, vers le haut
	- titre, auteurs, nb articles centrés 
		- je trouve le choix curieux
	- si les auteurs sont des liens signifiés avec une flèche pourquoi les autres liens ne sont pas signifiés comme tels ?

- détail article
	- header
		- je suggererais que tous les boutons soient tout le temps apparents dans leur case mais opacifiés pour ne pas avoir tout faire bouger suivant des conditions cumulées
			- i + fleche vers le haut + fleche vers la droite
		- bouton i ? = plan et appreil critique ? 
		- bouton flèche vers la droite ?
		- bouton flèche vers le haut ? = remonter en haut de l'article
			- suggestion = ajouter une barre horizontal au dessus de la flèche ?
		- notes en bas de la page en fixed ?
	- pourquoi séparer la table des matières des résumé notes bibliographie ressources remerciements
		- je ne suis pas sûr que pandoc puissent faire la différence entre ces différents éléments
	- pourquoi les mots-clés se retrouvent-ils dans un volet qui s'ouvre en cours de lecture ?
	- les figures sont listées avec des lettres (a,b,c), dans nos éditeurs on a adopté les nombres
	- appel de figure VS appel de note VS numérotation de paragraphe VS notation des notes VS notation des figures
		- peut être que otut ça dépend de Yeelena ?

- MANQUE : design de La 1 et des pages statiques (à propos, contact, présentation, modèle données, rgpd ...)
