*à lire avec les maquettes associées à Figma, dossier Problemata 15*

**PROBLEMATA BETA**

**CATEGORIE RESSOURCES**

**---**

**OABC&TB**

**12 et 16 avril 2021**

**---**

**GABARIT PAGE GÉNÉRALE CATÉGORIE RESSOURCES (cf. Figma)**

• trois rangées :

\> \[explorer\] Fonds

\> \[explorer\] Lignes

\> \[explorer\] Ensemble

Fonds

---

des images (prélevées dans la database des derniers ajouts relatifs aux
Fonds)

ces images sont alignées sur une rangées, avec leur titre (NOM DU
FONDS)\* et leur légende simple\*\*

ces images sont dans des rectangles fixes dans lequel l'image est
recadrée)\*\*\* pour garder une idée de thématisation

(\*) le nom **du fonds** peut tenir sur deux lignes, on peut aussi
demander aux éditrices un nom version courte ?

(\*\*) quel est le modèle de rédaction des légendes courtes ?    Où
place-t-on l'auctorialité des images (photographe, auteur d'œuvre...) à
définir avec les Marie et Catherine

(\*\*\*) à voir avec Jérémy et Vincent

Lignes

---

des images (prélevées dans la database des derniers ajouts relatifs aux
Lignes)

ces images sont alignées sur une rangées, avec leur titre (NOM DE LA
LIGNE)\* et leur légende simple\*\*

ces images sont dans des rectangles fixes dans lequel l'image est
recadrée)\*\*\* pour garder une idée de thématisation

(\*) le nom **de la ligne** peut tenir sur deux lignes, on peut aussi
demander aux éditrices un nom version courte ?

(\*\*) quel est le modèle de rédaction des légendes courtes ? Où
place-t-on l'auctorialité des images (photographe, auteur d'œuvre...) à
définir avec Marie et Catherine

(\*\*\*) à voir avec Jérémy et Vincent

Ensemble

---

des images (prélevées dans la database des derniers ajouts relatifs aux
ressources en générale)

ces images sont alignées sur une rangées (en haut, à gauche), avec leur
légende simple\*

ces images et légendes sont comprises dans des rectangles fixes dessinés
par des filets dans lesquels l'image s'affiche sans recadrage, d'où des
variations pour garder une idée de collection.

(\*) quel est le modèle de rédaction des légendes courtes?    Où
place-t-on l'auctorialité des images (photographe, auteur d'œuvre...) à
définir avec Marie et Catherine

**GABARIT PAGE RESSOURCES \> FONDS (cf. Figma)**

• une notule générale qui dit ce qu'est ce fonds (mentions importantes,
etc.)

• l'indication du nombre complet de ressources

• une barre RECHERCHER & FILTER\*

• un gaufrier d'images (classées par date de création ? de la plus
récente à la plus ancienne ?) avec leur légende simple\*\* (images
positionnées en haut à gauche sans recadrage)

• une option du style CHARGER PLUS (cf. le site du Walker art center :
https://walkerart.org/collections/browse )

Filtres (Fonds)

par type (photographie, illustration, etc.)

par date de création (créé en)

par date de mise en ligne (posté le)

(\*) concernant les filtres, nous avons travaillé avec les éléments
actuels pour cette version bêta. Mais des questions se posent déjà pour
savoir si il y a suffisamment de filtres ?

Tout en restant dans le tableau de modèle des données préétablies,
serait-il possible pouvoir chercher par Nom de créateur.trice
(designers, artistes, auteurs) ? Quel nom donné au photographe
d'exposition ? Faut-il mentionner des caractéristiques autres pour type
: maquette, schéma, etc. ?

(\*\*) quel est le modèle de rédaction des légendes courtes?    Où
place-t-on l'auctorialité des images (photographe, auteur d'œuvre...) à
définir avec Marie et Catherine

**GABARIT PAGE RESSOURCES \> LIGNES (cf. Figma)**

• une notule générale qui dit ce qu'est ce fonds (mentions importantes,
etc.)

• l'indication du nombre complet de ressources

• une barre RECHERCHER & FILTER\*

• un gaufrier d'images (classées par date de création ? de la plus
récente à la plus ancienne ?) avec leur légende simple\*\* (images
positionnées en haut à gauche sans recadrage)

• une option du style CHARGER PLUS (cf. le site du Walker art center :
https://walkerart.org/collections/browse )

Filtres (Lignes)

par type (photographie, illustration, etc.)

par date de création (créé en)

par date de mise en ligne (posté le)

(\*) concernant les filtres, nous avons travaillé avec les éléments
actuels pour cette version bêta. Mais des questions se posent déjà pour
savoir si il y a suffisamment de filtres ?

Tout en restant dans le tableau de modèle des données préétablies,
serait-il possible pouvoir chercher par Nom de créateur.trice
(designers, artistes, auteurs) ? Quel nom donné au photographe
d'exposition ? Faut-il mentionner des caractéristiques autres pour type
: maquette, schéma, etc. ?

(\*\*) quel est le modèle de rédaction des légendes courtes?    Où
place-t-on l'auctorialité des images (photographe, auteur d'œuvre...) à
définir avec Marie et Catherine

**GABARIT PAGE RESSOURCES \> ENSEMBLE (cf. Figma)**

• une notule générale qui dit ce qu'est ce fonds (mentions importantes,
etc.)

• l'indication du nombre complet de ressources

• une barre RECHERCHER & FILTER\*

• un gaufrier d'images (classées par date de création ? de la plus
récente à la plus ancienne ?)avec leur légende simple\*\* (images
positionnées en haut à gauche sans recadrage)

• une option du style CHARGER PLUS (CF. le site du Walker art center :
https://walkerart.org/collections/browse )

Filtres (Ensemble)

par type (photographie, illustration, etc.)

par date de création (créé en)

par date de mise en ligne (posté le)

par Ligne

par Fonds

(\*) concernant les filtres, nous avons travaillé avec les éléments
actuels pour cette version bêta. Mais des questions se posent déjà pour
savoir si il y a suffisamment de filtres ?

Tout en restant dans le tableau de modèle des données préétablies,
serait-il possible pouvoir chercher par Nom de créateur.trice
(designers, artistes, auteurs) ? Quel nom donné au photographe
d'exposition ? Faut-il mentionner des caractéristiques autres pour type
: maquette, schéma, etc. ?

(\*\*) quel est le modèle de rédaction des légendes courtes?    Où
place-t-on l'auctorialité des images (photographe, auteur d'œuvre...) à
définir avec Marie et Catherine

**GABARIT PAGE RESSOURCES \> NOTICE (cf. Figma)**

• une image en grand, ferrée en haut à gauche

• son auteur\* son titre, sa date de création

• une suite d'informations

--- type (photographie, schéma, etc.)

--- date de création

--- date de mise en ligne

--- description

--- fonds (cliquable)

--- lignes (cliquable)

--- articles relatifs (cliquable)

--- crédits

--- droits d'usage

(\*) se pose ici la question (et l'importance) du nom d'auteur. Est-ce
l'auteur direct du document ? l'auteur indirect du document (le
photographe d'expo, par exemple) ?

Que se passe-t-il si l'auteur est un photographie hyper connu ? Tout ne
peut pas être réglé dans la rubrique DESCRIPTION

Ce qui pose, de nouveau, la question des types de filtre pour les pages
FONDS, RESSOURCES, ENSEMBLE.

Quel est le modèle de rédaction des légendes courtes?    Où place-t-on
l'auctorialité des images (photographe, auteur d'œuvre...) à définir
avec Marie et Catherine

**cas particulier (lorsque l'on accède à cette page via la page
ARTICLE)**

**GABARIT PAGE RESSOURCES \> NOTICE (cf. Figma)**

• une image en grand, ferrée en haut à gauche

• son auteur\* son titre, sa date de création

• une suite d'informations

--- type (photographie, schéma, etc.)

--- date de création

--- date de mise en ligne

--- description

--- fonds (cliquable)

--- lignes (cliquable)

--- articles relatifs (cliquable)

--- crédits

--- droits d'usage

• supprimer le menu général (menu clavier-gellule) \*\*

(\*) se pose ici la question (et l'importance) du nom d'auteur. Est-ce
l'auteur direct du document ? l'auteur indirect du document (le
photographe d'expo, par exemple) ?

Que se passe-t-il si l'auteur est un photographie hyper connu ? Tout ne
peut pas être réglé dans la rubrique DESCRIPTION

Ce qui pose, de nouveau, la question des types de filtre pour les pages
FONDS, RESSOURCES, ENSEMBLE.

Quel est le modèle de rédaction des légendes courtes?    Où place-t-on
l'auctorialité des images (photographe, auteur d'œuvre...) à définir
avec Marie et Catherine

(\*\*) si le menu général reste lorsque l'on accède à cette page
RESSOURCE \> NOTICE via un ARTICLE, il se peut que le visiteur remonte à
la page générale des RESSOURCES et non de l'ARTICLE lui-même. Cela
pourrait-être assez dérangeant. À discuter avec Jérémy et Vincent pour
éviter de démultiplier les structures.
