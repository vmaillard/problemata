CR 22.10.2021
TB + BD + JdB + VM

La 1
——
Le titre est uniquement cliquable

Version mobile : retirer la légende de l’image afin de ne pas surcharger d’informations et de rendre l’accès cliquable plus simple au développement. Demander la confirmation aux éditrices.

Attention à la hauteur de l’image, il nous faut voir apparaître l’amorce de la 1 suivante afin de susciter l’action de scroller pour accéder aux contenus suivabt. Cette étape sera à réaliser lors de l’intégration des interfaces avec Jérémy et Vincent.


Un article
————
Le volet outil texte pour un article : 
Jérémy proposait de réduire le nombre d’entrées (sommaire, mots clés, abstract, ressources,…) en trouvant des intitulés qui permettraient de les regrouper. À discuter avec les éditrices.
Remarque Brice : L’accès au volet n’est pas si simple que cela, je crains que regrouper ces éléments en de grandes catégories ajoute de la complexité pour l’accès à des informations essentielles à la compréhension des articles. À discuter là aussi avec les éditrices.

Remarques générales
——————————

Bouton Problemata : environ 96px, fournir les éléments en SVG à Jérémy.

Le volet passe au-dessus pour une taille d’écran inférieure à 700px. Sinon, le volet décale les contenus comme sur les maquettes Figma Problemata 21 (est-ce bien cela Jérémy, Vincent ?)

Systématiser les règles des marges pour les textes. Étape à affiner avec Jérémy et Vincent lors de l’intégration des interfaces.

Pour les tableaux/listes (rubrique «articles» ou «une ligne») : 
— mettre un peu plus de marge à côté des +  
— Voici l’ordre d’activation souhaité par Jérémy et Vincent : 
    si l’on clique n’importe où sur une ligne d’un tableau on accède directement à un article, si l’on clique sur le + on déploie le tableau. C’est la seule action possible pour accéder aux informations relatives à l’article.
Remarque de Brice & Thomas :  
— L’action ne semble pas si naturelle à l’usage même si structurellement parlant le choix proposé par Jérémy et Vincent est juste. 
— Voici l’ordre que nous aurions préféré: 
    si l’on clique n’importe où dans une ligne d’un tableau (rubrique «articles» ou «une ligne») on déploie celle-ci. Seule l’action du clique sur le titre de l’article nous permettait d’accéder à l’article concerné directement. Pour nous, cet usage nous paraît très spécifique et s’adresse à des personnes connaissant (plus ou moins) à l’avance le propos de l’article, c’est pourquoi favoriser l’accès directement à l’article nous paraissait peu naturel. À discuter certainement avec les éditrices.