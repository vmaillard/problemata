Réunion Problemata – 4 rue Martel 


TO DO
écrire quelques lignes pour cadre, annoncer le workshop d'octobre


Arborescence
(déploiement des pages du site et de leurs contenus)

A propos 
	présentation
	liste des membres
	expliquer comment on peut ou pas contribuer aux contenus du site (rubrique contribution ??? = non)
	droits d'utilisations (politique de reproduction)
	droits des utilisateurs (rgpd)
	liste des partenaires
		divisible en 3 (obligé = paris saclay, fond = BK, remerciements = jannick)

Evénements
	liste d'événements (approche temporelle) : lancement de livre, journée d'étude, workshop
	(requête JSON vers réseaux sociaux par exemple)

Articles 
	liste de tous les articles
Lignes 
	liste de toutes les lignes
Index
	au même niveau pouvoir consulter article, ligne, auteur, utilisateur, événement


Modèles
(nommage et parti pris éditorial des objets)

Des articles autonomes, sans aucun lien avec une ligne de recherche.
Ignorer les tags = pas de système de tag ?

Ligne
	personne [role=éditeur]
	introduction
	liste d'articles
Article
	date d'ajout de l'article (dd.mm.yyyy)
	date de dernière modification
	date du sujet de l'article (yyyy)
	type de l'article (à définir ensemble)
	titre (+sous-titre)
	statut (published OR not)
	personne / contributeur / rôle
		auteur, traducteur, illustrateur, graphiste, …
		question du sujet dans le rôle d'une personne ?
	abstract (fr/en + langue originale)
	mots-clé	s
		sujet
		matière
		personne morale / physique
		lieu
	articles liés choisis par les éditeurs
	
	mot de l'éditeur : exemple de domius dans l'article de catarina rossi
	corps
		épigraphe
		citation
		référence
		...

	

Ressource
Personne
	requête à wikidata pour remplissage automatique en front
	personne moral (collectif, GRAV, GRAPUS) | personne physique (François Morellet)








Page

Article
	titre (+sous-titre)
	date d'ajout de l'item / mise en ligne [cliquable] [type+year]
	date de publication de l'article [cliquable] [quel échelle pour ce lien (année par position dans la fenêtre, décennie) (exemple : circa 1970)]
	type de l'article [cliquable] (exemple : toutes les entretiens)
	personne [cliquable] : renvoi vers la page de la personne qui liste tous les contenus de cette personne (interne |ligne, article], externe ???) en fonction de ces rôles, et sa biographie
	abstract (multi-lingue)
	lignes [cliquable] : page des lignes parents
	articles liés [carousel] : générée par les mots-clés OU choisis en back-office par les éditeurs
		bouton report pour ne plus jamais affiché un article en particulier
	article précédent / suivant
	bloc ressources


Ressource
	fond unknown ? = provenance non identifiée, avertissement avant la diffusion

Accueil
	focus de l'éditeur sur un champ, un thème, une période en particulier
	



Objectifs

v1 : lire les articles, les lignes, les fiches auteurs
v1.5 : lier les items, autopopulation des données par branchements externes
v2 : multiplication des sorties 
v3 : gérer ses propres contenus


Notes

mardi 22 octobre : journée de workshop à la BK ???


Mots-clés

On a un répertoire déjà de 125 mots-clés (basé sur le dictionnaire de S. Vial)
On doit pouvoir indexer des gens, des matériaux, des mouvements, …
