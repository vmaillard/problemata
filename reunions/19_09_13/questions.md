# Questions pour la réunion 05.09.2019

## Templates / Arborescence

- Quels degrés de titre possibles ?
- Est-ce qu'un sommaire apparaît dans l'article ?
- Est-ce qu'une infobox apparaît dans l'article ?
- Quelles lectures pour les images ?
- Est-ce qu'il y a besoin d'une biographie de l'auteur dans l'article ? ou bien d'une page auteur ?

## Types de croisement

- Les auteurs sont-ils cliquables dans le texte d'un article ?
- Est-ce qu'il intéressant de cliquer sur un auteur pour afficher des auteurs en lien ?
- Quels filtres utiliser ? (types d'item, couleurs, catégories, périodes, lieux, …)
- Quels ordres utilisés ? (alphabet du titre, alphabet de l'auteur, date d'ajout dans la base / date dans les métadonnées de l'item, …)

## Thèmes / Sujets / Catégories

- La page [Ettore Sottsass](http://dbpedia.org/page/Ettore_Sottsass) de DBpedia dans la propriété "subject" contient des thèmes intéressants : "Italian_designers", "Olivetti_people", "Italian_furniture_designers", "1917_births", "2007_deaths", "Italian_architects", "Italian_industrial_designers", "Italian_people_of_Austrian_descent". Est-ce que des thèmes comme cela sont à envisager ?

- Wikipedia liste des catégories associés au mot designer : [https://en.wikipedia.org/wiki/Category:Designers](https://en.wikipedia.org/wiki/Category:Designers). À chaque catégorie, on trouve d'autres sous-catégories, etc.\
On pourrait utiliser des catégories basées sur celles de Wikipedia :

```
People by occupation
├── Architects
│   └── …
├── Designers‎ 
│   ├── …
│   ├── Furniture designers‎
│   │   ├── …
│   │   ├── Italian furniture designers‎
│   │   │   ├── …
│   │   │   └── Ettore Sottsass
│   │   └── …
│   └── …
└── …
```

- Est-ce qu'il est judicieux de s'intéresser au [vocabulaire du design](ressources/vocab_desig_vial.pdf) en cours d'élaboration ?

## Liaison à d'autres bases

- À quelles bases se relier ?
	- [dbpedia](http://dbpedia.org/fct/facet.vsp) / [dbpedia sparql](http://dbpedia.org/sparql/)
	- [wikidata](https://www.wikidata.org/)
	- [viaf](https://viaf.org/)
	- [bnf](https://data.bnf.fr)
	- [yago](https://gate.d5.mpi-inf.mpg.de/webyago3spotlxComp/SvgBrowser/)

## Class d'items

Un item peut se résumer à un type (*class*) et des propriétés (*properties*). À un livre, on peut associer un type *book* et des propriétés / métadonnées spécifiques à la description d'un livre en découlent (*titre*, *auteur*, *date*).

- Quels sont tous les types (*class*) possibles ? Est-ce que la combinaison des types du Dublin Core Type, de la Bibliographic Ontology et Friend of a Friend suffisent ?

Les *class* du Dublin Core Type :

```
dcmitype
├── Collection
├── Dataset
├── Event
├── Image
├── InteractiveResource
├── MovingImage
├── PhysicalObject
├── Service
├── Software
├── Sound
├── StillImage
└── Text
```

Les *class* et les *sous-class* de la Bibliographic Ontology :

```
bibo
├── event:Event
│   ├── :Conference
│   ├── :Hearing
│   ├── :Interview
│   ├── :Performance
│   ├── :PersonalCommunication
│   └── :Workshop
├── terms:Agent
├── :Collection
│   ├── :MultiVolumeBook
│   ├── :Periodical
│   │   ├── :Code
│   │   ├── :CourtReporter
│   │   ├── :Journal
│   │   ├── :Magazine
│   │   └── :Newspaper
│   ├── :Series
│   └── :Website
├── :Document
│   ├── :Article
│   │   └── :AcademicArticle
│   ├── :AudioDocument
│   ├── :AudioVisualDocument
│   │   └── :Film
│   ├── :Book
│   │   └── :Proceedings
│   ├── :CollectedDocument
│   │   ├── :EditedBook
│   │   └── :Issue
│   ├── :DocumentPart
│   │   ├── :BookSection
│   │   │   └── :Chapter
│   │   ├── :Excerpt
│   │   │   └── :Quote
│   │   └── :Slide
│   ├── :Image
│   │   └── :Map
│   ├── :LegalDocument
│   │   ├── :LegalCaseDocument
│   │   │   ├── :Brief
│   │   │   └── :LegalDecision
│   │   └── :Legislation
│   │       ├── :Bill
│   │       └── :Statute
│   ├── :Manual
│   ├── :Manuscript
│   ├── :Note
│   ├── :Patent
│   ├── :PersonalCommunicationDocument
│   │   ├── :Email
│   │   └── :Letter
│   ├── :ReferenceSource
│   ├── :Report
│   ├── :Slideshow
│   ├── :Standard
│   ├── :Thesis
│   └── :Webpage
├── :DocumentStatus
├── :Event
└── :ThesisDegree
```

Les *class* et les *sous-class* de Friend of a Friend :

```
foaf
├── foaf:Agent
│   ├── foaf:Group
│   ├── foaf:Organization
│   └── foaf:Person
├── foaf:Document
│   ├── foaf:Image
│   └── foaf:PersonalProfileDocument
├── foaf:LabelProperty
├── foaf:OnlineAccount
│   ├── foaf:OnlineChatAccount
│   ├── foaf:OnlineEcommerceAccount
│   └── foaf:OnlineGamingAccount
└── foaf:Project
```

Le vocabulaire Schema va très loin dans le précision des *class* et *sous-class* (625 class) : https://schema.org/docs/full.html
De même pour le vocabulaire de DBpedia : http://mappings.dbpedia.org/server/ontology/classes/

## Début de liste des *class*

- foaf:Person
- foaf:Organization

--- 

- dcmitype:Event
- dcmitype:Image
- dcmitype:PhysicalObject
- dcmitype:Sound
- dcmitype:StillImage
- dcmitype:Text

--- 

- bibo:Conference
- bibo:Book
- bibo:Interview
- bibo:Performance
- bibo:MultiVolumeBook
- bibo:Periodical
- bibo:Journal
- bibo:Magazine
- bibo:Newspaper
- bibo:Series
- bibo:Website
- bibo:Article
- bibo:AcademicArticle
- bibo:AudioDocument
- bibo:AudioVisualDocument
- bibo:Film
- bibo:Book
- bibo:CollectedDocument
- bibo:EditedBook
- bibo:Issue
- bibo:DocumentPart
- bibo:BookSection
- bibo:Chapter
- bibo:Excerpt
- bibo:Quote
- bibo:Image
- bibo:Map
- bibo:Manual
- bibo:Manuscript
- bibo:Note
- bibo:Patent
- bibo:PersonalCommunicationDocument
- bibo:Email
- bibo:Letter
- bibo:ReferenceSource
- bibo:Report
- bibo:Standard
- bibo:Thesis
- bibo:Webpage

## Propriétés d'items

- Quelles sont les propriétés obligatoires pour chaque type de ressource ? (titre, auteur, date, lieu, …)
- Quelles sont les propriétes complémentaires ? (durée, date de début, date de fin, rôle, version de, réédition de, nombre d'œuvres …)

### Rôles des auteurs / contributeurs 

- Est-ce qu'il est souhaitable de décrire précisément le rôle des auteurs / contributeurs d'un item ? L'extrême serait d'utiliser [MARC Relators](http://id.loc.gov/vocabulary/relators.html). La [GND Ontology](https://d-nb.info/standards/elementset/gnd) comporte quant à elle quelques propriétés qui affinent les propriétés 'auteur'.

## Bibliographie

- Est-ce que la bibliographie est une succession d'items référencés dans la base Problemata ? L'idée est de pouvoir la générer à la volée suivant différentes normes.

