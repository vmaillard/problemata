 
PROBLEMATA

séance travail équipe Design \| Développement \| Édition

le 17.02.21

Relevé de décisions



[Présents : ]{.ul}

Thomas Bouville Catherine Geel

Lorette Collard Marie Lejault

Jérémy De Barros Vincent Maillard

Brice Domingues  



**État d'avancement édition**

[Prochaines publications sur Problemata : ]{.ul}

1 ligne Bibliothèque de textes pour lancement avril 2022 (40 textes,
certains présentés avec leur pdf= ressource)

1 collection As Long avec quelques ch. du livre P. Sparke pour mai 2022

1 collection Radical Notes avril 2022



**Design article / gabarit notules**

Le gabarit « notule » associé à un texte n'a pas été dessiné.

En vue de la publication de la ligne Bibliothèque, il a été convenu :

Si les chercheurs ne souhaitent pas faire de notules, le texte
« fondamental » est intégré sous forme d'article, sans notule associé.
Les informations contextuelles + mention du nom du chercheur qui a
recommandé l'article figureront dans l'abstract.

À l'issue de la séance Omeka du 17.02.22 au soir, difficile pour
certains chercheurs de ne pas mettre de notules. Proposition temporaire
de T&P : faire apparaître la notule à la suite du texte, avec
l'intitulé : « Commentaire par XXX ».



\[BD&TB :  En attendant de trouver une mise en forme adaptée pour les
notules et pour éviter toutes confusions et mauvaise interprétation :
Placer les notules en fin d'article avec la même stylisation du texte
principal.



Pour cette micro mise en forme d'attente des notules, on vous propose
une stylisation qui n'ajoute rien aux classes déjà créées. Cf. Figma
Problemata 21. À valider par CG/MJ & JM/VM



Attention aux confusion sur les notes de bas de page entre celle
associées au texte fondamental et aux notules. notamment si les auteurs
et autrices en utilisent --- faut-il les différencier ou les mettre à la
suite? --- L'option 2 semble la plus simple . Idem pour les numéros de
paragraphe, on peut aussi continuer la numérotation, c'est aussi la
solution la plus simple.



Par ailleurs et comme les mentions complémentaires, les notules devront
être intégrées et stylisées dans un temps 2 pour que tout s'enchaîne
convenablement. Qu'en pensez-vous ?



Les mentions complémentaires qui sont actuellement renseignées dans les
métadonnées (traducteur, contributeur, source) trouveront leur place
dans un temps 2. En attendant, elles figurent dans l'abstract.







**Article essai visuel**

L'essai visuel est un pdf (ressource) introduit par un court texte.



**Article multimédia**

Pour le pdf : on a une image (=
1^ère\ page)\ du\ pdf\ dans\ l'onglet\ des\ ressources\ en\ vis-à-vis\ de\ l'article.\ Possible\ de\ cliquer\ sur\ « lire\ le\ pdf »\ \>\ un\ nouvel\ onglet\ s'ouvre\ avec\ le\ lecteur\ de\ pdf\ du\ navigateur.\ ^

^Taille\ max\ du\ pdf :\ pas\ plus\ de\ 10\ Mo\ (à\ confirmer\ par\ VM\ et\ JDB)^

^\ Pour\ le\ lecteur\ audio :\ gloups,\ j'ai\ un\ trou...a-t-on\ statué ?\ ^

^\ Pour\ le\ lecteur\ vidéo :\ gloups,\ j'ai\ un\ trou...\ a-t-on\ statué ?^



\[BD & TB : Pour l'image, ce sera aux auteurs, autrice ou bien aux
éditrice de fournir les images. On note que les formats d'images peuvent
varie.

Pour les audio et vidéo, on remet cela au temps 2 de la mise à jour de
la Plateforme.\]



**La Une**:

Doit être retravaillée suite aux remarques CG+ML fin décembre. (la
rendre plus attractive visuellement)

Remise fichier validée par officeabc à JDB et VM au plus tard à le 15
mars.

\[BD & TB : OK\]



**Session Markdown :**

Journée Markdown avec JDB/VM/CG/LC/ ML et SG : à organiser début semaine
du 14 mars.

En attente propal de date par JdB et VM



**LOGOS**

Intégration des logos vue. Passer les logos en N&B + regroupement, ajout
et suppression tel qu'indiqué dans le mail de ML à VM et JDB le
18.02.22.

Pour la semaine du 21.02.22

Modifications dans textes etc. peuvent être faites par ML directement
dans Django \> prévenir JDB et VM avant/après ;



**Page RESSOURCES**

Validation de la page en l'état

\[BD & TB : OK\]



**RECAP planning lancement**

19.04 \> lancement en avant première auprès des partenaires (en visio)

21.04 \> lancement à ENS Ulm (dans le cadre séminaire Claude Imbert avec
C. Geel et C. Brunet)

29.04> lancement au Musée des arts déco

Le 18.05 \> lancement à St Etienne.



**DATAVIZ CCI**

Présentation des premières pistes de Datavisulaisation proposées par
Emile Greis pour la ligne CCI. Une présentation auprès des chercheurs de
Problemata aura lieu le 4.03.



Une séance de travail pour son intégration s'organise en mars avec E.
Greis, JDB, VM et CG/ML/LC.

\[BD & TB : Pouvons-nous être présents pour cette session de travail?
 Cela nous intéresse notamment sur les questions d'intégrations et les
enjeux de structuration de la Plateforme.\]



**SUIVI D'INTEGRATION \[OABC + TB + JB + VM\] pour une plateforme
optimisée pour le lancement aux arts décoratifs.**



**--- Vérification et discussions sur le travail en cours des
intégrations**

** • tailles de corps**

** • gestion des filets**

** • gestion des marges**

** • responsive**

** • longueurs de ligne**

** • gestion des couleurs**

** • gestion des intéractions**

