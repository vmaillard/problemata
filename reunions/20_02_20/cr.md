# Compte rendu réunion 20.02.20

avec : bd, cg, ml, vm
sur : skype

## FNSO

- avancer sur le cahier des charges pr qu'il soit une annexe importante au dossier
- cf. calendrier
- penser le budget et les échéances sur les 2 ans à venir

- penser le futur de Problemata :
    - UMS ?
    - quels sont les services proposés ?
    - formation à l'écriture ?

## tableau Article

**dcterms:mediator**

- enlever le symbole ⬙ de la fonctionnalité (présent seulement sur la "carte id" d'un Article)

**mots-clés**

- discussion autour de la nécessité de pouvoir séparer mots-clés contrôlés, mots-clés libres, personnes physiques, personnes morales et couverture
- en front : un click sur un mot-clé permet d'avoir une page avec les articles qui ont le même mot-clé

Décision :

Ce qui est visible en front :
- mots clés (libres et controlés)
- noms cités (physiques et morales)
- couverture (spatiale et temporelle)

Équivalent rdf :
- dcterms:subject
- dcterms:references
- dcterms:temporal + dcterms:coverage

**langues**

- Problemata peut accueillir des textes en espagnol et portugais

Décision :
- ajout de "es" et "pt" au voc contrôlé des langues

**dates**

- discussion autour de la manière de faire figurer les dates en front et ce qu'il doit se passer en filtre dans le back
- siècles : en front : on voit des des chiffres romains. Mais en métadonnées, il faut renseigner des chiffres (pour le tri et les filtres)
- on veut que le filtre "XV^e^" soit capable de retourner les dates : "XV^e^" comme "1492"
- métadonnées : quand on veut dire : "XV^e^" on écrit "1500s".
- il faudra donc mettre en place un filtre front pour que les dates digitales de siècles s'affichent en capitales romaines : "1500s" => "XV^e^"

Résumé :
- filtre "2010" = toutes dates entre 2010 et 2019 compris. Valeur du filtre : "^201" (intervalle [2010, 2019])
- filtre "XV^e^" = toutes dates dans le quinzième siècle. Valeur du filtre : "^14" (intervalle [1400, 1499])

**Publication originale**

Pour l'instant : dcterms:issued est utilisé mais le range attentu est une date alors qu'on souhaite stockée une réf bibliographique.

Proposition : 
- dcterms:isFormatOf | Publication originale

**dcterms:hasVersion / dcterms:isVersionOf**

- est-il possible que les ressources en lien soient de type Resource (des docs pdf par exemple) ?
- ce qui pourrait nécessiter un "dcterms:language" dans le tableau de Resource
=> ça pose un problème pour déterminer la version source d'un article. Dans la boucle Article, comment mettre en 1er un article qui est en fait une ressource ?

**dcterms:provenance**

Dans le tableau envoyé par ml suite à la réunion : "dcterms:provenance" est barré car recouperait "Source (référence de l’édition)"

Attention car "provenance" vient définir le lieu de conservation, ou un fonds, comme c'est le cas pour les Resource. Ce n'est pas la même chose que "Source (référence de l’édition)" qui est une réf bibio. À mon avis, il faut bien conserver cette propriété.


## avec officeabc

- réaliser des minitatures de pages avec les rdf
- avancer sur un organigramme complet du site (notamment % hyperliens)



## Calendrier 

**FNSO**

- 28.02 : remise d’un budget provisoire pour les 2 prochaines années Problemata (+ remise texte scindé pour réponse succincte point par point des diff. critères)
- 10.03 : remise du devis associé
- 15/20 mars : envoi du dossier FNSO pour relecture active de votre part.
- 31 mars midi : date limite de dépôt de dossier FNSO

**SESSION DE TRAVAIL AU VERT**

- Proposition pour session au couvent de la Tourette — proposition de date : 15 / 16 / 17 avril voir un jour en plus en moins

**SESSION TRAVAIL RÉUNION PARTENAIRES**

- 28 avril : réunion travail avec chercheurs actifs sur tableau métadonnées
- 29 mai PM : réunion CS : idéalement avec S. Pouyllau / et Jérémy et Vincent + présentation des possibles lignes de recherche recensée dans archives partenaires. (amphi art déco sûrement)