# CR réunion 2020.01.30

avec : officeabc, cg, ml, vm

chez : bd

objet : point sur les dernières réunion éditoriale, échange sur les maquettes / modèles de données / vocab contrôlés

## Points sur des rdv précédents

- cg et ml ont pu discuter avec le CNAP et sa directrice Béatrice Salmon, trjs soutien du CNAP, peut-être qu'un membre sera présent le 20.02.11
- la présence de L. Maurel est à confirmer pour 20.02.11
- Le Signe (Chaumont) n'a pas encore confirmer une présence le 20.02.11
- cg, ml, Karine Bomel, Laurence Mauderli et Jérôme Pasquet se sont réunis le 20.01.28 pour faire le point sur les vocabulaires contrôlés


## resource:Article

**dcterms:created** et **dcterms:date**

Discussion sur la colonne "date de création" dans le tableau listant tous les articles afin de lever la confusion entre `dcterms:created` et `dcterms:date`.

- `dcterms:created` => Date de création (si ≠ dcterms:date : édition tardive)
- `dcterms:date` => Date d'édition / publication de la ressource source à la ressource décrite

Décision :
- la colonne du tableau correspond `dcterms:created`
- si la date de création (`dcterms:created`) est inconnue => on remplit `dcterms:created` à partir de la date de publication (`dcterms:date`). `dcterms:created` et `dcterms:date` ont alors la même valeur. Cela pour éviter un champ `dcterms:created` vide au moment de filtrer ou trier les ressources Article.

**dcterms:description** et **dcterms:abstract**

- à revoir dans le modèle de données la différence entre `dcterms:description` et `dcterms:abstract` => à voir avec Marie pour les titres et définitions de ces propriétés + avec officeabc pour voir où ces propriétés apparaisssent dans les pages

Décision :
- décrire titre et définition pour `dcterms:description` et `dcterms:abstract`

**Sur une page Article, click sur le nom de l'auteur, de la ligne, de la mise en ligne**

- ici décision de simplification : il est pertinent de donner la possiblité de cliquer sur le nom de l'auteur ou de la ligne d'un article pour arriver sur une nouvelle page en relation. Mais moins pour le Type, et encore moins pour "Mise en ligne" et "Date de création"

Décision: 
- suppression click sur "Type, "Mise en ligne" et "Date de création"
- conservation du click sur "Ligne" et "Auteur" (sur leurs valeurs)

**À discuter**

- click sur nom d'auteur : tableau des auteurs filtrés ou page de l'auteur ? Idem pour le click sur le nom de la ligne : vers tableau des ligne filtré ou page de la ligne ?
- garder repérable l'article dont l'on vient dans la nouvelle page atteinte ?

Cette discussion vaut aussi pour une page de Resource.


## resource:Resource

**Date de mise en ligne**

- la date de mise en ligne est bien à conserver en tant que filtre et donc à ajouter au modèle de données

**Dates de création**

- ce point est abordé dans la partie "Tableau listant des ressources" (par rapport aux valeurs de l'input select)

**Titre / légende / description**

- suppression du champ Auteur et Description
- conservation du champ Titre `dcterms:title` et Abstract `dcterms:abstract`

Le titre est incité à contenir des informations sur qui réalise le document décrit. ex : "Hélène Baumann « Jean Prouvé industriel du bâtiment »". L'abstract permet une contextualisation historique, des précisions sur le contenu représenté et les auteurs, des précisions de médium et format.

**Alignement Fonds et Crédits**

- Fonds : `dcterms:provenance` ("Fonds Jean Prouvé, bibliothèque Kandinsky" url : l'url du portail)
- Crédits : `dcterms:rights` (copier coller de ce que donne le fonds)


## Champ recherche : gelule

Les champs recherche sont filtrés selon le type de ressource choisi. Si Article est choisi, la recherche se fait dans les ressources Article.

Il est possible également de restreindre la recherche suivant certaines propriétés : `dcterms:creator`, `dcterms:mediator`, `dcterms:subject`, `dcterms:abstract`.

La recherche se base sur la recherche Omeka S. Il peut y avoir donc y avoir du bruit, de la difficulté à combiner un rapport logique entre plusieurs termes.

**Décision :**
- on filtre par type de ressource
- pour l'instant, on ne filtre pas par propriétés : on fait du full search => à revoir avec des données si besoin d'ajouter des filtres plus restreints


## Tableau listant des ressources

**Nombre de résultats**

- à la manière du [Walker](https://walkerart.org/collections/browse), il paraît pertinent d'afficher le nombre de résultats disponibles à la requête en cours

Décision :
- regarder pour afficher le nombre de résultats d'une requête


**Tri des colonnes : une requête à la base ou sur la portion de résultats affichés ?**

- explications de potentiels bizarrerie quant au tri en js des colonnes des tableaux (on ne peut obtenir les ressources aux plus ancienne dates de création que si on ruse un peu). On va donc faire des requêtes à la base pour le tri.

Un des problèmes qui va se poser sera la division des lignes dans le cas de multi-auteurs : Omeka trie les résultats en prenant la première valeur d'un champ. Ainsi un article dont le nom du premier auteur commence par un "A" arrivera dans les premiers résulats, accompagné du second auteur qui pourrait commencer par "Z". Pour l'instant, on va simplement concaténer les résultats multiples.

Décision :
- le tri des colonnes fait appel à la base

**Input select "Date de création" pour le tableau de Resource**

Il a été convenu de travailler par décennies / par vingt ans. 

La liste de décénnies serait celle-ci :
- 2020
- à
- 1800
- XVIII
- XVII
- XVI
- XV
- XIV

Chaque valeur est un intervalle. Omeka ne permet pas le filtre par intervalles. Il faudra sûrement faire une requête comme ceci : `dcterms:created *= "199"` pour avoir les résultats de la décennie 1990.

Décision :
=> à préciser avec ce qui a vraiment été décidé au final


**Filtre et tri par défaut**


| Resource | Filtre par défaut                                 | Tri par défaut                                             |
|----------|---------------------------------------------------|------------------------------------------------------------|
| Line     | Ø                                                 | Alphabétique ascendant / Titre / dcterms:title             |
| Article  | "Mise en ligne" : "2000-2020" / dcterms:submitted | Numérique descendant / "Mise en ligne" / dcterms:submitted |
| Resource | "Mise en ligne" : "2000-2020" / dcterms:submitted | Numérique descendant / "Mise en ligne" / dcterms:submitted |
| Agent    | ?                                                 | ?                                                          |



## Schémas Omeka <-> Django

[Lien vers une version en cours de réalisation pour la présentation du 20.02.11](schemas_so_far_update.pdf)

Très bien. À voir si l'on remplace ou produit une définition pour : **get**, **post** et **view**.



## Vocabulaires contrôlés

**Types d'articles**

| fr                    | en           |
|-----------------------|--------------|
| Entretien             | Interview    |
| Communication         |              |
| Extrait de livre      | Book section |
| Fiction / Non fiction |              |
| Mémoire / thèse       |              |
| Essai                 |              |
| Autres                |              |

**Types de ressources**

On va simplifier les class `dctype:StillImage`, `dctype:MovingImage`, `dctype:Sound` pour simplement `bibo:Document`.

Les types de ressources sont à renseigner dans le champ `dcterms:Type`. Ils peuvent être multiples mais sont issus du vocabulaire contrôlé.

## Point sur les maquettes et coordination officeabc, jdb, vm

- proposition pour séparer les maquettes de la recherche de structure. càd que les hypothèses graphiques n'ont pas forcément à être présentes pour parler de structure => il est possible de travailler avec de simples miniatures de page, très schématique pour pouvoir valider ensemble (ex: la gelule qui peut se résumer à qqs hyperliens de navigation, liés à des champs texte pour la recherche, des champs filtres et tris pour les tableaux mais qui graphiquement évoque plus de choses).
- prévoir de "surtitrer" les gabarits de page avec les propriétés du modèle de données ("Titre" => "dcterms:title")
- importance de penser au responsive : proposition de quatre largeurs d'écran : 476px (téléphone), 768px (tablette), 1300px (laptop), 1800px (desktop)


## Points à préciser

- valeurs de l'input select mise en ligne pour les ressources
- tableau des filtres et tris par défaut
- click sur nom d'auteur : tableau des auteurs filtrés ou page de l'auteur ? Idem pour le click sur le nom de la ligne : vers tableau des ligne filtré ou page de la ligne ?
- click sur nom d'auteur : garder repérable l'article dont l'on vient dans la nouvelle page atteinte ?
- avoir le doc sur les vocs contrôlés pour pouvoir les ajouter au modèle de données

A prévoir :
- rdv avec ml (notamment) pour aligner les fichiers tableaux avec les métadonnées (avec des titres et définitions validées)
