**PROBLEMATA BETA**

**Retours éditrices**

1.  **GÉNÉRAL**

• Pas de traduction anglaise des colonnes et filtres.

• Pb avec le texte importé depuis omeka car pas d'enrichissements typo.

• Cela est particulièrement gênant pour :

\- abstract / intro de ligne (les ital des expos par ex. disparaissent)

• Les ital ne fonctionnent pas (autre typo plus petite а date)

• les gellules supérieures ne marchent pas bien (le 16.05.21) et
changent d'apparence (carrés left right ½) \> impossible de s'en servir
en l'état).

2.  **LA UNE**

• Faire des images pleine largeur (cf figma 19) : (cadrages ne sont pas
bons / crédit photo trop gros / etc. La mise en forme figma 19 nous
convient)

• Les ital ne fonctionnent pas.

• Texte Pbta modifier et remplacer par

*Problemata* est une plateforme de recherche dédiée à l'histoire du
design et ses études critiques.

*(Nous pouvons nous charger de la modif si nous avons accès au pandoc)*

• Dans les compteurs : on garde uniquement :

N lignes

N articles

N ressources

[Dans le détail :]{.ul}

**• Pour CCI une intro exposée** :

-   les 2 affiches ne sont pas alignées (si poss avoir la même taille
    pour 2 affiches)

-   renvoyer au texte intro \[ID 525\] de Catherine

-   changer l'affiche Sol et mur par celle dans le dossier sans
    l'écriture : « www.placedesarts.com ».

**• Le CCI et le Centre Pompidou**

-   Lien vers archives BK ne fonctionne pas

**• Les illustres inconnues**

-   on change le titre par : Les inconnues du CCI

-   Changer le texte :

> *Bunker Archéologie* (1975), *Les Immatériaux* (1985) sont des
> expositions célèbres. Qui sait qu'elles furent portées par le CCI ?
> Voir les images de la collection du site.

-   Renvoyer vers : La collection CCI de Problemata

**• Plongée dans les péripéties d'une expo inaugurale**

-   cadrage paysage

3.  **LA LIGNE**

[Général]{.ul}

• modifier le texte de présentation générique d'une ligne :

Une ligne répercute une recherche placée sous la responsabilité
scientifique des chercheurs réunis à cette occasion et en rassemble les
contributions.

Attention : le nombre d'articles a changé car il y avait un doublon.

Comment rendre lisible / visible l'article d'intro de la ligne ? \>
faire un type spécifique intro ? Comment la faire apparaître en haut du
tableau par défaut ?

[Détail :]{.ul}

• Le texte de présentation est la description courte et non l'abstract
plus développé (renseigné dans OmekaS).

• Tableau articles dans ligne : l'accès au article n'est pas du tout
intuitif : obligation de développer l'article pour pouvoir le lire en
cliquant sur FR !

• NOTE POUR PLUS TARD : L'apparition et la disparition du développé
article : le signe + et le signe - ne sont pas au même endroit, c'est un
peu « étrange » et peu intuitif pour refermer.

4.  **ARTICLE**

[Général]{.ul}

RAS

[Détail]{.ul}

• La légende des figures : N° de figure = n° des ITEMS

• Espacement différent entre légende et bas de l'image

• Légende des images : provient-elle d'Omeka ou du Markdown (si Omeka pb
d'enrichissement ?

• Lorsque l'on clique sur l'image : pas de renvoi vers la ressource
détaillée.

• Les images peuvent-être téléchargées / enregistrées sous (bloquer
cela)

• Lorsque volet image déplié : le résumé : pas très clair il s'agit du
résumé. On ne sait pas où commence l'article, sauf lorsque
« introduction » a été conservé (par erreur !!)

• Le sous-titre devient une ligne en soi est n'apparaît pas comme un
sous titre.

Ou le titre est coupé (texte de Laurence par ex.)

• Appels de note : en chiffre arabes / Notes en chiffres romains... :
système de renvoi pas tjrs good (se cache sous la barre gellule : Jérémy
semblait connaître la solution)

• La bibliographie apparaît avant les notes : c'est un peu étrange (à
régler dans un second temps)

• clic sur mot clé renvoie à une page d'erreur

• n° de § : pour certains articles, il s'arrête au cours de l'article.
La numérotation automatique génère des numérotations étranges. Comment
forcer certaines numérotations (ou empêcher la numérotation de lignes) ?

5.  **RESSOURCES**

[Page ressource globale]{.ul} :

• texte de présentation à changer :

Les ressources (images, documents, revues etc.) ont des origines
variées. On peut les consulter par institutions partenaires ou les
explorer dans leur ensemble. Mais elles s'organisent principalement en
collections. Une collection est un choix de ressources provenant d'un ou
plusieurs ensemble (fonds ou archives institutionnelle, privée et autres
origines) mais peut aussi être constituée de plusieurs chapitres d'un
ouvrage par exemple. La particularité des collections publiées par
Problemata est leur éditorialisation. Elle a lieu par ligne de recherche
ou par constitution autonome (pour une collection de textes de revues ou
l'ensemble d'une revue par ex.).

• centrer les ressources dans la case (format portrait ?)

[Page ressource détail :]{.ul}

• Dans les filtres : pour les types ressource, il faut suppr.
« entretien et communication » ce st des types article // idem en
anglais)

• Cote : attention la cote correspond а ce qui a été renseigné dans
OMEKA S pour  bibo:locator (= nom du fichier média) et non à « dcterms:
identifier (= cote)

• Dcterm publisher ?

• Droits d'usage : pour l'instant non réglé. SI automatisation poss dès
r

• Documents écrits composés de plusieurs pages : étrange d'avoir 1
ressource par page (courrier etc.) en attendant de trouver une solution,
nous renseignons le n° de page dans Omeka S.

• image en doublon / \> suppression

6.  **AUTEURS :**

Général : on voit apparaître la date de naissance (jour / mois année) \>
n'y a t il pas un filtre par décennie ?

7.  **À PROPOS :**

• Présentation :

-   « Objets éditoriaux » : faire passer avant « la recherche ouverte »
    (cf markdown)

-   Suppression de Stéphanie Geel (aussi faite dans le markdown)

-   Manque le texte : recherche graphique

-   Manque le texte : partenaires et financements (hyper important pour
    la Beta !!)

-   Manque texte et schémas interopérabilités
