**CR / Relevé de décisions PROBLEMATA**

Réunions du 5.03.21

Présentation avancées graphiques (9h -- 10h)

Réunion de travail Fonds / Ressources / Beta (14h30 -- 17h30)

Brice Domingues Vincent Maillard Jérôme Pasquet

Catherine Guiral Catherine Geel (Karine Bomel : absente / connexion
défectueuse)

Thomas Bouvil Marie Lejault

**Rappel et remise en perspective**

Le projet *Problemata* a débuté en janvier 2019.

[Au 1^er^ mars 2021, 65 000 euros]{.ul} ont été investis dans le
développement de la plateforme.

Graphistes / développeurs et éditeurs ont touché les mêmes honoraires.

(sauf Catherine Geel qui n'est pas payée pour ce projet).

Au 20 mai 2021, une version Beta sera présentée aux partenaires et
membres du CS.

À date, la version telle qu'annoncée vendredi 5 mars, ne peut être
considérée comme une version fonctionnelle.

-   Ceci est complexe à défendre et présenter aux partenaires du projet
    auxquels Catherine (chercheuse) et Marie (T&P et seced) s'adressent
    quasiment tout le temps toutes seules.

**Il importe donc de pouvoir établir un calendrier précis** des
prochaines versions et fonctionnalités associées, afin d'annoncer des
échéances réalistes aux partenaires actuels et à venir, ainsi qu'aux
chercheurs qui commencent le travail d'élaboration de lignes.

[Nous soulignons que nous sommes sur la version beta et que ces
livrables en font partie.]{.ul}

\@officeabc + T&P : calibrage textes courts des lignes à établir.

\@T&P : transmission des éléments de contenus de l'à propos : merci de
compléter / réagir.

L'à-propos sera traduit via *DeepL Pro* car sujet à des évolutions
régulières.

1.  **[Présentation du 5 mars 21 / Point]{.ul}**

• Principes de menu / pages ligne générale, développée et détaillée /

-   Déployée pour laptop / tablette et smartphone.

```{=html}
<!-- -->
```
-   Les éléments présentés sont validés dans leur ensemble du point de
    vue éditorial.\
    Points de développement à préciser avec VM et JdB (cf. CR rédigé par
    VM).

Remise des gabarits le 19.04.21 de abc aux dév.

-   Non présentées : la une / les pages auteurs (liste et détail) / les
    pages articles (liste/ développée/ détail) / les pages ressources
    (général / détail).

\@officeabc/VM et JdB : Sous quelles formes seront présentées ces
différentes pages ?

T&P souligne que les bios des auteurs sont prêtes et calibrées. Ce n'est
plus le peine de les demander.

T&P souligne que tous les calibrages (abstracts simplifiés, titres
articles, auteurs, biographies, intro ligne,

notion de ligne, nbre de signe pour les textes, et tout le bilinguisme)
ont été donnés le 18 janvier. Ce n'est plus la peine de le demander.

T&P souhaite recevoir les documents de présentation.

2.  **[Version Beta : Contenu, fonctionnalités, formes et
    calendrier]{.ul}**

Il semble problématique de ne pas livrer les articles mis en page dans
le gabarit qui serait final.

-   DEV : Les typographies redessinées ne seront pas intégrées (car
    temps important pour le calibrage typographique)

-   REMETTRE LA MAQUETTE SUR L'ÉCHEVEAU APRES MARKDOWN POUR LES DEV. ET
    LA COMPOSITRICE N'EST NI BUDGÉTÉ, NI PROVISIONNÉ.

Pour T&P, le CR du 18.12.21 reste la feuille de route des rendus /
fonctionnalités de la Beta du 20.05.21. T&P s'interroge donc sur la
question de la mise en page de l'article qui a toujours été désigné
comme le cœur de la plateforme.

Pour rappel, en termes de **design graphique** :

• Les articles / textes seront intégrés de façon brute suivant les
gabarits de Yeelena :

-   L'opérateur propose donc que le Spectral d'origine soit utilisé en
    attendant

-   Non acceptation de la solution pdf actée en réunion.

• Les développeurs mettront en place un site web navigable dans son
entièreté par zones fonctionnelles et au design brutaliste, intégration
des grandes variables graphiques disponibles

• Variables : jeu de couleurs, marges externes et internes, ligne de
base et corps typographiques suivant \[tout ce qui se règle de manière
systémique et qui ont des relations d'interdépendance\] (pour ce qui est
des réglages typographiques, pour plus de précision, et indépendamment
de *figma*, nous utiliserons une approche optique plutôt que numérique
lors de l'intégration)

• La version Beta sera déployée suivant logique responsive (desktop,
mobile et tablette)

• maquettes réglées du design graphique sur l\'entièreté du site (dépend
des précisions sur La 1 et les ressources) avec les typographies
redessinées (en exports depuis figma). Le principe graphique des
maquettes sera intégré avec un niveau de précision très relatif comparé
à la V1.

• Ossature de site navigable avec intégration des lignes structurelles
de design pour permettre de valider les principes de navigation.

• Seront désignés : le menu + code couleur / les filtres / les pages
statiques (header) et les pages de détail (lignes, auteurs, ressources).
Question pour la Une ?

**[Contenu Beta :]{.ul}**

[Ligne CCI]{.ul}

1 texte intro (relecture à faire)

5 à 6 articles chercheurs (relectures à faire)

15 articles étudiants (relectures OK)

5 traductions anglaises articles (3 traductions OK et relues)

Biographies des auteurs (en cours rédaction)

Env. 200 images

[Contenu Site]{.ul}

À propos FR/ENG (90% rédigé)

La Une : ? \> L'ensemble sera disponible au plus tard à la mi-avril.

@ VM et JdB : confirmation pour renseigner les métadonnées suivant [le
formulaire Omeka
S.](http://problemata.huma-num.fr/omeka_beta/login) (D'après notice
Omeka S envoyée en janvier) \> depuis janvier, des éléments semblent
avoir évolués (vocabulaire contrôlé des ressources, apparition de Base
ressources, etc.). Prévoir un temps T&P / VM pour balayer ensemble le
formulaire et s'assurer d'une écriture des métadonnées relativement
intuitive.

\@JdB et VM : confirmation possibilité d'utiliser [l'outil du workshop
pour écriture en
MarkDown](http://problemata.huma-num.fr/ws/workshop-markdown-pandoc/?page=index)
des textes CCI. / quid des images : ressources associées ? Les intègre
-t-on dans le même temps ?

[Warning de T&P]{.ul} : chaque intégration est payée : il ne faut pas
doublonner les intégrations entre Beta et V1.

**[Calendrier vers la Beta ]{.ul}**

+-------------+-------------+-------------------+------------+--------+
|             | **[Abc/     | **[T&P /          | **[De      | *      |
|             | Yel         | c                 | v.]{.ul}** | *[comm |
|             | ena]{.ul}** | hercheurs]{.ul}** |            | ents]{ |
|             |             |                   |            | .ul}** |
+=============+=============+===================+============+========+
| **[16       | *           |                   |            |        |
| .03]{.ul}** | *[Livraison |                   |            |        |
|             | spect       |                   |            |        |
|             | ral]{.ul}** |                   |            |        |
+-------------+-------------+-------------------+------------+--------+
| **[16.03-12 |             | > Prépa-copies    |            |        |
| .04]{.ul}** |             | > ligne CCI       |            |        |
+-------------+-------------+-------------------+------------+--------+
| **[9-20     |             | Intégration des   |            |        |
| .04]{.ul}** |             | contenus /        |            |        |
|             |             | métadonnées /     |            |        |
|             |             | etc. À préciser   |            |        |
|             |             | en amont avec VM  |            |        |
|             |             | et JDB session /  |            |        |
|             |             | travail pour      |            |        |
|             |             | intégrations      |            |        |
+-------------+-------------+-------------------+------------+--------+
| **[14       |             | **[Validation     |            |        |
| .04]{.ul}** |             | gaba              |            |        |
|             |             | rits ????]{.ul}** |            |        |
+-------------+-------------+-------------------+------------+--------+
| **[19       | Livraison   |                   |            |        |
| .04]{.ul}** | des         |                   |            |        |
|             | gabarits    |                   |            |        |
|             | design      |                   |            |        |
|             | définitifs  |                   |            |        |
|             | pour        |                   |            |        |
|             | dév         |                   |            |        |
|             | eloppement. |                   |            |        |
+-------------+-------------+-------------------+------------+--------+
| **[20.04-10 |             |                   | > Dév      |        |
| .05]{.ul}** |             |                   | eloppement |        |
|             |             |                   | > /        |        |
|             |             |                   | > i        |        |
|             |             |                   | ntégration |        |
|             |             |                   | > /        |        |
|             |             |                   | > (dé      |        |
|             |             |                   | buggage ?) |        |
+-------------+-------------+-------------------+------------+--------+
| **[10       | *           |                   |            |        |
| .05]{.ul}** | *[LIVRAISON |                   |            |        |
|             | DE LA BETA  |                   |            |        |
|             | A L'EQUIPE  |                   |            |        |
|             | ACTIVE POUR |                   |            |        |
|             | PRISE EN    |                   |            |        |
|             | M           |                   |            |        |
|             | AIN]{.ul}** |                   |            |        |
+-------------+-------------+-------------------+------------+--------+
| **[OCT.     | *           |                   |            |        |
| 21]{.ul}**  | *[LIVRAISON |                   |            |        |
|             | V1]{.ul}**  |                   |            |        |
+-------------+-------------+-------------------+------------+--------+

20 mai : présentation de la Beta en CS

**3• Réunion Fonds / Ressources**

Sur la base des exemples présentés par T&P : 4 typologies de
fonds (Revues / expositions / ressources / Ouvrage ou ensemble de
chapitres exclusifs)

• Plusieurs pistes sont évoquées (cf CR de Vincent Maillard), à l'issue
de la réunion, les choix s'orientent vers des Fonds intégrés dans la
rubrique (objet éditorial) Ressources.

[En termes d'enjeu éditorial]{.ul} : possible à condition que les fonds
soient très clairement / immédiatement rendus lisibles sur la page
ressources pour les chercheurs (publics du site) et les partenaires. La
Une permet de mettre régulièrement en lumière les fonds disponibles sur
Problemata, mais cet appel ne suffit pas. Pour T&P, l'option d'une
rubrique (objet éditorial) Fonds au côté d'articles / lignes /
ressources et auteurs permettait de répondre très clairement à cela.

\+ Structuration / organisation des contenus entre ressources et fonds
et « sous-fonds).

[En termes d'enjeux graphiques :]{.ul} organisation de la page (orga.
Bi-partite) + hiérarchisation des contenus (filtres / tris)

[En termes d'enjeux de développement :]{.ul} voir CR de Vincent.

3.  **Point Partenariat / \$\$**

À date : décision de ne pas déposer FNSO.

\@T&P rencontre le mudac pour partenariat autour de la revue RADDAR (>
importance des « Fonds »)

\@T&P en dialogue avec Isdat : en cours de concrétisation

\@T&P doit prendre contact avec Beaux-Arts de Rennes et de Metz.

\@T&P reprend contact avec Ministère de la Culture pour PIA4

\@Catherine Guiral \> réunion à monter pour rencontrer directrice étude
ENSBA Lyon.

**+ 18 mars :** après midi d'étude autour des droits de diffusion des
images et ressources.
