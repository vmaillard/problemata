# 2021.03.05 Problemata : séance Ressources / Fonds

## Fonds

Lecture du doc 201231_fonds_tp.docx :

Fonds :
- Ressources
- Revues
- Archives d’exposition
- Ouvrages ou ensemble de chapitre exclusif Problemata

Ce sont des types de fonds ?

---

Model Fonds :
- titre
- description
- dates
- médiateur
- institution
- type
- hasPart / isPart

---

Model fonds : hasPart : [Model Fonds, Model Fonds, ...]

Est-ce qu'il y bien des niveaux hiérarchiques comme ça ?
Est-ce qu'il y a :
- une page Liste Fonds pointant vers une page Détail Fonds avec une Liste des sous-fonds pointant à une page Détail sous-Fonds ?
- une page Liste Fonds pointant vers une page Détail sous-Fonds ?

---

Classes possibles pour les fonds :

- bibo:CollectedDocument = A document that simultaneously contains other documents.
- bibo:Collection = A collection of Documents or Collections.

Possible confusion avec les lignes dctype:Collection ?


## La Une

Types de blocs :
- Tribune
- Ventre
- RDC
- diaporama images

Technique :
- par posts ? on fait des versions ?
- avec des catégories
- date de mise en ligne