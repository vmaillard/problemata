

# CR Fonds / Ressources

(CR centré sur la question des Fonds et Ressources)

Avec : Catherine Geel, Marie Lejault, Jérôme Pasquet, Catherine Guiral, Thomas Bouville, Brice Domingues, Vincent Maillard. (Problème de connexion pour Karine Bomel qui n'a malheureusement pas pu nous rejoindre)

Le 2021.03.05 par visio

[Document préparatoire](https://framagit.org/vmaillard/problemata/-/blob/master/reunions/21_03_05/201231_fonds_tp.pdf)



## Point Design graphique

Typographie :
- le 16.03 : livraison complète
- questions à poser : remarques sur des problèmes de pathfinder + avoir les formats web (eot, woff, woff2)

Detail Ligne :
- "auteurs associés" : url Detail Auteur

Detail Ligne / Liste Article :
- au click développé : si media dispo => affichage première img
- au click développé : des liens vers l'article src et les traductions
- questions à rédiger et envoyer :
    - quelle gestion de largeur pour les colonnes ? il y a l'air d'y avoir 3 tailles : (1) auteur, (2) titre, (3) les autres. Est-ce une gestion proportionnelle liée au remplissage des colonnes d'une grille ou des tailles fixes ? "span=2 colonnes" ou "max-width=200px" ?
    - le click sur le titre de l'article renvoie-t-il au Detail Article ? ou faut-il obligatoirement développer la ligne (click sur +) ?
    - peut-on développer plusieurs articles sur la même page ?

Menu :
- au survol, quand est-ce que la l'entièreté du mot devient visible ? quand la transition s'interrompt ou dans sa progression ?
- quelle type de transition : linéaire, accélérée, ralentie ?
- quelle durée de transition : rapide, lente ?

Détail Article :
- pour la Beta pas de maquettes définitives dispo
- on travaille à une proposition économique, type dernier workshop



## Calendrier

- 18.03 : Discussion droits images (voir notamment [Images/Usages](https://www.inha.fr/fr/recherche/le-departement-des-etudes-et-de-la-recherche/domaines-de-recherche/programmes-en-cours/images-usages.html)) 
- fin mars : rdv avec RADDAR
- du 08.04 au 15.04 : Catherine et Marie sont dispos pour Problemata : il faut leur proposer des choses à faire
- 19.04 : maquettes DG finalisée
- 10.05 : la Beta est prête pour s'assurer que tout soit ok pour le CS
- 20.05 : CS Problemata



## Les typologies de fonds

Liste des types de Fonds (les intitulés peuvent changer) :
- Ressources
- Revues
- Archives d’exposition
- Ouvrages ou ensemble de chapitre exclusif Problemata



## Niveau hiérarchique

Un Fonds contient des Sous-fonds.

Par exemple : "Archives d’expositions Frac Hauts de France" contient "Ma vie dans les tubes (2006)" et "Femmes géantes (1996-2020)".



## Possibilités discutées

Trois possiblités :
1. Les Fonds sont des lignes thématiques
2. Les Fonds sont listés sur une page fonds
3. Les Fonds sont accessibles sur la page Liste Ressources

Brice et Catherine parlaient d'une piste où les lignes contiennent des bibliothèques, les trois points ci-dessus et détaillés ci-dessous peuvent ne pas être assez précis là-dessus.

Les discussions ont portés sur les possibilités de permettre la différence entre :
- des ressources provenant d'Articles (ressources directes)
- des ressources provenant de Fonds (ressources directes)



### 1 : Les fonds sont des lignes thématiques

- la définition des lignes est revue :
    - il y a des lignes **problématiques** : travail de recherche
    - et des lignes **thématiques** : travail de sélection dans une archive partenaire
- Liste Lignes :
    - différenciation graphique entre lignes problématiques et lignes thématiques
    - possible filtre supplémentaire "Problématique/Thématique"
    - exemple de ce qui figure pour une ligne thématique : "Archives d’expositions Frac Hauts de France" 
    - au click sur une ligne thématique on arrive sur Liste Ressources filtrée
- Liste Ressources :
    - ajout d'un filtre "Fonds"
    - division de la page selon les sous-Fonds



### 2 : Les fonds sont listés sur une page fonds

- le menu contient un lien supplémentaire vers une Liste Fonds



### 3 : Les fonds sont accessibles sur la page Liste Ressources

- Liste Ressources :
    - ajout d'un filtre "Fonds"
    - division de la page selon les sous-Fonds

La division se ferait sous ce format :
- Titre du sous-fonds
- Abstract du sous-fonds
- Liste de médias



## Proposition de modèles de données pour les fonds

Les classes pour les Fonds pourraient être : 
- `bibo:CollectedDocument` = "A document that simultaneously contains other documents."
- `bibo:Collection` = "A collection of Documents or Collections."

La class pour les Lignes est `dctype:Collection`, on pourrait avoir une confusion avec `bibo:Collection`.

(`dctype:Collection` = "An aggregation of resources. A collection is described as a group; its parts may also be separately described.")

Les propriétés du modèle pour Fonds pourraient être :
- Titre
- Description
- Dates
- Médiateur
- Institution
- Type
- Est compris dans (`isPartOf`)
- Url (voir paragraphe ci-dessous)

Pour qu'une Ressource pointe vers un Fonds, il lui faut aussi une propriété. Pour l'instant, on a `dcterms:provenance` = "Lien vers le fond de conservation". Si on pointe vers un Fonds à cet endroit, on perd l'url externe. Il faudrait alors ajouter une propriété "url" dans le modèle Fonds.



## Questions

- Est-ce que le document préparatoire contient l'entitèreté des données à stocker sur les fonds ?
- Est-ce que le modèle de données paraît convenir pour l'objet Fonds ?
- Sur la Liste Ressources :
    - Faut-il montrer la hiérarchie Fonds / Sous-fonds ?
    - Faut-il disposer d'un filtre pour les Fonds et/ou les Sous-fonds ?



## La Une

Nous n'avons pas eu le temps pour aborder la discussion pour La Une.

