# Choix typographiques : relevé de décisions

19.05.20

Réunion Discord

En présence virtuelle de :

- ANRT : Yeelena De Bels, Thomas Huot-Marchand
- officeabc : Brice Domingues, Catherine Guiral
- Coordination des chercheurs : Marie Lejault (T&P opérateur), Catherine Geel (Ens Paris-Saclay / Nancy)

**• Rappels des propositions :**

Hypothèses initiales suite recherches 2019 :

- Texte de labeur : **Louize**
- Texte de navigation : **Roobert**
- Péritexte : **Pitch**

Propositions de travail ANRT :

- Texte de labeur : **Spectral**
- Texte de navigation : **Inter**
- Péritexte : **IBM Plex mono**

**• Licences**

Thomas Huot-Marchand et Yeelena de Bels proposent des choix de caractères en licence OFL.

Plusieurs avantages :

- Possibilité d'adapter, redessiner, modifier le caractère, puis de le rediffuser en changeant le nom et en citant la source original (voir [règles des licences OFL](https://opensource.org/licenses/OFL-1.1), en particulier §3).
- Licence gratuite
- Facilitent l'intégration en termes de développement et codage du site.

Warning : les typo OFL google « cheval de Troie » / penser à installer les fichiers font sur le serveur dédié de Problemata.

Chercheur, opérateur, coordinateur proposent, suivant le coût des licences, d'envisager une combinaison de caractères en licence commerciale et caractères en licence OFL, tout en se disant suite à la réunion, qu'il serait intéressant/utile d'associer un dessinateur de caractères au projet.

**Il a été statué :**

• officeabc teste les attelages typographiques proposés par YdB et THM (en particulier l'inter et le spectral)

• YdB, avec officeabc, identifient les besoins en termes d'enrichissement des glyphsets, ainsi que des modifications à apporter au caractère inter (voir infra). Dans la droite ligne des recherches de YdB sur les typologies de textes, déterminer les modèles les plus propices à la composition typographique des articles.

- Sur ce sujet ne pas hésiter à faire appel aux chercheurs (quels signes ils/elles emploient, par exemple)
- Sur les modèles de composition/organisation typographique des articles, YdB peut proposer un premier tri à partir de l'ensemble de ses recherches.

• YdB, sur la base des origines du roobert singularise d'avantage l'inter.

- En regardant notamment le [forma](http://www.typenetwork.com/brochure/forma) (Nebiolo, c. 1968), et le [regular](https://www.a2-type.co.uk/regular) (A2-type, c. 2012)

- Il s'agirait de développer au sein de l'ANRT une recherche typographique pour Problemata en accord avec la philosophie du projet.

• Sur la base des tests et des besoins identifiés : hypothèses de re-travail du spectral. Ce re-travail sera a priori plus conséquent que celui sur l'inter. En effet, il s'agit ici d'apaiser ce que officeabc soulignait : « le côté incisif du spectral. » Pour cela des hypothèses :
- Thomas Huot-Marchand prend contact avec Prototypo pour évaluer les possibilités d'appliquer des évolutions sur des paramètres globaux.

- La question de faire « entrer un dessinateur de caractères » dans la boucle est posée, en regard du temps de travail nécessaire et des compétences de YdB. Cette hypothèse dépend aussi du résultat FNSO. Une somme pourrait être mise à disposition pour ce projet particulier, plutôt relatif au caractère de labeur (cf. Spectral).

• officeabc pose l'hypothèse de réduire à deux caractères l'attelage
(inter modifié + spectral modifié)

• officeabc / anrt / coordination de la recherche posent par écrit un court texte avec enjeux, histoires des différents caractères en regard de leurs usages et de leurs formes.

- Présentation synthétique à prévoir par THM & YDB à l'occasion du conseil scientifique de Problemata prévu fin juin.