# Réunion Markdown

ODJ :

- Présentation Massive Writer
- Syntaxe markdown
- Design et syntaxe notule
- Design et syntaxe des entretiens

<div class="headerLeft">jdb+vm pour Problemata</div>
<div class="headerRight">2022-03-16</div>


## Article - divers

- prévoir une class pour sortir des paragraphes du compteur de p
- glyphes dans un alphabet non latin :
	- voir ce qu'il faut faire au niveau des caractères typos
	- voir ce qu'il faut faire au niveau des bases de données



## Article - Notule

- remarque :
	- les compteurs des notes et des paragraphes se poursuivent sur la notule mais normal car intégration temporaire
- à prévoir/statuer :
	- design : y a-t-il une raison à l'enchaînement Titre 3 - Titre 2 (Titre du commentaire - Titre du chapitre) ? Ça ne paraît  pas très logique et cela génère des incohérences dans le sommaire. Un enchaînement Titre 2 - Titre 3 serait-il possible ?



## Article - Entretien

- pour l'instant un seul entretien à intégrer dans la plateforme
- remarque :
	- la maquette ne rend pas bien compte du jeu question/réponse classique mais pas dérangeant
- à prévoir/statuer :
	- peut-t-on mettre le nom des personnes en initiales ou pas ?
	- peut-t-on mettre ne pas mettre du tout de nom pour un interlocuteur ?
	- faut-il compter les paragraphes sur les entretiens ?
	- les entretiens doivent pouvoir avoir d'autres régimes de textes que le dialogue lui-même. Ils peuvent être précédés de paragraphes introductifs et suivis par d'autres (une notule par exemple)
- syntaxe markdown :
	- paraît faisable

Côté développement, la principale question est la manière dont les autres régimes de texte doivent être gérés. Pour avoir les deux colonnes, il faut casser la largeur maximale des articles classiques. On pourrait utiliser la syntaxe prévue pour une entrée du dialogue, mais c'est à tester et à confirmer avec le pôle design.