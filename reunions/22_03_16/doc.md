

# Réunion Markdown

ODJ :

- Présentation Massive Writer
- Syntaxe markdown
- Design et syntaxe notule
- Design et syntaxe des entretiens

<div class="headerLeft">jdb+vm pour Problemata</div>
<div class="headerRight">2022-03-16</div>

## Massive Writer

[http://problemata.org/massive_writer/](http://problemata.org/massive_writer/)



## Syntaxe markdown

[https://www.markdownguide.org/cheat-sheet/](https://www.markdownguide.org/cheat-sheet/)




## Design et syntaxe des notules


<img style="height: 10.5cm" src="notule.png">

Syntaxe :

```
---

> commentaire par Vincent

### Titre 3 - Titre du commentaire

## Titre 2 - Titre du chapitre

Le contenu de la notule.
```

<div style="page-break-after: always"></div>



## Design et syntaxe des entretiens

<img style="height: 19.5cm" src="entretien1.png">

<img style="height: 19.5cm" src="entretien2.png">

<img style="height: 24.5cm" src="entretien3.png">

<div style="page-break-after: always"></div>

Syntaxe 1 :

```
::::::::: {.row}

::: {.col}

Michel Legrand

:::

::: {.col}

Ce que dit Michel Legrand

:::

:::::::::
```

Syntaxe 2 :

```
::: {.person}

Michel Legrand

> Ce que dit Michel Legrand

:::
```

Syntaxe 3 :

```
[Michel Legrand]{.person}

> Ce que dit Michel Legrand
```