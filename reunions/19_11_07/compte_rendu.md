# Compte rendu pour la réunion du 2019.11.07

lieu : musée des arts déco

## Travail sur le tableau des ressources

### Tableau résultant des discussions 


| Nom         | Propriété            | Commentaire                                                                 |
|-------------|----------------------|-----------------------------------------------------------------------------|
| Class       |                      | Still image, Moving image, Sound                                            |
| Chercheur   | marcrel:col          | Nom du chercheur en charge de la ligne                                      |
| Identifiant | Id Omeka             | Identifiant unique généré par Omeka                                         |
| Type        | dcterms:type         | Choix de valeurs dans un vocabulaire contrôlé                               |
| Créateur    | dcterms:creator      | Liste des créateurs de la ressource et de son contenu                       |
| Titre       | dcterms:title        |                                                                             |
| Description | dcterms:description  | Description simple et factuelle / légende                                   |
| Abstract    | dcterms:abstract     | Description développée (contenu, contexte historique, rôles, format/medium) |
| Provenance  | dcterms:provenance   | Lieu de conservation                                                        |
| Source      | dcterms:source       | Lien vers la ressource source de la ressource décrite                       |
| Crédits     | dcterms:rights       |                                                                             |
| Copyright   | dcterms:rightsHolder |                                                                             |

### Colonne `id`

- nommage du fichier : conserver celui du fonds d'origine + bien garder la côte utilisée dans ce fonds
- l'id de Problemata sera celui de Omeka (identifiant unique pour toutes ressources)

À terme, à voir avec Huma-Num pour la mise en place de DOI

### Colonne `type` (dcterms:type)

Pour les ressources autre que "article", se fixer quelques types / class spécifiques à renseigner comme `class`dans Omeka

Liste des class :

> - Still image
> - Moving image	
> - Sound
> - (Agent)

La colonne `type` sert à être plus précis (anticipation de possibles futurs tris / listes + des recherche spécifique, par exemple quelqu'un qui chercherait "carte") par des valeurs controlées mais plus précises que la généricité des class.

Liste des valeurs :

> - photographie
> - dessin illustration croquis
> - plans, schémas
> - carte
> - datavisualisation, graphiques
> - publicité, communication
> - presse et édition
> - écrits (scan etc)

> => ajouter "vue d'exposition" ?

On fait ainsi la différence entre un nombre de `class` limité et des valeurs possibles pour la colonne `type` (dcterms:type)

### Colonnes `format` `medium` (dcterms:medium, dcterms:format)

Pour la description du support matériel et du format de la ressource, on incite à ce que cela soit fait dans la colonne `abstract`.

Si la ressource est décrite par un autre fonds, il faut ajouter l'hyperlien vers cette ressource externe. 

Mais si la ressource n'est pas décrite dans un autre fonds, il convient de renseigner ces informations. => à voir si cela se fait dans les colonnes `format` et `medium` ou bien dans `abstract`

### Colonne `créateur` (dcterms:creator)

Longue discussion. Les points de discussion sont :
- la création de ressource créateur
- la possibilité d'ajouter le rôle / profession des créateurs
- la différence entre la personne qui a fait la photo et la personne qui a fait la chaise dans la photo

La conclusion serait de renseigner, en plein texte, dans la colonne `créateur` toute personne ou organisation ayant créé la ressource : le photographe et le designer par exemple.

On pourrait ajouter le rôle sous cette forme : `Nom, Prénom (photographe)`. À voir si on ajoute les dates : `Nom, Prénom (YYYY-YYYY) (photographe)`.

=> un peu plus complexe à créer des pages auteurs qui listent les ressources où un créateur particulier est cité dans ce cas car il y aura du bruit.

=> à voir quelle solution envisager pour ajouter des informations supplémentaires sur un créateur (date de naissance, de mort, biographie).

Description compacte :

```
+------------------------+
|    __________.         |
|   /_/-----/_/|   __    |
|   ( ( ' ' ( (| /'--'\  |
|   (_( ' ' (_(|/.    .\ |
|   / /=====/ /|  '||'   |
|  /_//____/_/ |   ||    |
| (o|:.....|o) |   ||    |
| |_|:_____|_|/'  _||_   | 
|  '        '    /____\  |
+------------------------+

Nom, Prénom (photographe)   [dcterms:creator][0]
Nom, Prénom (designer)      [dcterms:creator][1]
Titre                       [dcterms:title]
Date de création            [dcterms:created]
```

### Colonne `description` (dcterms:description)

Description simple et factuelle (légende).

### Colonne `abstract` (dcterms:abstract)

Description développée. Consacrée au contenu de la ressource.

Le chercheur est incité à renseigner des informations concernant :
- le contexte historique
- les rôles / professions des créateurs
- le format et le medium
- le contenu représenté

### Colonnes `date` (dcterms:date, dcterms:created, dcterms:submitted)

Pour une ressource, pas besoin de spécifier la date de publication dans Problemata (pas de dcterms:submitted).

On demande de remplir la date de création de la ressource (dcterms:created). C'est cette date qui sera utilisée pour générer une description compacte.

Les dates liées à la source de la ressource (date d'impression, de version) sont décrites dans l'abstract. Idem pour les différentes versions existantes de la ressource décrite.

### Colonne `sujets` (dcterms:subject)

Pas de sujets / mots-clés au moment de décrire une ressource. 

C'est seule la description d'un article qui contient des sujets.

### Colonne `couverture temporelle` (dcterms:temporal, dcterms:spatial)

Pas de colonnes couverture temporelle et spatiale pour les ressources.

### Colonne `relation` (dcterms:relation)

Les ressources ne contiennent pas de lien vers les articles qui les contiennent (trop laborieux de décrire les liens contenus ➜ contenant).

Le colonne `relation` sera remplie pour les métadonnées d'un article, à savoir pour les ressources appelées par l'article. (liens contenant ➜ contenus)

La relation a une ligne ne se fait pas par une colonne / propriété Omeka.


## Questions et points à avoir en tête

- un article peut être la vidéo d'un entretien. Dans ce cas, le chercheur renseigne des métadonnées pour la vidéo. Puis ensuite pour l'article qu'il a écrit. Dans Omeka, on a alors 2 items. Dans le CMS éditorial, on a 1 article rattaché à une ligne, qui est lié à l'item Omeka article et référence l'item Omeka vidéo. La vidéo peut être précédée d'un texte introductif à ce moment.

- est-ce que pour une série de photos d'un auteur, on fait autant d'items que de photos ? ou bien on ajoute tous les médias dans un seul item ?

- prévoir une propriété du genre `same as` pour référencer le lien d'un fonds qui correspond à la même ressource.

- pour clarifier : pour une ressource non décrite dans un autre fonds, est-ce que le chercheur renseigne les colonnes `format` et `medium` ou bien il réserve ces informations dans la colonne `abstract` ?

- s'aligner sur le nommage + id pour les créateurs : http://www.isni.org/search

- je dois 17€ à Catherine (chocolat chaud + plat)

- prévoir la possibilité de filtrer tous les .pdf présents dans la base

- métadonnées Omeka : possible de faire des paragraphes à l'intérieur ? possible d'intégrer du markdown ? contre-indiqué ?

- sur la page front d'une ressource : avoir la possibilité d'avoir une liste des articles qui pointent vers la ressource

- si deux chercheurs s'écharpent sur l'abstract d'une même ressource, c'est l'équipe éditoriale qui peut trancher. Les chercheurs ont plus de marge de manœuvre dans leurs textes introductifs aux ressources qu'ils utilisent (en quoi elles se relient à leur ligne).

## Structure générale de Problemata

parties, lignes, articles, ressources

## Nouvelles

Karine quitte la bibliothèque Kandinsky et rejoint la bibliothèque du musée des arts décoratifs. Des questions se posent pour répondre à l'appel CollEx-Persée car il faut répondre avec en partenaire une bibliothèque labellisée et un fonds précis.

Nous avons rencontré Stéphanie Rivoire, directrice de la bibliothèque du MAD. Elle est intéressée par le projet. Discussion pour la lettre d'engagement CollEx-Persée, la mise dans la boucle du responsable du mécènat du MAD pour lui parler du projet. La réunion de février sera l'occasion d'inviter des partenaires actifs et potentiels. 