# Questions pour la réunion du 2019.11.07

## class Person / class Organization

- est-ce que la base Problemata accueille des ressources auteurs ?
- que met-on comme propriétés disponibles ?
- est-il nécessaire de préciser les lieux de naissance et de mort ?

Propriétés :

| Propriété       | Définition        |
|-----------------|-------------------|
| foaf:name       | Nom complet       |
| foaf:familyName | Nom de famille    |
| foaf:givenName  | Prénom            |
| marcrel:role    | Profession        |


Autre propriétés possibles :

| Propriété       | Définition        |
|-----------------|-------------------|
| bio:birth       | Date de naissance |
| bio:death       | Date de mort      |
| bio:biography   | Biographie        |


(pour installer BIO, il faut passer par leur github : https://github.com/iand/vocab-bio)

## class Article / Livre


| Propriété               | Définition                                                 |
|-------------------------|------------------------------------------------------------|
| marcrel:col             | Chercheur                                                  |
| dcterms:creator         | Auteur                                                     |
| dcterms:contributor     | Contributeur                                               |
| marcrel:trl             | Traducteur                                                 |
| dcterms:title           | Titre                                                      |
| dcterms:alternative     | Sous-titre                                                 |
| dcterms:abstract        | Abstract                                                   |
| dcterms:tableOfContents | Table des matières                                         |
| dcterms:subject         | Sujet                                                      |
| dcterms:temporal        | Couverture temporelle                                      |
| dcterms:spatial         | Couverture spatiale                                        |
| dcterms:language        | Langue                                                     |
| dcterms:submitted       | Date de mise en ligne sur Problemata                       |
| dcterms:created         | Date de création (si ≠ dcterms:date)                       |
| dcterms:date            | Date de la ressource source à la ressource décrite         |
| dcterms:source          | Ressource source à la ressource décrite                    |
| dcterms:issued          | Date de la première publication en langue originale        |
| dcterms:hasVersion      | Autres versions de la ressource (traduction, augmentation) |
| dcterms:relation        | Ressources en lien (images, autres articles)               |
| dcterms:provenance      | Lieu de conservation                                       |


Autres propriétés possibles :

- bibo:transcriptOf
- bibo:translationOf
- dcterms:isFormatOf pourrait être utilisée au lieu de dcterms:source
- dcterms:mediator pourrait être utilisée au lieu de marcrel:col   

Questions : 
- est-ce que l'on met le titre complet et on spécifie ensuite la division ?
- est-ce l'appartenance à une ligne se fait dans Omeka ? avec les items set ?
- quelle propriété pour la provenance d'un fond ?
- comment renseigner le titre original d'une ressource traduite ?


## class Ressource 

Liste des class possibles : [voir ici](../../reunions/19_09_13/questions.md#class-ditems), à retoucher avec ce qui a été fait en ws à l'ENS.


| Propriété               | Définition                                                 |
|-------------------------|------------------------------------------------------------|
| marcrel:col             | Chercheur                                                  |
| dcterms:creator         | Auteur                                                     |
| dcterms:contributor     | Contributeur                                               |
| dcterms:title           | Titre                                                      |
| dcterms:abstract        | Abstract                                                   |
| dcterms:description     | Description                                                |
| dcterms:subject         | Sujet                                                      |
| dcterms:submitted       | Date de mise en ligne sur Problemata                       |
| dcterms:created         | Date de création (si ≠ dcterms:date)                       |
| dcterms:hasVersion      | Autres versions de la ressource                            |
| dcterms:medium          | Support matériel de la ressource                           |
| dcterms:format          | Format, dimensions de la ressource                         |
| dcterms:relation        | Ressources en lien (images, autres articles)               |
| dcterms:provenance      | Lieu de conservation                                       |
| dcterms:rights          | Droits                                                     |

- pas compris dans le tableau, les éléments en-dessous de "priorisation"
- pas compris dans le tableau, au moment des dates : la différence entre "édition de la ressource" et "édition du contenu" => ah si c'est la différence entre l'image et ce qui est montré par l'image

## Omeka et qualification du rôle d'une Personne

- est-ce que la profession / rôle d'un créateur peut être différente à chaque fois ? Si elle est stable, il faudrait mieux faire une page ressource dès que l'on a besoin de décrire la profession d'un auteur.

## Multilingue

- quel est la langue par défaut de Problemata ?

## Couplage Omeka / CMS (ProcessWire)

Nous envisageons d'utiliser l'API, voir la bdd Omeka, pour faire des appels au moment de générer les pages et la navigation par un autre CMS. Est-ce correct ?

Le système d'ID unique d'Omeka permettrait de lier des ressources entre elles selon les besoins. La gestion multilingue serait gérée par le CMS (ProcessWire) qui sélectionnerait les données correspondants à la langue de la page parlée.

## Moteur de recherche

- comment faire que la recherche omeka parcourt des pdf ocr ?
- les textes des articles de Problemata seront conservés sur un autre CMS. Comment faire que la recherche Omeka parcourt également ces contenus ?

## Liaison à des bases externes

- est-ce que l'on permet l'affichage dans les pages du site, de données venant d'autres bases ?

À quelles bases externes se relier ?
- dbpedia (http://dbpedia.org/fct/facet.vsp / http://dbpedia.org/sparql/)
- wikidata (https://www.wikidata.org/)
- viaf (https://viaf.org/)
- data.bnf

## Bibliographie 

Comment automatiser la bibliographie, notamment les passages entre normes ? Est-ce que l'on décrit les éléments de la bibliographie dans la base Omeka (en référant les élements à des bases externes) ?

## Omeka

Italique dans omeka ?

## OCR

- quelles solutions OCR existent ?

Tesseract et ocrmypdf fonctionnent correctement. Est-ce que abbyy est mieux ?

## VM 

- quelle adresse demande-t-on ? problemata.huma-num.fr ?
- à qui adresser la demande de nom de domaine ?

- VM sous Ubuntu
- hardware : RAM et VCPU classiques, stockage de 1To
- deux comptes administrateurs

- usage : serveur lamp, peut-être ocr (Tesseract et ocrmypdf). Une installation Omeka S et un autre CMS (ProcessWire)


