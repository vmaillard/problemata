


# CR réunion outil d'écriture

Le 2021.04.09 par visio avec ml, cg, sg, jdb et vm

Outil déployé ici : [problemata.huma-num.fr/massive_writer](problemata.huma-num.fr/massive_writer/)



## Retours sur l'outil décriture précédent

- apostrophe droite
- espaces microtypos
- smallcaps
- lien + réf biblio "en ligne"



## Retours page Détail Article

- pas de compteurs pour les titres
- la description est prise en charge par l'opérateur de l'Article : on ne le fait par génération automatique



## À faire

- intégrer les smallcaps
- un filtre othotypo
- ajouter les filename dans le volet
- Omeka : mettre les mots-clés dans Omeka par ordre alphabétique
- Omeka : ajouter tous les articles à la ligne
- Omeka : supprimer le doublon d'item pour cg (attention aux items qui pointent vers l'item à supprimer) 
- jdb regarde la gestion multilingue django avant de stocker le rédactionnel "À propos"



## Omeka

- détailler les permissions des rôles



## Calendrier

- objectif : les .md des articles sont prêts début mai pour faire une migration vers django
- prévoir un rdv pour le nom de domaine problemata.org



