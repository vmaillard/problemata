TB, BD, JdB

- Uniformisation des en-têtes Une Ligne et Un Auteur
- Penser un menu sans ombre portée
- Générer un style de titre supplémentaire pour les textes
- Inverser l’ordre des cases pour les aside, la taille (médium/large) se retrouve avant ressources, notes, biblio, outils
- Dans le aside, pour les ressources, mettre fig. 1 au lieu de 1 pour éviter les confusions

à destination des éditrices

- Pour les articles multimédias
    - comment nomme-t-on un media ? Garde-t-on le terme fig.?
- Pour les articles en général
    - attention aux usages des styles typographiques des auteurs et autrices, notamment le gras. À éviter, sauf cas bien spécifique.
    - Faut-il augmenter le code des usages typographiques, notamment pour les graisses? Comment utilisons-nous le gras, lorsqu’il est déjà intégré dans les textes déjà saisis ?
- créer une flèche pour les notes 
- stylisé les bibliographies (on vient de finir cela avec Thomas, vous pouvez y jeter un œil)