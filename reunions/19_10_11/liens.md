# Liens


BNF :
- https://data.bnf.fr/semanticweb
- https://data.bnf.fr/opendata
- https://data.bnf.fr/images/modele_donnees_2018_02.pdf

BSAD :
- https://www.bsad.eu/index.php?lvl=categ_see&id=1

RDA :
- https://github.com/RDARegistry/RDA-Vocabularies/wiki/RDA-Technical-Guidelines
- http://www.rdaregistry.info/

FRBR :
- http://www.loc.gov/catdir/cpso/FRBRFrench.pdf