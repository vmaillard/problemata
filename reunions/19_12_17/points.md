# Points

**Ressource Agent**

L'ajout d'une ressource Agent pour bénéficier d'une solution pérenne au moment de décrire un créateur, et de lister les créateurs d'articles (à voir si cela s'applique aux chercheurs/médiateurs)

**Multilingue**

- la langue par défaut du site
- est-ce qu'il y aura bien des articles en "it" et "de" ?

**Liste de sujets**

la liste des sujets/mots-clés liés aux articles

**Parties et ligne**

la hiérarchie entre les parties et les lignes

**Contenu vraisemblable**

On va vite avoir besoin de vérifier la validité des hypothèses de navigation, notamment la gestion (filtre et tri) des dates. Pour cela, il faudra du contenu le plus vraisemblable possible. Est-ce que vous avez commencé à décrire des articles en fonction du tableau ? Pour l'instant, on fonctionne avec des échantillons, l'idée serait de tester avec du vrai contenu.

On peut en générer, en importer à partir de tableau.

**Gestion comptes utilisateurs**

Sur la base de scénarios : articulation humain - machine

**Filtres, tri et page de liste**
=> méthodes, ordres, intitulés

**barre de saisie**
attention au cumul de fonctionnalités : fil d'Ariane, input texte

**Noms de domaine**
+ rédaction questions Huma-Num

**Prestataires / Chercheurs**