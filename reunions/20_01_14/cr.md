Rendez-vous avec S. Pouyllau (Huma-Num)
Mardi 14 janvier 2020 – CG, ML, BD, JDB, VM

Compte rendu de réunion

**éditeur stylo**

- va être amélioré, idée de connecteurs
- pourquoi pas l'envisager, le considérer dans la perspective d'un outil d'écriture : Marcello Vitali-Rosati

**machine virtuelle**

- on aura l'autonomie
- il nous alerte sur le fait de mettre Omeka S sur une machine virtuelle: suivant les MAJ d'Omeka S, il y avait des cas de panne
- vigilance sur les montées de version des outils installés sur la machine
- sauvegarde snapshot des machines virtuelles: bien le faire, comme ça en cas de maintenance il peuvent remettre le dernier snapshot: l'équipe va nous le préciser
- contacter cogrid pour les spécifications

**nom de domaine**

- gandi, annualité à payer
- aussi pour le développement on utilise problemata.huma-num.fr
- renater pour le nom de domaine ? Théoriquement oui, ne fonctionne que sur le .fr, Stéphane ne comprend pas la logique, ils le font mais c'est très artisanal et laborieux en terme de procédure, s'armer de patience: ils prennent en charge le coût, mais ils ne sont pas très souple en matière de mise en place (délais, redirections de plusieurs semaines pour des raisons un peu bizarres)
- passer par le CNRS: délégation régionale qui gère la MSH ou Saclay par exemple, de mettre en place un nom de domaine pour nous: se renseigner (uniquement pour le .fr)

**modèle de données: peser le pour et le contre: trouver l'équilibre**

- propre ou pas propre d'administrer un chercheur à un item: 2 façons de faire: 
- partir du principe que la propriété mediator a une valeur qui correspond à une personne, ce qui nous intéresse c'est la valeur en tant que tel: référentiel de personne, ajout de prénom nom d'une personne = difficulté, si tu as une incertitude sur une personne, tu peux pas gérer ces incertitudes (idem pour les homonymes)
- aller chercher ontologie FOAF, plus précise, mais sémantiquement parlant on va sortir du périmètre de notre modèle de données choisi, plus détaillé = plus complexe à mettre en place = plus difficile à maintenir
- dans tous les cas aujourd'hui la plupart des modèles sont hybrides en ontologies
- construction d'une classe agent exprès
- avis de Stéphane: modèle très bien, pas besoin de spécifier des rôles pour les agents

**multilingue**

- contacter Daniel Berthereau : un des plus anciens développeurs français d'Omeka S
- tension entre les français et les autres développeurs (US): pas la même feuille de route EACH (US: framework de développement (ZEND framework) en fin de vie, aspect business de l'approche, périlleux)
- il va y avoir un fork à un moment donné: Daniel développe une api qui répond à ses besoins, qui sont aussi les nôtres
- Omeka S: entrepôt de données
- Nakala? : dépôt de données, des fichiers et des métadonnées, enrichi par la mécanique d'Isidore, pas trop un outil de modélisation de connaissances comme Omeka S
- EHESS utilise Omeka S
- Omeka S pas bien pour les grands volumes, contrairement à Nakala: le branchement entre les deux est intéressant, huma-num va release (juin – septembre, quelque part entre les 2) un écosystème constitué des deux
- cubic web?: utilisé par data-bnf: plus bas qu'Omeka, une autre stratégie que celle du CMS mais plutôt container de graphes de données, une surcouche à créer au dessus pour gérer cubic web = faire des graphe, construire des applications dessus 

**mailing list: solution**

- logiciel sympa: fonction newsletter incluse, à voir en fonction de la stratégie
- phénomène récent dans le mail universitaire, les universitaires ont ud mail avec le spam: les gens ne reçoivent plus leurs mails (mail de 300ko de html): plein de retours des universités qui bloquaient
- solution comme fraap-info (org) peut être prise en compte si mail
- mattermost pas les mêmes usages ni canaux
- mailchimp? (usine à gaz, tout le monde d'accord)

**OCR**

- depuis 6 ans ils utilisent abbyy sous 2 formes:

- intégrée à sharedocs avec une interface web
- api: envoyé à abbyy et récupérer les résultats
- note financière d'abbyy abusée: ils ont mis en place des quotas par groupe de projets depuis l'année dernière
- ils ont acheté une licence abbyy en réduisant le volume de droit de tirage d'OCR, sous forme d'un cluster d'OCRisation
- ils mettent en place des api sur lesquels on va pouvoir envoyer du document et recevoir des ocr en retour (dispatcher qui choisira tesseract ou abbyy, qui va être prêt au printemps [stable à ce moment là mais fonctionne pour le moment])
- discuter avec Gérald Foliot sur ce point là: savoir le volume de données (par an combien de documents veux-t-on numérisé? Ils ont besoin d'estimations pour négocier avec les fournisseurs, et adapter les serveurs), les types de documents, et construire le tuyau d'entrée dans l'api, et le tuyau de sortie (réponse de l'api en .pdf par exemple)

**Suivi Huma-Num**

-on va être suivi par la collègue de Stéphane qui s'appelle Hélène Jouguet

- être en contact avec Joël ou Gérald ou Thomas ou Laurent (sans doute Gérald sur certains aspects), on va passer de main en main avec ces gens, il 
ne faut pas s'inquiéter, un maximum par mail, pour des discussions plus techniques ou précises, ils vont nous contacter par téléphone (leur renseigner au premier contact), passer par mail à cogrid pour tracer tous les messages et échanges

**C. Geel:**

- projet actif à 6-7 personnes, et autour de lui une 15aine de partenaires
- projet open-source et pratique des sciences ouvertes
- demande pour faire venir Stéphane ou un acolyte de Stéphane au titre d'intervenant (expert extérieur à notre groupe, spécialiste d'un genre de choses [Penny Sparke qui va raconter le développement du champ de l'histoire du design en GB, Marin Dacos sur la science ouverte (ou alors Lionel Maurel), quelqu'un d'Huma-Num (Stéphane Pouyllau) pour évangéliser | expliquer ce que ça veut dire tout ça])
- structure GIS quand plein de gens hétérogènes dans le même projet
- nous on s'est dirigé vers l'association
- tenir au courant Stéphane Pouyllau pour le 11 février
