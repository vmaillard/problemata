# Problemata



## Documents importants

- [Modèle de données](/plateforme/interoperabilite/data_model.md)
- [Tableau des Agents](/plateforme/agents/agents.md)
- [Notice Omeka](/plateforme/notices/notice_omeka.md)
- [Approche front](/plateforme/front/front.md)
- [Documentation VM](/plateforme/virtual_machine/documentation_vm.md)
- [Pads](/plateforme/liens/pads/liens_pad.md)



## Structure du repo

- devis
- plateforme / *les documents à jour*
    - arborescence
    - articles
    - interopérabilité
    - notices
    - liens / *liens vers les pads et les sites web pour les tests*
    - templates
- reunions / *les réunions par ordre chronologique avec les documents du moment*
- sources / *les documents sources fournis par l'équipe*
- workshop / *les documents pour les workshops*


