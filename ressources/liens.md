# Liens

[Guide Markdown](https://www.markdownguide.org/basic-syntax)
[Guide Markdown](https://commonmark.org/help/)

[Guide Pandoc](https://pandoc.org/MANUAL.html)

[Bibliographie Chicago](https://www.chicagomanualofstyle.org/home.html)

[MARC Code List for Relators](http://loc.gov/marc/relators/relaterm.html)

[MARC Code List for Relators](http://id.loc.gov/vocabulary/relators.html)

Pour visualiser les réseaux / les tailles des ontologies / chercher une ontologie / chercher par terme possiblement présent dans des ontologies :
- https://lov.linkeddata.es/dataset/lov
- http://id.loc.gov/
- http://metadataregistry.org/
- http://www.michelepasin.org/support/ontospy-examples/

BNF :
- https://data.bnf.fr/semanticweb
- https://data.bnf.fr/opendata
- https://data.bnf.fr/images/modele_donnees_2018_02.pdf

Outils de web sémantique :
- [phpmybibli](https://www.sigb.net/) utilisé par https://www.bsad.eu/
- https://github.com/ckan/ckan
- [CubicWeb](https://www.cubicweb.org/) utilisé par https://data.bnf.fr/