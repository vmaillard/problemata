## Attention, le nombre de lignes vides est important pour faire matcher modèle et exemple.

### model : 26 lines

- [x] publication_title
- [x] print_identifier
- [x] online_identifier
- [x] date_first_issue_online
- [x] num_first_vol_online
- [ ] num_first_issue_online
- [ ] date_last_issue_online
- [ ] num_last_vol_online
- [ ] num_last_issue_online
- [x] title_url
- [ ] first_author
- [x] title_id
- [ ] embargo_info
- [x] coverage_depth
- [x] notes
- [x] publisher_name
- [x] publication_type
- [ ] date_monograph_published_print
- [ ] date_monograph_published_online
- [ ] monograph_volume
- [ ] monograph_edition
- [ ] first_editor
- [ ] parent_publication_title_id
- [ ] preceding_publication_title_id
- [x] access_type
- [x] bestppn

### example 1 : 26 lines

- ABE Journal
- 
- 2275-6639
- 2012
- 1
- 
- 
- 
- 
- http://journals.openedition.org/abe
- 
- abe
- 
- fulltext
- Full access to the HTML version of the content. Access to PDF and Epub reserved to subscribing institutions.
- InVisu
- serial
- 
- 
- 
- 
- 
- 
- 
- F
- 187652759


### example 2 : 26 lines

- Anatoli
- 2111-4064
- 2498-0730
- 2010
- 1
- 
- 
- 
- 
- http://journals.openedition.org/anatoli
- 
- anatoli
- 
- fulltext
- Full access to the HTML version of the content. Access to PDF and Epub reserved to subscribing institutions.
- CNRS Éditions
- serial
- 
- 
- 
- 
- 
- 
- 
- F
- 196329175