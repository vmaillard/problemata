## Notes générales
- ne pas vouloir fitter à tout prix aux modèles satisfaisant l'output "notes" ou un autre output

---

# Clarisse, Charlene, Léa
- commentaire rebond en dessous des blocs eux memes
- boite à idées générales
- système d'onglet fait pour discuter : pensée d'une interface

départ
---
- question / référence
    - user : dit quelque chose
    - user2 : dit quelque chose

- mot-clé

### gr1 : La fabrique du Sensible
#### ici

##### nomenclature
- pad
- calc  : 
---

# Milena, Valentine, Benjamin

### gr2 : designers aux langages non textuels 
#### inventaire d'ouvrages sous forme de références bibliographiques détaillées

##### nomenclature
- pad
    - archive de laboratoire
    - archive de laboratoire
    - l'interprétation modélisante, johanna drucker
    - le système minard, josst grootens & sandra renge

On distingue 2 types de blocs chez vous : 1 les archives, 2 les références livres
Que voulez-vous dire par le dressage de cet inventaire ? pouvez vous préciser un peu ?

<!--
    #NOM DE LA RÉFÉRENCE <br>
    ##Auteur(s)
    <p> date, lieu <br>
    Description de la référence </p>
    <br>
    ####
-->

- calc  : empty

---

# etienne, clara, camille, julie
- temps passé sur la facon dont on référence les documents et référencement lui-même
- volonté d'utiliser des mots-clés pour mettre en réseau
- statement : 

### gr3 : Artisan révélateur de survivances culturelles
#### inventaire d'ouvrages sous forme d'images ou d'extraits de références (très riche)

- que voulez-vous dire/faire par l'utilisation de ce corpus de réf ?
- quitte à marquer les auteurs, pourquoi juste marquer leur nom et non pas des commentaires qu'ils auraient à faire de manière directe, personnelle, "subjective" quant à l'ouvrage référencé ?
    - dans une certaine mesure, plutôt radicale, ne pourrait on pas se satisfaire que de ça pour entrer dans le corpus (en guise de porte d'entrée)

##### nomenclature
- pad
    - champ auteur de la référence : geste de signification de celui qui partage au sein de la base. Dans le pad il y a l'autorship signifié par la couleur de surlignement du texte, la présence et le renseignement de ce champ véhicule t il la volonté d'utiliser l'auteur de la référence dans le parcours d'une lecture ou d'une recherche ? Est-ce pour vous ou pour les autres à qui sont destinés ces contenus ?
    - champs éditeur et texte : les ouvrages en ont-il toujours besoin ? qui lorsqu'il s'agit de références de type iconographique ? peut on consevrer les mêmes champs ?
    - ne faut-il pas catégoriser les types de champs d'une manière plus distincte ? pour par exemple différencier les références de type a et b ?
    - quelle définition, quel terme à employer pour le bloc objet constitutif / structurel du pad ?
    - <div align="right">Ref Julie</div> : en HTML on peut utiliser des classes et ids pour distinguer à un niveau de plus la typologie des balises décrites
        - <div align="right" class="ref" id="ref-julie-[n]">Julie</div> : ce qui peut s'apparenter au besoin d'un pseudo code pour etre en mesure d'écrire ça de manière plus sensé (lisible pour vous) et plus rapide qu'écrire du HTML brut
             - orienter vers set de règles propre à leur nomenclature qui remplacerait le markdown ?

<!--
    #Titre de la référence 
    ##Auteur(Prénom/Nom)  
    <p>Date de création</p>
    ![Nom de l'image](URL de l'image)  
    <br>
    <p><em>Source</em></p>
    <p>Éditeur (si livre) / Type d'impression / technique (si image) / etc.</p>  
    <p>Texte<br> (retour à la ligne) 
    <strong>Suite du texte</strong> (gras) <em>lorem ipsum</em> (italique) <strong><em>lorem ipsum</strong></em> (gras+italique)</p>  
    ###Mots-clés
    <p align=right>Auteur de la référence (Nous)</p>
    ####Type de document (ferme le bloc)
-->

- calc : empty
    - peut être n'y a t il pas besoin d'utiliser le calc après tout si le pad est bien structuré et "ordonné" (set de règles) pour votre usage
    - quel nommage de colonnes satisferait ce dont vous dressez l'inventaire dans le pad ?
    - votre groupe a t il vraiment besoin du pad ?



---

# Rémi, Clément
- échelles du low-tech
    - un rating en entrée de ce corpus
        - qui rate ?
        - rating en fonction des technologies de l'époque ou écart avec l'idée de primitif ?
- tout ce qu'il faut pour construire un foyer
- définition de primitif et rustique et mise en échelle
- pour rejoindre cathérine : pragmatique + étrangeté/fantaisiste

### gr4 : > Autonomie / Technologie / Cultures vernaculaires / Partage des communs / Lowtech - Technique et Création autonome au sein de cultures vernaculaires
#### Base de données liée au wiki *"la maison rustique du XXIème siècle"*

##### nomenclature
- pad 
    - des blocs nommés et typés : respect de la structure fiche prévue pour "notes"
- calc

##### questions
##### suggestions

---

# Léa Grac
### gr5 : espaces urbains X économies
#### appropriation des mots/termes de l'économie / évolution de nos rapports avec certains termes selon les contextes

##### nomenclature
- pad
    - doit-on reconsidérer les richesses ?
    - les tiers-lieux peuvent-ils dynamiser l'économie ?
    - un urbanisme de l'inattendu
    - la preuve par 7
    - économie
    - économie sociale et solidaire
    - économie verte
    - économie inclusive
    - urbanisme
    - urbanisme circulaire
    - programmation ouverte
- calc
    - colonnes
        - mots
        - étymologie
        - définitions CNRTL
        - autres définitions
    - mots
        - économie solidaire
        - économie circulaire
        - économie de la circularité
        - économie de la fonctionnalité
        - économie sociale et solidaire
        - économie participative
        - économie inclusive
        - bien (1)
        - bien (2)
        - bénéfices
        - valeur
        - temps
        - crédit
        - monnaie
        - tiers-lieux
        - capitalisme
        - communisme
        - marxisme
        - socialisme
        - résilience
        - urbanisme
        - urbanisme circulaire
        - écologie

##### questions
- calc > colonnes > autres définitions : cela pose la question du référencement de la source des autres définitions = comment satisfaire cela ?
- calc > colonnes > autres définitions : cela pose aussi la question des multiples valeurs pour un même champ = idem ?
- calc > mots : quand on lit/lie les différents mots répertoriés, et quand on connait l'approche que tu arbores, on pousserait le vice jusqu'à avoir des sous-catégories presque gramaticales (de type adverbe, COD, verbe, adjectif, pluriel, singulier)
- calc > mots > bien (1) && bien (2) : cela pose la question de la somme nécessaire et le format d'une ligne pour arriver ou non (arbitraire et choisi) à rassembler ces 2 mots
- calc > mots > bénéfices && tiers-lieux : ils posent la question du pluriel et du coup de la présence du puriel pour une entrée / ligne
    - faut-il ajouter un champ pluriel dans ton tableau ou au contraire séparer les définitions entre singulier et pluriel metant en exergue l'emploi de ce mot dans ces 2 différentes formes ?

#### suggestions
- en répondant à ces questions, tu seras en mesure de radicaliser ton approche et de te distinguer les nuances entre les termes de glossaire, lexique et dictionnaire par exemple
- le pad contient en premier lieu des références invoquant des termes que tu listes dans ton calc : quelle valeur (preuve, nuance, démonstration, exemple...) ajoute-t-ils à leur équivalent tableau ?

---

# Alyson
- design de l'esthétique, utilisation des bactéries, synergie entre design et biologie
- inventaire des travaux, iconographies, dessins = fonds documentaire
- concluant et foireux
- mix entre travaux persos et travaux d'autres

th 1 : Bactérie Esthétique -Design du vivant
th 2 : Synergie du Design et de la biologie
th 3 : Biologie synthétique au service du processus de création

- cg : ajout de pistes prospectives, d'hypotheses fausses ?

### gr6 : Bactérie Esthétique -Design du vivant
#### Synergie du Design et de la biologie. Biologie synthétique au service du processus de création

##### nomenclature
- pad
    - dictionnaire / glossaire
    - utilisation du rich-text
    - mot esthétique : 
        - 2 types de mots pour une même entrée qui sont numérotées aussi

- calc
- dir
    - article/
    - brainstorming/
    - carte_mentale/