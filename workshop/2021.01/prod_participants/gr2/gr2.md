25.01.2021<br>

<i>Problema'chat</i> est une conversation entre, <span style="color:#ff3333">Miliana Rahouadj</span>, 
<span style="color:blue">Valentine Ducerf</span>,<br> deux étudiantes de l'ENS Paris-Saclay en formation pour la recherche en design, <br>et <span style="color:#15e125">Benjamin Boulesteix</span>, étudiant à l'ESAD de Reims en design graphique. <br>Cette conversation s'inspire de la discussion scientifique, soit la dernière étape <br>d'un article scientifique, et permet d'analyser et d'interpréter les résultats <br>des études menées. À partir du workshop, nous avons eu envie de nous rencontrer <br>et de découvrir le travail des uns et des autres à travers un support informatisé, <br>le chatting. Ainsi, les humanités numériques s'enracinent souvent d'une façon explicite <br>dans un mouvement en faveur de la diffusion, du partage et de la valorisation du <br>savoir, extrait du Manifeste des digital humanities.
<br> <br>
- - - <span style="color:#ff3333">Je connais Valentine depuis début septembre, j'ai vu qu'elle connaissait quelques <br> personnes de la promo mais nous n'avons jamais eu l'occasion de se raconter quoique <br>ce soit de personnel. En plus je connais mal son travail et pourtant je sens qu'il y a <br>des corrélations à découvrir.
 <br>Hâte.<br>
Benjamin a débarqué ce matin et je me dis qu'il s'été très vite acclimaté.. chose pas <br>facile quand on change de contexte de travail et d'enseignants. Je suis contente parce <br>que je sais que les éléments que l'on va se montrer les uns aux autres vont créer <br>un discours constructif.</span>
<br> <br>
- - - - <span style="color:blue">Moi je rajouterais que je ne connaissais absolument pas Miliana, ni même son projet <br>de recherche jusqu'à cette mise en commun et ce souhait de faire projet ensemble. <br>Je suis contente car on discute bien les trois et on tombe vite d'accord, bien que nos <br>intérêts ne soient pas forcément les mêmes. Miliana et Benjamin sont beaucoup <br>plus vifs et téméraires que moi, plus réservée. C'est bien, ça mixe les profils.</span>
<br> <br>
- - - <span style="color:#15e125">Je viens d'arriver et je connais personne mise à part Catherine Geel, alors que je l'ai <br>vu qu'une seule fois autour d'un rendez-vous pour participer au workshop. <br>De nature timide, je me suis dit que j'arrivais dans l'esprit de m'amuser, de rencontrer <br>d'autres étudiants et combattre cette timidité. <br> Après avoir découvert les personnes avec qui j'allais faire ce workshop, je me sens <br>soudainement soulagé. Hâte de voir ce que nous allons produire.</span> <br><br>
<br>

#![Image couverture livre](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///livre01.jpg)
- - - *10h45*<br><span style="color:blue">J'ai choisi ce livre car son auteure s'intéresse aux manières de représenter et donner corps à la donnée. Elle fait état de l'impasse dans laquelle on se trouve, avec à notre disposition l'illustration du graphiste d'un côté et de l'autre, le tableur excel édité à partir d'une feuille de calcul. Elle fait aussi état de la prévalence du texte, par rapport à l'image, alors que pour elle, cette dernière est justement plus à même de pouvoir évoquer un contenu et le faire parler, lui donner une (re)présentation. </span>

####

#<br>
- - - *10h52*<br><span style="color:#ff3333">Je ne connais pas ce bouquin mais ça me donne envie d'en savoir plus et surtout que tu me montres un exemple si tu peux, d'une idée modélisée.
J'ai vraiment l'impression que je pourrais me servir de ce principe pour modéliser certains concepts que je voudrais apprendre aux enfants autistes, comme par exemple "comment représenter la population des animaux marins ?"</span>
<br> <br>
- - - *10h55*<br><span style="color:blue">Pour l'instant je ne suis pas encore assez loin dans la lecture du livre, mais normalement elle parle par la suite d'outils qu'elle a élaboré spécialement pour la création de ce genre de contenus.</span>
<br> <br>
- - - *11h00*<br><span style="color:#15e125"> Dis-moi Valentine, comment cette édition a eu impact dans ton travail ? Et qu'est ce que tu en a retenu personellement ? Est-ce qu'elle t'a amené une réponse à un problème rencontré ou inspiré pour un futur projet ?</span><br><br>
- - - *11h15*<br><span style="color:blue">En fait, tout est parti de la lecture d'un article, qui se trouve dans la revue *"Sciences du design"*. C'était au tout début d'année, lors de mon premier passage à la bibliothèque de l'Ens. Je cherchais quelque chose à lire pour le week-end et ce numéro là m'a particulièrement interpellée, dans la mesure ou je suis fascinée par les algorithmes, par leur capacité à façonner le monde sans qu'on s'en aperçoive vraiment.<br><br>
#![revue](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///revue.jpg)
####
#![dominique](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///livre_algorithme.jpg)
####
- - - *11h30*<br><span style="color:blue">De fil en aiguille, je me suis retrouvée avec ce livre de Johanna Drucker. Je compte approfondir mes recherches dans ce genre de domaine, entre algorithmes, visualisation et données. Je ne sais pas encore le pourquoi du comment, mais il reste encore tout à faire et imaginer dans ce milieu-là, étant donné sa jeunesse. En tant que designer graphique, il y a donc énormément à faire et rechercher dans le domaine de la visualisation, du numérique et des données. Mais je m'intéresse aussi à d'autres modalités de la représentation, comme ce que je propose plus bas, avec les cartes d'avant internet et les planches anatomiques.</span>
<br> <br>
- - - *11h33*<br>
<span style="color:#15e125"> D'accord. &#128077; </span><br><br>

####

- - - *13h07*<br><span style="color:#ff3333">
Ça me fait quand même penser à ces deux définitions, je sais pas ce que <br>t'en penses ? <br>Je trouve qu'on peut aisément faire le rapprochement entre la datavisualisation et <br>le fonctionnement de la pensée, précisément dans le langage.<br>
NEUROLINGUISTIQUE subs. fem.<br>
− La neurolinguistique est l'étude des phénomènes neuronaux qui contrôlent <br>la compréhension, la production et l'acquisition du langage. C'est un domaine <br>interdisciplinaire qui s'inspire des méthodes et des théories de neurosciences, <br>de linguistique, de sciences cognitives, de neuropsychologie et même d'informatique.<br>
LINGUISTIQUE subs. fem.<br>
− Étude d'une langue ou des langues considérées du point de vue de la signification; <br>théorie tentant de rendre compte des structures et des phénomènes de la signification <br>dans une langue ou dans le langage.<br></span>
<br>
- - - *13h20*<br><span style="color:blue"> Ici une vidéo à regarder, sur une expérience menée par le chercheur Richard Feynman.<br>
https://www.youtube.com/watch?v=WDGAO-UGRPE <br> 
<span style="color:blue">Il questionne les parties du cerveau mobilisées lorsque l'on lit, pense, ou compte. <br>Par exemple, lui, arrive à lire tout en égrenant 60s dans sa tête. Il cherche à mettre en <br>évidence que l'on peut associer le langage réel, lu ou parlé avec le langage que l'on se dit <br>dans sa tête : quand on se parle à soi-même.</span>
<br> <br>
- - - *14h00*<br><span style="color:#ff3333">C'est génial comme principe ! Merci pour ton partage.
<br> <br>
- - - *14h10*<br><span style="color:#15e125">Comme tu as été la première à l'évoquer, j'aimerai comprendre pourquoi <br>le langage ou la notion de langage est si important pour toi, Miliana. <br>Est-ce due à une expérience personnelle ?</span>
<br><br>
- - - *14h15*<br><span style="color:#ff3333">En réalité je me suis penchée sur la question d'inclusion scolaire des jeunes <br>ayant des troubles (comme la dyslexie ou l'autisme ou encore l'hyperactivité, <br>il y a notamment un designer qui a travaillé là-dessus, tu connais peut-être <br>*Victor Papanek*) et suite à ça j'ai compris qu'aujourd'hui l'éducation nationale <br>ne faisait pas forcément le nécessaire pour inclure ces profils atypiques qui <br>parfois même, dérangent. En bref je me suis interrogée sur une nouvelle forme <br>de langage qui puisse se partager entre différents profils neurologiques, qui <br>peut aussi bien passer par l'objet que l'espace que le graphisme, afin que les jeunes <br>puissent se développer correctement par l'apprentissage. En effet la question de <br>l'autisme m'est personnelle et c'est pour ça que je suis en train d'établir un inventaire <br>des difficultés du spectre autistique pour parvenir à apporter des réponses par <br>une méthode, un outil ou autre, je suis en pleine exploration. Et pour construire <br>cette nouvelle méthode il faut que je passe par la phase de conceptualisation, en <br>l’occurrence tu vas voir par l'inspiration de diverses références.<br>
Est-ce-que ça te parle ? De quelle manière tu abordes le langage de ton côté ? <br>Je suis curieuse de savoir.</span>  
<br> 
- - - *14h17*<br><span style="color:#15e125">Je trouve ça très touchant de ta part de t’intéresser à cela. C'est très admiratif sans <br>compter qu'il y a surement beaucoup de choses à faire pour eux.</span> <br>&#x1F44F;&#x1F44F;&#x1F44F;&#x1F44F;&#x1F44F;&#x1F44F;
<br> <br>
- - - *14h20*<br><span style="color:#ff3333">Ah merci c'est gentil ! je te mets le début de mes recherches juste en dessous. <br>Mais dis m'en plus sur ton travail !
<br>
<br>

26.01.2021<br>*09h00*
#![tabouretsselonvalentine](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//hncerh.JPG)
![tabouretsselonvalentinepartiedeux](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Miliana///r%c3%a9seaux%20complexes.JPG)
- - - *09h20*<br><span style="color:#ff3333">Je vous montre à quoi ressemble ma toute dernière piste de recherche que je commence à explorer. Il s'agît donc de l'aboutissement d'une recherche iconographique que j'ai effectué en partie sur le site internet du CNAM, en fait il y a plusieurs ouvrages illustrés et anciens mis à disposition et j'ai une affection particulière pour ces instruments de laboratoires, je les trouve fascinants, esthétiques, et plein de mystère, ne connaissant pas bien la chimie, j'ai envie de comprendre comment par la volonté de faire des expériences on en vient à concevoir ce genre d'outils. Aussi je leur trouve une très grande qualité constructive et j'aimerai m'en inspirer, pour sortir de l'outil basique et sans âme, je trouve qu'il y a tout un pan à découvrir sur l'histoire de l'objet qui peut agrémenter sa manière d'en imaginer de nouveaux.</span>
####

#<br>
- - - *09h30*<br><span style="color:#ff3333">Peut-être que ça peut vous faire penser à des tabourets mais ce sont des objets utilisés pour faire de la chimie.</span><br><br>
- - - *09h33*<br><span style="color:blue">En effet, je les ai pris pour des tabourets à première vue, mais je pense que je n'ai pas forcément tort, puisque ces objets présentent une assise (destinée à recevoir la fiole ou Erlenmeyer) ainsi que des pieds, sur-élevant l'ensemble. On peut donc les qualifier de tabouret *fioliques*.<br><br>
- - - *09h37*<br><span style="color:#ff3333">Jolie métaphore, j'aime bien ! Et pour le coup maintenant je comprends pourquoi y'a eu une confusion visuellement. </span><br> <br>
- - - *09h40*<br><span style="color:blue"> Donc si je comprends bien, tu relies ici ces instruments scientifiques - ces outils de chimiste- au langage, par le fait qu'on puisse en extraire leur architecture comme mode de pensée et rationalisation humaine? Est-ce que tu veux dire que par le simple fait de les observer, on puisse les comprendre et en extraire un langage, ou une sorte de logique interne qui les animerait?
<br> Ou alors il faudrait les comprendre comme un potentiel d'objets propres à recueillir des données ?</span>  <br><br>
- - - *09h45*<br><span style="color:#ff3333">C'est exactement ça ! Je t'avoue que c'est encore un peu abstrait pour moi, ça reste un champ d'investigation. Mais bien vu !</span> 
####

- - - *10h30*<br><span style="color:#ff3333">En fait j'aurai peut-être dû commencer par vous montrer ça, c'est là que je me rend <br>compte que ce qui me paraît évident ne l'est pas pour vous. Surtout si j'explique pas <br>depuis le début, hinhin.<br>
<br>

#![Outilsaccade](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Miliana///outil%20saccade.JPG)<br><br>
- - - <span style="color:#ff3333">Voici le lien vers la vidéo explicative si vous voulez aller voir, </span><br>
https://www.youtube.com/watch?v=9W2XgBzyFBA
####
<br>
#![schéma](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Miliana///documentation%20workshop%204.png)
<br>
####

- - - *10h40*<br><span style="color:blue">En réaction à tes outils de chimistes, propres donc à pouvoir symboliser des concepts <br>et la véhiculation de la pensée, j'aimerais proposer ici des planches issues d'un travail <br>précédent. Je pense en effet que ces cartes et planches anatomiques peuvent entrer en <br>résonnance, par le fait qu'elles appellent certaines modalités de représentation, afin de <br>pouvoir véhiculer des informations, sans avoir recours au langage. Ces modes de <br>représentation sont des outils de langage à part entière.<br><br>

#<span style="color:blue"><font size="4">La carte, le territoire et l'anatomie</font>
<span style="color:blue">La carte comme système de données pré-sélectionnées et composées dans un ensemble
Rendre compte d'une représentation scientifique</span><br><br>
<span style="color:blue">Ces planches sont issues d'un travail précédent, mettant en lien cartes territoriales et anatomies. Ce sujet avait pour titre "Écorcher le(s) monde(s), voulant dire ainsi que ces deux systèmes avaient besoin d'un regard porté sur l'intérieur, et donc de la nécéssité d'ouvrir les chairs et les enveloppes, afin de mettre à nu ce qu'ils comportaient. Le lien entre cartes et anatomie peut s'expliquer en partie par ce concept là de l'invisible sous-jacent, mais aussi par cette volonté de pouvoir réussir à discener des espaces qui sans ces représentations, ont du mal à exister, puisqu'on ne peut les voir directement.<br><br>
![planche 1](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///Planches///Recherche_doc_Valentine_Ducerf-7.jpg)
![planche 2](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///Planches///Recherche_doc_Valentine_Ducerf-8.jpg)
![planche 3](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///Planches///Recherche_doc_Valentine_Ducerf-9.jpg)
<br><br>
L'ensemble des planches, <br>
http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///Planches///Recherche_doc_Valentine_Ducerf-1.jpg <br>
http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///Planches///Recherche_doc_Valentine_Ducerf-2.jpg <br>
http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///Planches///Recherche_doc_Valentine_Ducerf-3.jpg <br>
http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///Planches///Recherche_doc_Valentine_Ducerf-4.jpg <br>
http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///Planches///Recherche_doc_Valentine_Ducerf-5.jpg <br>
http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///Planches///Recherche_doc_Valentine_Ducerf-6.jpg <br>
http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///Planches///Recherche_doc_Valentine_Ducerf-7.jpg <br>
http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///Planches///Recherche_doc_Valentine_Ducerf-8.jpg <br>
http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///Planches///Recherche_doc_Valentine_Ducerf-9.jpg <br>
####

#<span style="color:blue"> <font size="4"> Le corps et la science, comment communiquer ce dernier sans le recours au langage?</span></font><br><br>
<span style="color:blue"> L'usage de la représentation, physique et dessinée<br><br></span>
![josephinium](https://i.pinimg.com/originals/11/13/d1/1113d132f4a018f5d20ead2b07f97664.jpg)<br><br>
Les modèles de cire dans des postures très lascives du Josephinium de Vienne.<br><br>
- - - *12h00*<br>Planches anatomiques<br><br>
#![planche digestion](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///test///c009.jpg)<br><br>
![planche cerveau](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///test///c043.jpg) 
####
- - - *12h10*<br><span style="color:#ff3333">Wahou j'adore tes visuels, qui plus est je trouve qu'il y a vraiment des liens évidents. Je ne sais pas bien expliquer, mais j'ai envie de te donner une liste de vocabulaire pour que tu me dises si ça fait sens pour toi : Réseau, neurones, informations, circulation, vivant, communication, anatomie, pensée, etc. <br>
- - - *12h15*<br><span style="color:blue">Oui, quand tu me dis ça, j'ai envie de te répondre avec une carte heuristique. Ce sont en effet des termes avec lesquels je suis familière.</span><br><br>
http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///carte_intro-02.png<br>
- - - *14h03*<br><span style="color:#ff3333">Par contre la photo de la dame reconstituée en cire, je trouve ça dérangeant... On dirait une poupée sensuelle qui pose alors qu'elle est complétement éventrée...Et à la fois le travail de précision est remarquable.
</span>
- - - *14h12*<br><span style="color:blue">En effet, c'est ça qui est très particulier avec cette collection de modèles de cire, car tous sont représentés dans des postures assez dérangeantes, voire complètement sexualisées comme ici. La touche du petit coussin de satin et du collier de perles ne font que renforcer cela. Par ailleurs, ils sont d'une grande beauté et incroyablement bien conservés, puisque datent du XVIIIè siècle. La collection est conséquente, comporte près de 1200 modèles de cire, mais aussi toute l'instrumentation scientifique et médicale de l'époque (notamment des scies à amputations par exemple). Un magnifique musée unique en son genre qui vaut bien le déplacement jusqu'à la capitale autrichienne.
####

27.01.2021<br>*09h02*
- - - <span style="color:blue">Donc là on passe de la valeur de la mise en signe du statique avec la carte et le corps, par la mise en signe du mouvement avec la notation gymnique?<br> Ou peut-être qu'on peut comprendre ça comme des systèmes d'écritures différentes, avec d'un côté un système d'écriture pour représenter une donnée géographique et terrestre, de l'autre côté, un système d'écriture pour la représentation des corps ouverts et statiques, et donc là, avec un système d'écriture pour les *vivants*, avec ce système d'écriture pour la notation gymnique?</span><br><br>

#![Camille Trimardeau](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Benjamin///Camille%20Trimardeau.jpg)
Fig.1
####
# 
<br>
- - - *14h42*<br><span style="color:#15e125">En évoquant cette notion de langage, j'ai tous de suite pensé aux travail de Camille Trimardeau, Fig.1. Un système d’écriture pour la notation gymnique, Camille Trimardeau.</span> <br> 
<br>
<br>
>
> - *Lien visuel et graphique immédiat en réponse* <br>
#![Mot arabe](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///texte_arabe.png)
####
- - - *14h48*<br><span style="color:blue">Ce que je trouve intéressant ici: ce sont deux langages que je ne maîtrise pas mais dont la force de mouvement et du geste dessiné se ressent. On perçoit le mouvement dans les deux, et mon incompétence à les "lire" me permet alors de me focaliser sur un autre régime de l'image et à les considérer pour leur représentation pure.<br>
**Question de la langue qu'on ne comprend pas ?**</span>
#![Quantange](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///test///quantange.png)
<br>
### Le Quantange <br>
*Pierro di Sciullo*
####
#![Alphabet illisible](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///test///illisible.jpg)
<br>
###L'alphabet Illisible <br>
*Jurriaan Schrofer*
####

- - - *15h02*<br><span style="color:blue">J'aime bien ces deux alphabets car ils questionnent notre rapport au langage et à l'écriture.</span>

<br>
####
- - - *16h30*
<br>
<span style="color:blue">Je propose cet essai d'écriture pour rebondir sur "l'alphabet", ou plutôt la proposition de *mise en signe* à travers la référence de la notation gymnique. En effet, il me semble que le travail de Camille Trimardeau possède une double envie: celle à la fois de pouvoir retranscrire le mouvement des gymnastes grâce à un langage qui lui est propre, mais aussi de réussir à formuler ce dernier grâce à une mise en signe des différents mouvements effectués. On voit nettement la différence entre une rondade, roulade ou juste un enchaînement dans l'exemple qui est proposé. La mise en signe est évidente, et profondément efficace.<br><br>
- - - *16h39*<br><span style="color:#ff3333">Je trouve ça très intéressant j'attends que tu nous montres pour visualiser.
- - - *16h43*<br><br>

#<span style="color:blue"><font size="4">Le signe, valeur cardinale et protéiforme de l'affiche</font><br>
<span style="color:blue">Un travail que j'ai réalisé l'année dernière, c'est un texte portant sur la valeur du signe au travers du graphisme et plus particulièrement de l'affiche.</span><br><br>
http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///HDD_Ducerf_valentine_2.pdf
<br><br>
<font size="4"><span style="color:blue">*Le design a-t-il pour vocation a produire des signes?*<br><br></font>
<span style="color:blue">J’aborderai cette problématique sous un champ défini qui est celui du design graphique. En effet, ce dernier ayant parmi ses vocations, l’idée d’organiser et d’ordonnancer lettrages et images mais aussi de créer des signes reconnaissables tels que la signalétique ou des logos. Il est évident alors qu’il en produit – on parle même de « signes typographiques », mais est-ce là sa seule vocation ?
Il convient d’abord de préciser ce que l’on entend par signe. Quelle est la différence entre signe et symbole par exemple ? La définition de signe en elle-même est vaste et sur le site du CNRTL, c’est la troisième proposition qui nous intéresse : Objet, représentation matérielle d’un objet (figure, dessin, son, geste, couleur) ayant, par rapport naturel ou par convention, une certaine valeur, une certaine signification dans un groupe humain donné. Signe graphique, sonore, visuel ; signe magique ; signe de deuil.
Un signe permet donc de fédérer sous une même représentation et sous l’égide de conventions quelque chose ayant du sens, là ou un symbole (en sémiotique dans la terminologie de CH. S. Pierce) est un signe dont la relation à l’objet est conventionnelle (par opposition à l’icône dont la relation est analogique et à l’indice dont la relation est causale) ; c’est-à-dire qu’elle n’est pas manifestée par le symbole en question (bouton on/off ou les paons de Junon symboles de fidélité).
Le signe ici a donc une relation forte au signifié, il lui est lié intrinsèquement. Mais on pourrait de suite relier signe et symbole, car on parle certes de signe typographique (ils le sont par convention) mais puisque ce n’est que conventionnel, ce sont aussi des symboles selon la définition de Pierce.
J’étudierai ici trois productions d’affichistes d’époques différentes que sont Raymond Savignac, Nikolai Prusakov et Philippe Apeloig sous trois grands thèmes qui permettent d’analyser ces trois affiches complètement, tant visuellement que conceptuellement.


####
# <font size="4"> Les affiches de Savignac, Prusakov et Apeloig
![affiches](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///affiches2.jpg)
![apeloig2](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr2/files//Valentine///affiches3.jpg)
####







*
