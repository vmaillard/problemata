<p><strong> La nostalgie, frein mélancolique à la conceptualisation de l'artisanat.</strong></p>

<p>L’artisanat est un travail de la matière, un rapport au monde, qui dépasse la question d’une pratique technique traditionnelle. La survivance technique est parfois travestie en nostalgie des techniques rudimentaires, coupant court à une piste exploratoire plus contemporaine, voire futuriste, qui va de l’avant. Les techniques sont du passé, donc dépassées : en retirant ce voile mélancolique sur la réalité technique (dépassée) de l’objet, on l’observe de façon plus objective, dans son potentiel de réalisation.<br>
<br>Nous envisageons la technique comme façon de faire, un façonnage par le monde (socio technique) et moins comme une tradition enfermant la technique dans une passivité propre à l’histoire. L’artisan-producteur est imprégné de son territoire. Territoire à la fois empreint des charges sociales environnantes et réserve de matériaux. Les survivances — gestes qui traversent les âges sans changer dans leur essence — sont des manières d’extraire et de projeter des idées dans la matière, au travers de médiums aujourd’hui très différents. Dans le numérique se retrouve une dialectique de la main de l’Humain, visible au travers des influences comportementales de création.</p>

---

Thématiques :
    
    -1/Territoire/espace (→cartographier l'artisanat):
Australie
Maghreb
Delft
Chine
Islande
Limousin
Pays bas
Mexique
Medina de Fès
Québec
Inde
Bergerac
Décharge
France
Angleterre
Etats-Unis
Italie
Turin
Danemark

    - 2/Couleurs (pigments, teintes):
Bleu
Vert
Anthracite
Cuivre 
Brun 
Bois
Blanc
Ocre
Fer
Bronze

    - 3/Matériaux :
Minerai 
Fibre végétale
Porcelaine
PVC
Saule pleureur 
Acacia
E-déchet
Métal
Déchet 
Organique
Céramique
Grès
Bronze
Fer

    - 4/Pratiques institutionnelles:
Savoir-faire 
Petite série
Production de masse
Pièce unique
Chasse
Numérique
Impression 3D
Archéologie
Design viral
Chimie
Développement local?
Mondialisation
Régulation
Transmission
Collection
Évolution

    - 5/Croyances théoriques / Idéologie:
Alchimie
Occidentalisme
Eurocentrisme
Colonialisme
Archaïsme
Archéologie
Chimie
Design viral
Développement local
Folklore
Anthopocène
Durabilité
Transformation
Evolution
Arts&Crafts
Adaptation
Religion

    - 6/Typologie:
Outils
Arme
Ustensile 
Impression 3D
Porcelaine
Simulacre
Disparition
Numérique
Métallurgie
Maillage
Tissage
Ornement
Empreinte

    - 7/Peuple, identité:
Occident
Folklore
Touareg
Nordique
Océanie
Langage

    - 8/La gestuelle:
Cognition
Geste
Outils
Chasse
Impression 3D
Expérience
Métallurgie 
Maillage
Tissage
Empreinte
Langage

---


# Les recompositions de l'artisanat : des corporations à la « première entreprise de France »
##Henri Jorda
<p>2012</p>
<br>
<p><em>https://www.cairn.info/revue-marche-et-organisations-2006-1-page-39.htm</em></p>
<p>Éditeur (si livre) / Type d'impression / technique (si image) / etc.</p>  
<p>Depuis une vingtaine d’années, le regard porté sur l’artisanat a changé. Crise du salariat oblige, certains y voient désormais un réservoir d’emplois, un secteur d’activités faiblement capitalistique, particulièrement attractif pour des travailleurs recherchant l’indépendance ou, plus simplement, un emploi. Artisans et professions libérales ont l’opportunité de valoriser des savoir-faire que les grandes organisations ne peuvent ou ne veulent pas gérer. Par ailleurs, depuis que l’esprit d’entreprise souffle sur le Vieux Continent, l’image de l’artisanat fait l’objet d’une communication nouvelle, tant la catégorie artisan » est jugée péjorative par les nouveaux « chefs d’entreprise ». Cet article vise à éclairer les enjeux du passage de l’artisanat à l’entrepreneuriat dans une mise en perspective historique. Les exigences « modernes » en termes de gestion et de concurrence recomposent la tradition artisanale sur deux plans : les manières de faire (ou de travailler) et les manières d’être (ou de s’organiser).</p>
* <em>Matériaux, croyance, Pratique institutionnelles, Peuple-identité, Territoire-espace</em>
<br></br>
###Savoir-faire
###Design viral
###Petite série
###Évolution
###Adaptation
###Occident
###France
*<p align=right>Réf. Etienne</p>
####Article


#As above, so below
##Xandra Van Der Eijk
<p>2017</p>
![As above, so below](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Etienne///170412-di-collected-kirstie-en-xandra-3-1200x.jpg)
<br>
<p><em>https://xandravandereijk.nl/projects/as-above-so-below</em></p>
<p>La designer récupère la poudre de météores comme minerai. Détection des particules par leur électromagnétisme.</p>
* <em>Matériaux, Croyances théoriques-Idéologie, Pratiques institutionnelles, Peuple-identité, Territoire-espace</em>
<br></br>
###Minerai
###Terre 
###Ocre
###Alchimie
###Expérience
###Archéologie
###Pays-bas
*<p align=right>Réf. Etienne</p>
####Projet 


#L'art et l'Artisanat
##William Morris
<p>1889</p>
<br>
<p><em>Source</em></p>
<p>Éditeur (si livre) / Type d'impression / technique (si image) / etc.</p>  
<p>Pour les préraphaélites, comme pour William Morris, la distinction entre l'art et l'artisanat, entre la conception et l'exécution, devait être abolie : tout homme, à son échelle, pouvait être producteur de beauté que ce soit dans la réalisation d'un tableau, d'un vêtement, d'un meuble ; et tout homme également avait le droit, dans sa vie quotidienne, d'être entouré de beaux objets.
Un cours essai sur les dégats de la révolution industrielle anglaise sur l'environnement visuel du quotidien, sur le kitsch de l'ère victorienne, et pour un renouveau des arts appliqués de qualité.</p>
* <em>Croyances théoriques-Idéologie, Pratiques institutionnelles,La gestuelle, Territoire-espace</em>
<br></br>
###Arts & Crafts
###Savoir-faire
###Évolution
###Geste
###Langage 
###Angleterre
*<div align="right">Réf. Julie</div>
####Livre



# Le règne végétal : divisé en traité de botanique générale[...]
##Jean-Augustin Barral
<p>1864</p>
![Règne végétal](https://gallica.bnf.fr/ark:/12148/bpt6k9609767n/f221.highres)
<br>
<p><em>https://gallica.bnf.fr/ark:/12148/bpt6k9609767n/f221.highres</em></p>
<p>Édition 1864-1869  planche 54</p>
<p>Planche monographie imprimée</p>
* <em>Matériaux, Croyances théoriques-Idéologie, Pratiques institutionnelles, Peuple-identité, Territoire-espace</em>
<br></br>
###Organique
###Fibre végétale 
###Savoir-faire
###Évolution
###Adaptation
###Alchimie
###Geste
###Occident
*<div align="right">Réf. Julie</div>
####Ouvrage


# Le règne végétal : divisé en traité de botanique générale[...]
##Jean-Augustin Barral
<p>1864</p>
![Règne végétal](https://gallica.bnf.fr/ark:/12148/bpt6k9609767n/f225.highres)
<br>
<p><em>https://gallica.bnf.fr/ark:/12148/bpt6k9609767n/f225.highres</em></p>
<p>Édition 1864-1869  planche 55</p>
* <em>La gestuelle, Pratiques institutionnelles, Croyances théoriques-Idéologie, Territoire-espace</em>
<br></br>
###Outil
###Collection
###Savoir-faire
###Évolution
###Adaptation
###Geste
###Occident
*<div align="right">Réf. Julie</div>
####Ouvrage


# Le geste et la parole
##André Leroi-Gourhan
<p>1964</p>
<br>
<p><em>Source</em></p>
<p>Éditeur (si livre) / Type d'impression / technique (si image) / etc.</p>  
synthèse sur le comportement matériel de l'homme.
<p>Partant des observations de la neuro-physiologie, il montre que l'emploi simultané de la main et de la face mûrit dans le comportement d'un nombre important d'espèces depuis les origines. L'évolution du corps et du cerveau et celle des manifestations techniques et esthétiques permettent de dégager une véritable "paléontologie du langage". La notion zoologique du territoire est ensuite exploitée pour définir l'économie des sociétés de chasseurs-ramasseurs, les modalités de l'apparition de l'élevage et de l'agriculture, puis l'enchaînement des conséquences techno-économiques qui conduit aux techniques du feu (céramique, métallurgie), à la formation des classes sociales et au développement du dispositif urbain.<br>
Technique, économie, langage se coordonnent ici depuis le plus lointain passé jusqu'à l'examen des chances biologiques de l'homme futur, dans la recherche d'une image totale du développement humain.</p>
*<em>Pratiques institutionnelles, La gestuelle, Peuple-identité, Territoire-espace</em>
<br></br>
###Évolution
###Adaptation
###Geste
###Langage
###Cognition
###Occident
###France
###Identité
*<p align=right>Réf. Julie</p>
####Livre

#Philosophie du geste
##Michel Guérin
<p>1995</p>

<br>
<p><em>Source</em></p>
<p>Éditeur (si livre) / Type d'impression / technique (si image) / etc.</p>  
<p>Construisant au fil de son œuvre ce qu’il nomme une figurologie, Michel Guérin, comme autant de fragments, a retenu ici quatre gestes : faire (le geste de la technique et du travail), donner (celui du social et des échanges), écrire (le geste renversé, révolté), danser (le geste pur). Pointant le geste comme première tournure de la pensée et de l’action, l’auteur révèle de façon pertinente sa part dans la construction progressive d’une œuvre et, interrogeant le sens du geste, fait apparaître que c’est le geste lui-même qui fait sens.</p>
*<em>La gestuelle, Pratiques institutionnelles,Territoire-espace, Peuple-Identité </em>
<br></br>
###Geste
###Adaptation
###Langage
###Occident
###France
*<p align=right>Réf. Julie</p>
####Livre


#The evolution of culture
## A. L. F. Pitt-Rivers
<p>1827-1900</p>
![Pitt-Rivers](https://i.pinimg.com/originals/5e/be/72/5ebe72c1da852c2c1956b84a55f72067.jpg)
<br>
<p><em>https://i.pinimg.com/originals/5e/be/72/5ebe72c1da852c2c1956b84a55f72067.jpg</em></p>
<p>Édition 1864-1869  planche 55</p>
<p>Planche monographie imprimée</p>
*<em>La gestuelle, Pratiques institutionnelles, Croyances théoriques-Idéologie, Peuple-Identité, Type, Matériaux, Territoire-espace</em>
<br></br>
###Savoir-faire
###Petite série
###Évolution
###Adaptation
###Geste
###Outil
###Occident
###Arme
###Archaïsme
###Collection
###Métal
*<p align=right>Réf. Julie</p>
####Image


#Évolution radiale d’armes australiennes à partir d’un simple bâton droit
## A. L. F. Pitt-Rivers
<p>1906</p>
![Pitt-Rivers](https://journals.openedition.org/gradhiva/docannexe/image/2741/img-5.jpg)
<br>
<p><em>https://journals.openedition.org/gradhiva/docannexe/image/2741/img-5.jpg</em></p>
<p>Planche monographie imprimée</p>
*<em>Pratiques institutionnelles, La gestuelle, Croyances théoriques-Idéologie, Matériaux, Type, Peuple-Identité, Territoire-espace</em>
<br></br>
###Savoir-faire
###Petite série
###Évolution
###Adaptation
###Geste
###Outil
###Australie
###Océanie
###Arme
###Archaïsme
###Collection
###Métal
*<p align=right>Réf. Julie</p>
####Image

#Exemple de lignée d’outils proposée
##A. L. F. Pitt-Rivers
<p>1868</p>
![Pitt-Rivers](https://journals.openedition.org/gradhiva/docannexe/image/2741/img-4.jpg)
<br>
<p><em>https://journals.openedition.org/gradhiva/docannexe/image/2741/img-4.jpg</em></p>
<p>Planche monographie imprimée</p>
*<em>La gestuelle, Pratiques institutionnelles, Croyances théoriques-Idéologie, Type, Peuple-Identité, Territoire-espace</em>
<br></br>
###Savoir-faire
###Petite série
###Évolution
###Adaptation
###Geste
###Outil
###Australie
###Océanie
###Arme
###Archaïsme
###Collection
###Métal
*<p align=right>Réf. Julie</p>
####Image

#<em>Homo Sapiens</em>
##Marie Garnier
<p>Date de création</p>
![Marie Garnier](https://journals.openedition.org/gradhiva/docannexe/image/2583/img-2-small465.jpg)
<br>
<p><em>https://journals.openedition.org/gradhiva/docannexe/image/2583/img-2-small465.jpg</em></p>
<p>Édité chez ENOstudio</p>
<p>Objet de design culinaire en grès</p>
« Back to basics : design “ néolithique ” en cuisine » ; http://sleekdesign.canalblog.com, archive (...)
<p><em>Le biface, très simple, très ancien</em></p>
<p>Homo sapiens : le premier outil préhistorique pour l’homme moderne ! Transformer la matière culinaire, aiguiser une lame d’acier avec une pierre taillée… Des gestes simples et intuitifs qui nous rappellent les premiers gestes de l’homme et établissent un lien entre le présent et le passé. Ce caillou sculpté (grès) est un véritable « bijou de cuisine » qui répond à plusieurs fonctions : aiguiser les couteaux mais aussi concasser, broyer des grains de poivre ou de l’ail… Conçu pour être suspendu à un crochet, il se range facilement tout en décorant la pièce de sa présence insolite.</p>
*<em>La gestuelle, Pratiques institutionnelles, Croyances théoriques-Idéologie, Type, Peuple-Identité, Territoire-espace, Matériaux, Couleurs</em>
<br></br>
###Archaïsme 
###Simulacre
###Pièce unique
###Évolution
###Geste
###Outil
###Ustensile
###Occident
###Archéologie
###Grès
###Blanc
*<p align=right>Réf. Julie</p>
####Projet

#Cuillère pyrogravée
##Groupe des Iullemmeden Kel Ataram
<p>XXe siècle</p>
![Cuillère](https://journals.openedition.org/gradhiva/docannexe/image/2583/img-6-small580.jpg)
<br>
<p><em>https://journals.openedition.org/gradhiva/docannexe/image/2583/img-6-small580.jpg</em></p>
<p>Genève, musée d'Ethnographie</p>
<p>Cuillère pyrogravée en bois d’acacia d’origine touarègue</p>
<p>La forme de ma cuillère touarègue s’adapte avec économie à sa fonction. Sa beauté vient de ce que ses formes sont minimales : aucune superfluité de forme ne s’ajoute à ce qui est nécessaire pour qu’elle soit une bonne cuillère (alors que si la courbure de son manche ne prolongeait pas parfaitement la courbure de sa partie convexe, il y aurait entre l’une et l’autre un angle, une discontinuité « superflue »)</p>
*<em>Pratiques institutionnelles, Matériaux, Peuple-Identité, Croyances théoriques-Idéologie, Type, Territoire-espace, Couleurs</em>
<br></br>
###Archaïsme
###Archéologie
###Petite série
###Acacia
###Bois
###Touareg
###Maghreb
###Ustensile
###Identité
###Durabilité
*<p align=right>Réf. Julie</p>
####Objet

#Comment nous vivons, comment nous pourrions vivre
## William Morris
<p>XIXe siècle</p>
<br>
<p><em>Source</em></p>
<p>Edition Rivages Poche Petite Bibliothèque</p>
<p>Ce volume réunit trois de ces conférences. Il y examine le système économique de son temps et démontre avec ferveur ce que ce système a de déshumanisant, il s'y interroge sur les arts décoratifs et sur leur importance dans la vie quotidienne. Dans ces pages, Morris affirme ses préceptes favoris : aucun objet chez soi qui ne soit beau ou utile. Aucun travail qui ne soit une joie à accomplir. Rien de plus important que la beauté, l'amitié et la solidarité.</p>
*<em>Croyances théoriques-Idéologie,Pratique institutionnelles, Matériaux, Peuple-Identité,  Type, Territoire-espace, Couleurs</em>
<br></br>
###Arts & Crafts
###Adaptation
###Geste
###Langage
###Occident
###Angleterre
###Identité
*<p align=right>Réf. Julie</p>
####Livre

#Architecture en fibres végétales d'aujourd'hui
## Dominique Gauzin-Müller
<p>2019</p>
<br>
<p><em>Source</em></p>
<p>Édition MUSEO</p>
<p>Mettre en œuvre des matériaux à base de plantes à croissance rapide est une gigantesque opportunité pour stocker dès maintenant une grande quantité de carbone, et lutter ainsi contre les dérèglements climatiques. Habitat, équipements ou bâtiments d’activités, les 50 bâtiments biosourcés décrits dans cet ouvrage ont été choisis parmi les 226 candidats du FIBRA Award, premier prix mondial des architectures contemporaines en fibres végétales. Réalisés en bambou, chaume, paille, feuilles de palmier, écorces, herbes de la Mer du Nord ou du plateau andin, voire briques de champignons, ces exemples inspirants issus de 45 pays incitent à la redécouverte de matériaux abondants et bon marché, dont la transformationdemande peu d’énergie.<br>
Une contribution majeure à la transition écologique et sociétale !</p>
*<em> Matériaux,Pratique institutionnelles,La gestuelle,  Type</em>
<br></br>
###Fibres végétales
###Savoir-faire
###Évolution
###Geste
###Langage 
###Organique
###Durabilité
###Développement local
*<p align=right>Réf. Julie</p>
####Livre

#Quand Foucault dit "nous"...
##Alain Brossat
<p>2011</p>
<br>
<p><em>https://journals.openedition.org/appareil/1265</em></p>
<p>Ce texte remet en cause une lecture de l’œuvre de Michel Foucault inspirée par les études post-coloniales, fondée sur la notion d’une perspective « eurocentrique », « occidentaliste » inhérente à cette œuvre. Il tente de montrer comment le travail archéologique de Foucault procède d’opération de découpe et de topologisation des objets étudiés qui incluent l’élément du relatif et la dimension de l’historicité. Il présente toutes les conditions qui entourent l’énonciation d’un « nous » dans l’œuvre de Foucault..</p>
*<em>Croyances théoriques-Idéologie, Type, Peuple-Identité</em>
<br></br>
###Colonialisme
###Eurocentrisme
###Geste
###Occidentalisme
###Identité
*<p align=right>Réf. Julie</p>
####Article

#De la survivance culturelle à la démocratie
## Daniel Benghozi
<p>2005</p>
<p><em>https://papyrus.bib.umontreal.ca/xmlui/bitstream/handle/1866/16518/Benghozi_Daniel_2005_memoire.pdf?sequence=1</em></p>
<p>Rapport de thèse , évaluation les forces et les faiblesses qui soutient que certaines cultures sont menacées de disparition et que l’État peut légitimement agir pour leur préservation. Or, pour constater la disparition d’une culture, il faut être en mesure de distinguer entre sa transformation et la disparition d’une de ses composantes essentielles. Nous étudions donc, d’abord, les questions épistémiques ayant trait au phénomène de la disparition des cultures, pour ensuite mieux évaluer la validité de l’aspect normatif de la thèse. Nous examinons donc les origines intuitives et empiriques du souci pour la survivance pour dégager ensuite deux positions épistémiques (essentialiste et nominaliste) qui entraînent des conclusions normatives très différentes, puisqu’elles déterminent la frontière entre ce qui constitue un changement à l’intérieur d’une culture et sa disparition pure et simple.</p>
*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, Peuple-Identité</em>
<br></br>
###Colonialisme
###Évolution
###Langage
###Occident
###Mondialisation
*<p align="right">Réf. Julie</p>
####Article

#Arcanum
##Olivier Van Herpt  
<p>2017</p>  
![1](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///porcelain-3d-printer.jpg)
![2](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///3d-printing-porcelain.jpg)
![3](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///porcelain-delft-blue-cobalt-pigment.jpg)
![4](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///porcelain-delft-blue.jpg)
![5](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///texture-delft-blue-porcelain.jpg)
![6](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///Capture%20d%e2%80%99e%cc%81cran%202021-01-25%20a%cc%80%2014.07.01.png)
<br>
<p>Image: Mr.Frank</p>  
<p><em>https://oliviervanherpt.com/3d-printing-porcelain/</em></p>
<p><strong><em>3D Printing with Porcelain</em></strong><br>
In the footsteps of Ineke Hans, Wieki Somers and Jurgen Bey, Olivier was asked to make a contemporary porcelain piece for the Kunstmuseum Den Haag. Olivier developed the first large-scale porcelain 3D printing technique to meet the commission for the museum.<br>
The pieces that were 3D printed in porcelain now adorn a Dutch delftware flower pyramid base dating from the 17th century. To celebrate delftware, Olivier 3D printed fourteen stackable porcelain pieces mixed with cobalt oxide pigment to evoke a new interpretation of delftware.<br>
The piece will remain in the permanent collection of the museum and is currently on display. The piece is called Arcanum. <br>      
Arcanum refers to the secrets and mysteries that surround porcelain. For centuries European artisans tried unsuccessfully to copy the Chinese. Chinese porcelain was widely admired and traded worldwide for thousands of years. Europeans attempted to imitate it but were thwarted by the technical complexity of making porcelain. Delftware itself originated once artisans in the Netherlands were able to make a simulacrum of Chinese porcelain. <br> 
Olivier also had his own secrets to unlock. He had to unravel how to 3D print porcelain and produce a museum quality piece.  Incorporating the cobalt in a coherent way proved problematic and this needed to be solved. Shrinkage of the porcelain during drying and firing also had to be corrected for. Eventually, Olivier succeeded in developing porcelain 3D printing with the cobalt oxide pigment. Then he had to design porcelain pieces that would complement the flower pyramid in sedately beautiful yet contemporary way. To take the old and make new in a respectful way is a challenge indeed.
</p>  
*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, Peuple-Identité, La gestuelle, Territoire-Espace, Matériaux, Couleurs, Typologie</em>
<br></br>
###Savoir-faire
###Pièce unique
###Geste
###Langage
###Delft
###Pays-bas
###Bleu
###Outil
###Impression 3D 
###Numérique
###Porcelaine
<p align=right>Réf. Clara</p> 
####Projet


#Sandbeests
##Theo Jansen 
<p>1990</p>
![2020.022](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Etienne///2020.022-685x1024.jpg)
![2017.047](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Etienne///2017.047-674x1024.jpg)
![2020.029](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Etienne///2020.029-696x1024.jpg)
<br>
<p><em>https://www.strandbeest.com/</em></p>
<p>Techniques dérivées du vent, généalogie des éléments d'ossature des structures.</p>
*<em>Pratique institutionnelles, Territoire-Espace, Matériaux</em>
<br></br>
###Élément
###Évolution
###PVC
###Pièce unique
###Savoir-faire
###Déchet
###Pays-bas
<p align=right>Réf. Etienne</p>
####Projet

#The five ages of man 
##Gerard Heard
<p>1960</p>
![2014.48.19](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Etienne///Capture%20d%e2%80%99e%cc%81cran%202020-12-28%20a%cc%80%2014.48.19.png)
<br>
<p><em>https://www.geraldheard.com/books/2017/7/15/the-five-ages-of-man</em></p>
<p>L'évolution de l'Homme au travers des éléments, l'expérience du monde façonne l'être au travers d'attitudes corporelles, la renaissance, la catharsis, l'inspiration, l'illumination, la transformation.</p>
*<em>Pratique institutionnelles, Peuple-Identité, La gestuelle, Territoire-Espace</em>
<br></br>
###Transformation
###Évolution
###Élément
###Langage
###Angleterre
###Occident
*<p align=right>Réf. Etienne</p>
####Livre 

#The Willow Project
##Iceland Academy of The Arts
<p>2015</p>
![936-52](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Etienne///iceland-academy-arts-students-willow-project-materials-designmarch_dezeen_936_52.jpg)
![936-8](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Etienne///iceland-academy-arts-students-willow-project-materials-designmarch_dezeen_936_8.jpg)
![936-12](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Etienne///iceland-academy-arts-students-willow-project-materials-designmarch_dezeen_936_12.jpg)
<br>
<p><em>https://www.lhi.is/en/willow-project/en</em></p>
<p>Au cours du processus de recherche, trois processus principaux ont été rendus : l'ébullition, la combustion et la distillation. Inspirés par la circulation naturelle de la matière, rien d'autre que de l'eau et de la chaleur ont été ajoutés au bois et tous les sous-produits ont été traités comme des ressources précieuses. En déconstruisant l'arbre à l'échelle microscopique et en réassemblant les éléments trouvés, les nouveaux matériaux rendus pouvaient se maintenir par eux-mêmes. Ils pourraient tous retourner à la forêt pour se nourrir.</p>
*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, Peuple-Identité, Territoire-Espace, Matériaux, Couleurs</em>
<br></br>
###Organique
###Fibres végétales
###Transformation
###Alchimie
###Occident
###Nordique
###Islande
###Brun
###Saule Pleureur
###Durabilité
*<p align=right>Réf. Etienne</p>
####Projet

#Are we human? 
##Beatriz Colomina & Mark Wigley 
<p>2016</p>
![arewehuman](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Etienne///arewehuman.jpg)  
<br>
<p><em>https://www.lars-mueller-publishers.com/are-we-human</em></p>
<p>Lars Müller Publishers</p>  
<p>Les notes de terrain de Colomina et Wigley proposent une archéologie de la façon dont le design est devenu viral et est maintenant plus grand que le monde. Elles s'étendent sur les quelques centaines de milliers d'années et les dernières secondes pour examiner la relation plastique unique entre le cerveau et l'artefact. Un portrait vivant en ressort.</p>
*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, La gestuelle, Territoire-Espace, Typologie</em>
<br></br>
###Archéologie
###Design viral
###Outils
###Production de masse
###Arts & Crafts
###États-Unis
<p align=right>Réf. Etienne</p>
####Livre

#Adaptive Manufacturing
##Olivier Van Herpt 
<p>2014 - on going</p>
![ovh-sens-1](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///3d-print-ceramic-object-herpt-wassink.jpg)  
![ovh-sens-2](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///ceramic-3d-printed-clay.jpg)
![ovh-sens-3](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///adaptive-manufacturing-mesh.jpg)
![ovh-sens-4](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///olivier-van-herpt-sander-wassink-adaptive-3d-printing.jpg) 
![ovh-sens-5](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///adaptive-manufacturing-printer.jpg)
<br>
<p><em>https://oliviervanherpt.com/adaptive-manufacturing/</em></p>
<p>Image: Olivier Van Herpt & Ronald Smits</p>  
<p>
<strong><em>Adaptive Manufacturing, A Sensory Machine That Feels Its Environment</strong></em><br>
Adaptive Manufacturing is a collaborative project by Sander Wassink and Olivier van Herpt started in 2014. An essential part of their concept is to highlight the production process. Adaptive Manufacturing takes as its point of departure the question of how technological production has replaced the craftsman and thus removed all traces of human and local influence.
<br>
At the foundation of every product there is the production process. When we replaced the craftsmen by machines we lost the translation of local influences into our products. What if our machines could become more sensory? What if the machine could sense the local environment and incorporate it into the production process?
<br>
This research looks into ways we could regain that lost connection with the production of objects. To do so, they decided to design scripts which distil shapes and textures from external phenomena. External information is measured by sensors, eventually translated into specific behaviours of the printer through software. You could call it a sensory machine that feels its environment, translating input into a document of a specific time, location or raw material. However the machine does not operate autonomously. Indeed the designers role is about selecting and distilling only certain features from the complexity which surrounds us.                                        
<br>
</p>  
*<em>Pratique institutionnelles, La gestuelle, Territoire-Espace, Matériaux, Couleurs, Typologie</em>
<br></br>
###Impression 3D
###Savoir-faire
###Pièce unique
###Terre
###Céramique
###Numérique
###Ocre
###Pays-bas
###Emprunte
###Brun
*<p align="right">Réf. Clara</p>
####Projet

#Les Artisans et les Métiers.
## Maurice Robert 
<p>1999</p>
<p><em>https://www.persee.fr/doc/dhs_0070-6760_2001_num_33_1_2446_t1_0624_0000_4</em></p>
<p>À contre-courant des études du travail ou de l'artisanat qui mettent l'accent sur l'histoire des techniques, cette courte synthèse adopte une approche ethno-sociologique : elle privilégie l'acteur sans négliger l'outil. L'A., connu pour ses travaux sur le Limousin, insiste sur l'ambiguïté des vocabulaires (traduisant la diversité des situations) en liant le métier d'artisan aux représentations socio-techniques dominantes, aux arts mécaniques ou aux métiers manuels, et débouchant sur une figure contradictoire de l'artisan, acteur tantôt archaïque tantôt innovant, qui participe de l'avant-garde ou du folklore. L'image du sans-culotte ne relève-t-elle pas d'un processus analogue en assimilant abusivement le sans-culotte à l'artisan, à partir de la participation massive des gens de métier au mouvement révolutionnaire ? L'A. retrace le contexte socio-technique de l'emploi des différentes notions, de l'apparition du mot artisan (1546) qui traduit une réalité sociale et juridique identifiable, à celui de corporation (1672) utilisé surtout par ceux qui font preuve d'innovation en matière d'organisation sociale et par ceux qui en réclament déjà la suppression.</p>
*<em>Pratique institutionnelles, Peuple-Identité, La gestuelle, Territoire-Espace</em>
<br></br>
###Savoir-faire
###Adaptation
###Geste
###Langage
###Occident
*<p align="right">Réf. Julie</p>
####Article

#La Survivance des Lucioles
##Georges Didi-Huberman
<p>2009</p>
![couverture-survivance](https://images-na.ssl-images-amazon.com/images/I/71Lg9n+x+9L.jpg)  
<br>
<p><em>http://www.leseditionsdeminuit.fr/</em></p>
<p>Les Éditions de Minuit</p>  
<p>Dante a, autrefois, imaginé qu'au creux de l'Enfer, dans la fosse des  conseillers perfides , s’agitent les petites lumières (lucciole) des âmes mauvaises, bien loin de la grande et unique lumière (luce) promise au Paradis. Il semble bien que l’histoire moderne ait inversé ce rapport : les  conseillers perfides  s’agitent triomphalement sous les faisceaux de la grande lumière (télévisuelle, par exemple), tandis que les peuples sans pouvoir errent dans l’obscurité, telles des lucioles.
Pier Paolo Pasolini a pensé ce rapport entre les puissantes lumières du pouvoir et les lueurs survivantes des contre-pouvoirs. Mais il a fini par désespérer de cette résistance dans un texte fameux de 1975 sur la disparition des lucioles. Plus récemment, Giorgio Agamben a donné les assises philosophiques de ce pessimisme politique, depuis ses textes sur la  destruction de l’expérience  jusqu’à ses analyses du  règne  et de la  gloire .
On conteste ici ce pronostic sans recours pour notre  malaise dans la culture . Les lucioles n’ont disparu qu’à la vue de ceux qui ne sont plus à la bonne place pour les voir émettre leurs signaux lumineux. On tente de suivre la leçon de Walter Benjamin, pour qui déclin n’est pas disparition. Il faut  organiser le pessimisme , disait Benjamin. Et les images — pour peu qu’elles soient rigoureusement et modestement pensées, pensées par exemple comme images-lucioles — ouvrent l’espace pour une telle résistance.<br> 
</p>  
*<em>Territoire-Espace. Peuple-identité. Croyances théoriques-Idéologique</em>
<br></br>
###Italie
###Folklore
###Adaptation
###Occident
*<p align=right>Réf. Clara</p>
####Livre



#L'appui technique aux artisans 
##A. Letowski / J. Quémeré
<p>1985</p>
<p><em>https://www.persee.fr/doc/ecoru_0013-0559_1985_num_169_1_3193</em></p>
<p>Éditeur (si livre) / Type d'impression / technique (si image) / etc.</p>  
<p>
<strong>La Direction de l'Artisanat demande chaque année aux Chambres de métiers et Organisations professionnelles un compte rendu d'activité relatif aux temps passés par les agents d'assistance technique financés par le Ministère; ces agents sont pour une très large part, ceux qui mettent en œuvre l'appui technique.</strong>
</p>
*<em>Pratique institutionnelles, Peuple-Identité, Territoire-Espace</em>
<br></br>
###Savoir-faire
###Langage
###Évolution
###Occident
###Identité
###Développement local
*<p align=right>Réf. Julie</p>
####Article

#Micro Urban Mining
##Jorien Wiltenburg
<p>2015</p>
![urbanmining](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Etienne///63c2acea23154e41571851ba39f14194.jpg)
<br>
<p><em>https://www.studiojorienwiltenburg.com/projectmicrourbanmining</em></p>
<p>La micro-exploitation minière urbaine est une forme d'extraction de métaux à petite échelle par des particuliers. Un scénario d'avenir, qui fait ode aux métaux (naturels) oubliés. La micro-extraction minière urbaine est un système basé sur des processus existants qui peuvent être transformés en une nouvelle vision sur la manière de traiter les déchets électroniques.</p>  
*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, Peuple-Identité, La gestuelle, Territoire-Espace, Matériaux, Couleurs, Typologie</em>
<br></br>
###Minerai
###Déchet
###Outil
###E-déchet
###Savoir-faire
###Développement local
###Adaptation
###Anthropocène
###Occident
###Cuivre
###Maillage
###Tissage
###Métal
###Durabilité
###Expérience
###Pays-bas
*<p align=right>Réf. Etienne</p>
####Projet

#Filmer, montrer, entendre des savoir-faire. Regards et écoutes croisés dans la médina de Fès
##Baptiste Buob
<p>2009</p>
<p><em>https://journals.openedition.org/ateliers/8206</em></p>
<p> En opérant un retour sur le déroulement d’une enquête ethnofilmique menée auprès des dinandiers de la médina de Fès, cet article s’interroge sur ce qui est initialement apparu comme une contradiction : les travailleurs ont tous accepté la présence de l’ethnologue-cinéaste et de sa caméra dans leurs ateliers, mais ils ne s’impliquaient pas lors des entretiens prenant appui sur les films. Ce texte offre un éclairage sur les décalages pouvant exister entre les discours de ces artisans et la réalité sociotechnique de leur condition, tout en présentant une réflexion sur certaines caractéristiques d’une méthode de recherche ethnographique fondée sur une pratique cinématographique. Afin de s’accommoder d’une situation qu’ils rejettent, les artisans reprennent les termes d’un discours patrimonial idéalisant une réalité socio-économique ; le contexte du travail favorise l’établissement d’une forme de coopération originale entre filmant et filmés ; le refus de procéder à un échange de vues prenant appui sur les films peut être productif dans la compréhension d’un objet de recherche.</p>
*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, Peuple-Identité, La gestuelle, Territoire-Espace, Matériaux,Typologie</em>
<br></br>
###Geste
###Savoir-faire
###Médina de Fès
###Maghreb 
###Petite série
###Langage
###Développement local
###Folklore
*<p align=right>Réf Julie</p>
####Article

#Conditions socio-économiques et de travail des créateurs et créatrices, artistes et artisan·e·s de l’audiovisuel
##Catherine Ellyson et Mathilde Forest Rivière
<p>2009</p>
<p><em>http://www.sartec.qc.ca/media/uploads/colloque/sartec-revue-de-litterature-web-02.pdf</em></p>
<p> Dans le contexte d’une reconfiguration du système de production et de distribution de l’industrie audiovisuelle et d’une mutation plus générale du marché du travail, la présente revue de littérature synthétise les différents documents disponibles sur les conditions de travail des créateurs et créatrices, artistes et artisan·e·s de l’audiovisuel au Québec. La première section présente tout d’abord les apports substantiels du secteur culturel et de l’industrie audiovisuelle à la société canadienne et québécoise. La deuxième section décrit les divers systèmes qui structurent cette industrie et encadrent le travail des créateurs et créatrices, artistes et artisan·e·s. Les principaux enjeux auxquels fait actuellement face l’industrie sont discutés dans la troisième section. La question du travail atypique et autonome parmi la population générale est traitée dans la quatrième section. Enfin, reprenant les conclusions des sections précédentes, la cinquième section discute du travail atypique et autonome chez les créateurs et créatrices, artistes et artisan·e·s de l’audiovisuel, identifiant les facteurs du recours au travail autonome et analysant ses retombées. Si la présente revue de littérature s’intéresse principalement aux créateurs et créatrices, artistes et artisan·e·s de l’audiovisuel au Québec, des études dont l’objet s’en écarte légèrement ont été intégrées à l’analyse – par exemple sur les industries culturelles et créatives ou encore, sur les conditions socio-économiques des intermittent·e·s français·e·s et belges – cela afin de présenter un nombre suffisant d’avis et ce, dans la perspective la plus large possible. L’objet (les industries culturelles et créatives, le domaine des arts et de la culture, l’industrie du cinéma et de la télévision, etc.), le périmètre géographique (région de Montréal, Québec, Canada, Europe) et l’année de référence des études ont des impacts évidents sur leurs conclusions.</p>
*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, La gestuelle, Territoire-Espace, Typologie</em>
<br></br>
###Savoir-faire 
###Petite série
###Adaptation
###Geste
###Québec
*<p align=right>Réf. Julie</p>
####Article

#Les artisans et les métiers 
##Maurice Robert
<p>1999</p>
<br>
<p><em>https://gallica.bnf.fr/ark:/12148/bpt6k48136052.texteImage</em></p>
<p>Presse universitaire de France</p>  
<p>Analyse la place et l'évolution des métiers et de l'artisanat des origines nos jours. Sans occulter les aspects économiques et juridiques, l'auteur étudie les conditions socio-anthropologiques d'exercice des activités aux XVIIIe, XIXe et XXe siècles et le passage des métiers l'artisanat.</p>
*<em>Pratique institutionnelles, La gestuelle, Territoire-Espace</em>
<br></br>
###Savoir-faire
###Geste
###Évolution 
###Adaptation 
###Langage
###Occident
*<p align=right>Réf. Julie</p>
####Livre

#Plasma rock
##Inge Sluijs 
<p>2017</p>
![Plasma Rock](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Etienne///PlasmaRock3.jpg)  
<br>
<p><em>https://www.ingesluijs.nl/work/brine-awptm</em></p>
<p>Témoin des actions de l’Homme, Plasma rock de Inge Sluijs est une matière chimérique, contre nature, utilisant une technique de chauffe à très haute température pour fondre des déchets hétéroclites en une seule matière. Il s’agit d’un sorte de fossilisation artificielle de la trace des rejets humains et de l’indifférence face à l’incinération constante des ressources. Nous sommes ici spectateurs d’un soit disant monde-objet indestructible (perpetuel renouvellement de la production) réduit à un agglomérat carbonisé. </p>  

*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, La gestuelle, Territoire-Espace, Matériaux</em>
<br></br>
###Transformation
###Déchets
###Savoir-faire
###Minerai
###Anthropocène
###Occident
###Anthracite
###Décharge
###Archéologie
###Simulacre
###Expérience
###Alchimie
###Empreinte
###Angleterre
*<p align=right>Réf. Etienne</p>
####Project

#Botanica
##Formafantasma
<p>2011</p>
![Botanica](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Etienne///botanica-1.JPG)  
<br>
<p><em>https://www.formafantasma.com/botanica</em></p>
<p>La botanique, en tant que discipline, a commencé avec les premiers efforts de l'homme pour identifier les plantes comestibles, médicinales et vénéneuses, ce qui en fait l'une des plus anciennes sciences. Il y a plus de deux siècles, les plantes ont également été classées en fonction de leurs sécrétions, en tant que matière première potentielle. Les objets exposés dans la collection Botanica sont conçus comme si l'ère pétrolière dans laquelle nous vivons n'a jamais eu lieu. </p>  
*<em>Peuple-Identité, Territoire-Espace, Matériaux, Couleurs</em>
<br></br>
###Organique
###Expérience
###Collection
###Brun
###Petite série
###Geste
###Occident
###Pays-bas
###Anthropocène
###Fibre végétale
###Durabilité
*<p align=right>Réf. Etienne</p>
####Projet

#Terre cuite émaillée
##Bernard Palissy
<p>vers 1510 - 1589 ou 1590</p>
![palissy1](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///louvre-bernard-palissy-plat-ovale.jpg)  
![palissy2](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///l.jpg)
![palissy3](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///bernard%20palissy.jpg)
![palissy5](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///7._Bernard_Palissy_tirage_le%cc%81zard_glac%cc%a7ure%cc%81.jpg)
![palissy6](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///5._Bernard_Palissy_moule_grenouille.jpg)
![palissy4](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//cla///6._Bernard_Palissy_tirage_de_grenouille.jpg)
<br>
<p><em>(google images à revoir et subdiviser en plusieurs articles)</em></p>
<p>Bernard Palissy, Moule de grenouille, terre cuite blanche, vers 1550-72 ?,
Écouen, musée national de la Renaissance.<br> (retour à la ligne) 
<strong>Suite du texte</strong> (gras) <em>lorem ipsum</em> (italique) <strong><em>lorem ipsum</strong></em> (gras+italique)</p>  
*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, La gestuelle, Territoire-Espace, Matériaux, Couleurs, Typologie</em>
<br></br>
###Savoir-faire
###Pièce unique
###Geste
###Archéologie
###Empreinte
###Porcelaine
###Vert 
###Bleu
###France
*<p align=right>Auteur de la référence (Nous)</p>
####Type de document (ferme le bloc)

#Matériaux pour servir à l'histoire de Bergerac
##Lutel, Perigueux, Rudel
<p>1701-1800</p>
<br>
<p><em>https://gallica.bnf.fr/ark:/12148/btv1b10035051t/f105.item</em></p>
<p>Documents (1198-1675) ; « Lo libro de vita, loquel es remembranssa dels grands suols et damnages que son estat fachs à la vila de Braguayrac » (1378-1577), copie ; Recherches historiques sur la ville de Bergerac ; « Coutumes et statuts de la ville de Bergerac, traduits du latin en...</p>
*<em>Pratique institutionnelles, La gestuelle, Territoire-Espace, Typologie</em>
<br></br>
###Bergerac
###Limousin
###Savoir-faire
###Langage
###Occident
###France
###Développement local
*<p align=right>Réf. Julie</p>
####Livre

#Matériaux pour servir à l'histoire de la philosophie de l'Inde / par P. Regnaud,...
##Regnaud, Paul
<p>1876-1878</p>
<p><em>https://gallica.bnf.fr/ark:/12148/bpt6k331895/f1.planchecontact</em></p>
<p>Texte<p>
*<em>Pratique institutionnelles, La gestuelle, Territoire-Espace</em>
<br></br>
###Inde
###Langage
###Savoir-faire
*<p align=right>Réf. Julie</p>
####Livre

#La Ressemblance par contact
##Georges Didi-Huberman 
<p>2008</p>
![La ressemblance par contact](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Camille///La%20ressemblance%20par%20contact.jpg)  
<br>
<p><em>Source</em></p>
<p>Édité par <em>Les Éditions de Minuit</em></p>  
<p>Pourquoi les artistes modernes et contemporains ont-ils, aussi obstinément, exploré et utilisé les ressources de l'empreinte, cette façon en quelque sorte préhistorique d’engendrer les formes ? - En quoi le jeu, apparemment si simple, de l’organe (la main…), du geste (enfoncer…) et de la matière (le plâtre…) accède-t-il à la complexité d’une technique et d’une pensée de la  procédure  ? – En quoi cette technique, qui d’abord suppose le contact, transforme-t-elle les conditions fondamentales de la ressemblance et de la représentation ? – À quel genre d’érotisme ce travail du contact donne-t-il lieu ? – Quelle sorte de mémoire et de présent, quelle sorte d’anachronisme l’empreinte propose-t-elle à l’histoire de l’art aujourd’hui ?<br>
À ces questions le présent essai tente de répondre en retraçant une histoire synoptique de l’empreinte, mais aussi en modifiant nos façons habituelles de regarder l’image dans sa singularité : depuis le modèle optique, voire métaphysique, de l’imitation obtenue vers celui, tactile et technique, de son travail en acte. Cela pour modifier nos façons habituelles de comprendre chaque œuvre d’art – celle de Marcel Duchamp prise ici comme cas exemplaire – dans son historicité : depuis le modèle déductif qui peut nous faire imaginer un mouvement de  progrès  du modernisme au postmodernisme, vers un modèle plus complexe qui tient compte des intrications de temporalités hétérogènes dont toute image est faite.</p> 
*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, La gestuelle, Typologie</em>
<br></br>
###Empreinte 
###Geste
###Cognition
###Archéologie 
###Archaïsme
###Simulacre 
###Pièce unique
###Savoir-faire
###Transmission
###Évolution
###Disparition
###Geste
*<p align=right>Réf. Camille</p>
####Livre

#Suaire de Turin
##Inconnu (s.n.)
<p>entre 1260 et 1390</p>
![Suaire de Turin](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Camille///Shroudofturin.jpg)  
<br>
<p><em>https://upload.wikimedia.org/wikipedia/commons/9/9d/Shroudofturin.jpg?uselang=fr</em></p>
<p>Drap de lin jauni de 4,42 mètres de long sur 1,13 mètre de large</p>  
<p>Le saint Suaire de Turin est une pièce de lin tissé en chevron. Sans aucune trace de peinture, elle est tâchée de sang humain, brûlée à divers degrés, depuis de légères roussissures largement déployées jusqu'à des carbonisations d'incendie caractérisées. Elle représente de toute évidence le linceul d'un corps humain qui l'a maculé de ses empreintes faciale et dorsale ainsi que des coulées du sang de ses blessures. L'Église a toujours considéré que c'est Jésus de Nazareth qui avait été enseveli dans ce linceul au soir de sa mort, et qu'il l'avait déposé au matin de sa résurrection.</p>  
*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, Peuple-Identité, La gestuelle, Territoire-Espace, Matériaux</em>
<br></br>
###Empreinte
###Italie
###Turin
###Brun
###Jaune
###Fibre végétale
###Pièce unique
###Archéologie
###Transmission
###Religion
###Geste
###Tissage
###Folklore
*<p align=right>Réf. Camille</p>
####Objet

#Moule à hosties
##Inconnu (s.n.)
<p>14e siècle</p>
![Moule à hosties](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Camille///Moule%20a%cc%80%20hosties.jpg) 
<br>
<p><em>Paris, musée de Cluny - musée national du Moyen Âge</em></p>
<p>Fer forgé, France (origine)</p>  
<p>Scènes de la Vie du Christ</p>  
*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, Peuple-Identité, La gestuelle, Territoire-Espace, Matériaux, Couleurs, Typologie</em>
<br></br>
###France
###Fer
###Métal 
###Production de masse
###Petite série
###Savoir-faire
###Archéologie
###Transmission
###Religion
###Transformation
###Outil
###Ustensile
###Empreinte
###Ornement
###Geste
###Folklore
*<p align=right>Réf. Camille</p>
####Objet

#Jambière à spirales 
##Inconnu (s.n.)
<p>1 200-1 100 avant notre ère</p>
![Jambière à spirales](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Camille///jambie%cc%80res%20a%cc%80%20spirales.jpg)  
<br>
<p><em>Champlay, la Colombine (Yonne) Bronze final.</em></p>
<p>Bronze</p>  
<p>Cette plaque de bronze prolongée par des volutes est une parure féminine qui était portée par paire au niveau des mollets ou au-dessus de la cheville.</p>  
*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, Peuple-Identité, Territoire-Espace, Matériaux, Couleurs</em>
<br></br>
###Bronze 
###France
###Métal
###Petite série 
###Pièce unique
###Archéologie
###Ornement
###Folklore
*<p align=right>Réf. Camille</p>
####Objet

#Figurine d’oiseau en terre cuite de Tigy 
##Inconnu (s.n.)
<p>XIIe-Xe siècle avant notre ère</p>
![Oiseau 01](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Camille///oiseau01.jpg) 
![Oiseau 02](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Camille///oiseau02.jpg)  
<br>
<p><em>Le Bois des Hauts (lieu-dit) (Loiret)</em></p>
<p>Céramique fine brune et lustrée</p>  
<p>À partir du XIIe siècle avant notre ère, la pratique de l’incinération se développe largement en Europe.<br>
Les cendres des défunts sont recueillies dans une urne funéraire disposée avec soin dans des fosses aménagées où sont déposés également des céramiques et quelques objets personnels comme par exemple des bracelets en bronze, des épingles, un couteau ou une pince à épiler.<br>
La petite figurine d’oiseau en céramique fine brune et lustrée de Tigy fut mise au jour à l’occasion des fouilles réalisées de 1973 à 1975 par la Société archéologique de Vienne-en-Val au lieu-dit Le Bois des Hauts dans une nécropole à incinérations datée de la phase moyenne de l’âge du Bronze final (XIIe-Xe siècle avant notre ère). Elle provient de la sépulture 6 où elle avait été déposée avec un petit gobelet dans une grande tasse fermée par une assiette.<br>
Sa forme, très simple, se résume à un corps ovoïde surmonté d’un petit cou et d’une tête à peine marquée, sans bec ni yeux. Elle est creuse et sa base est ouverte. Elle porte sur une face un décor de grosses ponctuations obtenues sur pâte fraîche à l’aide d’un poinçon.</p>  
*<em>Pratique institutionnelles, Peuple-Identité, La gestuelle, Territoire-Espace, Matériaux, Couleurs, Typologie</em>
<br></br>
###France
###Brun
###Céramique
###Savoir-faire
###Petite série
###Pièce unique
###Archéologie
###Transmission
###Folklore
###Transformation
###Simulacre
###Ornement
###Occident
*<p align=right>Réf. Camille</p>
####Objet

#La cuirasse de Saint-germain-du-Plain 
##Inconnu (s.n.)
<p>1200 -1100 avant notre ère</p>
![Cuirasse 01](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Camille///cuirrasse%2001.jpg)
![Cuirasse 02](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Camille///Cuirasse%2002.jpg)
![Cuirasse 03](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Camille///Cuirasse%2003.jpg)
<br>
<p><em>Âge du Bronze final, Saint-Germain-Plain (Saône-et-Loire)</em></p>
<p>Alliage cuivreux</p>  
<p>Cette belle cuirasse pêchée au XIXe siècle dans la Saône à Saint-Germain-du-Plain (Saône-et-Loire) ne déparerait pas sur les épaules d’Achille. <br>
Elle a servi de modèle à Bartholdi pour la statue équestre de Vercingétorix (Clermont-Ferrand).<br>
Et pourtant elle n’est ni grecque ni gauloise mais date de l’âge du Bronze final. Âgée de plus de 3000 ans, il s’agit d’une des plus anciennes cuirasses du monde. Elle a été fabriquée par un artisan bronzier de très grand talent.
Formée de deux coques en tôle de bronze d’un peu moins d’un millimètre d’épaisseur constituant le plastron et le dosseret de l’armure, ce type de cuirasse est généralement coulé dans un alliage composé d’environ 90 % de cuivre et à 10 % d’étain ce qui garantit une certaine plasticité, une bonne résistance mécanique et une belle couleur jaune doré au métal.</p>  
*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, Peuple-Identité, Territoire-Espace, Matériaux, Couleurs, Typologie</em>
<br></br>
###France
###Bronze
###Métal
###Savoir-faire
###Arme
###Chasse
###Pièce unique 
###Archéologie
###Folklore
###Métallurgie
###Ornement
###Empreinte
*<p align=right>Réf. Camille</p>
####Objet

#Moule de hache avec son noyau 
##Inconnu (s.n.)
<p>1200 -1100 avant notre ère</p>
![Moule de hache](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Camille///Moule%20de%20hache.jpg)  
<br>
<p><em>Sorel Moussel, le Fort-Harrouard (Eure-et-Loir) Bronze final</em></p>
<p>Bronze</p>  
<p>Les moules, dans lesquels étaient coulés le bronze fondu à environ 1000 degrés, pouvaient être en pierre, pour les objets peu volumineux, en terre cuite et même en bronze, comme ce moule bivalve destiné à la fabrication de haches.<br>
Un noyau, également en bronze et fixé à l’intérieur du moule, permettait d’obtenir la partie creuse de la hache. Une fois coulée, la hache nécessitait une finition à froid.</p>  
*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, Peuple-Identité, Territoire-Espace, Matériaux, Couleurs, Typologie</em>
<br></br>
###Bronze
###France
###Métal
###Savoir-faire
###Petite série
###Chasse
###Arme
###Archéologie
###Folklore
###Outil
###Métallurgie
###Empreinte
###Geste
*<p align=right>Réf. Camille</p>
####Objet

#Hache danoise 
##Inconnu (s.n.)
<p>Milieu du IXe-début du Xe siècle apr. J.-C.</p>
![Hache danoise](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Camille///hache%20danoise.jpg)  
<br>
<p><em>Les Andelys (Eure)</em></p>
<p>Fer</p>  
<p>Découverte au fond de la Seine près des Andelys (Eure) à la fin du XIXe siècle, cette hache est un témoin des expéditions vikings et de l’intégration progressive de ces populations scandinaves, avec plus ou moins de succès, dans les sociétés d’Europe occidentale.<br><br>
PLUS DE MILLE ANS DANS LES EAUX DE LA SEINE<br><br>
Acquise par le musée en 1901, cette hache fait partie d’un ensemble de 75 objets métalliques, pour la plupart médiévaux et modernes, provenant de dragages de la Seine aux environs de Vernon (Eure). Il s’agit du don de Mme Caméré, veuve d’un des ingénieurs en charge de travaux d’aménagement sur la Seine dans la seconde moitié du XIXe siècle. Les conditions de découvertes sont floues, mais l’objet a peut-être été trouvé en 1882, près de la petite commune normande des Andelys, dans le département de l’Eure. À l’exception des quelques arrachements, la bonne conservation de l’arme s’explique par son séjour prolongé de plus de mille ans dans les eaux de la Seine.<br>
Cette hache fait partie des objets postérieurs à l’époque mérovingienne, peu nombreux, qui sont conservés au musée d’Archéologie nationale. En effet, une grande majorité des collections du musée sont issues de fouilles en contexte funéraire, inévitablement privilégiées par les chercheurs du XIXe et du début du XXe siècles pour la quantité et la qualité du mobilier exhumé. Or, dès le début du VIIIe siècle, les traditions funéraires évoluent, et l’on renonce à déposer des objets auprès des défunts, probablement dans une recherche d’humilité face à Dieu. De fait, les collections sont donc beaucoup moins riches en mobilier archéologique pour la fin du premier Moyen Âge. Bien qu’ils nous privent de contexte archéologique, les dragages de fleuves offrent donc au moins un aperçu du mobilier en usage à l’époque carolingienne.<br><br>
UNE ARME VIKING <br><br>
Si la plupart des haches ont une fonction d’outil, on ne peut douter que celle-ci est une arme de guerre. L’archéologue norvégien Jan Petersen (De Norske Vikingesverd, 1919) définit cette catégorie d’objets comme des "haches danoises », et celle du musée d’Archéologie nationale appartient plus précisément au type G, en usage d’après lui entre le milieu du IXe et la fin du Xe siècle et qu’il décrit comme des « haches à tranchants et lobes d’emmanchement symétriques ».<br>
La lame de la hache des Andelys a l’aspect d’un triangle isocèle qui se resserre vers la douille dans laquelle s’encastre le manche. Sa forme hexagonale est due aux deux lobes qui se développent perpendiculairement au manche afin d’améliorer sa fixation. La symétrie des tranchants des haches du type G permet de les distinguer du type L, qui apparaît peu de temps après. Les « haches danoises » de type L ont un angle de fer nettement plus aigu dans leur partie haute. Apparues vers 900, elles concurrencent dans un premier temps les haches du type G, avant de les supplanter un siècle plus tard.<br>
Le manche en bois n’est pas conservé, mais les descriptions faites dans les textes carolingiens laissent croire qu’il aurait pu mesurer plus d’un mètre et demi. La hache des Andelys se tient donc à deux mains et sa relative légèreté permet de frapper avec autant de force que de rapidité. Les lames du type L, plus aiguës, confèrent quant à elles une plus grande puissance à l’impact. À titre de comparaison, les mercenaires danois du roi Harold représentés sur la Tapisserie de Bayeux sont équipés des haches les plus récentes, celles du type L, qui supplantent celles du type G au début de l’an mille.<br><br>
DES DANOIS EN FRANCE ? <br><br>
Découverte en 1886, la « hache danoise » la plus célèbre du monde scandinave provient de la tombe de Mammen, près de Viborg (Danemark). Il s’agit d’une sépulture particulièrement riche, appartenant à un personnage de haut rang, qui y était enterré avec de la vaisselle précieuse et deux haches. La plus belle des deux sert de référence pour le « style de Mammen », l’une des phases de l’art des vikings, caractérisé notamment par des décors végétaux et des motifs animaliers particulièrement développés. Rattachée au type G de Jan Pertersen, elle porte un décor d’incrustations d’argent figurant un oiseau dans un entrelacs de végétaux.
La forme triangulaire de son fer est caractéristique de la production scandinave d’époque viking et se distingue des haches carolingiennes, dont le fer est en T. De nombreuses haches sont découvertes sur des sites norvégiens, russes ou irlandais, et des mercenaires suédois, appelés les « porteurs de haches », sont au service des empereurs romains d’Orient. Si elle n’est pas aussi luxueuse que celle de Mammen, la hache des Andelys est une arme du même type, et probablement contemporaine. Vraisemblablement forgée en Scandinavie, elle est introduite dans le monde carolingien à l’occasion des incursions vikings entre la fin du VIIIe et le milieu du XIe siècle. Leur vitesse et leur mobilité permettent à ces derniers de pénétrer profondément dans les terres en naviguant le long des fleuves, notamment la Seine et la Loire.<br>
En échange de la reconnaissance de leur autorité, le roi carolingien Charles III le Simple décide en 911, en concluant le traité de Saint-Clair-sur-Epte, de confier l’administration et la défense de la basse vallée de la Seine, l’actuelle Normandie, à un chef viking nommé Rollon. Datée entre le milieu du IXe et la fin du XIe siècle, la hache des Andelys appartient donc peut-être à l’un de ces « hommes du Nord » venus s’installer définitivement en territoire franc, et dont la Normandie tire encore son nom.<br><br>
BIBLIOGRAPHIE<br><br>
PERRIER, Daniel. Une hache danoise. In : Archéologia, juin 2016, n°544, p. 22-23.<br>
PETERSEN, Jan. De Norske Vikingesverd : En Typologisk-Kronologisk Studie Over Vikingetidens Vaaben, Kristiania : I komission hos Jacob Dybwad, 1919.
</p> 
*<em>Croyances théoriques-Idéologie, Pratique institutionnelles, Peuple-Identité, La gestuelle, Territoire-Espace, Matériaux, Couleurs, Typologie</em>
<br></br>
###Nordique
###Danemark
###Fer
###Métal
###Petite série
###Arme
###Chasse
###Archéologie
###Évolution
###Folklore
###Outil
###Disparition
###Occident
###Métallurgie
*<p align=right>Réf. Camille</p>
####Objet



#Matrice secondaire de moule
##Abd al-Jabbar al-Samarqandi
<p>18e siècle</p>
![Matrice pot](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Camille///matrice%20pot.jpg)  
<br>
<p><em>Paris, musée du Louvre</em></p>
<p>Terre cuite</p> 
*<em>Pratique institutionnelles, Peuple-Identité, Territoire-Espace. Matériaux. Croyances théoriques-Idéologie. Typologie</em>
<br></br>
###Religion
###Maghreb 
###Céramique
###Petite série
###Savoir-faire
###Archéologie
###Collection
###Folklore
###Transformation 
###Outil
###Ustensile
###Empreinte
###Ornement
*<p align=right>Réf. Camille</p>
####Objet

#Matrice de sceau
##Inconnu (s.n.)
<p>(s.d.)</p>
![Matrice de sceau](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Camille///matrice%20de%20seau.jpg)  
<br>
<p><em>Lille, Palais des Beaux-Arts</em></p>
<p></p> 
*<em>Pratique institutionnelles, Peuple-Identité. Territoire-Espace. Croyances Théoriques-Idéologie. Matériaux. Typologie. Gestuelle</em>
<br></br>
###France
###Métal
###Savoir-faire
###Pièce unique 
###Archéologie 
###Transmission 
###Occident
###Folklore
###Outil
###Ornement
###Geste

*<p align=right>Réf. Camille</p>
####Objet

#Totomoxtle
##Fernando LaPosse
<p>2018</p>
![Lucy](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Etienne///lucy-with-harvest-and-colored-leaves.jpg)  
![Lucy](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr3/files//Etienne///totomoxtle-legacy-table-detail.jpg)
<br>
<p><em>https://www.fernandolaposse.com/projects/totomoxtle/</em></p>
<p>Totomoxtle se concentre sur la régénération des pratiques agricoles traditionnelles au Mexique, et la création d'un nouvel artisanat qui génère des revenus pour les agriculteurs appauvris et favorise la préservation de la biodiversité pour la sécurité alimentaire future.<br> 
</p>  
*<em>Pratique institutionnelles, Peuple-Identité, Territoire-Espace, Matériaux</em>
<br></br>
###Savoir-faire
###Durabilité
###Développement local
###Fibre végétale
###Identité
###Mexique 
*<p align=right>Réf. Etienne</p>
####Projet

#Le véhicule électrique
##Inconnu (s.n.)
<p>3e année, Nouvelle série-n°8, Septembre 1929</p>
![Véhicule électrique](http://cnum.cnam.fr/GIFS/P143.8/0015.T.55.1296.1670.jpg)
<br>
<p><em>http://cnum.cnam.fr/CGI/redir.cgi?P143.8</p>
<p> Les chariots à accumulateurs dans les ateliers d'entretien de la S. T. C. R. P.</em></p>
<p>bulletin trimestriel de la Société pour le développement des véhicules électriques
*</p>
<em>Croyances théoriques-Idéologie, Pratique institutionnelles, Peuple-Identité, La gestuelle, Territoire-Espace, Matériaux, Couleurs, Typologie</em>
<br></br>
###France
###Production de masse
###Savoir-faire
###Transmission
###Transformation
###Outil
###Empreinte
*<p align=right>Réf. Julie</p>
####Article


#ART DE PRÉPARER ET D'IMPRIMER LES ÉTOFFES EN LAINES
##M. Rolland De La Platière
<p>1780</p>
![machine](http://cnum.cnam.fr/GIFS/4KY58.19/0164.P.2x1.1120.874.gif)
<br>
<p><em>http://cnum.cnam.fr/CGI/sresrech.cgi?4KY58.19/164</p>
<p>SUIVI DE L'ART DE FABRIQUER LES PANNES OU PELUCHES, LES VELOURS FACON UTRECHT, ET LES MOQUETTES, étoffes les plus susceptibles de l'impression & du gauffrage [sic, gaufrage].</p>
<p> Par M. ROLLAND DE LA PLATIERE, inspecteur-général des manufactures de Picardie, associé des académies royales des sciences, belles-lettres & arts de Rouen, Villefranche, etc. & correspondant de la société royale des sciences de Montpellier (p.107)</p>
<em>Croyances théoriques-Idéologie, Pratique institutionnelles, La gestuelle, Territoire-Espace, Matériaux,Typologie</em>
<br></br>
###France
###Production de masse
###Savoir-faire
###Transmission
###Transformation

###Outil
###Geste
*<p align=right>Réf. Julie</p>
####Livre



