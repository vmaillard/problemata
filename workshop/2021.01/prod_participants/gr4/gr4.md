#L'abeille et la ruche (livre)
## Alain Péricard
![abeilleRuche](https://www.colibris-laboutique.org/1649-large_default/l-abeille-et-la-ruche.jpg)
<br><br>[L'abeille et la ruche : Manuel d'apiculture écologique](http://univ-toulouse.scholarvox.com/catalog/book/docid/88866102?searchterm=maraichage)
###Apiculture
####Agriculture

#Shelter (livre)
![shelter](https://images.squarespace-cdn.com/content/v1/55e89a25e4b0434fc3438dc7/1442348917293-FZEAWMY35WB7DODNHPDH/ke17ZwdGBToddI8pDm48kBBdV2PWRHvTsgs6oYf3PxB7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UQA4tUtueOu0T67yiCXmJeDox0N1tEVYw7zCmYU2SAyHWCpHOZh6hSrxq3ojnEVSGQ/shelter.jpg)
##Lloyd Kahn
###Lloyd Kahn
####Architecture

#The Half-acre Homestead (livre)
##Lloyd Kahn & Lesley Creed
![santéSols](http://problemata.huma-num.fr/ws/workshop-massive-attack/gr4/files/hw_FC.jpg)
###Santé des sols
###Lloyd Kahn
####Architecture

#Small Homes, the right size (livre)
![smallHomes](https://images.squarespace-cdn.com/content/55e89a25e4b0434fc3438dc7/1485297634504-WOGY2TD00764NM0CUZ9H/SMHO-1800w.jpg)
##Lloyd Kahn
###Lloyd Kahn
####Architecture

#Tiny Homes on the Move (livre)
![tinyHomes](https://images-na.ssl-images-amazon.com/images/I/A1zYObeLDKL.jpg)
##Lloyd Kahn
###Transport
###Lloyd Kahn
####Architecture

#Tiny Homes : Simple Shelter (livre)
![tinyHomesSimple](https://images.squarespace-cdn.com/content/v1/55e89a25e4b0434fc3438dc7/1442439533418-FLXC4R81XF99XXG3WB8J/ke17ZwdGBToddI8pDm48kBBdV2PWRHvTsgs6oYf3PxB7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UQA4tUtueOu0T67yiCXmJeDox0N1tEVYw7zCmYU2SAyHWCpHOZh6hSrxq3ojnEVSGQ/tiny_homes.jpg)
##Lloyd Kahn
###Lloyd Kahn
####Architecture

#Builders of the Pacific Coast (livre)
![buildersPacific](https://images-na.ssl-images-amazon.com/images/I/91brFvxuZEL.jpg)
##Lloyd Kahn
###Lloyd Kahn
####Architecture

#HomeWork (livre)
![homeWork](https://images-na.ssl-images-amazon.com/images/I/81JnUVvSdQL.jpg)
##Lloyd Kahn
###Lloyd Kahn
####Architecture

#Shelter II (livre)
##Lloyd Kahn
![shelter2](https://images-na.ssl-images-amazon.com/images/I/913DEPdRRJL.jpg)
###Lloyd Kahn
###Copeaux Cabana ???
####Architecture

#Driftwood Shacks: Anonymous Architecture Along the California Coast (livre)
![driftwood](https://images-na.ssl-images-amazon.com/images/I/71wKwsNkt0L.jpg)
##Lloyd Kahn
###Lloyd Kahn
###Charpente
####Architecture

#The Barefoot Architect (livre)
![barefoot](https://images-na.ssl-images-amazon.com/images/I/61OwQsC2v3L._SX324_BO1,204,203,200_.jpg)
##Lloyd Kahn ?? Johan van Lengen
###Lloyd Kahn
####Architecture

#The Septic System Owner’s Manual (livre)
![septic](https://images.squarespace-cdn.com/content/55e89a25e4b0434fc3438dc7/1442349208622-EZ4580F46L2CNEIA8MNC/septic.jpg?format=1500w&content-type=image%2Fjpeg)
##Lloyd Kahn
###Lloyd Kahn
###Sanitaire
####Sanitaire

#Vélo électrique Solaire. Transport gratuit (vidéo)
##Barnabé Chaillot
![veloElectrique](https://i.ytimg.com/vi/w6V8lTJ-d8w/maxresdefault.jpg)<br><br>
[Lien vers la vidéo](https://www.youtube.com/watch?v=w6V8lTJ-d8w)
###Barnabé Chaillot
###Transport
###Energie
####Energie

#FR Transition énergétique d'une yourte: Episode 20 (vidéo)
##Barnabé Chaillot
![yourte](https://i.ytimg.com/vi/vXptoJ6EJPg/maxresdefault.jpg)<br><br>
[Lien vers la vidéo](https://www.youtube.com/watch?v=vXptoJ6EJPg)
###Barnabé Chaillot
###Energie
####Architecture

#Eau de pluie dans les WC (vidéo)
##Barnabé Chaillot
![eauDePluieWc](https://i.ytimg.com/vi/kXVqazu-12g/maxresdefault.jpg)<br><br>
[Lien vers la vidéo](https://www.youtube.com/watch?v=kXVqazu-12g)
###Barnabé Chaillot
###Sanitaire
####Sanitaire

#Je bois mon eau de pluie :) (vidéo)
##Barnabé Chaillot
![boireEauDePluie](https://i.ytimg.com/vi/9ooBy1MuVoE/maxresdefault.jpg)<br><br>
[Lien vers la vidéo](https://www.youtube.com/watch?v=9ooBy1MuVoE)
###Barnabé Chaillot
###Sanitaire
####Sanitaire

#Off Grid Log Cabin Built by One Man: Log Gables and a Bushcraft Mystery (vidéo)
##Advoko makes
![offGridLog](https://i.ytimg.com/vi/fQSpQH9t_mM/maxresdefault.jpg)<br><br>
[Lien vers la vidéo](https://www.youtube.com/watch?v=fQSpQH9t_mM)
###Advoko Makes
###Bushcraft
####Architecture

#Un four à pizza maison construit en 5 minutes (vidéo)
##Ma ferme autonome
![pizzaFour](https://i.ytimg.com/vi/aWfI0h9QVvk/maxresdefault.jpg)<br><br>
[Lien vers la vidéo](https://www.youtube.com/watch?v=aWfI0h9QVvk)
###Ma ferme autonome
###Lowtech
####Alimentation??

<!--

#--- (vidéo)
##Townsends
//lie pour la vidéo()
###---
###----
####a definir

#--- (vidéo)
##L'archi pelle
//lie pour la vidéo()
###---
###----
####a definir

#--- (vidéo)
##Les ateliers du Hameau
//lie pour la vidéo()
###---
###----
####a definir

-->

#Shelter publications (blog)
##Lloyd Kahn
[Lien vers le site](https://www.shelterpub.com/)
###Lloyd Kahn
####Architecture

#L'énergie autrement (blog)
[Lien vers le site](http://energie-autrement.blogspot.com/2020/)
##Barnabé Chaillot
###Barnabé Chaillot
###Energie
####Energie

#Toilettes sèches familiales (blog)
##Lowtech Lab
##Maisons Nomades
[Lien vers le site](https://wiki.lowtechlab.org/wiki/Toilettes_sA13%A8ches_familiales)
###Lowtech Lab
###Maisons Nomades
###Sanitaire
####Sanitaire

#Low tech : Une entreprise Toulousaine remplace les parpaings par des palettes (article)
##Esprit Cabane
##Sofrinov
![murPalette](https://www.espritcabane.com/img/creations/montage-murs-en-palettes1.jpg)<br><br>
[Lien vers le site](https://www.espritcabane.com/blog/maison-ecolo/low-tech-une-entreprise-toulousaine-remplace-les-parpaings-par-des-palettes/)
###Esprit Cabane
###Sofrinov
###Lowtech
###Maçonnerie
###Menuiserie
####Architecture

# Commuons (blog)
##Pilippe Aigrain
[Lien vers le site](http://paigrain.debatpublic.net/)
###Pilippe Aigrain
###Social
###Communauté
####Société

#Lowtech Magazine (blog)
##Kris de Decker
[Lien vers le site](https://www.lowtechmagazine.com/)
###Lowtech Magazine
###Lowtech
####Lowtech

#NO TECH MAGAZINE (blog)
##Auteurs multiples
[Lien vers le site](https://www.notechmagazine.com/)
###No Tech Magazine
###Lowtech Magazine
###Lowtech
####Lowtech

#Agroforesterie (pratique)
##Collectif Ecorce
![bucheronnage à cheval](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr4/files//architecture///08.jpg)<br><br>
Débardage à cheval
###Lowtech
###Agroforesterie
####Agriculture

#Une nouvelle grange pour Flaceleyre (édifice)
##Copeaux Cabana
![grangeCC](https://copeauxcabana.fr/media/news/img/_7180160.jpg.650x0_q85_upscale.jpg)
<br><br>[Lien vers le site](https://copeauxcabana.fr/projets/2/)
###Copeaux Cabana
###Charpente
####Architecture

#L'atelier de Copeaux (édifice)
##Copeaux Cabana
![atelierCC](https://copeauxcabana.fr/media/news/img/Atelier_copeauxcabanaa.JPG.650x0_q85_upscale.jpg)
<br><br>[Lien vers le site](https://copeauxcabana.fr/projets/54/)
###Copeaux Cabana
###Charpente
####Architecture

#Cabanes en tous genres (édifice)
##Copeaux Cabana
![cabanesCC](https://copeauxcabana.fr/media/news/img/cabane_Yogan.jpg.650x0_q85_upscale.jpg)<br><br>
[Lien vers le site](https://copeauxcabana.fr/projets/56/)
###Copeaux Cabana
###Charpente
###Transport
####Architecture

#Restauration ruine (édifice)
##Copeaux Cabana
![ruineCC](https://copeauxcabana.fr/media/news/img/DSC08867.JPG.650x0_q85_upscale.jpg)<br><br>
[Lien vers le site](https://copeauxcabana.fr/projets/47/)
###Copeaux Cabana
###Maçonnerie
####Architecture

#La miellerie (édifice)
##Copeaux Cabana
![miellerieCC](https://copeauxcabana.fr/media/news/img/IMG_20170427_172314_copie.jpg.650x0_q85_upscale.jpg)<br><br>
[Lien vers le site](https://copeauxcabana.fr/projets/27/)
###Copeaux Cabana
###Charpente
####Architecture

#L'école des renardes (école)
##Fanny Colin
--------
###Fanny Colin
###Social
###Enseignement
####Société

#École territoriale  (concept)
##Alberto Magnaghi
[Lien vers un document explicatif](https://halshs.archives-ouvertes.fr/halshs-02980788/document)
###Alberto Magnaghi
###Société
###Enseignement
####Société

#Copeaux Cabana (collectif)
##Copeaux Cabana
![Cabanes](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr4/files//architecture///hw_88-89+(1).jpg)
Collectif de charpentiers<br>
Situés en Dordogne, ils font des **cabanes** et des *bateaux*
###Copeaux Cabana
###Charpente
####Acteur

#L'atelier Paysan (coopérative)
![atelierPaysan](https://static.mediapart.fr/etmagine/default/files/2017/09/06/axp-07958.jpg)<br><br>
[Lien vers le site](https://www.latelierpaysan.org/)
<br>"L'Atelier Paysan est une coopérative (SCIC SARL). Nous accompagnons les agriculteurs et agricultrices dans la conception et la fabrication de machines et de bâtiments adaptés à une agroécologie paysanne. En remobilisant les producteurs et productrices sur les choix techniques autour de l’outil de travail des fermes, nous retrouvons collectivement une souveraineté technique, une autonomie par la réappropriation des savoirs et des savoir-faire."<br><br>
###Machinerie agricole
###Lowtech
###Enseignement
####Acteur

#Open source Ecology (association?)
[Lien vers le site](https://www.opensourceecology.org/gvcs/)
<br>"The Global Village Construction Set (GVCS) is a modular, DIY, low-cost, high-performance platform that allows for the easy fabrication of the 50 different Industrial Machines that it takes to build a small, sustainable civilization with modern comforts. We’re developing open source industrial machines that can be made at a fraction of commercial costs, and sharing our designs online for free"<br><br>
###Machinerie
###Lowtech
####Acteur

#Prommata (association)
![prommata](https://www.pagesjaunes.fr/media/ugc/prommata_00924600_130209365)<br><br>
[Lien vers le site](https://www.prommata.org/?lang=fr)
<br>"PROMMATA est une association à but non lucratif créée en 1991 par des paysans français persuadés que la Traction Animale Moderne est un moyen pour soutenir et développer l’agriculture paysanne et revitaliser l’espace rural."<br><br>
###Agriculture
####Acteur

#Fanny Colin (artisane)
###Fanny Colin
###Charpente
###Social
###Enseignement
####Acteur

#La Quadrature du Net (association)
![quadrature](https://www.laquadrature.net/wp-content/uploads/sites/8/2019/10/og.png)<br>
<br>"La Quadrature du Net promeut et défend les libertés fondamentales dans l’environnement numérique. L’association lutte contre la censure et la surveillance, que celles-ci viennent des États ou des entreprises privées. Elle questionne la façon dont le numérique et la société s’influencent mutuellement. Elle œuvre pour un Internet libre, décentralisé et émancipateur."<br><br>
###La Quadrature du Net
###Société
####Acteur

#M.A.U.S.S. (mouvement)
##Alain Caillé
[Lien vers lesite](https://www.revuedumauss.com.fr/Pages/APROP.html)
<br>Mouvement anti-utilitariste en sciences sociales<br><br>
###Alain Caillé
###Société
###Communauté
####Acteur

#ARESO (association)
![areso](https://www.cycl-op.org/data/sources/users/204/20200813100748-logoareso---33prct.jpg)<br><br>
[Lien vers le site](http://www.areso.asso.fr/)
<br>Association régionale d'Eco-Construction du Sud-Ouest<br><br>
###Société
###Enseignement
###Architecture
####Acteur

#Lloyd Kahn (auteur)
![lloydKahn](https://upload.wikimedia.org/wikipedia/commons/3/33/Lloyd_Kahn_2004.jpg)<br><br>
"Lloyd Kahn (born April 28,[1] 1935) is the founding editor-in-chief of Shelter Publications, Inc., and is the former Shelter editor of the Whole Earth Catalog. He is also an author, photographer, and pioneer of the green building and green architecture movements. "<br><br>
[Lien pour le site](https://www.lloydkahn.com/)
###Lloyd Kahn
###Transport
###Architecture
###Menuiserie
###Sanitaire
####Auteur

#Alberto Magnaghi (auteur)
![alberto](https://i0.wp.com/www.eterotopiafrance.com/wp-content/uploads/12190133_547743092040189_2545313285109796153_n.jpg)<br><br>
"Alberto Magnaghi, né à Turin en 1941, fondateur et président de la Société des territorialistes (Società dei territorialisti), est surtout connu en France pour son ouvrage Le Projet local (Magnaghi 2003)"
###Alberto Magnaghi
###Société
###Enseignement
####Auteur

#Alain Péricard (auteur)
![alainPericard](https://ecosociete.org/uploads/book_author/picture/346/Alain-Pe_ricard_site-web.jpg)<br><br>
[Alain Péricard](http://univ-toulouse.scholarvox.com/catalog/search/searchterm/Péricard)
###Alain Péricard
###Apiculture
####Auteur

#Philippe Aigrain (auteur)
<br>
"Je suis actif dans diverses initiatives politiques, associatives ou scientifiques. Impliqué dans la solidarité avec les exilés, je suis l’un des initiateurs de J’accueille l’étranger qui vise à rendre l’accueil et le soutien à son principe plus visible."
<br><br>
###Alain Péricard
###Société
####Auteur

#Institut momentum  (institut)
![institutMementum](https://www.institutmomentum.org/wp-content/uploads/2020/04/logo_momentum.gif)<br><br>
[Lien vers le site](https://www.institutmomentum.org/)
###Institut momentum
###Société
###Enseignement
####Auteur

<!--
________________________________________________________________________


> Autonomie / Technologie / Cultures vernaculaires / Partage des communs / Lowtech - Technique et Création autonome au sein de cultures vernaculaires<br> <br>
>> Base de données liée au wiki *"la maison rustique du XXIème siècle"*


répertorier des acteurs porteurs de savoir-faire dans le domaines de l'artisanat et de la création, dans le domaine plus particulier de l'habitat lowtech. concevoir des liens entre les porteurs de savoir et entre ces porteurs et les savoirs, puis 'noter'/'rate' ces liens en vue des niveaux de qualifications dans le savoir et/ou des niveaux de tech du savoir


//tout refaire au propre depuis le tableur en format pad

2 types d'objets 

référence
    ex : lowtech mag

    type : livre entier, article dans un livre, interview
    titre : 
    date : 
    description : 
    lien :

acteur / porteur
    ex : Copeaux Cabana
    
    type : personne physique / personne morale / collectif / studio / atelier / ...
    geoloc : 
    nom : 
    description : 
    lien : 
    domaine(s) : Line12(architecture), charpente, lowtech
    tech rate : lowtech / midtech / hightech //OU// 1->10 (de 0 à 10)
    reference(s) : Line1 (lowtechmag)
    
    
(youtube: https://www.youtube.com/watch?v=McFqG0Hkm0E&t=602s)
domaine
    nom : architecture





machinerie agricole
https://www.opensourceecology.org/gvcs/

https://www.prommata.org/




#Maraîchage
##Cultures cultures cultures
#Tomates
Ressources culture de tomates
####legumes
#Courgettes
Ressources courgettes
####legumes
#Salades
Ressources salades
####legumes
####Agriculture

#Machinerie agricole
>###Technique et souveraineté :
>
> - L'atelier paysan
> - Lowtech magasine

#Reproduction des semences

#Diverses serres

#Contre-cultures architecturales

#Lowtechs

Monsieur Chaillot fait des vidéos
*
####lowtech
#Gestion des eaux

*-->
