
#Bienvenue sur le Framapad dédié aux **Formes et usages d'un temps de l'échange [EconomieXdesign]** !

Ce service s’inscrit dans le réseau associatif Framasoft qui propose un ensemble de sites et de projets autour du logiciel libre, sa culture et son état d’esprit.

   ###**« Did I worked on the project, or did the project work on me ; perhaps, the both. » Cycle of Mental Machine, Electrifying Mojo**

        ➡ Comment commencer ?
        • Renseignez votre nom ou pseudo, en cliquant sur l’icône «utilisateur » en haut à droite.
        • Choisissez votre couleur d'écriture au même endroit.
        • Lancez-vous : écrivez sur votre pad !
        • Les contributions de chacun se synchronisent « en temps réel » sous leur propre couleur.

        ➡ Comment partager / collaborer ?
        • Sélectionnez et copiez l'URL (l'adresse web dans la grande barre en haut à gauche du 
        navigateur)
        • Partagez-là à vos collaborateurs et collaboratrices (email, messagerie, etc.)
        • Attention : toute personne ayant cette adresse d'accès peut modifier le pad à sa 
        convenance.
        • Utilisez l'onglet chat (en bas à droite) pour séparer les discussions du texte sur 
        lequel vous travaillez.

        ➡ Comment sauvegarder ?
        • Il n'y a rien à faire : le texte est automatiquement sauvegardé, à chaque caractère 
        tapé.
        • Marquez une version (un état du pad) en cliquant sur l’icône « étoile ».
        • Retrouvez toute l'évolution du pad et vos versions marquées d'une étoile dans 
        l’historique (icône « horloge »).
        • Importez et exportez votre texte avec l'icône « double flèche» (formats HTML, texte 
        brut, PDF, ODF…) ou avec un copier/coller.

        Important ! N’oubliez pas de conserver quelque part l’adresse web (URL) de votre pad.

        **ATTENTION**
        CETTE INSTANCE PROPOSE DES PADS À EFFACEMENT AUTOMATIQUE !
        VOS PADS SERONT AUTOMATIQUEMENT SUPPRIMÉS AU BOUT DE 7 JOURS SANS ÉDITION !

        Si le contenu de votre pad hebdomadaire a été effacé, c'est qu'il n'avait pas été 
        modifié depuis plus de 7 jours consécutifs.
<br>
        --> Lien vers le Markdown : <http://problemata.huma-num.fr/ws/workshop-massive-attack/doc/>

![fleche](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr5/files//Tutoriel///Fichier%207-20.jpg)

####


#Présentation de l'espace de travail  

![schema](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr5/files//Tutoriel///Parcours%20interface.jpg)
        

##Corpus de références 
##**1. Parcourir le corpus de références**  

--> [Visualisation] Output Note <http://problemata.huma-num.fr/ws/workshop-massive-attack/output/note/?pad=https://hebdo.framapad.org/p/gr5&calc=https://lite.framacalc.org/gr5&db=../gr5/files> 

--> [Edition] Framapad <https://hebdo.framapad.org/p/gr5>

*Regroupement et croisement de références associées à l'économie, l'écologie et l'urbanisme.*

##Lexicographie 
##**2. Comparer les définitions des termes clés**  
--> [Visualisation et édition] Framacalc <https://lite.framacalc.org/gr5>

*Glossaire et croisement de mots et de définitions*

##Proxémie 
## **3. Liens et appropriation des termes** 
--> [Visualisation] Output Graph <http://problemata.huma-num.fr/ws/workshop-massive-attack/output/graph/?pad=https://hebdo.framapad.org/p/gr5&calc=https://lite.framacalc.org/gr5&db=../gr5/files>

--> [Edition] Framacalc <https://lite.framacalc.org/gr5>

*texte explicatif* --> explication de l'appropriation des termes "relation" et "association"

####


#Un urbanisme de l'inattendu, 2019
##*Patrick Bouchain, scénographe, urbaniste, architecte*
###*livre*

![Un urbanisme de l'inattendu](https://pbs.twimg.com/media/EPCnJ5WWsAAaE7n.jpg)
#La preuve par 7
##projet d'urbanisme expérimental
                                                                                                                                                                                                                                
La Preuve par 7 est une démarche expérimentale d’urbanisme et d’architecture qui accompagne des porteurs de projets urbains, d’équipements, d’habitat, en cours de développement à travers la France, à 7 échelles territoriales : un village, un bourg, une ville moyenne, des territoires métropolitains, une métropole, un équipement structurant et un territoire d’outre mer. 
[Voir plus] <https://lapreuvepar7.fr/>
####programmation ouverte
####urbanisme


#**Doit-on reconsidérer les richesses?, 2021**
##*Patrick Viveret, philosophe - Mathieu Baudin* 
###*podcast, Les Eclaireurs x L'Institut des futurs souhaitables
*
<div style="position:relative;padding-bottom:56.25%;height:0;overflow:hidden;"><iframe style="width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden"        frameborder="0"        type="text/html"        src="https://www.dailymotion.com/embed/video/x7ytrmk?autoplay=1"        width="100%"        height="100%"        allowfullscreen        allow="autoplay">      </iframe>    </div>

####économie

#Saison Brune, 2012
##Squarzoni Philippe
###BD
![Saison Brune](https://www.editions-delcourt.fr/special/saisonbrune/head.jpg)
####écologie

#Walden Note Money
##Austin Houldsworth
###projet de design
<iframe title="vimeo-player" src="https://player.vimeo.com/video/86508725" width="640" height="360" frameborder="0" allowfullscreen></iframe>

--> <https://www.austinhouldsworth.co.uk/#/vii/>
####design

#Charte AFD de l'éco-designer
<http://www.alliance-francaise-des-designers.org/charte-des-ecodesigners.html>

Nous entendons par écodesign, une approche de conception qui prend en compte la responsabilité écologique, sanitaire, la justice sociale et l’apport culturel — pour nos contemporains et les générations futures — dans l’innovation, la conception et le développement de produits et services. L’écodesign intègre ces paramètres dans une approche systémique. Cette approche doit se comprendre dans une démarche d’amélioration continue en fonction des avancées technologiques, scientifiques et de connaissances humaines du moment. Il est de notre devoir d’apporter une contribution au développement durable, dans le cadre nos métiers, nos services et des produits qui en résultent.

####design

#Économie du futur : êtes-vous techno-optimiste ou techno-sceptique ?, 2016    
##Hubert Guillaud
###article
<http://www.internetactu.net/2016/06/23/economie-du-futur-etes-vous-techno-optimiste-ou-techno-sceptique/>

Au début des années 30, Keynes dans « Perspectives économiques pour nos petits-enfants« , estimait que la crise en cours était passagère. Pour Keynes, le problème économique était amené à disparaître avec l’enrichissement de la société, comme le problème alimentaire avait disparu au début du XXe siècle grâce à l’augmentation de la productivité agricole et la transition démographique.

**Keynes estimait que vers 2030 l’humanité serait si riche qu’elle ne travaillerait plus que quelques heures par jour et ne s’intéresserait plus qu’à l’art, la métaphysique ou la morale…**

Si Keynes ne s’est pas trompé sur le rythme de la croissance sur un siècle (environ 2 % par an), ni sur l’évolution du revenu (multiplié par 7 en un siècle), il s’est complètement trompé sur l’usage que nous ferions de cette richesse. Comme Marx, Keynes a mal mesuré la capacité d’extension de nos estomacs en matière de richesse matérielle.

**C’est ce qu’explique le paradoxe d’Easterlin : quel que soit le niveau de richesse d’une société cette richesse n’est pas corrélée au niveau de bien-être des gens. [...] Pour l’économiste, « les besoins humains ne sont jamais absolus. Passé le seuil de subsistance, ils sont relatifs. On est heureux ou riche par rapport aux autres ! » La consommation est éminemment sociale. Pour l’économiste James Duesenberry, le consommateur doit toujours rester au même niveau que ses voisins pour ne pas se sentir déclassé. Pris dans une course mimétique, le système d’accumulation des biens se révèle infini, profondément inefficient.**

**Que se passerait-il si la croissance économique s’arrêtait pour de bon ? Nos sociétés pourraient-elles survivre à la dépression collective qui en découlerait ? Faut-il faire le deuil de la croissance ? Peut-on la ranimer ? Peut-on passer à autre chose, voire, comme nous y invitait Keynes nous passer de la question économique ? Quelles sont les perspectives de croissance de nos sociétés avancées ? Sommes-nous arrivés à un plateau ? On ne sait pas, concède l’économiste.**

####économie


#L'Ere de l'Opulence, 1958
##Kenneth Galbraith
###livre
![cover](https://images-na.ssl-images-amazon.com/images/I/41HWs32KGGL._SX342_BO1,204,203,200_.jpg)
####économie

#URBANISME
![Projet urbanisme circulaire Nantes](https://www.nantes-amenagement.fr/wp-content/uploads/2017/12/20170920_PLI_Note-m%C3%A9thode-AMI-Economie-circulaire-003.jpg)
#PROGRAMMATION OUVERTE
####
####
####

#Dico de l'éco
## <https://www.economie.gouv.fr/facileco/dico-eco>
##<https://www.economie.gouv.fr/facileco/joseph-schumpeter>
####

#Qu’est-ce que l’économie du partage partage ? (1/3) : la professionnalisation de nos rapports sociaux, 2014       
##Hubert Guillaud
###article
<http://www.internetactu.net/2014/07/08/quest-ce-que-leconomie-du-partage-partage-13-la-professionnalisation-de-nos-rapports-sociaux/>

**« L’équipe a certes élargi son champ d’inscrits, mais considérablement détruit l’esprit du covoiturage, mettant en avant des notions sécuritaires, monétaires, en oubliant le reste. »**
####économie

#Il devient impératif de redéfinir le mot "économie", 2020
##*Entretien de Alain Denautl (philosphe) par Pablo Maillé (Usbek & Rica)*
###*article, entretien*
<https://usbeketrica.com/fr/article/il-devient-imperatif-de-redefinir-economie>
![L'économie esthétique](https://usbeketrica.com/uploads/images/thumb_840xh/5e6286af5facf.jpg)
####économie

# Fabrique urbaine et réappropriation citoyenne : l’urbanisme transitoire comme ruse ?, 2020
##Cécile Diguet
###article
<http://www.revuesurmesure.fr/issues/reprendre-la-ville/fabrique-urbaine-et-reappropriation-citoyenne-lurbanisme-transitoire-comme-ruse>
![matériaux](http://www.revuesurmesure.fr/content/1-issues/1-reprendre-la-ville/15-fabrique-urbaine-et-reappropriation-citoyenne-lurbanisme-transitoire-comme-ruse/%40bellastock.recuperation-de-matiere-projet-caserne-mellinet-ave-atelier-georges-nantes-2017.jpg)
####urbanisme

#La fonction symbolique de l'argent, 2008 
##Ilana Reiss-Schimmel
###article scientifique
<https://www.cairn.info/revue-dialogue-2008-3-page-7.htm>

![pièce](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr5/files//Images///piece.png)

####économie

# **Il n’y a jamais eu de système démocratique qui n’ait pas été capitaliste, 2018**
##*Vincent Edin*
### *article Usbek et Rica*
<https://usbeketrica.com/fr/article/il-n-y-a-jamais-eu-de-systeme-democratique-non-capitaliste>
![Illustration article](https://usbeketrica.com/uploads/images/thumb_840xh/5b98d05cbacdb.jpg)
####économie

#Homo Economicus, 2012
##Ayreen Anastas & Rene Gabri, Pavel Büchler, Zachary Formwalt, Franck Scurti, Andreas Siekmann, Mona Vătămanu & Florin Tudor
### Exposition, Cabinet, Londres
<https://contemporaryartdaily.com/2012/02/homo-economicus-at-cabinet/>
![expo](https://s3.amazonaws.com/contemporaryartgroup/wp-content/uploads/2012/02/3_B-630x700.jpg)
####art

#**Les tiers-lieux peuvent-ils dynamiser l'économie?, 2020**
##*Juliette BompointDirectrice de l'association Mains d'oeuvres, à Saint-Ouen - Fabrice LextraitAncien directeur et cofondateur de la Friche La Belle de Mai, à Marseille - Patrick Lévy-WaitzPrésident de la Fondation ITG travailler autrement - Olivier AudetDirecteur du Konk Ar Lab - Thomas Chauvineauanimateur sur France Inter*
###*podcast, France Inter*
<https://www.franceinter.fr/emissions/le-debat-de-midi/le-debat-de-midi-28-juillet-2020>

--> France Tiers-lieux : <https://francetierslieux.fr/>

![carte tiers-lieux france](https://3.bp.blogspot.com/-0UTQm2kHJl4/XJMcpnfwM5I/AAAAAAAAHho/YlRgpBqTUawY6DtD1IccjMtICc1ZBYVFACLcBGAs/s1600/Tiers%2Blieux%2Bla%2Bcarte%2Bdu%2BCGET3.jpg)

NB --> lien entre design thinking + design organisationnel et création de tiers-lieux

####urbanisme

#L'économie expliquée aux humains, 2020
##Delannoy Emmanuel
###Livre

#ECONOMIE
#ECOLOGIE
#ECONOMIE CIRCULAIRE
####
####
####

####écologie

#Vers un nouveau capitalisme
##Arte
###documentaire vidéo

<iframe width="1280" height="720" src="https://www.youtube.com/embed/Gxf-hnKH3-4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

####économie

#Mind map Tiers-lieux
![mind map tiers-lieux](https://anis.asso.fr/IMG/arton4848.jpg?1540271121)
Ref --> <https://anis.asso.fr/Intervention-ANIS-Catalyst-Lille-3.html?page=article_imprime&id_article=4848>
####tiers-lieux

                   
