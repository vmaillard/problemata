

Bonjour et bienvenue dans cet espace de partage bienveillant et collaboratif 

---


# La fabrique du Sensible

*Introduction* <br><br>
Nous avons pris le partie de travailler ensemble sur la notion de sensible intrinsèque au design pouvant alors faire des lien sociaux au coeurs des administrations ou instances publiques, des communautés, ou plus largement au cœur de ce qui fait que ce lien social est possible.  La sensibilité mobilise des capacités réflexives chez certains individus que le raisonnement n’ébranle pas. Le sensible peut-être une porte d’entrée, un levier, avant d’être un moyen de connaissance. Il ne s’agit pas pour autant d’opposer connaissance rationnelle et connaissance sensible mais plutôt de profiter de leur complémentarité dans la perspective d’un enrichissement de notre monde de sens. Pour être activité la fabrique du sensible doit offrir différents niveaux d’actions et de perception. <br> 
<br> 
**Pour être activité la fabrique du sensible intervient à différents niveaux perceptibles, comment le design permet il de les communiquer ? rendre visible ? rendre tangible ?**

#### 

#Boite à idées
---
Tout est sensible dans notre environnement, c'est la perception que l'on en a qui peut être plus rationnelle ou sous un filtre émotionnel, sensoriel
---
<br>
## Questionnements
* Tout design n'est-il pas sensible ? 
* Est-ce que le sensible ne se révèle pas par la transversalité des savoirs ?
* Le sensible est-il toujours une forme d'interaction ? un réacteur / une réaction
* L'absence d'interaction sensible est-elle une violence ? cf torture
* Est-ce que l'étrange c'est ce qui n'est pas familier ?
* Le sensible peut-il être inhibé par la routine ?

---
##Objectifs
Développer en tant qu'outil de pensée, de mise en commun à la recherche
Définir les pratiques du sensible en design
####

#Schémas de pensée
Recherches de visualisation
*![titre](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr1/files//photosbrainstorm///DSC_1025.jpg)
####

<br>

---

##Références

---

#Mots-clefs
-----------sphère intime-----------interaction sociale----------ma japonais-------interaction du vivant-------interaction au milieu-----------communication verbale----------horizontalité--------expérience-------signaux-------étrange------temporalité------médiation-------empathie corporelle-----enseignement
####



---


# L'amour est-il l'opium du peuple ?
## Eva Illouz, sociologue sur France Culture, 2019
* (youtube:https://www.youtube.com/watch?v=dTtlrdyvqww)
"Le capitalisme a produit à partir des années 20 des marchandises émotionnelles, qui n'ont pas été conceptualisées en tant que tel par la sociologie. Chez Marx la marchandise est un objet qu'on produit dans des champs ou dans des usines, et dont la valeur est déterminé par le temps de travail."
#§
#### interaction sociale
#§
#### sphère intime
####

# Learn to unlearn
## Lina Marie Köppen
* ![Tux, the Linux mascot](http://img2.archilovers.com/story/85a75ef3f1754d23b63bdee061bc878c.JPG)
#§
#### expérience
#§
#### empathie corporelle
#§
#### interaction au milieu
####

#Pensée de W.Gropius sur le rassemblement
## Walter Gropius
<code>
« Une éducation large doit montrer la juste voie pour la juste forme de collaboration future entre l’artiste, le savant et le commerçant. Seule cette collaboration peut leur permettre d'élaborer un standard de production qui ait l'homme pour mesure, c'est-à-dire qui prenne les impondérables de notre existence autant en compte que les besoins physiques. Je crois à l'importance croissante du travail en groupe pour la spiritualisation du standard, vital dans la démocratie. » 
</code>
<br>
Discours d'inauguration de l'école d'Ulm, 1953
#§
#### interaction au milieu
#§
#### enseignement
####

#Le design, entre recherche et science
##émission France Culture, 2020
*((https://www.franceculture.fr/emissions/la-suite-dans-les-idees/le-design-entre-recherche-et-science)
#§
#### médiation
#§
#### expérience
#§
####interaction sociale
####

# Ugo La Pietra, Recupero e reinvenzione, 1969-1975, exposition du Frac-centre sur le designer et architecte Ugo La Pietra
## Ugo La Pietra
* ![Tux, the Linux mascot](https://www.frac-centre.fr/gestion/public/upload/oeuvre/maxi/LAPI_009_15_02.jpg)
* ![Tux, the Linux mascot](https://www.frac-centre.fr/gestion/public/upload/oeuvre/maxi/LAPI_009_15_01.jpg)
#§
#### interaction au milieu
#§
#### sphère intime
####

#L’École du Blé en Herbe à Trébédan
## la designere Matali Crasset, 2015
*(vimeo :https://vimeo.com/139071421)
* ![Tux, the Linux mascot](https://www.classe-de-demain.fr/medias/files/thumbsSource/ecole-trebedan-image-principale_825x415.png)
#§
####enseignement
#§
####médiation
####


# Walking
## Robert Wilson
2008
* ![Tux, the Linux mascot](https://scontent-cdg2-1.xx.fbcdn.net/v/t1.0-9/98050438_3002319006472216_570682054195281920_o.jpg?_nc_cat=100&ccb=2&_nc_sid=9e2e56&_nc_ohc=xyNts1PuObAAX_vPO5w&_nc_ht=scontent-cdg2-1.xx&oh=4ecacaae040a451000f538b74faab669&oe=6037CF57)
#§
#### interaction au milieu
#§
#### expérience
#§
#### sphère intime
#§
#### temporalité
####

# Le Musée Juif de Berlin
## Daniel Libeskind
* ![Tux, the Linux mascot](https://vivreaberlin.com/wp-content/uploads/2015/07/5277_3ba1e068309d.jpg)
#§
#### interaction au milieu
#§
#### expérience
####

# Vivre avec le trouble
## Donna haraway, 2019

* ![Tux, the Linux mascot](https://mollatcommon.blob.core.windows.net/notices54/2382967_medium.jpg)
#§
#### expérience
#§
#### interaction du vivant
#§
#### interaction au milieu
####

# The Hidden Dimension
## Edward T Hall
"People like to keep certain distances between themselves and other people or things. And this invisible bubble of space that constitutes each person's "territory" is one of the key dimensions of modern society. Edward T. Hall, author of The Silent Language, introduced the science of proxemics to demonstrate how man's use of space can affect personal and business reltions, cross-cultural interactions, architecture, city planning, and urban renewal."
* ![Tux, the Linux mascot](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr1/files//refsLea///The%20hidden%20dimension.JPG)
#§
#### sphère intime
#§
#### empathie corporelle
#§
#### ma japonais
#§
#### horizontalité
#### 


# Phénoménologie de la perception
## Merleau Ponty
"Quel est le sujet de la perception ? Rapports du sentir et des conduites : la
qualité comme concrétion d'un mode d'existence, le sentir comme coexistence.
La conscience engluée dans le sensible. Généralité et particularité des
« sens ». Les sens sont des « champs ». La pluralité des sens. Comment
l'intellectualisme la dépasse et comment il a raison contre l'empirisme."
* => Voir PDF dans le folder de léa
#§
#### expérience
#§
#### interaction du vivant
#§
#### temporalité
####

# Zygmunt Bauman. Individual and society in the liquid modernity
## Palese P.
"Social aggregation and organization are deprived
of their traditional tasks: they stop being identity
dimensions of the subject capable of providing a set of standards
and benchmarks. The individual becomes an isolated
monad always looking for new forms of socialization, which
instead of providing safety and welfare, increase the gap between
man and the Self and between man and the other." 
* => Voir PDF dans le folder de léa
#§
#### test
#§
#### test
#### 

# Vitra Fire Station
## Zaha Hadid
* ![Tux, the Linux mascot](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr1/files//refsLea///Zaha%20Hadid-%20Vitraa%20Fire%20Station.jpg)
#§
#### expérience
#§
#### interaction au milieu
####

# Pavillon de conférence
## Tadao Ando
* ![Tux, the Linux mascot](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr1/files//refsLea///Tadao%20Ando%20-%20Vitraa.jpg)
#§
#### expérience
#§
#### interaction au milieu
####

#  "l'anthropocène depuis le théâtre de l'opprimé, histoire d'habiter en ville", les halles du faubourg, 2 Février 2020
## Maria grace Salamanca gonzalez (Dr en philosophie, recherche sur le terrain)
Les ateliers du théâtre de l'opprimé fonctionnent sur l'idée d'une communication sensorielle par les outils du théâtre ( représentation symbolique, physicalisation de ressentis) pour ensuite être suivi de temps d'échanges philosophiques 

*![Tux, the Linux mascot](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr1/files//RefsClarisse///theatre.JPG)
*
#§
####interaction sociale
#§
####ma japonais
#§
####expérience
#§
####sphère intime
#§
####signaux
####

# Crinoline
## 1830
* ![Tux, the Linux mascot](http://problemata.huma-num.fr/ws/workshop-massive-attack//gr1/files//refsLea///1860s-metal-hoop-crinoline-dress.jpg)
Relié au schéma de représentation spécial du corps, l'histoire révèle une zone de l'intime ainsi qu'une place, qu'une importance donné au corps complètement différente. 
#§
#### expérience
#§
#### empathie corporelle
#§
#### interaction au milieu
####




