# Objectifs
- préciser le discours, prendre position
- stabiliser les modèles en découlant
- ramener à des formes concrètes tout ça

---

# gr1
## Clarisse, Charlene, Léa
### Sur la description et le partage du sensible (la fabrique du sensible)

Dessin / schema d'une interface en onglets avec plusieurs typologies de contenus, du neutre au personnel.
Une structure assez organisé (partie 1, partie 2 ...) qui se déploit dans la verticalité de la page, comme un fil.
Structure : 
    - introduction
    - boite à idées (problématiques ?)
    - références : décrites par des questions relatives à la problématique posée par l'auteur
    - les avis
    - questionnements que soulève ces problématiques, par le prisme de ces exemples
Calc qui tend à définir de manière hiérarchique les domaines ou le jargon du sensible.
Une interface pour se poser des questions de manère historique, et un comité éditorial qui y répond par des références et les questions qu'elles apportent.

#### Préparation des données
#### Output

---

# gr2
## Miliana, Valentine, Benjamin
### Modèle de page conversationnelle

Modalités à définir et à mesurer.
Chacun amène une référence sur laquelle les autres peuvent rebondir.
Fin du tableau = quand est-ce que se finit une conversation ?
Par rapport à nos outils : pourquoi pas essayer la même chose sur une vraie messagerie ?
Contexte / cadre temporel à définir clairement ainsi que légéreté des informations pouvant être citées : peut-on juste partager une image en guise de référence ou doit-on inclure sa fiche descriptive normalisée ?
A quelle niveau du caractère scientifique sommes nous ? Quelle facette de la méthodologie scientifique veut-on mettre en exergue ?
Emoji pour décrire la réaction face à un commentaire d'un autre.

#### Préparation des données
#### Output
- écriture d'un scénario
- épreuve du modèle dans une messagerie classique
- dessins et schémas de ce qu'ils imaginent
- description correcte des données, et rapports "parallèles" (parallèlité et bifurcations)

---

# gr3
## Etienne, Clara, Camille, Julie

N'ont pas formulé clairement leur prise de position ou les questions qu'ils se posent. 
Inventaire de références ou celui qui partage est référencé pour eux et pour nous.
Peut il exister 2 fois la même référence lorsque 2 personnes ont la même ?
En tout cas le médiateur peut être une entrée chez eux et ce n'est pas anodin.

#### Préparation des données
#### Output

---

# gr4
## Rémi, Clément
### lowtech rating

On les envoyé vers le système de rating.
primitif -> rustique -> lowtech
L'idée ici serait d'inventer une échelle simple, qui ne serait pas pour autant illégitime por entrer dans le corpus.
Envie de prendre la construction d'un habitat en guise de prétexte de recherche / orienter la navigation.
J'ai presque envie de les mettre sur la piste de la séquence, comme si on, devait choisir des paramètres (que l'on peut changer à tout moment) pour configurer un produit.

item
domain : architecture
lowtech rate : 

avoir accès à l'histoire des techniques de ce type par le pretexte de la construction
approche historique
savoirs faires et acteurs en assistance à la thématique de la construction d'un habitat


#### Préparation des données
#### Output

cartographie des acteurs / porteurs des savoirs-faire lowtech
tisser des liens entre ces acteurs / porteurs
outil graph

filiation

---

# gr5
## Léa Grac
### La nuance par la définition, la précision de la nuance par le cas d'usage

Une base de références (pad) qui font exemples / cas d'usages des entrées d'un glossaire (calc).
J'imagine assez une interface à double entrée :
    - une entrée par les références, de manière posée, échnatillonnée, favorisant les textes de 4-5 minutes, relatant des pensées et des faits
    - une entrée lexicale, ou par les mots on saisit leur nuance (par le biais d'auteurs affiliés à des définitions) et on rebondit vers d'autres mots ou des cas d'usage
Léa a la volonté d'insuffler ses positions dans les choix de définition pour les mots par source=auteur.
C'est peut être une dimension de plus pour son glossaire, la dimension de la pensée de quelqu'un.
Questions de structure à résoudre
    - quid des colonnes cnrtl et autres ?
    - quid de la source de la définition normalisée ?
    - que faire lorsqu'on mot a plusieurs sens ? colonnes multiples "sens" suivi des définitions nuancées ? 

#### Préparation des données
#### Output

---

# gr6
## Alyson
### Tentatives référencées

D'après Catherine, on peut aller vers la piste de l'ajout de pistes prospectives et d'hypotheses fausses.
Des entrées "concluant" et "foireux" suivi de la date peuvent être relevantes dans l'idée d'un parcours de sa base (qui reste à construire).
Mesure de la pertinence du lien entre biologie et design (exemple du biomimétisme juste comme motif).

#### Préparation des données
#### Output