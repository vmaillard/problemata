# Workshop Problemata

## Web sémantique

![](img/triplet1.png)

![](img/triplet.png)

- Sujet -> Prédicat -> Objet
- Berlin -> Has population -> 3 520 031
- Ressource -> Propriété -> Valeur
- Sommet -> arc -> sommet

![](img/triplet_col.png)

![](img/triplet_graph.png)

RDF = Resource Description Framework

- Resource : une chose qui peut avoir une uri
- Description : attributs, propriétés, caractéristiques
- Framework : langage de syntaxe

=> Giant Global Graph

![](img/giant_graph.png)

## Vocabulaires controlés

### De nombreux vocabulaires à disposition

![](img/voc.png)

Pour trouver des vocabulaires :
[](https://lov.linkeddata.es/dataset/lov/)

### Dublin Core

![](img/voc_dc.gif)

Propriété | Sous-propriété | Description | Format 
--- | --- | --- | ---
Title / Titre | | Un nom donné à la ressource | | 
Title | Alternative / Titre alternatif | Un autre nom de la ressource | 
Creator / Créateur | | Une entité responsable au premier chef de l'élaboration de la ressource | 
Subject / Sujet | | Le sujet de la ressource (mots-clés) |
Description | | Une explication de la ressource | 
" | Table Of Contents / Table des matières | Une liste de sous-unités de la ressource |
" | Abstract / Résumé | Un résumé de la ressource |
Publisher / Éditeur | | Une entité responsable de la mise à disposition de la ressource |
Contributor / Collaborateur | | Une entité responsable d'une contribution à la ressource |
Date | | Un point ou une période dans le temps associés à un événement dans le cycle de vie de la ressource |
" | Date Created / Date de création | La date de création de la ressource |
" | Date Valid / Date de validité | La date (souvent un intervalle) de validité d'une ressource 	|
" | Date Available / Date de disponibilité | La date (souvent une période) à laquelle la ressource a été disponible ou le deviendra |
" | Date Issued / Date de parution | La date de parution formelle (par exemple, la publication) de la ressource |
" | Date Modified / Date de modification | La date à laquelle la ressource a été modifiée |
" | Date Submitted / Date de soumission | Comme exemples de ressources pour lesquelles une date de soumission serait pertinente, une thèse (soumise à un département d'université) ou un article (soumis à un journal) | 
Type | | La nature ou le genre de la ressource |
Format | | Le format de fichier, le support physique ou les dimensions de la ressource |
" | Extent / Étendue | La taille ou la durée de la ressource |
" | Medium / Support | La matière ou le support physique de la ressource |
Identifier / Identificateur | | Une référence univoque vers la ressource dans un contexte donné |
Source | | Une ressource liée de laquelle dérive la ressource décrite |
Language / Langue | | La langue de la ressource |
Relation | | Une ressource liée |
" | Is Version Of / Est une version de | Une ressource liée dont la ressource décrite est une version, une édition ou une adaptation |
" | Has Version / A pour version | Une ressource liée qui est une version, une édition ou une adaptation de la ressource décrite |
" | Is Replaced By / Est remplacé par | Une ressource liée qui supplante, déplace ou remplace la ressource décrite |
" | Replaces / Remplace | Une ressource liée qui est supplantée, déplacée ou remplacée par la ressource décrite |
" | Is Required By / Est exigé par | Une ressource liée qui exige de la ressource décrite qu'elle soutienne sa fonction, sa distribution (delivery) ou sa cohérence |
" | Requires / Nécessite | Une ressource liée qui est exigée par la ressource décrite pour soutenir sa fonction, sa distribution ou sa cohérence |
" | Is Part Of / Est une partie de | |
" | Has Part / A pour partie | Une ressource liée qui est incluse physiquement ou logiquement dans la ressource décrite | 
" | Is Referenced By / Est référencé par | Une ressource liée qui référence, cite ou encore pointe vers la ressource décrite |
" | References / Référence | Une ressource liée qui est référencée, citée ou encore vers laquelle pointe la ressource décrite |
" | Is Format Of / Est un format de | Une ressource liée qui est en substance la même que la ressource décrite mais dans un autre format |
" | Has Format / A pour format | Une ressource liée qui est en substance la même que la ressource décrite préexistente mais dans un autre format |
" | Is Version Of | |
Coverage / Couverture | | Le thème spatial ou temporel de la ressource, l'applicabilité de la ressource dans l'espace ou la juridiction dont dépend la ressource |
" | Spatial / Couverture spatiale | Les caractéristiques spatiales de la ressource |
" | Temporal / Couverture temporelle | Les caractéristiques temporelles de la ressource |
Rights / Droits | | Une information à propos des droits détenus dans et sur la ressource |
" | License / Licence | Un document légal donnant permission officielle de faire quelque chose avec la ressource |
" | Access Rights / Droits d'accès | Une information à propos de qui peut accéder à la ressource ou une indication de son état de protection |

Dublin Core utilisé par certains sites : [https://firstmonday.org/ojs/index.php/fm/article/view/6984/6090](https://firstmonday.org/ojs/index.php/fm/article/view/6984/6090)

### RDA Registry

Énormément de propriétés, suivant si on décrit un Agent, un Work : [http://www.rdaregistry.info/](http://www.rdaregistry.info/)

### MARC Relators

Exploration des différents rôles possibles pour un auteur : [http://id.loc.gov/vocabulary/relators.html](http://id.loc.gov/vocabulary/relators.html)

### Des vocabulaires pour les valeurs à rentrer

Il existe aussi des vocabulaires contrôlés pour la suggestion de valeurs : [http://id.loc.gov/authorities/subjects.html](http://id.loc.gov/authorities/subjects.html)

## Modèle bibliographique FRBR

(Functional Requirements for Bibliographic Records)

![](img/frbr1.png)

![](img/frbr2.png)

Modèlisation d'un manuscrit :

![](img/frbr_modelisation_manuscrit.png)

Modèle de données utilisés pour data.bnf.fr :

![](img/modele_donnees_BNF_2018_02.pdf)

## Références 


- Wikipedia : [https://fr.wikipedia.org/w/index.php?title=Ettore_Sottsass](https://fr.wikipedia.org/w/index.php?title=Ettore_Sottsass)
- DBpedia : [http://dbpedia.org/page/Ettore_Sottsass](http://dbpedia.org/page/Ettore_Sottsass)

- BNF : [https://data.bnf.fr/fr/11925190/ettore_sottsass/](https://data.bnf.fr/fr/11925190/ettore_sottsass/)
	- le concept Work -> Expression -> Manifestation -> Item
    - [http://reliures.bnf.fr/](http://reliures.bnf.fr/) et son jeu de vocabulaire spécifique

- [https://www.bsad.eu/](https://www.bsad.eu/) :
    - les fichiers d'autorité utilisés : [https://www.bsad.eu/index.php?lvl=categ_see&id=12](https://www.bsad.eu/index.php?lvl=categ_see&id=12)

- Ethica Spinoza : [http://app.ethica-spinoza.net/](http://app.ethica-spinoza.net/)
