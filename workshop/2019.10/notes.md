# Notes pour le workshop - Semaine du 21 octobre 2019

## Identifiants


## Présentation

- une petit collection d'images pour les concepts sémantiques

## Outils 

- Script léger d’affichage CSV, JSON à partir d’une source locale ou en ligne
On pourrait avoir une page qui parse le csv global, pour lister tous les articles, du genre : "Titre / Auteur" avec la possibilité de tri multiple à partir des sujets par exemples.

## Liens 
- https://lite.framacalc.org/base-problemata


## Requêtes DBpedia

- cours inria sur sparql : https://www.canal-u.tv/producteurs/inria/cours_en_ligne/web_semantique_et_web_de_donnees/le_langage_de_requete_sparql
- ref w3c : https://www.w3.org/TR/sparql11-query/#convertGroupAggSelectExpressions
- tuto : http://fr.dbpedia.org/sparqlTuto/tutoSparql.html

## SPARQL

SELECT : ce que l'on veut / stocke la réponse à la question
FROM : où on le veut
WHERE { comme on le veut / pose la question }

- fonctionne par triplet pour la requête

ex :
`?x		rdf:type	ex:Person`
== requête des ressources de type Person, stockées en ?x

ex :
`
SELECT ?x WHERE {
	?x		rdf:type	ex:Person .
	?x		ex:name		?name .
}
`
== recherche des ?x de type Person qui ont un nom ?name
avec conjonction ET entre les 2 requêtes

ex :
`?x ex:firstname "Fabien", "Lucien" .`
== requêtes des ressources aux prénoms selon deux possibilités

- les espaces de noms peuvent être préfixés :
`PREFIX mit: <http://www.mit.edu#>`
=>
`?student mit:registeredAt ?x .
?x foaf:homepage <http://mit.edu> .`

- opérateurs :
OPTIONAL : si pas de réponses, on a quand même la réponse obligatoire

`WHERE {
	?person foaf:homepage <http://fabien.info>
	OPTIONAL { ?person foaf:name ?name . }
}`
== on récupère le nom si on le trouve

UNION : on utilise la première clause ou la seconde, parfois les deux, il faut obligatoirement une réponse pour les deux clauses

`WHERE {
	?person foaf:name ?name .
	{
		?person foaf:homepage <http://fabien.info>
	}
	UNION
	{
		?person foaf:homepage <http://bafien.info>
	}
}`
== personne qui ont un nom et une page d'acceuil qui correspond

- MINUS : négation : ressource qui NE vérifie pas une clause
`MINUS { ?x a ex:Man }`

- VALUES : possible de renseigner des valeurs pour filtrer la requête

`
SELECT ?person ?name 
WHERE {
	?person foaf:name ?name .
}
VALUES ?name { "Peter" "Pedro" "Pierre" }
`

- Motifs de chemin
Il est possible de spécifier des méthodes de parcourt :
`/` : séquence (deux arcs entre eux)
`+` : un ou plusieurs ressources

ex :
`?x foaf:knows+ ?friend`
== requête de toutes les ressources liées par la propriété "knows"

- DISTINCT
pour avoir des résultats distincts



TEST 

---
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT DISTINCT ?x ?birth ?death ?nationality 

WHERE {

?x a dbo:Architect .
?x dbo:birthDate ?birth .
?x dbo:deathDate ?death .
?x dbo:nationality ?nationality 

FILTER (xsd:date(?birth) <= "2007-12-31"^^xsd:date) .
FILTER (xsd:date(?death) >= "1917-09-14"^^xsd:date) .
FILTER (?nationality = dbr:Italians) .
}
--- 

---
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT DISTINCT ?x ?nationality ?birth ?death

WHERE {

?x a dbo:Architect .
?x dbo:birthDate ?birth .
?x dbo:nationality ?nationality 

OPTIONAL {
?x dbo:deathDate ?death .
FILTER (xsd:date(?death) >= "1917-09-14"^^xsd:date)
}

FILTER (xsd:date(?birth) <= "2007-12-31"^^xsd:date) .
FILTER (xsd:date(?birth) >= "1830-01-01"^^xsd:date) .

FILTER (?nationality = dbr:Italians) .
}
---


## Mediawiki

Principes :
- catégories et sous-catégories
- extension Semantic Mediawiki

## Semantic Mediawiki

Depuis 2005

Principe :
- Sémantique : l'information est décrite de manière explicite
- ajout d'annotations sémantiques au début, au sein d'un texte
- on crée des propriétés sémantiques, on crée un template qui contient les propriétés voulues, avec l'extension PageForms on peut créer un formulaire

 une page selon un template, on dispose ensuite de métadonnées en rapport

Triplet :
- Sujet -> Prédicat -> Objet
- Berlin -> Has population -> 3 520 031
- Ressource -> Propriété -> Valeur

- Les propriétés sont une notion différente des catégories
- Une propriété doit avoir un type de valeur spécifique ("Page", "Text", "Number", "Email", "URL","Telephone number", "Temperature", "Boolean") => cf. la page /Spécial:Types

Fonctionnement :
- créer une propriété en spécifiant le type de valeur possible
- éditer une page en utilisant cette propriété
- créer des templates


Exemples d'annotations :
- annotation plein texte :
`[[Property name::Property value]]`

- annotation plein texte avec du texte d'affichage : 
`[[Property name::link|alternate text]]`

- annotation cachée et à propriétés multiples :
`
{{#set:
 Property name=Property value
 |Property2 name=Property2 value
}}
`

- annotation à plusieurs valeurs pour une même propriété :
`
{{#set:
	Property name=Property value 1; Property value 2
	|+sep=;
}}
`

Exemples de requêtes :
- 

- Requêtes de propriétés inversées (on cherche la mère de Michael) :
`{{#ask: [[-Has mother::Michael]] }}`

- Requêtes des pages de pays avec leurs villes
`
{{#ask:
 [[Category:Countries]]
 |?Has name=Country name
 |?-Has country=Cities in country
}}
`
=> Possible de montrer : à partir de livres édités par un éditeur. Sur la page de cet éditeur tous les livres qui ont lui pour éditeur.

- Multilingue :
On crée des pages pour chaque version de ressource et on les relie entre elles. À ajouter à chaque page (changer le code langue en fonction de celui sur la page) :
`{{interlanguagelink|fr: Nom de l'article en référence }}`






Ressources :

Intro et tuto :
https://www.semantic-mediawiki.org/w/images/8/80/20171004_Beginners_tutorial_to_Semantic_MediaWiki.pdf

Étapes par étapes avec PageForms : https://www.mediawiki.org/wiki/Extension:Page_Forms/Quick_start_guide

Annotation and query example :
https://www.youtube.com/watch?v=1CaNJLsAI2A

Utilisation de #set :
https://www.semantic-mediawiki.org/wiki/Help:Setting_values/Working_with_the_template_parameter

Utilisation de +sep :
https://www.semantic-mediawiki.org/wiki/Help:Setting_values/Working_with_the_separator_parameter

Création de sous-objets :
https://www.semantic-mediawiki.org/wiki/Help:Adding_subobjects

Multilingue :
- https://vimeo.com/115871518
- https://github.com/SemanticMediaWiki/SemanticInterlanguageLinks/blob/master/README.md
- https://github.com/SemanticMediaWiki/SemanticInterlanguageLinks/blob/master/docs/01-parser-function.md

Templates et infobox : 
https://www.semantic-mediawiki.org/wiki/Help:Semantic_templates

Importing vocabularies :
https://www.youtube.com/watch?v=ZsNMFpNfUdE

Création d'annotation :
https://www.semantic-mediawiki.org/wiki/Help:In-text_annotation#Datatypes_for_properties

Liste de propriétes :
https://www.semantic-mediawiki.org/wiki/Special:Properties

Liste de datatypes : 
https://www.semantic-mediawiki.org/wiki/Help:List_of_datatypes

Gestion des catégories :
https://sandbox.semantic-mediawiki.org/wiki/Cat%C3%A9gorie:Recipe

