# Guide du chercheur ou du contributeur
## Workshop - Semaine du 21 octobre 2019

### Écriture 

- Si l'écriture se fait dans un traitement de texte, bien penser à utiliser les input select "Titre 1", "Titre 2". Cela facilite le traitement automatique et sémantique du texte
- Si l'écriture se fait dans un traitement de texte, ne pas inclure les images directement dans le fichier texte mais les placer dans un dossier en les nommant par ordre. Dans le texte, faire une simple référence non ambigüe, par exemple : [IMG 1].
